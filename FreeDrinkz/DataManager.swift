//
//  DataManager.swift
//  Roasted
//
//  Created by Manish Gumbal on 21/01/17.
//  Copyright © 2017 Belal. All rights reserved.
//

import Foundation
class DataManager{
    
//    static var userId:Int? {
//        set {
//            UserDefaults.standard.setValue(newValue, forKey: kUserId)
//            UserDefaults.standard.synchronize()
//        }
//        get {
//            return UserDefaults.standard.integer(forKey: kUserId)
//        }
//    }
//
    static var currentLat:Double? {
        set {
            UserDefaults.standard.setValue(newValue, forKey: "currentLat")
          //  print(newValue!)

            UserDefaults.standard.synchronize()
        }
        get {
         //   print(UserDefaults.standard.double(forKey: "currentLat"))
            return UserDefaults.standard.double(forKey: "currentLat")
        }
    }
    static var currentLong:Double? {
        set {
            UserDefaults.standard.setValue(newValue, forKey: "currentLong")
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.double(forKey: "currentLong")
        }
    }

    static var isCallRestroWebservice:Bool? {
        set {
            UserDefaults.standard.setValue(newValue, forKey: "isCallRestroWebservice")
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.bool(forKey: "isCallRestroWebservice")
        }
    }
    
    static var restroListData: Data? {
        set {
            UserDefaults.standard.setValue(newValue, forKey: "restroList")
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.object(forKey: "restroList") as? Data
        }
    }
}

