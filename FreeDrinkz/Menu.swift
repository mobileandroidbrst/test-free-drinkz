
//
//  Menu.swift
//  FreeDrinkz
//
//  Created by Brst-Pc109 on 27/03/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Stripe

class Menu: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, PayPalPaymentDelegate {

    @IBOutlet weak var menuCollec: UICollectionView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var plusView: UIView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var successView: UIView!
   
    @IBOutlet weak var paymentView: UIView!
    @IBOutlet weak var paypalView: UIView!
    @IBOutlet weak var creditView: UIView!
    
    // 19 June :-
    @IBOutlet var cardCountLbl: UILabel!
    
    // 21 June :-
    var jsonParameters = [String:Any]()
    var recieverIdStrI = String() // reciever-id coming from I'm Buying Request Screen.
    var recieverIdStrW = String() // reciever-id coming from Who's Buying Request Screen.
    
    // 25 June :-
    var cart_id = String()
    
    var productsArray = [JSON]()
    var otherRestaurantId = String()
    var isOther = Bool()
    var isSendDrink = Bool()
    var isAcceptIm = Bool()
    var isAcceptWhos = Bool()
    var recieverIdStr = String()

    var payPalConfig = PayPalConfiguration() // default
    var resultText = "" // empty
    var productName = String()
    var productAmount = String()
    // 28 May :-
    var newProductAmount = String()
    
    var productId = String()
    var transactionId = String()
    var requestIdStr = String()
    var userId = String()

    var isEmail = Bool()
    
    // 21 June :-
    var status = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        print(recieverIdStrI)
        print(recieverIdStrW)
        print(otherRestaurantId)
        
        // 19 June(Showing Initial Items in cart on Screen) :-
       // getCart()
        
        Singleton.sharedInstance.payFromCreditCard = "no"
        paypalView.layer.cornerRadius = 4
        paypalView.layer.masksToBounds = false;
        paypalView.layer.shadowColor = UIColor.black.cgColor
        paypalView.layer.shadowOpacity = 0.3
        paypalView.layer.shadowRadius = 2
        paypalView.layer.shadowOffset = CGSize(width: 1, height: 1)
        
        creditView.layer.cornerRadius = 4
        creditView.layer.masksToBounds = false;
        creditView.layer.shadowColor = UIColor.black.cgColor
        creditView.layer.shadowOpacity = 0.3
        creditView.layer.shadowRadius = 2
        creditView.layer.shadowOffset = CGSize(width: 1, height: 1)

        if isOther == true {
            backView.isHidden = false
            plusView.isHidden = true
        }
        if isSendDrink == true {
            titleLbl.text = "Send Food/Drinkz"
            
            
            // Set up payPalConfig
            
            
            payPalConfig.acceptCreditCards = true
            payPalConfig.merchantName = "Satinderjeet Pawar's Test Store"
            payPalConfig.merchantPrivacyPolicyURL = URL(string: "https://www.paypal.com/webapps/mpp/ua/privacy-full")
            payPalConfig.merchantUserAgreementURL = URL(string: "https://www.paypal.com/webapps/mpp/ua/useragreement-full")
            
            // Setting the languageOrLocale property is optional.
            //
            // If you do not set languageOrLocale, then the PayPalPaymentViewController will present
            // its user interface according to the device's current language setting.
            //
            // Setting languageOrLocale to a particular language (e.g., @"es" for Spanish) or
            // locale (e.g., @"es_MX" for Mexican Spanish) forces the PayPalPaymentViewController
            // to use that language/locale.
            //
            // For full details, including a list of available languages and locales, see PayPalPaymentViewController.h.
            
            payPalConfig.languageOrLocale = Locale.preferredLanguages[0]
            
            // Setting the payPalShippingAddressOption property is optional.
            //
            // See PayPalConfiguration.h for details.
            
            payPalConfig.payPalShippingAddressOption = .payPal;
            
            print("PayPal iOS SDK Version: \(PayPalMobile.libraryVersion())")
            PayPalMobile.preconnect(withEnvironment: "sandbox")

            
        }
        
        indicator.startAnimating()
        indicator.isHidden = false
        
        getEmailMethod()
        
        
      //  sendDrinkAcceptWhosMethod()
        
        
        
     //   sendDrinkAcceptWhosCreditMethod ()
        
        if Singleton.sharedInstance.userTypeStr == "place" {
            titleLbl.isHidden = false
            menuCollec.frame.origin.y = 125
        } else {
            titleLbl.isHidden = true
            menuCollec.frame.origin.y = 100
        }
}

    override func viewWillAppear(_ animated: Bool) {
        
        // 19 June(Showing Initial Items in cart on Screen) :-
        getCart()
        
        if Singleton.sharedInstance.payFromCreditCard == "yes" {
            Singleton.sharedInstance.payFromCreditCard = "no"
            
            paymentView.isHidden=true
            
            self.indicator.startAnimating()
            self.indicator.isHidden = false
            
            if self.isSendDrink == true && self.isAcceptIm == true{
                self.sendDrinkAcceptCreditMethod()
            } else if self.isSendDrink == true && self.isAcceptWhos == true{
                self.sendDrinkAcceptWhosCreditMethod()
            } else {
                self.sendDrinkAPICreditMethod()
            }

       
        } else {
            self.paymentView.isHidden = true
            getProductMethod()
        }
            
        
}
    
//*********************************** 19 June(Show Inital Items in cart) ***************************//
     func getCart() {
        
        let json = ["user_id":Singleton.sharedInstance.userIdStr,
                    "auth_token":Singleton.sharedInstance.userTokenStr,
                    ] as [String : Any]
        
        
        print(json)
        
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/users/getcount", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    let json = JSON(data: response.data! )
                    print(json)
                    if let resDict = json["data"].dictionaryObject {
                        print(resDict)
                        self.cardCountLbl.text = resDict["count"] as? String
                            
                    }
               }
                    
                else if((response.error) != nil) {
                    
                    self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                    
                }
                
                self.indicator.stopAnimating()
                self.indicator.isHidden = true
                
        }
    }
    
   // MARK: Get Payment Email
    
    func getEmailMethod () {
        

        let json = ["user_id":Singleton.sharedInstance.userIdStr,
                    "auth_token":Singleton.sharedInstance.userTokenStr,
                    ] as [String : Any]
        
        
        print(json)
        
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/users/getpaypalemail", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    let json = JSON(data: response.data! )
                    if let descriptionStr = json["description"].string {
                        print(descriptionStr)
                        if descriptionStr == "Wrong auth token" {
                            
                            
                            UserDefaults.standard.setValue(nil, forKey: "auth_token")
                            UserDefaults.standard.setValue(nil, forKey: "user_id")
                            UserDefaults.standard.setValue(nil, forKey: "user_type")
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "Login") as! Login
                            vc.isExpired = true
                            self.navigationController?.pushViewController(vc,animated: true)
                            
                        }  else if descriptionStr == "Success" {
                            
                            
                            if json["data"]["email"].string != nil {
                                
                                self.isEmail = true

                                
                            } else {
                                
                                self.isEmail = false
                            }
                            
                        }
                        
                        
                        
                        //Now you got your value
                    }
                } else if((response.error) != nil){
                    
                    self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                    
                }
                
                self.indicator.stopAnimating()
                self.indicator.isHidden = true
                
        }
        
    }
    
    
    // MARK: Button Actions
    
    @IBAction func AddProduct(_ sender: Any) {

        if(isEmail) {
        
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "AddProducts") as! AddProducts
            navigationController?.pushViewController(vc,animated: true)

        } else {
        
            alertViewMethod(titleStr: "", messageStr: "Please first add your paypal email or bank details in payment method")
        }
        
    }

    
    
    @IBAction func PaymentBack(_ sender: Any) {
        paymentView.isHidden = true
    }

    
    @IBAction func Back(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
  
    @IBAction func PaymentAction(_ sender: Any) {
        
        if ((sender as AnyObject).tag == 1) {
            creditView.layer.borderColor = UIColor(red: 36/255, green: 69/255, blue: 138/255, alpha: 0.7).cgColor
            creditView.layer.borderWidth = 1
            
            paypalView.layer.borderColor = UIColor.white.cgColor
            paypalView.layer.borderWidth = 1
            
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "AddPayment") as! AddPayment
            vc.user_Id = userId
             print(vc.user_Id)
            vc.product_Id = productId
            print(vc.product_Id)
            vc.restro_Id = otherRestaurantId
            print(vc.restro_Id)
           vc.price = productAmount
            print(vc.price)
            vc.requestIdStr = requestIdStr
            print(requestIdStr)
            
            print(vc.requestIdStr)
            vc.recieverId = recieverIdStr
            print( vc.recieverId)
        


        
            navigationController?.pushViewController(vc,animated: true)
            
        }else {
            
            paypalView.layer.borderColor = UIColor(red: 36/255, green: 69/255, blue: 138/255, alpha: 0.7).cgColor
            paypalView.layer.borderWidth = 1
            
            creditView.layer.borderColor = UIColor.white.cgColor
            creditView.layer.borderWidth = 1
            
            sendDrinkMethod()
        }
        
    }

    
    // MARK: AlertView
    
    func alertViewMethod(titleStr:String, messageStr:String) {
        let alert = UIAlertController(title: titleStr, message: messageStr, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

    // MARK: Get Products Method
    
    func getProductMethod () {
        
        var  json = ["user_id":Singleton.sharedInstance.userIdStr,
            ] as [String : Any]
        
        
        if isOther == true {
        
            json = ["user_id":otherRestaurantId,
                    ]
        }
        
        
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/users/getproducts", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    let json = JSON(data: response.data! )
                    
                    if let descriptionStr = json["description"].string {
                        print(descriptionStr)
                        if descriptionStr == "Resturant Product list" {

                             self.productsArray = json["data"]["productlist"].array!
                            print(self.productsArray.count)
                          
                            // 21 June :-
                            if let rStatus = json["data"]["resturant_details_status"].string
                            {
                                self.status = rStatus
                            }
                       
                        }
                        //Now you got your value
                        self.menuCollec.reloadData()
                    }
                } else if((response.error) != nil){
                    
                    self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                    
                }

                self.indicator.stopAnimating()
                self.indicator.isHidden = true
        }
        
    }
    // MARK: Get Delete Products Method
    
    func deleteProductMethod (_ productId: String) {
        
        let  json = ["user_id":Singleton.sharedInstance.userIdStr,
                     "auth_token":Singleton.sharedInstance.userTokenStr,
                     "product_id":productId
                     ] as [String : Any]
        
        print(json)
        
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/users/delproducts", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    let json = JSON(data: response.data! )
                    if let descriptionStr = json["description"].string {
                        print(descriptionStr)
                        if descriptionStr == "Product delete Successfully" {
                            self.getProductMethod()
                        }
                        //Now you got your value
                        self.menuCollec.reloadData()
                    }
                } else if((response.error) != nil){
                    
                    self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                    
                }

                self.indicator.stopAnimating()
                self.indicator.isHidden = true
        }
        
    }

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return productsArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MenuCell", for: indexPath) as! MenuCell
        
        
        if (UIScreen.main.bounds.size.height==568 || UIScreen.main.bounds.size.height==480) {
            cell.menuImg.layer.cornerRadius=70/2
        }
         else if (UIScreen.main.bounds.size.height==736 ) {
            cell.menuImg.layer.cornerRadius=cell.menuImg.frame.size.height/2
        } else {
        
            cell.menuImg.layer.cornerRadius=55/2

        }

        
        cell.menuImg.clipsToBounds=true

        cell.menuNameLbl.text = productsArray[indexPath.row]["title"].string!
        // 10 June :-
        // cell.menuImg.sd_setImage(with: URL(string: productsArray[indexPath.row]["image"].string!), placeholderImage: UIImage(named: "gallery"))
        let urlStr : String = (productsArray[indexPath.row]["image"].string!)
        let urlFormattedStr = urlStr.replacingOccurrences(of: " ", with: "%20")
        cell.menuImg.sd_setImage(with: URL(string:urlFormattedStr), placeholderImage: UIImage(named: "gallery"))
        
        cell.priceLbl.text = "$\(productsArray[indexPath.row]["price"].float!)"
        cell.catLbl.text = "(\(productsArray[indexPath.row]["category_name"].string!))"

        
        cell.layer.cornerRadius = 4
        cell.layer.masksToBounds = false;
        cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.shadowOpacity = 0.3
        cell.layer.shadowRadius = 2
        cell.layer.shadowOffset = CGSize(width: 1, height: 1)

        if isOther == true {
            cell.deleteBtn.isHidden = true
            cell.deleteImg.isHidden = true
        }
        
        cell.deleteBtn.addTarget(self, action: #selector(DeleteProduct(_:)), for: .touchUpInside)
        cell.deleteBtn.tag = indexPath.row
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        if (UIScreen.main.bounds.size.height==568 || UIScreen.main.bounds.size.height==480) {
            return CGSize(width: 145,height: 146)
        }
        if (UIScreen.main.bounds.size.height==736 ) {
            return CGSize(width: 125,height: 129)
        }
        return CGSize(width: 112,height: 114)
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if isOther != true {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "AddProducts") as! AddProducts
            vc.isEdit = true
            vc.productIdStr = String(describing: self.productsArray[indexPath.row]["product_id"].int!)
            vc.priceStr = String(describing: self.productsArray[indexPath.row]["price"].float!)
            vc.titleStr = self.productsArray[indexPath.row]["title"].string!
            vc.productImgStr = self.productsArray[indexPath.row]["image"].string!
            vc.categoryStr = self.productsArray[indexPath.row]["category_name"].string!
            vc.catIdStr = String(describing: self.productsArray[indexPath.row]["category_id"].int!)

            navigationController?.pushViewController(vc,animated: true)
            
        }
        
        else if isSendDrink == true {
        
            // 29 April :- Need to do change in it.
            productId = String(describing: self.productsArray[indexPath.row]["product_id"].int!)
            productAmount = String(describing: self.productsArray[indexPath.row]["price"].float!)
            productName = self.productsArray[indexPath.row]["title"].string!
            newProductAmount = String(describing:((self.productsArray[indexPath.row]["price"].float!) + 1) )
            
            
            // 29 April :-
            print(productId)
            print(productAmount)
            print(productName)
            print(recieverIdStr)
            print(requestIdStr)
            
            let totalAmount = ((self.productsArray[indexPath.row]["price"].float!) + 1)
            let message = "$1-surcharge" + " " + "and" + " " + "Total-Amount:"  + " " + "$" + String(describing: totalAmount)
            
//******************************** 19 June(Adding Items to cart) *********************************  :-
            
            // 25 June :-
           // self.indicator.isHidden = false
           // self.indicator.startAnimating()
             self.indicator.isHidden = false
             self.indicator.stopAnimating()
            
            
            let timestamp = NSDate().timeIntervalSince1970
            print(timestamp)
            print(String(format: "%.0f", (timestamp)))
            let timestampp = (String(format: "%.0f", (timestamp)))
            print(timestampp)
            
            // 21 June :-
            let savedValueI = UserDefaults.standard.bool(forKey: "iambuying")
            let savedValueW = UserDefaults.standard.bool(forKey: "whosbuying")
            
            if savedValueI  == true {
                UserDefaults.standard.set(false, forKey: "iambuying")
                UserDefaults.standard.synchronize()
                jsonParameters = ["user_id":Singleton.sharedInstance.userIdStr,
                        "receiver_id":recieverIdStrI,
                        "auth_token":Singleton.sharedInstance.userTokenStr,
                        "product_id":productId,
                        "amount":newProductAmount,
                        "resturant_id":otherRestaurantId,
                        "timestamp":timestampp,
                        "request_id":requestIdStr,
                        "request_form":"1"]
                print(jsonParameters)
            }
         
           else if savedValueW  == true {
                UserDefaults.standard.set(false, forKey: "whosbuying")
                UserDefaults.standard.synchronize()
                jsonParameters = ["user_id":Singleton.sharedInstance.userIdStr,
                                  "receiver_id":recieverIdStrW,
                                  "auth_token":Singleton.sharedInstance.userTokenStr,
                                  "product_id":productId,
                                  "amount":newProductAmount,
                                  "resturant_id":otherRestaurantId,
                                  "timestamp":timestampp,
                                  "request_id":requestIdStr,
                                  "request_form":"2"]
                print(jsonParameters)
                
            }
                
             else if (savedValueI  == false) && (savedValueW == false) {
                jsonParameters = ["user_id":Singleton.sharedInstance.userIdStr,
                              "receiver_id":recieverIdStr,
                              "auth_token":Singleton.sharedInstance.userTokenStr,
                              "product_id":productId,
                              "amount":newProductAmount,
                              "resturant_id":otherRestaurantId,
                              "timestamp":timestampp ]
                    print(jsonParameters)
                
             }
            
            // Commented on 21 June :-
//             let  json = ["user_id":Singleton.sharedInstance.userIdStr,
//                         "receiver_id":recieverIdStr,
//                         "auth_token":Singleton.sharedInstance.userTokenStr,
//                         "product_id":productId,
//                         "amount":newProductAmount,
//                         "resturant_id":otherRestaurantId,
//                         "timestamp":timestampp
//
//                ] as [String : Any]
//
//            print(json)
        
            // 21 June :-
            if self.status == "0" {
                let alert = UIAlertController(title: "", message: "Buisness-Details of this restaurant is not added", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
           
           // 28 May :- Adding $1 in Total-Payment.
            let alert = UIAlertController(title: "Add This Item to Cart", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                
                // 25 June :-
                 self.indicator.isHidden = false
                 self.indicator.startAnimating()
                
                // 25 June :-
//                   if (UserDefaults.standard.value(forKey: "restroid") != nil) {
//                     let value:String = UserDefaults.standard.value(forKey: "restroid") as! String
//                      if value != self.otherRestaurantId {
//                       self.removeCart()
//                 }
                
                    
//            else {
                
                // 29 April :-
               // paymentView.isHidden = false
                self.paymentView.isHidden = true
             
             // calling API for adding items to cart :-
                Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/users/addtocart", method: .post, parameters:self.jsonParameters , encoding: JSONEncoding.default)
                    .responseJSON { response in
                        print(response.result)   // result of response serialization
                        print(response)
                        if let jsonData = response.result.value {
                            print("JSON: \(jsonData)")
                            let json = JSON(data: response.data! )
                            print(json)
                            if let resDict = json["data"].dictionaryObject {
                                print(resDict)
                                if let cardDetailsDict = resDict["cart_details"] as? [String:Any]{
                                    print(cardDetailsDict)
                                    
                                    // 25 June :-
                                    self.cart_id = cardDetailsDict["cart_id"] as! String
                                    print(self.cart_id)
                                    UserDefaults.standard.set(self.cart_id, forKey: "cart_id")
                                    UserDefaults.standard.synchronize()
                                    
                                    
                                     self.cardCountLbl.text = cardDetailsDict["count"] as? String
                                     self.indicator.stopAnimating()
                                     self.indicator.isHidden = true
                                    
                                    let alert = UIAlertController(title: "", message: "Item is added in Cart", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                    
                                    // 25 June :-
                                     UserDefaults.standard.set(self.otherRestaurantId, forKey: "restroid")
                                    UserDefaults.standard.synchronize()

                                 }
                            }
                            
                           
                        } else if((response.error) != nil){
                            self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                        }
                        
                        self.indicator.stopAnimating()
                        self.indicator.isHidden = true
                        
          
                        
                }
               
                 //   } // else
             //   } //first if condition
                
            }
        ))
            self.present(alert, animated: true)
            
        }
        
    }
   
// 25 June :-
    func removeCart()  {
        
        let alert = UIAlertController(title: "You already saved cart for other restaurant", message: "Click OK to view cart.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                
                let cart_idd = UserDefaults.standard.value(forKey: "cart_id")
                UserDefaults.standard.removeObject(forKey: "cart_id")
                UserDefaults.standard.synchronize()
                
                
                let Parameters =     ["user_id":Singleton.sharedInstance.userIdStr,
                                      "auth_token":Singleton.sharedInstance.userTokenStr,
                                      "cart_id": cart_idd!]
                    as [String:Any]
                
                Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/users/removecart", method: .post, parameters: Parameters , encoding: JSONEncoding.default)
                    .responseJSON { response in
                        print(response.result)   // result of response serialization
                        print(response)
                        if let jsonData = response.result.value {
                            print("JSON: \(jsonData)")
                            let json = JSON(data: response.data! )
                            print(json)
                            if let resDict = json["data"].dictionaryObject {
                                print(resDict)
                                if let message = resDict["message"] as? String {
                                    print(message)
                                    if message == "successfully remove cart" {
                                        
                                        self.indicator.stopAnimating()
                                        self.indicator.isHidden = true
                                        
                                        let alert = UIAlertController(title: "", message: "Cart clear successfully", preferredStyle: UIAlertControllerStyle.alert)
                                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                                        self.present(alert, animated: true, completion: nil)
                                        
                                    }
                                }
                            }
                            
                            
                        } else if((response.error) != nil){
                            self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                        }
                        
                        self.indicator.stopAnimating()
                        self.indicator.isHidden = true
                        
                }
                
            } )); self.present(alert, animated: true)
      }
    
    
    func DeleteProduct(_ button: UIButton) {
        
        let alert = UIAlertController(title: nil, message: "Are you sure you want to delete?", preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let deleteBtn = UIAlertAction(title: "Delete", style: .destructive, handler: {(_ action: UIAlertAction) -> Void in
           
            
            self.indicator.startAnimating()
            self.indicator.isHidden = false
            
            
            print(button.tag)
            print(String(describing: self.productsArray[button.tag]["product_id"].int!))
            self.deleteProductMethod(String(describing: self.productsArray[button.tag]["product_id"].int!))
            
        })
        
        
        alert.addAction(deleteBtn)
        alert.addAction(cancelAction)
        present(alert, animated: true)
        
        
    }

    
    // MARK: PayPal Payment Meyhods
    
    // MARK: Single Payment
    
    func sendDrinkMethod() {
        // Remove our last completed payment, just for demo purposes.
        resultText = ""
        
        // Note: For purposes of illustration, this example shows a payment that includes
        //       both payment details (subtotal, shipping, tax) and multiple items.
        //       You would only specify these if appropriate to your situation.
        //       Otherwise, you can leave payment.items and/or payment.paymentDetails nil,
        //       and simply set payment.amount to your total charge.
        
        // Optional: include multiple items
        let item1 = PayPalItem(name: productName, withQuantity: 1, withPrice: NSDecimalNumber(string: productAmount), withCurrency: "USD", withSku: productId)
    //    let item2 = PayPalItem(name: "Free rainbow patch", withQuantity: 1, withPrice: NSDecimalNumber(string: "0.00"), withCurrency: "USD", withSku: "Hip-00066")
    //    let item3 = PayPalItem(name: "Long-sleeve plaid shirt (mustache not included)", withQuantity: 1, withPrice: NSDecimalNumber(string: "37.99"), withCurrency: "USD", withSku: "Hip-00291")
        
        let items = [item1]
        let subtotal = PayPalItem.totalPrice(forItems: items)
        
        // Optional: include payment details
        let shipping = NSDecimalNumber(string: "0.00")
        let tax = NSDecimalNumber(string: "0.00")
        let paymentDetails = PayPalPaymentDetails(subtotal: subtotal, withShipping: shipping, withTax: tax)
        
        let total = subtotal.adding(shipping).adding(tax)
        
        let payment = PayPalPayment(amount: total, currencyCode: "USD", shortDescription: "Food/Drinkz", intent: .sale)
        
        payment.items = items
        payment.paymentDetails = paymentDetails
        
        if (payment.processable) {
            let paymentViewController = PayPalPaymentViewController(payment: payment, configuration: payPalConfig, delegate: self)
            present(paymentViewController!, animated: true, completion: nil)
        }
        else {
            // This particular payment will always be processable. If, for
            // example, the amount was negative or the shortDescription was
            // empty, this payment wouldn't be processable, and you'd want
            // to handle that here.
            print("Payment not processalbe: \(payment)")
        }
        
    }
    

    // PayPalPaymentDelegate
    

    
    
    func payPalPaymentDidCancel(_ paymentViewController: PayPalPaymentViewController) {
        print("PayPal Payment Cancelled")
        resultText = ""
        successView.isHidden = true
        paymentViewController.dismiss(animated: true, completion: nil)
    }
    
    func payPalPaymentViewController(_ paymentViewController: PayPalPaymentViewController, didComplete completedPayment: PayPalPayment) {
        print("PayPal Payment Success !")
        paymentViewController.dismiss(animated: true, completion: { () -> Void in
            // send completed confirmaion to your server
            print("Here is your proof of payment:\n\n\(completedPayment.confirmation)\n\nSend this to your server for confirmation and fulfillment.")
            
            
            let dict = completedPayment.confirmation["response"] as! [String:Any]
            
            self.transactionId = dict["id"] as! String
            
            
            self.indicator.startAnimating()
            self.indicator.isHidden = false

            if self.isSendDrink == true && self.isAcceptIm == true{
                self.sendDrinkAcceptMethod()
            } else if self.isSendDrink == true && self.isAcceptWhos == true{
                self.sendDrinkAcceptWhosMethod()
            } else {
                self.sendDrinkAPIMethod()
            }
            
            self.resultText = completedPayment.description
            
            self.paymentView.isHidden = true
            self.showSuccess()
        })
    }

    // MARK: Helpers
    
    func showSuccess() {
        successView.isHidden = false
        successView.alpha = 1.0
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.5)
        UIView.setAnimationDelay(2.0)
        successView.alpha = 0.0
        UIView.commitAnimations()
    }

  
    // MARK: Send Drinkz API
    
    func sendDrinkAcceptWhosMethod() {
        
        let  json = ["user_id":Singleton.sharedInstance.userIdStr,
                     "auth_token":Singleton.sharedInstance.userTokenStr,
                     "receiver_id":recieverIdStr,
                     "status":"2",
                     "request_id": requestIdStr,
                     "product_id":productId,
                     "resturant_id":otherRestaurantId,
                     "transaction_id":transactionId,
                     "amount":productAmount
            
            ] as [String : Any]
        
        print(json)
        
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/members/whobuyingformeaccept", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    let json = JSON(data: response.data! )
                    if let descriptionStr = json["description"].string {
                        print(descriptionStr)
                        if descriptionStr == " Drink send  successfully" {
                         //   self.alertViewMethod(titleStr: "", messageStr: "You have been successfully sent food/drinkz.")
                            _ = self.navigationController?.popViewController(animated: true)
                        }
                        //Now you got your value

                    
                    }
                } else if((response.error) != nil){
                    self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                }
                
                self.indicator.stopAnimating()
                self.indicator.isHidden = true

        }
        
    }
    

    
    func sendDrinkAcceptMethod () {
        
        let  json = ["user_id":Singleton.sharedInstance.userIdStr,
                     "auth_token":Singleton.sharedInstance.userTokenStr,
                     "request_id":requestIdStr,
                     "status":"2",
            "product_id":productId,"resturant_id":otherRestaurantId,"transaction_id":transactionId,"amount":productAmount
            
            ] as [String : Any]
        
      //  restruant_id
        
 
        print(json)

        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/members/updatebuystatus", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    let json = JSON(data: response.data! )
                    if let descriptionStr = json["description"].string {
                        print(descriptionStr)
                        if descriptionStr == "Drink Aceepted successfully" {
                     //       self.alertViewMethod(titleStr: "", messageStr: "You have been successfully sent food/drinkz.")
                            _ = self.navigationController?.popViewController(animated: true)
                        }
                        //Now you got your value
                        self.menuCollec.reloadData()
                    }
                } else if((response.error) != nil){
                    self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                }
                self.indicator.stopAnimating()
                self.indicator.isHidden = true

        }
    }

    // 9 May :- 
    func getCurrentTimeStampWOMiliseconds(dateToConvert: NSDate) -> String {
        let objDateformat: DateFormatter = DateFormatter()
        objDateformat.dateFormat = "yyyy-MM-dd"
        let strTime: String = objDateformat.string(from: dateToConvert as Date)
        let objUTCDate: NSDate = objDateformat.date(from: strTime)! as NSDate
        let milliseconds: Int64 = Int64(objUTCDate.timeIntervalSince1970)
        let strTimeStamp: String = "\(milliseconds)"
        return strTimeStamp
    }
    
    
    func sendDrinkAPIMethod () {
        
        // 9 May :- Time-Stamp is added
        
        let now = NSDate()
        let nowTimeStamp = self.getCurrentTimeStampWOMiliseconds(dateToConvert: now)
        print(nowTimeStamp)
        
        let  json = ["user_id":Singleton.sharedInstance.userIdStr,
                     "auth_token":Singleton.sharedInstance.userTokenStr,
                     "receiver_id":recieverIdStr,
                     "product_id":productId,"resturant_id":otherRestaurantId,"transaction_id":transactionId,"amount":productAmount,"timestamp": nowTimeStamp
                     
                     ] as [String : Any]
        
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/members/sendproductdrink", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    let json = JSON(data: response.data! )
                    if let descriptionStr = json["description"].string {
                        print(descriptionStr)
                        if descriptionStr == "Drink successfully offered!" {
                            self.alertViewMethod(titleStr: "", messageStr: "You have been successfully sent food/drinkz.")
                        }
                        //Now you got your value
                        self.menuCollec.reloadData()
                    }
                } else if((response.error) != nil){
                    self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                }
                
                self.indicator.stopAnimating()
                self.indicator.isHidden = true

         }
        
    }
    
    
    
    
    
    
    
    
    
    // MARK: Send Drinkz Credit API
    
    func sendDrinkAcceptWhosCreditMethod () {
        
        let  json = ["user_id":Singleton.sharedInstance.userIdStr,
                     "auth_token":Singleton.sharedInstance.userTokenStr,
                     "receiver_id":recieverIdStr,
                     "status":"2",
                     "request_id": requestIdStr,
                     "product_id":productId,
                     "resturant_id":otherRestaurantId,
                     "card_num":Singleton.sharedInstance.cardNoStr,
                     "exp_date":Singleton.sharedInstance.expDateStr,
                     "amount":productAmount
            
            ] as [String : Any]
        
        
        print(json)
        
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/members/whobuyingformeacceptauthorize", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    let json = JSON(data: response.data! )
                    if let descriptionStr = json["description"].string {
                        print(descriptionStr)
                        if descriptionStr == " Drink send  successfully" {
                            //   self.alertViewMethod(titleStr: "", messageStr: "You have been successfully sent food/drinkz.")
                            _ = self.navigationController?.popViewController(animated: true)
                        } else if descriptionStr == "Payment Unsuccessful" {
                            self.alertViewMethod(titleStr: "", messageStr: json["error"]["error_desc"].string!)
                        }
                        //Now you got your value
                        
                        
                    }
                } else if((response.error) != nil){
                    self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                }
                
                self.indicator.stopAnimating()
                self.indicator.isHidden = true
                
        }
        
    }
   
    
    
    
    
    
    func sendDrinkAcceptCreditMethod () {
        
        // 9 May :- Time-Stamp is added.
        
        let now = NSDate()
        let nowTimeStamp = self.getCurrentTimeStampWOMiliseconds(dateToConvert: now)
        print(nowTimeStamp)
        
         let  json = ["user_id":Singleton.sharedInstance.userIdStr,
                     "auth_token":Singleton.sharedInstance.userTokenStr,
                     "request_id":requestIdStr,
                     "status":"2",
                     "product_id":productId,"resturant_id":otherRestaurantId,
                     "card_num":Singleton.sharedInstance.cardNoStr,
                     "exp_date":Singleton.sharedInstance.expDateStr,
                     "amount":productAmount,
                     "timestamp":nowTimeStamp
            
            ] as [String : Any]
        
        //  restruant_id
        
        
        print(json)
        
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/members/updatebuystatusauthorize", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    let json = JSON(data: response.data! )
                    if let descriptionStr = json["description"].string {
                        print(descriptionStr)
                        if descriptionStr == "Drink Aceepted successfully" {
                            //       self.alertViewMethod(titleStr: "", messageStr: "You have been successfully sent food/drinkz.")
                            _ = self.navigationController?.popViewController(animated: true)
                        } else if descriptionStr == "Payment Unsuccessful" {
                            self.alertViewMethod(titleStr: "", messageStr: json["error"]["error_desc"].string!)
                        }
                        //Now you got your value
                        self.menuCollec.reloadData()
                    }
                } else if((response.error) != nil){
                    self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                }
                self.indicator.stopAnimating()
                self.indicator.isHidden = true
                
        }
        
    }
    
    
    
    
    // generate token through stripe
    
    
    
//    func hitCardService(){
//        
//     //   self.activityIndicator.isHidden = false
//      //  self.activityIndicator.startAnimating()
//        let stripCard = STPCard()
//        
//        // Split the expiration date to extract Month & Year
//        if self.txtFieldExpDate.text?.isEmpty == false {
//            let expirationDate = self.txtFieldExpDate.text?.components(separatedBy:"-")
//            let expMonth = UInt((expirationDate?[1])!)
//            let expYear = UInt((expirationDate?[0])!)
//            
//            // Send the card info to Strip to get the token
//            stripCard.number = self.txtFCardNum.text
//            stripCard.cvc = self.txtFieldCVV.text
//            stripCard.expMonth = expMonth!
//            stripCard.expYear = expYear!
//        }
//        
//        
//        // validate card
//        
//        do{
//            
//            let data  = try  stripCard.validateReturningError()
//            
//        }
//            
//        catch{
//            
//            print(error.localizedDescription)
//            let objAlertController = UIAlertController(title: "" ,message: error.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
//            let objAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler:nil)
//            objAlertController.addAction(objAction)
//            self.present(objAlertController, animated: true, completion: nil)
//            self.activityIndicator.isHidden = true
//            self.activityIndicator.stopAnimating()
//            
//        }
//        
//        
//        
//        
//        
//        STPAPIClient.shared().createToken(with: stripCard, completion: { (token, error) -> Void in
//            guard let stripeToken = token
//                else {
//                    NSLog("Error creating token: %@", error!.localizedDescription);
//                    return
//            }
//            
//            print(stripeToken)
//            
//            
//            //    if stripCard.validateReturningError() == ""{
//            
//            
//            
//            self.postStripeToken(token!)
//            
//            
//            //   }
//            
//            
//        })
//    }
//    
//    
    func sendDrinkAPICreditMethod () {
        
        let  json = ["user_id":Singleton.sharedInstance.userIdStr,
                     "auth_token":Singleton.sharedInstance.userTokenStr,
                     "receiver_id":recieverIdStr,
                     "product_id":productId,
                     "resturant_id":otherRestaurantId,
                     "card_num":Singleton.sharedInstance.cardNoStr,
                     "exp_date":Singleton.sharedInstance.expDateStr,
                     "amount":productAmount
            
            ] as [String : Any]
        
        
        print(json)
        
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/members/sendproductdrinkauthrize", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    let json = JSON(data: response.data! )
                    if let descriptionStr = json["description"].string {
                        print(descriptionStr)
                        if descriptionStr == "Drink successfully offered!" {
                            self.alertViewMethod(titleStr: "", messageStr: "You have been successfully sent food/drinkz.")
                        } else if descriptionStr == "Payment Unsuccessful" {

                            self.alertViewMethod(titleStr: "", messageStr: json["error"]["description"].string!)
                        }
                        //Now you got your value
                        self.menuCollec.reloadData()
                    }
                } else if((response.error) != nil){
                    self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                }
                
                self.indicator.stopAnimating()
                self.indicator.isHidden = true
                
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 
    // 19 June(Cart-Button Action) :-
     @IBAction func cartBtnAction(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CartDetailVC") as! CartDetailVC
        navigationController?.pushViewController(vc,animated: true)
     }
    
}
