//
//  CardDetailsNew.swift
//  FreeDrinkz
//
//  Created by Brst981 on 17/11/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import QuartzCore
import Stripe

class CardDetailsNew: UIViewController,UITextFieldDelegate {
    
    
    
    // outlets
    
    @IBOutlet var cardDetailTV: UITableView!
    @IBOutlet var saveButton: UIButton!
    @IBOutlet var txtFCardNum: UITextField!
    @IBOutlet var txtFieldExpDate: UITextField!
    @IBOutlet var txtFieldCVV: UITextField!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var backBtn: UIButton!
    
    // variables
    var paymentArray  = NSMutableArray()
    var cardType = NSArray()
     var underlyingError: NSError?
   
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
         self.activityIndicator.isHidden = false
        
        
        customization()
        
        
        getCardDetails()
        
        
        

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func customization(){
        
        
        // code for setting the button to round
        saveButton.layer.cornerRadius = saveButton.layer.frame.height/2
        saveButton.clipsToBounds = true
        
        
        // code for opening the keyboard in number pad
        self.txtFieldCVV.keyboardType = UIKeyboardType.numberPad
        self.txtFCardNum.keyboardType = UIKeyboardType.numberPad
        

        // code for showing the encyrypted cvv no
        self.txtFieldCVV.isSecureTextEntry = true
        
        
    }
    
    
    // code for validation
    
    func checkValidation() -> Bool {
        
        if self.txtFCardNum.text == "" {
            
            let objAlertController = UIAlertController(title: "Error",message: "Please enter the card number", preferredStyle: UIAlertControllerStyle.alert)
            let objAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler:nil)
            objAlertController.addAction(objAction)
            self.present(objAlertController, animated: true, completion: nil)
            
            return false
            
            
            
        }

        
        
        else if self.txtFieldExpDate.text == ""{
            
            let objAlertController = UIAlertController(title: "Error",message: "Please enter the expiry date", preferredStyle: UIAlertControllerStyle.alert)
            let objAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler:nil)
            objAlertController.addAction(objAction)
            self.present(objAlertController, animated: true, completion: nil)
            
            return false
            
            
        }
        else if self.txtFieldCVV.text == ""{
            
            let objAlertController = UIAlertController(title: "Error",message: "Please enter the cvv number", preferredStyle: UIAlertControllerStyle.alert)
            let objAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler:nil)
            objAlertController.addAction(objAction)
            self.present(objAlertController, animated: true, completion: nil)
            
            return false
            
            
        }
        else{
            
            return true
            
        }
        
        
    }
    
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        
        if textField == txtFCardNum {
        var newString: String = (txtFCardNum.text! as NSString).replacingCharacters(in: range, with: string)
        if (newString.characters.count) > 16 {
            let objAlertController = UIAlertController(title: "" ,message: "You cannot enter more than 16 digits", preferredStyle: UIAlertControllerStyle.alert)
            let objAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler:nil)
            objAlertController.addAction(objAction)
          self.present(objAlertController, animated: true, completion: nil)
            return false

        }
       
    }
        
            if textField == txtFieldExpDate{
                
               var strText: String? = txtFieldExpDate.text
                if strText == nil {
                    strText = ""
                }
                
                
                strText = strText?.replacingOccurrences(of: "-", with: "")
                
                
                if strText!.characters.count > 1 && strText!.characters.count % 4 == 0 && string != "" {
                    textField.text = "\(txtFieldExpDate.text!)-\(string)"
                    return false
                }
                
          }
    
    
        if textField == txtFieldCVV {
            
            guard let text = textField.text else { return true }
            let newLength = text.characters.count + string.characters.count - range.length
            //   let newLengthAgain = cvvTxt.text
            if (newLength > 3) {
                
                let objAlertController = UIAlertController(title: "" ,message: "You card's security code is invalid", preferredStyle: UIAlertControllerStyle.alert)
                let objAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler:nil)
                objAlertController.addAction(objAction)
                self.present(objAlertController, animated: true, completion: nil)
            }
        
    }

        
        
        
        return true

        
    }
    
    // self.objAlertController(message: "You cannot enter more than 16 digits")
    
    //Create Alert View
//    func objAlertController(message:String)
//    {
//        let objAlertController = UIAlertController(title: "Error",message: message, preferredStyle: UIAlertControllerStyle.alert)
//        let objAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler:nil)
//        objAlertController.addAction(objAction)
//        self.present(objAlertController, animated: true, completion: nil)
//    }
    
    
    
    
    // code for save action
  @IBAction func saveDetails(_ sender: Any) {
   
        // code for checking validation
        if  checkValidation() == true {
            
            hitCardService()
            
        }
   }
    
   // code for poping back to view controller
    
    @IBAction func popBack(_ sender: Any) {
    
    self.navigationController?.popViewController(animated: true)
    
    
    }
    
    
    
    
    
  // code for saving details through stripe
    func hitCardService(){
        
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
        let stripCard = STPCard()
        
        // Split the expiration date to extract Month & Year
        if self.txtFieldExpDate.text?.isEmpty == false {
            let expirationDate = self.txtFieldExpDate.text?.components(separatedBy:"-")
            let expMonth = UInt((expirationDate?[1])!)
            let expYear = UInt((expirationDate?[0])!)
            
            // Send the card info to Strip to get the token
            stripCard.number = self.txtFCardNum.text
            stripCard.cvc = self.txtFieldCVV.text
            stripCard.expMonth = expMonth!
            stripCard.expYear = expYear!
        }
        
        
        // validate card
       
        do{
           
        let data  = try  stripCard.validateReturningError()
           
        }
        
        catch{
            
            print(error.localizedDescription)
            let objAlertController = UIAlertController(title: "" ,message: error.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
            let objAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler:nil)
            objAlertController.addAction(objAction)
            self.present(objAlertController, animated: true, completion: nil)
            self.activityIndicator.isHidden = true
            self.activityIndicator.stopAnimating()
            
        }
     
        
        
        
        
        STPAPIClient.shared().createToken(with: stripCard, completion: { (token, error) -> Void in
            guard let stripeToken = token
                else {
                    NSLog("Error creating token: %@", error!.localizedDescription);
                    return
            }
            
            print(stripeToken)
            
            
        //    if stripCard.validateReturningError() == ""{
                
                
                
                self.postStripeToken(token!)
   
                
         //   }
            
            
       })
  }
  

   // add web service method
   func postStripeToken(_ token: STPToken){
    
    
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
        self.txtFieldExpDate.isUserInteractionEnabled = true
        self.txtFieldCVV.isUserInteractionEnabled = true
        self.txtFCardNum.isUserInteractionEnabled = true
        self.backBtn.isEnabled = true
        self.saveButton.isEnabled = true
        self.cardDetailTV.isUserInteractionEnabled = true
    

        let p : Parameters = ["user_id":Singleton.sharedInstance.userIdStr,"auth_token":Singleton.sharedInstance.userTokenStr,"stripe_token":token.tokenId]
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/members/addCustomerPaymentProfile", method: .post, parameters: p, encoding: JSONEncoding.default)
            .responseString(completionHandler: { (response) in
                
                DispatchQueue.main.async{
                print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    let json = JSON(data: response.data!)
                   let statusCode = json["status_code"].intValue
                   print(statusCode)
                    if  statusCode == 200{
                        
                        let message = json["description"].string
                        let objAlertController = UIAlertController(title: message ,message: "", preferredStyle: UIAlertControllerStyle.alert)
                        let objAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler:nil)
                        objAlertController.addAction(objAction)
                        self.present(objAlertController, animated: true, completion: nil)
                        self.txtFCardNum.text = ""
                        self.txtFieldCVV.text = ""
                        self.txtFieldExpDate.text = ""
                        self.getCardDetails()
 
                        
                     }
                    else{
                        
                    let objAlertController = UIAlertController(title: "" ,message: "Error", preferredStyle: UIAlertControllerStyle.alert)
                       let objAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler:nil)
                       objAlertController.addAction(objAction)
                        self.present(objAlertController, animated: true, completion: nil)
                  }
                
                    DispatchQueue.main.async {
                        self.activityIndicator.stopAnimating()
                        self.activityIndicator.isHidden = true
                        self.cardDetailTV.reloadData()
                        self.backBtn.isEnabled = true
                        self.txtFieldExpDate.isUserInteractionEnabled = true
                        self.txtFieldCVV.isUserInteractionEnabled = true
                        self.txtFCardNum.isUserInteractionEnabled = true
                        self.saveButton.isEnabled = true
                        self.cardDetailTV.isUserInteractionEnabled = true



                    }
                    
                }
           }
        })
    
    
    }
    
    
    // code for getting car details
    func getCardDetails(){
        
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
        self.txtFieldExpDate.isUserInteractionEnabled = true
        self.txtFieldCVV.isUserInteractionEnabled = true
        self.txtFCardNum.isUserInteractionEnabled = true
        self.backBtn.isEnabled = true
        self.saveButton.isEnabled = true
       
        let p : Parameters = ["user_id":Singleton.sharedInstance.userIdStr,"auth_token":Singleton.sharedInstance.userTokenStr]
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/members/getcustomerprofile", method: .post, parameters: p, encoding: JSONEncoding.default)
            .responseString(completionHandler: { (response) in
                print(response)
                DispatchQueue.main.async {
              if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    let json = JSON(data: response.data!)
                 if let jsonDict = json.dictionaryObject {
                        print(jsonDict)
                        if let sources = jsonDict["sources"] as? NSDictionary {
                        print(sources)
                        let paymentarr = sources["data"] as! NSArray
                          print(paymentarr)
                        self.paymentArray = paymentarr.mutableCopy() as! NSMutableArray
                            print(self.paymentArray)
                             self.cardType = self.paymentArray.value(forKey: "brand") as! NSArray
                            print(self.cardType)
                          let cardNum = self.paymentArray.value(forKey: "last4") as! NSArray
                          print(cardNum)
                            DispatchQueue.main.async {
                            self.activityIndicator.stopAnimating()
                            self.activityIndicator.isHidden = true
                            self.cardDetailTV.reloadData()
                            self.txtFieldExpDate.isUserInteractionEnabled = true
                            self.txtFieldCVV.isUserInteractionEnabled = true
                            self.txtFCardNum.isUserInteractionEnabled = true
                            self.backBtn.isEnabled = true
                            self.saveButton.isEnabled = true
    
                            }
                        }
                        
                        else{
                            
                            self.activityIndicator.stopAnimating()
                            self.activityIndicator.isHidden = true
                            self.cardDetailTV.isHidden = true
                            
                        }
                        
                    }
                    
                }
           }
               
      })
 
    }
    



}

extension CardDetailsNew:UITableViewDelegate,UITableViewDataSource{
    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
      print(self.paymentArray.count)
      return self.paymentArray.count

        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:CardDetailTableViewCell = self.cardDetailTV.dequeueReusableCell(withIdentifier: "cardDetail") as! CardDetailTableViewCell
        
        let dict =  self.paymentArray[indexPath.row] as! NSDictionary
        let cardNum = dict["last4"] as? String
        let cardString = "XXXX XXXX XXXX"
        cell.lblCardNumber.text = "\(cardString) \(cardNum!)"
        cell.lblCardName.text = dict["brand"] as? String
        
        
        // code for making the selection color disappear
        cell.selectionStyle =  UITableViewCellSelectionStyle.none
        
        return cell
        
        }
    
    // code for deleting the card information
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
     }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.delete) {

            let  localDic = self.paymentArray.object(at: indexPath.row) as! NSDictionary
           removeSavedCard(customer_id: localDic.value(forKey: "customer") as! String,card_id:localDic.value(forKey: "id") as! String, indexToRemove: indexPath.row)
     }
    }
    
    // 10 June(On Selecting the Particular Card from Saved Cards and delete it.) :-
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath as IndexPath, animated: true)
        
        let alert = UIAlertController(title: "Alert !!", message: "Are you sure that You want to remove this saved-card?", preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "OK", style: .default, handler: { action in
            
            let  localDic = self.paymentArray.object(at: indexPath.row) as! NSDictionary
            self.removeSavedCard(customer_id: localDic.value(forKey: "customer") as! String,card_id:localDic.value(forKey: "id") as! String, indexToRemove: indexPath.row)
            
         })
        alert.addAction(ok)
        
        let cancel = UIAlertAction(title: "Cancel", style: .default, handler: { action in
        })
        alert.addAction(cancel)
        
        DispatchQueue.main.async(execute: {
            self.present(alert, animated: true)
        })
        
    }
    
    
    // code for deleting the saved credit card info
    func removeSavedCard(customer_id: String,card_id : String, indexToRemove : Int){
        
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
        self.backBtn.isEnabled = false
        self.txtFieldExpDate.isUserInteractionEnabled = false
        self.txtFieldCVV.isUserInteractionEnabled = false
        self.txtFCardNum.isUserInteractionEnabled = false
        self.saveButton.isEnabled = false
        self.cardDetailTV.isUserInteractionEnabled = false
        
        let p : Parameters = ["user_id":Singleton.sharedInstance.userIdStr,"auth_token":Singleton.sharedInstance.userTokenStr,"customer_id":customer_id,"card_id":card_id]
        print(p)
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/members/deleteCustomerPaymentProfile", method: .post, parameters: p, encoding: JSONEncoding.default)
            .responseString(completionHandler: { (response) in
                print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    let json = JSON(data: response.data!)
                    print(self.paymentArray.removeObject(at: indexToRemove))
                
                    DispatchQueue.main.async {
                        self.activityIndicator.isHidden = true
                        self.activityIndicator.stopAnimating()
                        self.cardDetailTV.reloadData()
                        self.backBtn.isEnabled = true
                        self.txtFieldExpDate.isUserInteractionEnabled = true
                        self.txtFieldCVV.isUserInteractionEnabled = true
                        self.txtFCardNum.isUserInteractionEnabled = true
                        self.saveButton.isEnabled = true
                        self.cardDetailTV.isUserInteractionEnabled = true

                        // 10 June :-
                      
                        let alert = UIAlertController(title: "", message: "This Card is deleted !!", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)

                    }

                    
                }
                    
           
                
            })

        }
    
    
}
