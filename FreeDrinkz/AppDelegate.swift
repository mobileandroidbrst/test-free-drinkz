//
//  AppDelegate.swift
//  FreeDrinkz
//
//  Created by Brst-Pc109 on 24/03/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import FBSDKCoreKit
import Fabric
import TwitterKit
import CoreLocation
import Firebase
import UserNotifications
import FirebaseMessaging
import FirebaseInstanceID
import Alamofire
import GooglePlaces
import Stripe

// 12 June :-
import SwiftyJSON


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, GIDSignInDelegate, UNUserNotificationCenterDelegate, FIRMessagingDelegate {
    
    // 12 June :-
    var batchCount = 0
    var loopCount = 0
    var isMesageCount = false
    
    
    var window: UIWindow?
    var locationManager = CLLocationManager()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        let center = UNUserNotificationCenter.current()
        center.delegate = self
        center.requestAuthorization(options: ([.badge, .sound, .alert]), completionHandler: {(_ granted: Bool, _ error: Error?) -> Void in
            if error == nil {
                print("request succeeded!")
            }
        })
        
        
        if UserDefaults.standard.bool(forKey: "SetInitialTimeLocOn") == false {
            UserDefaults.standard.set("1", forKey: "locationKey")
            UserDefaults.standard.set(true, forKey: "SetInitialTimeLocOn")
        }
        // Enter Google Place
        
        GMSPlacesClient.provideAPIKey("AIzaSyCCRH1MkYF3Be8NXXGI8OCgorPyole30zo")
        //    GMSPlacesClient.provideAPIKey("AIzaSyDfcZ0miS7VPPS7tYA38zb1eQAciEnSvR4")
        
        
        // Enter your paypal credentials
        
        PayPalMobile .initializeWithClientIds(forEnvironments: [PayPalEnvironmentProduction: "YOUR_CLIENT_ID_FOR_PRODUCTION",
                                                                PayPalEnvironmentSandbox: "AQUHtPz0085gZXNjKXtwPdeMKeNXHoNrt8AsYhj3lsmXtrL8eapDbRhxKuj0Bd8nWygUdQqgnYRyPM0M"])
        
        
        // Firebase configure
        
        FIRApp.configure()
        
        // Register for notifications
        
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization( options: authOptions, completionHandler: {_, _ in })
            
            // For iOS 10 data message (sent via FCM)
            FIRMessaging.messaging().remoteMessageDelegate = self
            
        } else {
            let settings: UIUserNotificationSettings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        // Google SignIn
        
        var configureError: NSError?
        GGLContext.sharedInstance().configureWithError(&configureError)
        assert(configureError == nil, "Error configuring Google services: \(String(describing: configureError?.localizedDescription))")
        GIDSignIn.sharedInstance().delegate = self
        
        // Twitter using Fabric
        
        Fabric.with([Twitter.self])
        
        // Facebook configure
        
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().keyboardDistanceFromTextField = CGFloat(10)
        IQKeyboardManager.sharedManager().preventShowingBottomBlankSpace = true
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
        IQKeyboardManager.sharedManager().shouldShowTextFieldPlaceholder = true
        IQKeyboardManager.sharedManager().shouldPlayInputClicks = true
        
        
        if UserDefaults.standard.value(forKey: "socialLoginType") != nil {
            Singleton.sharedInstance.socialLoginType = UserDefaults.standard.value(forKey: "socialLoginType")! as! String
        }
        
        
        // Check Whether user is autologin
        
        if UserDefaults.standard.value(forKey: "auth_token") != nil {
            Singleton.sharedInstance.userTokenStr = UserDefaults.standard.value(forKey: "auth_token")! as! String
            Singleton.sharedInstance.userIdStr = UserDefaults.standard.value(forKey: "user_id")! as! String
            
            Singleton.sharedInstance.userTypeIdStr = UserDefaults.standard.value(forKey: "user_type")! as! String
            
            if UserDefaults.standard.value(forKey: "latitude") != nil {
                Singleton.sharedInstance.latitudeStr = UserDefaults.standard.value(forKey: "latitude")! as! String
                Singleton.sharedInstance.longitudeStr = UserDefaults.standard.value(forKey: "longitude")! as! String
            }
            
            if Singleton.sharedInstance.userTypeIdStr == "1" {
                print("member")
                Singleton.sharedInstance.userTypeStr = "member"
                
                getOnlineMethod(onlieStatus: "1")
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let viewController = storyboard.instantiateViewController(withIdentifier :"Search") as! Search
                let navController = UINavigationController.init(rootViewController: viewController)
                self.window = UIWindow(frame: UIScreen.main.bounds)
                self.window?.rootViewController = navController
                self.window?.makeKeyAndVisible()
                
                
            } else if Singleton.sharedInstance.userTypeIdStr == "3" {
                print("artist")
                Singleton.sharedInstance.userTypeStr = "artist"
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let viewController = storyboard.instantiateViewController(withIdentifier :"ArtistProfile") as! ArtistProfile
                let navController = UINavigationController.init(rootViewController: viewController)
                self.window = UIWindow(frame: UIScreen.main.bounds)
                self.window?.rootViewController = navController
                self.window?.makeKeyAndVisible()
                
                
            } else {
              //  else if Singleton.sharedInstance.userTypeIdStr == "2" {
                    print("place")
                    Singleton.sharedInstance.userTypeStr = "place"
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let viewController = storyboard.instantiateViewController(withIdentifier :"Search") as! Search
                    let navController = UINavigationController.init(rootViewController: viewController)
                    self.window = UIWindow(frame: UIScreen.main.bounds)
                    self.window?.rootViewController = navController
                    self.window?.makeKeyAndVisible()
                    
                //}
            }
        }
        // Stripe Configuration
        // 29 April :-
        // Stripe.setDefaultPublishableKey("pk_test_WCQxXqaQVKCK8jshV5j3NSz3")
        Stripe.setDefaultPublishableKey("pk_test_Lig5XyvimHvh4wPmaQQQ5Ojr")
        
        
        // Override point for customization after application launch.
        return true
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        
        
        completionHandler([UNNotificationPresentationOptions.sound,UNNotificationPresentationOptions.alert])
        
        firebaseMessagesLocations()
        
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        //     if Singleton.sharedInstance.userTypeIdStr == "1" && Singleton.sharedInstance.userTypeIdStr == "3"{
        //
        //         let viewController = storyboard.instantiateViewController(withIdentifier :"MyOrders") as! MyOrders
        //        let viewController1 = storyboard.instantiateViewController(withIdentifier :"Search") as! Search
        //        let navController = UINavigationController.init(rootViewController: viewController1)
        //        self.window = UIWindow(frame: UIScreen.main.bounds)
        //        self.window?.rootViewController = navController
        //        self.window?.makeKeyAndVisible()
        //        navController.pushViewController(viewController, animated: false)
        //
        //
        //    }
        
        
        let userInfo = response.notification.request.content.userInfo as? NSDictionary
        let info = userInfo!["aps"] as? NSDictionary
        //print(info)
        let messageCode = info!["message_code"] as? Int
        let id = info!["resturant_id"] as? String
        //print(id)
        if messageCode == 601{
            
            let viewController = storyboard.instantiateViewController(withIdentifier :"MyOrders") as! MyOrders
            let viewController1 = storyboard.instantiateViewController(withIdentifier :"Search") as! Search
            let navController = UINavigationController.init(rootViewController: viewController1)
            self.window = UIWindow(frame: UIScreen.main.bounds)
            self.window?.rootViewController = navController
            self.window?.makeKeyAndVisible()
            navController.pushViewController(viewController, animated: false)
        }
        else if messageCode == 605{
            
            
            let info = userInfo!["aps"] as? NSDictionary
            let id = info!["resturant_id"] as? String
            let viewController = storyboard.instantiateViewController(withIdentifier :"PlaceProfile") as! PlaceProfile
            if id != nil{
                
                viewController.restaurantId = id!
                
            }
            viewController.fromNavStr = "other"
            let viewController1 = storyboard.instantiateViewController(withIdentifier :"Search") as! Search
            let navController = UINavigationController.init(rootViewController: viewController1)
            self.window = UIWindow(frame: UIScreen.main.bounds)
            self.window?.rootViewController = navController
            self.window?.makeKeyAndVisible()
            navController.pushViewController(viewController, animated: false)
            
        }
            
        else {
            
            let comingFrom = userInfo!["coming_from_about"] as? String
            print("Message ID: \(String(describing: comingFrom))")
            var otherUserId = String()
            if userInfo!["other_user_id"] != nil{
                print("Message ID: \(otherUserId)")
                otherUserId = userInfo!["other_user_id"] as! String
            }
            if otherUserId != "" && comingFrom != ""{
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let viewController = storyboard.instantiateViewController(withIdentifier :"Messages") as! Messages
                viewController.fromPush = true
                viewController.recieverIdStr = otherUserId
                let navController = UINavigationController.init(rootViewController: viewController)
                self.window = UIWindow(frame: UIScreen.main.bounds)
                self.window?.rootViewController = navController
                self.window?.makeKeyAndVisible()
                
                
            }
            
            
        }
        
    }
    // MARK: Notifications
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print(error)
    }
    
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        FIRInstanceID.instanceID().setAPNSToken(deviceToken, type: .sandbox)
        //     print(FIRInstanceID.instanceID().token())
        Singleton.sharedInstance.deviceTokenStr = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
        let data = Singleton.sharedInstance.deviceTokenStr as? String
        UserDefaults.standard.setValue(data, forKey: "deviceToken")
        UserDefaults.standard.synchronize()
        print(Singleton.sharedInstance.deviceTokenStr)
        
    }
    // The callback to handle data message received via FCM for devices running iOS 10 or above.
    
    public func applicationReceivedRemoteMessage(_ remoteMessage: FIRMessagingRemoteMessage) {
        let d : [String : Any] = remoteMessage.appData["notification"] as! [String : Any]
        let body : String = d["body"] as! String
        print(body)
        
    }
    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // Print message ID.
        //        if let messageID = userInfo[gcmMessageIDKey] {
        //            print("Message ID: \(messageID)")
        //        }
        
        // Print full message.
        print(userInfo)
        
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        
        
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        print(userInfo)
        
        
        if application.applicationState == .inactive || application.applicationState == .background{
            print("inactive")
            if let comingFrom = userInfo["coming_from_about"] {
                print("Message ID: \(comingFrom)")
                var otherUserId = String()
                if userInfo["other_user_id"] != nil{
                    print("Message ID: \(otherUserId)")
                    otherUserId = userInfo["other_user_id"] as! String
                }
                
                //  30 May :-
                //                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                //                let viewController = storyboard.instantiateViewController(withIdentifier :"Messages") as! Messages
                //                viewController.fromPush = true
                //                viewController.recieverIdStr = otherUserId
                //                let navController = UINavigationController.init(rootViewController: viewController)
                //                self.window = UIWindow(frame: UIScreen.main.bounds)
                //                self.window?.rootViewController = navController
                //                self.window?.makeKeyAndVisible()
            }
            
        }
        
        // Print message ID.
        if let messageID = userInfo["gcm.message_id"] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    
    
    // MARK: Google SignIn Delegate Methods
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
        // ...
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if (error == nil) {
            // Perform any operations on signed in user here.
            print("\(user.userID)")
        } else {
            print("\(error.localizedDescription)")
        }
        
    }
    // MARK: Open Url Methods
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        //    return SDKApplicationDelegate.shared.application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
        
        
        let googleDidHandle =  GIDSignIn.sharedInstance().handle(url, sourceApplication: sourceApplication, annotation: annotation)
        
        
        let facebookDidHandle = FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
        
        
        return googleDidHandle || facebookDidHandle
        
    }
    
    @available(iOS 9.0, *)
    func application(_ application: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any]) -> Bool {
        //    return SDKApplicationDelegate.shared.application(application, open: url, options: options)
        
        
        let googleDidHandle =  GIDSignIn.sharedInstance().handle(url,sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplicationOpenURLOptionsKey.annotation])
        
        
        let facebookDidHandle = FBSDKApplicationDelegate.sharedInstance().application(application, open: url, options: options)
        
        
        return googleDidHandle || facebookDidHandle
    }
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        if UserDefaults.standard.value(forKey: "auth_token") != nil && Singleton.sharedInstance.userTypeIdStr == "1" {
            getOnlineMethod(onlieStatus: "0")
        }
        
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        //        if application.applicationState == .background || application.applicationState == .inactive {
        //
        //
        //            let content = UNMutableNotificationContent()
        //            content.title = String.localizedUserNotificationString(forKey: "Hello!", arguments: nil)
        //            content.body = String.localizedUserNotificationString(forKey: "You are allmost here", arguments: nil)
        //            content.sound = UNNotificationSound.default() as? UNNotificationSound
        //            // Deliver the notification in five seconds.
        //            let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
        //            let request = UNNotificationRequest(identifier: "FiveSecond", content: content, trigger: trigger)
        //            // Schedule the notification.
        //            let center = UNUserNotificationCenter.current()
        //            center.add(request, withCompletionHandler: {(_ error: Error?) -> Void in
        //                if error == nil {
        //                    print("Local Notification succeeded")
        //
        //                    let alertView = UIAlertView(title: "Notification Received", message: content.title, delegate: nil, cancelButtonTitle: "OK", otherButtonTitles: "")
        //                    alertView.show()
        //                    self.locationManager.startUpdatingLocation()
        //
        //                }
        //                else {
        //                    print("Local Notification failed")
        //                }
        //            })
        //
        //
        //        }
        
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        UIApplication.shared.applicationIconBadgeNumber = 0
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        FBSDKAppEvents.activateApp()
        if UserDefaults.standard.value(forKey: "auth_token") != nil {
            
            firebaseMessagesLocations()
            
            if  Singleton.sharedInstance.userTypeIdStr == "1" {
                getOnlineMethod(onlieStatus: "1")
            }
        }
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        
        
    }
    
    // 13 June :-
    
    func firebaseMessagesLocations(){
        batchCount = 0
        loopCount = 0
        isMesageCount = true
        
        var locAry = [String]()
        FIRDatabase.database().reference().child("users").child(Singleton.sharedInstance.userIdStr).child("conversations").observeSingleEvent(of: .value, with:  { (snapshot) in
            if snapshot.exists() {
                print(")_)_)_)_)_)_)_)_ \(snapshot)")
                
                if let value = snapshot.value as? [String: Any] {
                    for k in value.keys {
                        let loc = value[k] as? [String: String]
                        locAry.append(loc!["location"] ?? "")
                    }
                }
                self.firebaseMessagesCount(locAry: locAry)
            }
        })
    }
    
    func firebaseMessagesCount(locAry: [String]) {
        
        //    print(")_)_)_)_)_)_)_)_ \(locAry)")
        
        
        if loopCount >= locAry.count {
            isMesageCount = false
            // Post notification
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constant().FD_UpdateMessageBedge), object: nil)
            
            return
        }
        
        let ref = FIRDatabase.database().reference().child("users").child("conversations").child(locAry[loopCount]).queryOrdered(byChild: "isRead").queryEqual(toValue: "false")
        ref.observeSingleEvent(of: .value, with:  { (snapshot) in//.observe(.value, with: { snapshot in
            if snapshot.exists() {
                
                print("snapshot:\(snapshot)")
                
                
                for snap in snapshot.children {
                    let  data = (snap as! FIRDataSnapshot).value as! [String: Any]
                    if data["fromId"] as? String != Singleton.sharedInstance.userIdStr {
                        self.batchCount = self.batchCount.advanced(by: 1)
                        
                    }
                }
                
                self.loopCount = self.loopCount.advanced(by: 1)
                self.firebaseMessagesCount(locAry: locAry)
                
            }
            else {
                self.loopCount = self.loopCount.advanced(by: 1)
                self.firebaseMessagesCount(locAry: locAry)
                
            }
        })
    }
    
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
    // MARK: Get Online Method
    
    func getOnlineMethod (onlieStatus: String) {
        
        let json = ["user_id":Singleton.sharedInstance.userIdStr,
                    "auth_token":Singleton.sharedInstance.userTokenStr,
                    "online_status":onlieStatus
            ] as [String : Any]
        
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/users/getonline", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
        }
        
    }
    
    // 25 April :-
    //
    //    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any]) -> Bool {
    //
    //        return true
    //    }
    
    
    //     func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
    
    //    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any]) -> Bool {
    //
    //        //        print("app: \(app)")
    //        print("url: \(url)")
    //        //        print("options: \(options)")
    //
    //        if let sourceApplication = options[UIApplicationOpenURLOptionsKey.sourceApplication] {
    //
    ////            if (String(describing: sourceApplication) == "com.apple.SafariViewService") {
    ////                NotificationCenter.default.post(name: Notification.Name(rawValue: kSafariViewControllerCloseNotification), object: url)
    ////                return true
    ////            }
    //        }
    //
    //        return true
    //    }
    
    
    
    
    
    
    
    
}

