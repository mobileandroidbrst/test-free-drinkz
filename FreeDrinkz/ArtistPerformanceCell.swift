//
//  ArtistHeader.swift
//  FreeDrinkz
//
//  Created by Brst-Pc109 on 28/03/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

import UIKit

class ArtistPerformanceCell: UITableViewCell {

    
    @IBOutlet weak var performanceLbl: UILabel!

    @IBOutlet weak var aboutTxt: UITextView!
    @IBOutlet weak var aboutView: UIView!
    
    @IBOutlet weak var editAboutBtn: UIButton!
    @IBOutlet weak var editPerformanceBtn: UIButton!
    @IBOutlet weak var aboutArtistView: UIView!
    @IBOutlet weak var editAboutImg: UIImageView!
    @IBOutlet weak var editPerformanceImg: UIImageView!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var deleteBtn: UIButton!


    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
