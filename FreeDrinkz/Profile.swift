//
//  Profile.swift
//  FreeDrinkz
//
//  Created by Brst-Pc109 on 25/03/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

import UIKit
import SideMenu
import TwitterKit
import FBSDKCoreKit.FBSDKGraphRequest
import FBSDKLoginKit
import SDWebImage
import Alamofire
import SwiftyJSON
import MapKit
import Firebase
import  UserNotifications
import CoreLocation
import FBSDKShareKit
import AVFoundation
import AVKit


class Profile: UIViewController, UITableViewDelegate, UITableViewDataSource, MKMapViewDelegate, FBSDKSharingDelegate, CLLocationManagerDelegate,UIGestureRecognizerDelegate,AVPlayerViewControllerDelegate {

    @IBOutlet weak var profileTbl: UITableView!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var blockView: UIView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    var fromNavStr = String()
    var nameStr = String()
    var locationStr = String()
    var userImgStr = String()
    var facebookFeedsArray = NSMutableArray()
    var postArray = NSMutableArray()
    var twitterFeedsArray = [JSON]()
    var socialPostsArray = [JSON]()
    var memberProfileId = String()
    var isConnected = Bool()
    var latitudeStr = String()
    var longitudeStr = String()
    var fromViewControllerStr = String()
    var socialFeedsArray = NSMutableArray()
    var otherUserCheckin = Bool()
    var checkInLat = String()
    var checkInLong = String()
    var checkInRest = String()
    var checkInLoc = String()
    var userIdStr = Int()
    var userTokenStr = String()
    var lat = Double()
    var long = Double()
    var latitude = Double()
    var longitude = Double()
    var restroListing = [NSDictionary]()
    var mainRestList = [NSDictionary]()
    var distanceInMetres = CLLocationDistance()
    var restroName = String()
    var checkLocation = false
    var isLocation = String()
    var isOn = Bool()
    var locationManager = CLLocationManager()
    var locationValue = String()
    var addressString = String()
    var currentlatitudeStr = Double()
    var currentlongitudeStr = Double()
    var notificationSetting = String()
    var feedsArray = NSMutableArray()
    var feedStr = String()
    var paginationDict = NSDictionary()
    var videoUrl = NSURL()
    var type = String()
    var showPostVideoBtn = Bool()
    
    // 20 May :-
    var frndUnfrndStr = String()
    
    // 5 June :-
    var userId = String()
    var userType = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(memberProfileId)
        
        
        
        userIdStr = 103
        userTokenStr = "467df55d796ff7ceb13862cd10a33da2"
        lat = 30.6752976
        long = 76.8227178
        if Singleton.sharedInstance.socialLoginType == "" {
            Singleton.sharedInstance.socialLoginType = "1"
            UserDefaults.standard.setValue(Singleton.sharedInstance.socialLoginType, forKey: "socialLoginType")
            
        }

        definesPresentationContext = true
        
        self.navigationController?.isNavigationBarHidden = true
        
        setupSideMenu()
        
        print(fromNavStr)
        
        if fromNavStr == "other" {
            
            if Singleton.sharedInstance.userTypeStr == "member" {
                blockView.isHidden = false
                checkBlockMethod()
            }
            backView.isHidden = false
            searchView.isHidden = true
            memberProfileMethod()
        }
        else {
            memberProfileId = Singleton.sharedInstance.userIdStr
            print(memberProfileId)
            profileMethod()
            twitterMethod()
            fetchFacebookFeeds()
        }
       
        
        
     
        // Mark: Checking whether location is on/off
        let locationStatus = UserDefaults.standard.value(forKey: "locationKey") as? String
        if locationStatus != nil{
            
            self.locationValue = (UserDefaults.standard.value(forKey: "locationKey") as? String)!
            
        }

       
        
    //MARK: Notification Center Work
    NotificationCenter.default.addObserver(self, selector:#selector(self.videoPath(_:)), name:NSNotification.Name(rawValue: "notificationName"), object: nil)
        
        self.LongPressGesture()
        
    }
 
 
 
    func LongPressGesture() {
        //Long Press
        let longPressGesture:UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
        longPressGesture.minimumPressDuration = 0.5
        longPressGesture.delegate = self
        self.profileTbl.addGestureRecognizer(longPressGesture)
    }
    
    func handleLongPress(longPressGesture:UILongPressGestureRecognizer) {
        
        let p = longPressGesture.location(in: self.profileTbl)
        let indexPath = self.profileTbl.indexPathForRow(at: p)
        
        let alert = UIAlertController(title:"", message:"Are you sure you want to delete?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: {(action:UIAlertAction!) in
            
            if indexPath != nil {
                let dict =  self.feedsArray[(indexPath?.row)!] as? NSDictionary
                let id = dict?.value(forKey:"id") as? String
                self.deleteFeed(requestId:id!)
            }
            
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    //MARK: Video Path Method
   func videoPath(_ notification: NSNotification) {
        
     if let videoUrlTest = notification.userInfo?["video"]  {
        
         self.videoUrl = videoUrlTest as! NSURL
        // self.capture(outputFileURL:self.videoUrl)
         let storyboard = UIStoryboard(name: "Main", bundle: nil)
         let vc = storyboard.instantiateViewController(withIdentifier: "SearchRestaurant") as! SearchRestaurant
         vc.videoUrl =  self.videoUrl
         self.navigationController?.pushViewController(vc,animated: true)
        
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = 0.5
        locationManager.delegate = self as CLLocationManagerDelegate
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        
 
        // 22 May :-
      // memberProfileMethod()
        newFeedsSection()
     
    

    }
    
    //MARK: Location manager delegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        
        //  let valueToCompare: Double = 50
        
        DataManager.currentLat = locations.last?.coordinate.latitude
        DataManager.currentLong = locations.last?.coordinate.longitude
        self.locationManager.stopUpdatingLocation()
        if self.locationValue == "1"{
            
            self.getAddressFromLatLon(pdblLatitude: DataManager.currentLat! , withLongitude:  DataManager.currentLong! )
            
        }
        else{
            
            print("Our location is off")
            
            
        }
        
        return
        
        
    }

    // Getting current address from current latitude and longitude
    func getAddressFromLatLon(pdblLatitude: Double, withLongitude pdblLongitude: Double) {
        
        
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = pdblLatitude
        center.longitude = pdblLongitude
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                
                let pm = placemarks! as [CLPlacemark]
                if pm.count > 0 {
                    let pm = placemarks![0]
                    print(pm.country ?? String())
                    print(pm.locality  ?? String())
                    print(pm.subLocality  ?? String())
                    print(pm.postalCode  ?? String())
                    
                  //  self.addressString = ""
                    if pm.subLocality != nil {
                        self.addressString =  self.addressString + pm.subLocality! + ", "
                    }
                    
                    if pm.locality != nil {
                        self.addressString = self.addressString + pm.locality! + ", "
                    }
                    if pm.country != nil {
                        self.addressString = self.addressString + pm.country! + ", "
                    }
                    if pm.postalCode != nil {
                        self.addressString = self.addressString + pm.postalCode! + " "
                    }
                    print(self.addressString)
                   
                }
                
        })
        
    }
    
    
    /**
     Sent to the delegate when the share completes without error or cancellation.
     - Parameter sharer: The FBSDKSharing that completed.
     - Parameter results: The results from the sharer.  This may be nil or empty.
     */
    public func sharer(_ sharer: FBSDKSharing!, didCompleteWithResults results: [AnyHashable : Any]!) {
        print(results)
        
    }
    
    func sharer(_ sharer: FBSDKSharing!, didFailWithError error: Error!)
    {
        print("sharer NSError")
        print(error.localizedDescription)
    }
    
    func sharerDidCancel(_ sharer: FBSDKSharing!)
    {
        print("sharerDidCancel")
    }

//    override func viewWillAppear(_ animated: Bool) {
//    }
    // MARK: Facebook Method
    
    func fetchFacebookFeeds() {
        let params: [AnyHashable: Any] = ["fields": "id, name, story, message, from, picture, object_id, created_time, description"]
     //   let params: [AnyHashable: Any] = ["fields": "id, name, story, message, from, picture, object_id, created_time, description, link, source, properties, type"]

        /* make the API call */
        let request : FBSDKGraphRequest  = FBSDKGraphRequest(graphPath: "/me/feed", parameters: params, httpMethod: "GET")
        
        
        request.start(completionHandler: { (connection, result,  error) in
            // Handle the result

            
            if (result != nil) {
                let dictFromJSON = result as! NSDictionary
                print("result: \(dictFromJSON)")
                self.facebookFeedsArray = (dictFromJSON["data"]! as! NSArray).mutableCopy() as! NSMutableArray
                print("result: \(self.facebookFeedsArray)")
                print("count: \(self.facebookFeedsArray.count)")
                if self.facebookFeedsArray.count>0 {
                    self.saveSocialMediaFeeds()
                } else {
                    if self.fromNavStr == "other" {
                        if self.isConnected == true {
                            self.memberProfileMethod()
                        }
                    } else {
                        if self.isConnected == true {
                            self.profileMethod()
                        }
                    }
                    
                }

                print(self.facebookFeedsArray)
                self.profileTbl.reloadData()
                
                
            }
            
            
            self.indicator.stopAnimating()
            self.indicator.isHidden = true
            
            
        })
        
    }
    
    
    // MARK: Twitter Method
    
    func twitterMethod() {
        let userID = Twitter.sharedInstance().sessionStore.session()?.userID
        let client = TWTRAPIClient(userID: userID)
        let statusesShowEndpoint = "https://api.twitter.com/1.1/statuses/user_timeline.json"
        let params = ["count": "5"]
        var clientError : NSError?
        
        let request = client.urlRequest(withMethod: "GET", url: statusesShowEndpoint, parameters: params, error: &clientError)
        
        client.sendTwitterRequest(request) { (response, data, connectionError) -> Void in
            if connectionError != nil {

            } else {
                let jsonVal = JSON(data: data! )
                self.twitterFeedsArray = jsonVal.array!
                print(self.twitterFeedsArray)
                if self.twitterFeedsArray.count>0 {
                    self.saveTwitterFeeds()
                }
                
                print(self.twitterFeedsArray)
                
                self.profileTbl.reloadData()
            }
            
            self.indicator.stopAnimating()
            self.indicator.isHidden = true
            
        }
        
    }
    
    
    // MARK: Social Media Feeds Method
    
    
    func saveTwitterFeeds() {
        
        
        let array = NSMutableArray()
        
        for item in twitterFeedsArray {

            let stringg = item["text"].string!
            let result = stringg.replacingOccurrences(of: "[^\\x00-\\x7F]", with: "", options: NSString.CompareOptions.regularExpression, range: nil)
            print(result)
            
            
            let nameStrr = item["user"]["name"].string!
            let nameResult = nameStrr.replacingOccurrences(of: "[^\\x00-\\x7F]", with: "", options: NSString.CompareOptions.regularExpression, range: nil)


            let image = item["user"]["profile_image_url_https"].string!
            let matchInfo: NSDictionary = ["id": String(describing: item["id"].int!), "created_time": item["created_at"].string!, "description": result, "from": ["name": nameResult, "id": image]]
            
            
            array.add(matchInfo)
            
        }
        
        
        //    let array1 = Array(array.prefix(1))
        //   print(array1.count)
        
        
        let json = ["user_id":Singleton.sharedInstance.userIdStr,
                    "auth_token":Singleton.sharedInstance.userTokenStr,
                    "feeds_type":"2",
                    "data": array
            ] as NSDictionary
        
        print(json)
        let postData: Data? = try? JSONSerialization.data(withJSONObject: json, options: [])
        let request = NSMutableURLRequest(url: NSURL(string: "http://beta.brstdev.com/freedrinkz/api/web/v1/users/savefeeds")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.httpBody = postData! as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {

            } else {
                let dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                if dataString != nil {
                    let json = JSON(data: data! )
                    if let descriptionStr = json["description"].string {
                        print(descriptionStr)
                        if descriptionStr == "Feed added Successfully" {
                            print("added")
                            
                            if self.fromNavStr == "other" {
                                if self.isConnected == true {
                                    self.memberProfileMethod()
                                } else {
                                    self.memberProfileMethod()
                                }
                            } else {
                                if self.isConnected == true {
                                    self.profileMethod()
                                } else {
                                    self.profileMethod()
                                }
                            }

                            

                            
                        }
                        //Now you got your value
                    }
                }
                
                
                
            }
        })
        
        dataTask.resume()
        
    }
    
    
    
    
    
    func saveSocialMediaFeeds() {
        
 
        for (index, element) in facebookFeedsArray.enumerated() {

            var dict = element as!  [String: Any]

            if dict["description"] != nil {
                let stringg = dict["description"]! as! String
                let result = stringg.replacingOccurrences(of: "[^\\x00-\\x7F]", with: "", options: NSString.CompareOptions.regularExpression, range: nil)
                dict["description"] = result
            }
            if dict["story"] != nil {
                let stringg = dict["story"]! as! String
                let result = stringg.replacingOccurrences(of: "[^\\x00-\\x7F]", with: "", options: NSString.CompareOptions.regularExpression, range: nil)
                dict["story"] = result
            }

            if dict["message"] != nil {
                let stringg = dict["message"]! as! String
                let result = stringg.replacingOccurrences(of: "[^\\x00-\\x7F]", with: "", options: NSString.CompareOptions.regularExpression, range: nil)
                dict["message"] = result
            }
            facebookFeedsArray.replaceObject(at: index, with: dict)

        }
        
        
        let json = ["user_id":Singleton.sharedInstance.userIdStr,
                    "auth_token":Singleton.sharedInstance.userTokenStr,
                    "feeds_type":"1",
                    "data": facebookFeedsArray
            ] as NSDictionary
        
        print(json)
        
        let postData: Data? = try? JSONSerialization.data(withJSONObject: json, options: [])
        
        
        let request = NSMutableURLRequest(url: NSURL(string: "http://beta.brstdev.com/freedrinkz/api/web/v1/users/savefeeds")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.httpBody = postData! as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {

            } else {
                
                let dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                
                if dataString != nil {
                    let json = JSON(data: data! )
                    if let descriptionStr = json["description"].string {
                        print(descriptionStr)
                        if descriptionStr == "Feed added Successfully" {
                            print("added")
                            if self.fromNavStr == "other" {
                                if self.isConnected == true {
                                    self.memberProfileMethod()
                                } else {
                                    self.memberProfileMethod()
                                }
                            } else {
                                if self.isConnected == true {
                                    self.profileMethod()
                                } else {
                                    self.profileMethod()
                                }
                            }

                        }
                        //Now you got your value
                    }
                }
            }
        })
        
        dataTask.resume()
        
    }

    fileprivate func setupSideMenu() {
        // Define the menus
        SideMenuManager.menuLeftNavigationController = storyboard!.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as? UISideMenuNavigationController
        
        // Enable gestures. The left and/or right menus must be set up above for these to work.
        // Note that these continue to work on the Navigation Controller independent of the View Controller it displays!
        SideMenuManager.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
        SideMenuManager.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
        
        
        // Set up a cool background image for demo purposes
        //     SideMenuManager.menuAnimationBackgroundColor = UIColor(patternImage: UIImage(named: "background")!)
        SideMenuManager.menuAnimationBackgroundColor = UIColor(white: 1, alpha: 0.0)
    }
    
    
    
    // MARK: CheckBlockMethod Method
    
    func checkBlockMethod () {
        
        let json = ["user_id":Singleton.sharedInstance.userIdStr,
                    "auth_token":Singleton.sharedInstance.userTokenStr,
                    "block_user_id":memberProfileId
            ] as [String : Any]
        print(json)
        
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/members/checkblockuser", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    let json = JSON(data: response.data! )
                    if let descriptionStr = json["description"].string {
                        print(descriptionStr)
                        if descriptionStr == "Wrong auth_token" {
                            
                            
                            UserDefaults.standard.setValue(nil, forKey: "auth_token")
                            UserDefaults.standard.setValue(nil, forKey: "user_id")
                            UserDefaults.standard.setValue(nil, forKey: "user_type")
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "Login") as! Login
                            vc.isExpired = true
                            self.navigationController?.pushViewController(vc,animated: true)
                            
                        } else if descriptionStr == "User is not Blocked" {

                            print("User is not Blocked")
                            
                            if (json["data"]["check_detail"].dictionary != nil) {
                                print("user is checked-in")
                                self.otherUserCheckin = true
                                
                                self.checkInLat = json["data"]["check_detail"]["latitude"].string!
                                self.checkInLong = json["data"]["check_detail"]["longitude"].string!
                                self.checkInRest = json["data"]["check_detail"]["resturant_name"].string!
                                
                                if json["data"]["check_detail"]["address"].string != nil {
                                    self.checkInLoc = json["data"]["check_detail"]["address"].string!
                                }
                                
                                if json["data"]["check_detail"]["city"].string != nil {

                                    self.checkInLoc = "\(self.checkInLoc), \(json["data"]["check_detail"]["city"].string!)"
                                    
                                }
                                if json["data"]["check_detail"]["state"].string != nil {
                                    self.checkInLoc = "\(self.checkInLoc), \(json["data"]["check_detail"]["state"].string!)"

                                }

                                
                                self.profileTbl.reloadData()

                            }
                            
                            
                        } else if descriptionStr == "User is Blocked" {
                            
                            print("User is Blocked")
                            
                            let alert = UIAlertController(title: "", message: "User is not available for your profile.", preferredStyle: UIAlertControllerStyle.alert)
                            let okAction = UIAlertAction(title: "Ok", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                                
                                if !self.fromViewControllerStr.isEmpty {
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let vc = storyboard.instantiateViewController(withIdentifier: self.fromViewControllerStr)
                                    self.navigationController?.pushViewController(vc,animated: true)
                                } else {
                                    _=self.navigationController?.popViewController(animated: true)
                                }
                                
                            })
                            alert.addAction(okAction)
                            self.present(alert, animated: true)

                        }

                        //Now you got your value
                    }
                    
                } else if((response.error) != nil){
                    
                }
        }
        
    }
    // MARK: Get Profile Method
    func profileMethod() {
        
        let json = ["user_id":Singleton.sharedInstance.userIdStr,
                    "auth_token":Singleton.sharedInstance.userTokenStr
            ] as [String : Any]
        
    Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/users/getprofile", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    let json = JSON(data: response.data! )
                    if let descriptionStr = json["description"].string {
                        print(descriptionStr)
                        if descriptionStr == "Wrong auth_token" {
                            
                            
                            UserDefaults.standard.setValue(nil, forKey: "auth_token")
                            UserDefaults.standard.setValue(nil, forKey: "user_id")
                            UserDefaults.standard.setValue(nil, forKey: "user_type")
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "Login") as! Login
                            vc.isExpired = true
                            self.navigationController?.pushViewController(vc,animated: true)
                            
                        } else if descriptionStr == "User Profile" {
                            
                            if json["data"]["gender"].string != nil {
                                Singleton.sharedInstance.genderStr =  String(describing: json["data"]["gender"].string!)
                            } else {
                                Singleton.sharedInstance.genderStr =  ""
                            }
                            
                           
                            if json["data"]["address"].string != nil {
                                
                                print(String(describing: json["data"]["address"].string!))
                                
                                // 6 June :-
                                Singleton.sharedInstance.addressStr =  String(describing: json["data"]["address"].string!)
                               //  6 June :-
                                 self.locationStr = Singleton.sharedInstance.addressStr
                                
                            } else {
                                Singleton.sharedInstance.addressStr =  ""
                                self.locationStr = ""
                                
                            }
                            
                            if json["data"]["city"].string != nil {
                                Singleton.sharedInstance.cityStr =  String(describing: json["data"]["city"].string!)
                               
                               
                              self.locationStr =  "\(String(describing: json["data"]["city"].string!))"
                                print(String(describing: json["data"]["city"].string!))
                                
                            } else {
                                Singleton.sharedInstance.cityStr =  ""
                            }
                            
                            
                            if json["data"]["state"].string != nil {
                                Singleton.sharedInstance.stateStr =  String(describing: json["data"]["state"].string!)
                                
                              self.locationStr =  "\(self.locationStr), \(String(describing: json["data"]["state"].string!))"
                                
                            } else {
                                Singleton.sharedInstance.stateStr =  ""
                            }
                            
                            if json["data"]["country"].string != nil {
                                Singleton.sharedInstance.countryStr =  String(describing: json["data"]["country"].string!)
                                
                             self.locationStr =  "\(self.locationStr), \(String(describing: json["data"]["country"].string!))"
                                
                                
                            } else {
                                Singleton.sharedInstance.countryStr =  ""
                            }
                            
                            
                            if json["data"]["zip_code"].string != nil {
                                Singleton.sharedInstance.zipcodeStr =  String(describing: json["data"]["zip_code"].string!)
                                
                                self.locationStr =  "\(self.locationStr), \(String(describing: json["data"]["zip_code"].string!))"
                                
                            } else {
                                Singleton.sharedInstance.zipcodeStr =  ""
                            }
                            
                            
                            if json["data"]["profile_pic_url"].string != nil {
                                Singleton.sharedInstance.userImgStr =  String(describing: json["data"]["profile_pic_url"].string!)
                                Singleton.sharedInstance.userImgStr =  Singleton.sharedInstance.userImgStr.replacingOccurrences(of: "_normal", with: "")
                                self.userImgStr = Singleton.sharedInstance.userImgStr
                                
                            } else {
                                Singleton.sharedInstance.userImgStr =  ""
                                self.userImgStr = ""
                                
                            }
                            if json["data"]["email"].string != nil {
                                Singleton.sharedInstance.emailStr =  String(describing: json["data"]["email"].string!)
                            } else {
                                Singleton.sharedInstance.emailStr =  ""
                            }
                            if json["data"]["age"].int != nil {
                                Singleton.sharedInstance.ageStr =  String(describing: json["data"]["age"].int!)
                            } else {
                                Singleton.sharedInstance.ageStr =  ""
                            }
                            
                            
                            if json["data"]["firstname"].string != nil {
                                Singleton.sharedInstance.firstNameStr =  String(describing: json["data"]["firstname"].string!)
                            } else {
                                Singleton.sharedInstance.firstNameStr =  ""
                            }
                            
                            
                            if json["data"]["lastname"].string != nil {
                                Singleton.sharedInstance.lastNameStr =  String(describing: json["data"]["lastname"].string!)
                            } else {
                                Singleton.sharedInstance.lastNameStr =  ""
                            }
                            
                            
                            self.nameStr = "\(Singleton.sharedInstance.firstNameStr) \(Singleton.sharedInstance.lastNameStr)"
                            
                            if json["data"]["latitude"].string != nil {
                                Singleton.sharedInstance.latitudeStr =  String(describing: json["data"]["latitude"].string!)
                                UserDefaults.standard.setValue(Singleton.sharedInstance.latitudeStr, forKey: "latitude")
                                self.latitudeStr = Singleton.sharedInstance.latitudeStr
                            } else {
                                Singleton.sharedInstance.latitudeStr =  "0.0"
                                self.latitudeStr = "0.0"
                            }
                            
                            if json["data"]["longitude"].string != nil {
                                Singleton.sharedInstance.longitudeStr =  String(describing: json["data"]["longitude"].string!)
                                UserDefaults.standard.setValue(Singleton.sharedInstance.longitudeStr, forKey: "longitude")
                                self.longitudeStr = Singleton.sharedInstance.longitudeStr
                                
                            } else {
                                Singleton.sharedInstance.longitudeStr =  "0.0"
                                self.longitudeStr = "0.0"
                            }
                            
                            if json["data"]["check_in_status"].int != nil {
                                let status =  json["data"]["check_in_status"].int!
                                if status == 0 {
                                    Singleton.sharedInstance.checkInStatus =  false
                                } else {
                                    Singleton.sharedInstance.checkInStatus =  true
                                }
                            } else {
                                
                                Singleton.sharedInstance.checkInStatus =  false
                            }
                            
                            if json["data"]["resturant_id"].int != nil {
                                Singleton.sharedInstance.checkInRestId =  String(describing: json["data"]["resturant_id"].int!)
                                
                                
                            } else {
                                Singleton.sharedInstance.checkInRestId =  ""
                                
                            }
                            
                            
                            
                            var token = String()
                            if FIRInstanceID.instanceID().token() != nil {
                                token = FIRInstanceID.instanceID().token()!
                            } else {
                                token = ""
                            }
                            let values = ["name": self.nameStr as String, "userid": Singleton.sharedInstance.userIdStr as String, "fcm_token": token, "profile_pic_url": Singleton.sharedInstance.userImgStr] as [String : Any]
                            FIRDatabase.database().reference().child("users").child(Singleton.sharedInstance.userIdStr).child("credentials").updateChildValues(values, withCompletionBlock: { (errr, _) in
                                if errr == nil {
                                    
                                }
                            })
                            
                            
                            
                            self.socialPostsArray = json["data"]["userfeeds"].array!
                            print(self.socialPostsArray.count)
                            
                            
                            var array1 = NSMutableArray(array: json["data"]["userfeeds"].arrayObject!)
                            
                            let predicate = NSPredicate(format: "feeds_type == %d", Int(Singleton.sharedInstance.socialLoginType)!)
                            array1 = NSMutableArray(array: array1.filter { predicate.evaluate(with: $0) } )
                            
                            let array2  = NSMutableArray()
                            for (_, element) in array1.enumerated() {
                                
                                var dict = element as!  [String: Any]
                                
                                
                                if dict["feeds_type"]  as! Int == 1 {
                                    
                                    
                                    let dateStr = dict["created_time"]!
                                    print(dateStr)
                                    let dateFormatter = DateFormatter()
                                    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZ" //twitter
                                    dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
                                    let date = dateFormatter.date(from: dateStr as! String)
                                    print(date!)
                                    dict["created_time"] = date!
                                    
                                    
                                } else {
                                    
                                    
                                    let dateStr = dict["created_time"]!
                                    print(dateStr)
                                    let dateFormatter = DateFormatter()
                                    dateFormatter.dateFormat = "eee MMM dd HH:mm:ss ZZZZ yyyy" //twitter
                                    dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
                                    let date = dateFormatter.date(from: dateStr as! String)
                                    print(date!)
                                    dict["created_time"] = date!
                                    
                                }
                                
                                array2.add(dict)
                                
                            }
                            
                            
                            
                            let descriptor: NSSortDescriptor = NSSortDescriptor(key: "created_time", ascending: false)
                            let sortedarray = array2.sortedArray(using: [descriptor])
                            self.socialFeedsArray = NSMutableArray(array: Array(sortedarray.prefix(5))) //Will convert [Any] to NSArray
                            print(self.socialFeedsArray)
                            
                            
                            
                            self.profileTbl.reloadData()
                        }
                        //Now you got your value
                    }
                    self.indicator.stopAnimating()
                    self.indicator.isHidden = true
                    
                } else if((response.error) != nil){
                    
                    
                    self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                    self.indicator.stopAnimating()
                    self.indicator.isHidden = true
                    
                }
        }
        
    }
    // MARK: Get Member Profile Method
    func memberProfileMethod(){
        
        let json = ["user_id":Singleton.sharedInstance.userIdStr,
                    "auth_token":Singleton.sharedInstance.userTokenStr,
                    "other_user_id":memberProfileId
            ] as [String : Any]
        print(json)
        
        
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/members/artistprofile", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    let json = JSON(data: response.data! )
                    if let descriptionStr = json["description"].string {
                        print(descriptionStr)
                        if descriptionStr == "Wrong auth token" {
                            
                            
                            UserDefaults.standard.setValue(nil, forKey: "auth_token")
                            UserDefaults.standard.setValue(nil, forKey: "user_id")
                            UserDefaults.standard.setValue(nil, forKey: "user_type")
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "Login") as! Login
                            vc.isExpired = true
                            self.navigationController?.pushViewController(vc,animated: true)
                            
                        }  else if descriptionStr == "Normal profile with feeds list" {
                            
                            
                            if self.fromNavStr == "other" {
                                
                                // 20 May :- (Checking is that particular Frnd is user's frnd or not)
                                if json["data"]["User_data"][0]["Friend_Unfriend"] == 1
                                {
                                 self.frndUnfrndStr = "1"
                                }
                                else {
                                   self.frndUnfrndStr = "0"
                                }
                               
                                
                               if json["data"]["User_data"][0]["profile_pic_url"].string != nil {
                                    self.userImgStr =  String(describing: json["data"]["User_data"][0]["profile_pic_url"].string!)
                                    self.userImgStr =  self.userImgStr.replacingOccurrences(of: "_normal", with: "")

                                    
                                } else {
                                    self.userImgStr = ""
                                    
                                }
                                
                                if json["data"]["User_data"][0]["notification_setting"].string != nil {
                                    self.notificationSetting = String(describing: json["data"]["User_data"][0]["notification_setting"].string!)
                                }
                                
                                
                                if json["data"]["User_data"][0]["firstname"].string != nil {
                                    self.nameStr =  String(describing: json["data"]["User_data"][0]["firstname"].string!)
                                } else {
                                    self.nameStr =  ""
                                }
                                
                                
                                if json["data"]["User_data"][0]["lastname"].string != nil {
                                    self.nameStr = "\(self.nameStr) \(String(describing: json["data"]["User_data"][0]["lastname"].string!))"
                                }
                                
                                 // 6 June :-
//                                if json["data"]["User_data"][0]["address"].string != nil {
//
//                                    self.locationStr =  String(describing: json["data"]["User_data"][0]["address"].string!)
//                                } else {
//                                    self.locationStr =  ""
//                                }
//
                                
                                
                                if json["data"]["User_data"][0]["city"].string != nil {
                                    // 6 June :-
                                   self.locationStr =  "\(String(describing: json["data"]["User_data"][0]["city"].string!))" //\(self.locationStr),
                                    
                                }
                                
                                if json["data"]["User_data"][0]["state"].string != nil {
                                   
                                     self.locationStr =  "\(self.locationStr), \(String(describing: json["data"]["User_data"][0]["state"].string!))"
                                }
                                
                                if json["data"]["User_data"][0]["zip_code"].string != nil {
                                    self.locationStr =  "\(self.locationStr), \(String(describing: json["data"]["User_data"][0]["zip_code"].string!))"
                                }
                           
                                
                                if json["data"]["User_data"][0]["latitude"].string != nil {
                                    self.latitudeStr =  String(describing: json["data"]["User_data"][0]["latitude"].string!)
                                } else {
                                    self.latitudeStr =  "0.0"
                                }
                                
                                if json["data"]["User_data"][0]["longitude"].string != nil {
                                    self.longitudeStr =  String(describing: json["data"]["User_data"][0]["longitude"].string!)
                                } else {
                                    self.longitudeStr =  "0.0"
                                }
                            } else {
                                if json["data"]["User_data"][0]["gender"].string != nil {
                                    Singleton.sharedInstance.genderStr =  String(describing: json["data"]["User_data"][0]["gender"].string!)
                                } else {
                                    Singleton.sharedInstance.genderStr =  ""
                                }
                                
                                
                                if json["data"]["User_data"][0]["address"].string != nil {
                                    // 6 June :-
                                    Singleton.sharedInstance.addressStr =  String(describing: json["data"]["User_data"][0]["address"].string!)
                                   
                                    self.locationStr = Singleton.sharedInstance.addressStr
                                    
                                } else {
                                    Singleton.sharedInstance.addressStr =  ""
                                    self.locationStr = ""
                                    
                                }
                                
                                if json["data"]["User_data"][0]["city"].string != nil {
                                    // 6 June :-
                                    Singleton.sharedInstance.cityStr =  String(describing: json["data"]["User_data"][0]["city"].string!)
                                    self.locationStr =  "\(self.locationStr), \(String(describing: json["data"]["User_data"][0]["city"].string!))"
                                    
                                    
                                } else {
                                    Singleton.sharedInstance.cityStr =  ""
                                }
                                
                                
                                if json["data"]["User_data"][0]["state"].string != nil {
                                    // 6 June :-
                                    Singleton.sharedInstance.stateStr =  String(describing: json["data"]["User_data"][0]["state"].string!)
                                    self.locationStr =  "\(self.locationStr), \(String(describing: json["data"]["User_data"][0]["state"].string!))"
                                    
                                } else {
                                    Singleton.sharedInstance.stateStr =  ""
                                }
                                
                                
                                
                                if json["data"]["User_data"][0]["zip_code"].string != nil {
                                    // 6 June :-
                                    Singleton.sharedInstance.zipcodeStr =  String(describing: json["data"]["User_data"][0]["zip_code"].string!)
                                    self.locationStr =  "\(self.locationStr), \(String(describing: json["data"]["User_data"][0]["zip_code"].string!))"
                                    
                                } else {
                                    Singleton.sharedInstance.zipcodeStr =  ""
                                }
                                
                                
                                if json["data"]["User_data"][0]["profile_pic_url"].string != nil {
                                    Singleton.sharedInstance.userImgStr =  String(describing: json["data"]["User_data"][0]["profile_pic_url"].string!)
                                    self.userImgStr = Singleton.sharedInstance.userImgStr
                                    
                                } else {
                                    Singleton.sharedInstance.userImgStr =  ""
                                    self.userImgStr = ""
                                    
                                }
                                if json["data"]["User_data"][0]["email"].string != nil {
                                    Singleton.sharedInstance.emailStr =  String(describing: json["data"]["User_data"][0]["email"].string!)
                                } else {
                                    Singleton.sharedInstance.emailStr =  ""
                                }
                                if json["data"]["User_data"][0]["age"].int != nil {
                                    Singleton.sharedInstance.ageStr =  String(describing: json["data"]["User_data"][0]["age"].int!)
                                } else {
                                    Singleton.sharedInstance.ageStr =  ""
                                }
                                
                                
                                if json["data"]["User_data"][0]["firstname"].string != nil {
                                    Singleton.sharedInstance.firstNameStr =  String(describing: json["data"]["User_data"][0]["firstname"].string!)
                                } else {
                                    Singleton.sharedInstance.firstNameStr =  ""
                                }
                                
                                
                                if json["data"]["User_data"][0]["lastname"].string != nil {
                                    Singleton.sharedInstance.lastNameStr =  String(describing: json["data"]["User_data"][0]["lastname"].string!)
                                } else {
                                    Singleton.sharedInstance.lastNameStr =  ""
                                }
                                
                                
                                self.nameStr = "\(Singleton.sharedInstance.firstNameStr) \(Singleton.sharedInstance.lastNameStr)"
                                
                                if json["data"]["User_data"][0]["latitude"].string != nil {
                                    Singleton.sharedInstance.latitudeStr =  String(describing: json["data"]["User_data"][0]["latitude"].string!)
                                    self.latitudeStr = Singleton.sharedInstance.latitudeStr
                                } else {
                                    Singleton.sharedInstance.latitudeStr =  "0.0"
                                    self.latitudeStr = "0.0"
                                }
                                
                                if json["data"]["User_data"][0]["longitude"].string != nil {
                                    Singleton.sharedInstance.longitudeStr =  String(describing: json["data"]["User_data"][0]["longitude"].string!)
                                    self.longitudeStr = Singleton.sharedInstance.longitudeStr
                                    
                                } else {
                                    Singleton.sharedInstance.longitudeStr =  "0.0"
                                    self.longitudeStr = "0.0"
                                }
                                

                                var token = String()
                                if FIRInstanceID.instanceID().token() != nil {
                                    token = FIRInstanceID.instanceID().token()!
                                } else {
                                    token = ""
                                }
                                let values = ["name": self.nameStr as String, "userid": Singleton.sharedInstance.userIdStr as String, "fcm_token": token, "profile_pic_url": Singleton.sharedInstance.userImgStr] as [String : Any]
                                FIRDatabase.database().reference().child("users").child(Singleton.sharedInstance.userIdStr).child("credentials").updateChildValues(values, withCompletionBlock: { (errr, _) in
                                    if errr == nil {
                                        
                                    }
                                })
                                
                                self.profileTbl.reloadData()
                            }

                            
                            self.profileTbl.reloadData()
                            
                        }
                        
                        self.indicator.stopAnimating()
                        self.indicator.isHidden = true
                        
                        //Now you got your value
                    }
                } else if((response.error) != nil){
                    
                    self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                    self.indicator.stopAnimating()
                    self.indicator.isHidden = true
                    
                }
                
        }
        
    }

    
    //MARK: New Feeds Section Api Implementation
    func newFeedsSection(){
      
        feedStr = "Feeds"
        let userId = Singleton.sharedInstance.userIdStr
        let authToken = Singleton.sharedInstance.userTokenStr
        DispatchQueue.main.async {
            
            //self.activityIndicator.isHidden = false
            // self.activityIndicator.startAnimating()
            
        }
        var parameterDict = JSONDictionary()
        parameterDict["user_id"] = userId
        parameterDict["auth_token"] = authToken
        parameterDict["other_user_id"] = ""
        print(parameterDict)
        ApiExtension.sharedInstance.getFeeds(parameter:parameterDict, completionHandler:{(result)in
            
            let data = result.value(forKey:"data") as? NSDictionary
            let statusCode = data?.value(forKey:"Success_code") as? String
            if  statusCode == "201"{
                
              let array = data?.value(forKey:"feed_list_share") as? NSArray
              self.feedsArray = (array?.mutableCopy() as? NSMutableArray)!
              self.paginationDict = (data?.value(forKey:"meta") as? NSDictionary)!
              self.profileTbl.reloadData()
            
            }
            
        })
        
    }
    // MARK: AlertView
    func alertViewMethod(titleStr:String, messageStr:String) {
        let alert = UIAlertController(title: titleStr, message: messageStr, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    // MARK: UITableview Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if fromNavStr == "other" {
            return socialFeedsArray.count
        }
        if feedStr == "Feeds"{
            
         return feedsArray.count
        
        }
        return socialFeedsArray.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
       if feedStr == "Feeds"{
       
      
        let dict = feedsArray[indexPath.row] as? NSDictionary
        print(dict!)
        self.type = (dict?.value(forKey:"type") as? String)!
        
        // Case in which image is posted on feeds screen :-
        if self.type == "image"{
           
            let cellOne:FeedCellImage = profileTbl.dequeueReusableCell(withIdentifier: "FeedCellImage") as! FeedCellImage
            cellOne.selectionStyle = .none;

            // 5 June (For Sending user-id to next screen on feedOtherImgBtn) :-
             // print(dict?.value(forKey:"user_id"))
            // print(dict?.value(forKey:"other_user_type"))
            self.userId = dict?.value(forKey:"other_user") as! String
            self.userType = dict?.value(forKey:"other_user_type") as! String
            
            let otherUserProfilePic = dict?.value(forKey:"img_url") as? String
            let otherUserName = dict?.value(forKey:"other_user_name") as? String
            let userPic = dict?.value(forKey:"other_user_profile_pic") as? String
            
            // 10 June :-
         // cellOne.feedImage.sd_setImage(with:URL(string:otherUserProfilePic!), placeholderImage:UIImage(named:""))
            let urlStr : String = (otherUserProfilePic!)
            let urlFormattedStr = urlStr.replacingOccurrences(of: " ", with: "%20")
            cellOne.feedImage.sd_setImage(with: URL(string:urlFormattedStr), placeholderImage: UIImage(named: ""))
            
            
            
            cellOne.lblFeed.text = "Received free drink from \(String(describing: otherUserName!)) using Free Drinkz App"
            
            // 10 June :-
           // cellOne.feedOtherImg.sd_setImage(with:URL(string:userPic!), placeholderImage:UIImage(named:""))
            let urlStr1 : String = (userPic!)
            let urlFormattedStr1 = urlStr1.replacingOccurrences(of: " ", with: "%20")
            cellOne.feedOtherImg.sd_setImage(with: URL(string:urlFormattedStr1), placeholderImage: UIImage(named: ""))
            
            
            
            
            
            cellOne.lblInfo.text = otherUserName
            cellOne.feedImage.layer.cornerRadius = 2.0
            cellOne.feedImage.clipsToBounds = true
            cellOne.feedOtherImg.layer.cornerRadius = cellOne.feedOtherImg.frame.height/2
            cellOne.feedOtherImg.clipsToBounds = true
            cellOne.clipsToBounds = true
            cellOne.lblInfo.text =  "From: \(String(describing: otherUserName!))"
            
            // 5 June :-
             cellOne.feedOtherImgBtn.addTarget(self, action: #selector(MoveToSender(_:)), for: .touchUpInside)
            
            // 15 May :- Placing Time stamp value in Table view's cell.
              print((dict!.value(forKey:"timestamp") as! String))
              if let timestamp = dict!["timestamp"] as? String {
                //  cell.dateTimeLbl.text = timestamp
                
                let timestampDouble = Double(timestamp)
                let date = Date(timeIntervalSince1970: timestampDouble!)
                let dateFormatter = DateFormatter()
             //   dateFormatter.timeZone = TimeZone(abbreviation: "UTC") //Set timezone that you want
                dateFormatter.locale = NSLocale.current
                // dateFormatter.dateFormat = "yyyy-MM-dd HH:mm" //Specify your format that you want
                dateFormatter.dateFormat = "dd-MMMM-yyyy hh:mm a"
                
                let strDate = dateFormatter.string(from: date)
                
                cellOne.lblDate.text =  "Date:" + strDate
                
           }
            
       
            return cellOne

      }
            // Case in which Video is posted on feeds Screen:-
        else{
            
            let cellTwo:FeedCellVideo = profileTbl.dequeueReusableCell(withIdentifier: "FeedCellVideo") as! FeedCellVideo
            let otherUserProfilePic = dict?.value(forKey:"thumb_url") as? String
            
            // 10 June :-
            //cellTwo.imgVideo.sd_setImage(with:URL(string:otherUserProfilePic!), placeholderImage:UIImage(named:""))
            let urlStr2 : String = (otherUserProfilePic!)
            let urlFormattedStr2 = urlStr2.replacingOccurrences(of: " ", with: "%20")
            cellTwo.imgVideo.sd_setImage(with: URL(string:urlFormattedStr2), placeholderImage: UIImage(named: ""))
            
            
            
            cellTwo.imgVideo.layer.cornerRadius = 2.0
            cellTwo.imgVideo.clipsToBounds = true
            cellTwo.lblAddress.text = "Address:\(String(describing: dict?.value(forKey:"location") as! String))"
            cellTwo.lblNote.text = "Note:\(String(describing: dict?.value(forKey:"notes") as! String))"
            
            // 15 May :-
            cellTwo.lblRestroName.text = "Restuarant Name:\(String(describing: dict?.value(forKey:"other_user_name") as! String))"
           
            // 17 May :- Placing Time stamp value in Table view's cell.
            print((dict!.value(forKey:"timestamp") as! String))
            if let timestamp = dict!["timestamp"] as? String {
                //  cell.dateTimeLbl.text = timestamp
                
                let timestampDouble = Double(timestamp)
                let date = Date(timeIntervalSince1970: timestampDouble!)
                let dateFormatter = DateFormatter()
              //  dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone that you want
                dateFormatter.locale = NSLocale.current
                // dateFormatter.dateFormat = "yyyy-MM-dd HH:mm" //Specify your format that you want
                dateFormatter.dateFormat = "dd-MMMM-yyyy hh:mm a"
                
                let strDate = dateFormatter.string(from: date)
                
                cellTwo.LblDate.text =  "Date:" + strDate
                
            }
            
            cellTwo.btnVideoClick.isEnabled = true
            cellTwo.btnVideoClick.tag = indexPath.row
            cellTwo.btnVideoClick.addTarget(self, action: #selector(showVideo), for: UIControlEvents.touchUpInside)
            cellTwo.selectionStyle = .none;
            return cellTwo

        }
            
      
      }
        
        else{
            
            let cell:PostCell = profileTbl.dequeueReusableCell(withIdentifier: "PostCell") as! PostCell
        cell.selectionStyle = .none;
            let dict = socialFeedsArray[indexPath.row] as! NSDictionary
            if dict["feeds_type"]  as! Int == 1 {
                cell.postTypeImg.image = UIImage(named: "fbgray")
                cell.postImg.sd_setImage(with: URL(string: "http://graph.facebook.com/\(dict["from_id"]!)/picture?type=large"), completed: {(image,  error,  cacheType, imageURL) -> Void in
                    
                })
            } else {
                
                cell.postTypeImg.image = UIImage(named: "twittergray")
                
                
                var image = dict["from_id"]  as! String
                image = image.replacingOccurrences(of: "_normal", with: "")
                
                cell.postImg.sd_setImage(with: URL(string: image), completed: {(image,  error,  cacheType, imageURL) -> Void in
                })
                
            }
            
            let date = dict["created_time"]! as! Date
            let dateString = timeAgoSinceDate(Date(), currentDate: date, numericDates: true)
            print(dateString)
            cell.timeLbl.text = dateString
            cell.nameLbl.text = dict["name"] as? String
        
            let text =  cell.nameLbl.text
            print(text)
            UserDefaults.standard.set(text, forKey:"textNameKey")
            UserDefaults.standard.synchronize()
            if dict["description"] as! String != "" {
                cell.descriptionLbl.text = dict["description"]! as? String
            } else if dict["message"] as! String != "" {
                cell.descriptionLbl.text = dict["message"] as? String
            } else if dict["name"]  as! String != "" {
                cell.descriptionLbl.text = dict["name"] as? String
                if dict["story"]  as! String != "" {
                    cell.descriptionLbl.text = "\(cell.descriptionLbl.text!)- \(dict["story"]!)"
                }
            } else if dict["story"]  as! String != "" {
                cell.descriptionLbl.text = dict["story"] as? String
            } else {
                cell.descriptionLbl.text = ""
            }
        
            return cell

        }
    
    }
    
    // 5 June(After Clicking From Where we are receiving drinkz we can can click and check the Sender-Profile) :-
    func MoveToSender(_ button: UIButton) {
        
        if self.userType == "1" {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "Profile") as! Profile
            vc.fromNavStr = "other"
            vc.showPostVideoBtn = true
            print(self.userId)
            vc.memberProfileId = self.userId
            navigationController?.pushViewController(vc,animated: true)
         }
            
        else if self.userType == "2" {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "PlaceProfile") as! PlaceProfile
            vc.fromNavStr = "other"
            print(userId)
            vc.restaurantId = self.userId
            navigationController?.pushViewController(vc,animated: true)
        }
            
        else if self.userType == "3" {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ArtistProfile") as! ArtistProfile
            vc.fromNavStr = "other"
            vc.artistId = self.userId
            navigationController?.pushViewController(vc,animated: true)
        }
            
        else if self.userType == "4" {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "PlaceProfile") as! PlaceProfile
            vc.fromNavStr = "other"
            vc.restaurantId = self.userId
            navigationController?.pushViewController(vc,animated: true)
        }
            
        else if self.userType == "5" {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "PlaceProfile") as! PlaceProfile
            vc.fromNavStr = "other"
            vc.restaurantId = self.userId
            navigationController?.pushViewController(vc,animated: true)
        }
            
        else if self.userType == "6" {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "PlaceProfile") as! PlaceProfile
            vc.fromNavStr = "other"
            vc.restaurantId = self.userId
            navigationController?.pushViewController(vc,animated: true)

        }
            
        else if self.userType == "7" {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "PlaceProfile") as! PlaceProfile
            vc.fromNavStr = "other"
            vc.restaurantId = self.userId
            navigationController?.pushViewController(vc,animated: true)
        }
    
        
    }
    
    
//    //MARK: Delete Action
//    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool{
//        
//         return true
//
//    }
//    
//    
//    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
// 
//            
//            if (editingStyle == UITableViewCellEditingStyle.delete) {
//                let alert = UIAlertController(title:"", message:"Are you sure you want to delete?", preferredStyle: UIAlertControllerStyle.alert)
//                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: {(action:UIAlertAction!) in
//                    
//                    let dict =  self.feedsArray[indexPath.row] as? NSDictionary
//                    let id = dict?.value(forKey:"id") as? String
//                    self.deleteFeed(requestId:id!)
//                    
//                }))
//                alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil))
//                self.present(alert, animated: true, completion: nil)
//                
//            }
//  
//        
//    }
    
    
    //MARK: deleting the feeds
    func deleteFeed(requestId: String) {
        
        self.view.isUserInteractionEnabled = false
        
        let json = ["user_id":Singleton.sharedInstance.userIdStr,
                    "auth_token":Singleton.sharedInstance.userTokenStr,
                    "feed_id":requestId
            ] as [String : Any]
        
        print(json)
        
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/users/deleteuserfeeds", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
                self.view.isUserInteractionEnabled = true
                print(response.result)   // result of response serialization
                print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    
                      self.newFeedsSection()
                    
                    // self.imBuyimgTbl.reloadData()
                    if((response.error) != nil){
                        self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                    }
                    self.indicator.stopAnimating()
                    self.indicator.isHidden = true
                }
                
        }
        
    }
    
    
    //MARK: To show image or video in detail
     @objc func showVideo(_ sender: Any) {
        

     let dict = feedsArray[(sender as AnyObject).tag] as? NSDictionary
        let videoUrll = dict?.value(forKey:"video_url") as? String
        
        let videoUrl = videoUrll?.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)
        

        let valueVideo = NSURL(string : videoUrl!)?.pathExtension
            if valueVideo == "mp4"{

                let videoURL = URL(string:videoUrl!)
                let player = AVPlayer(url: videoURL!)
                
                 player.allowsExternalPlayback = true
               
                let playerViewController = AVPlayerViewController()
                playerViewController.player = player
                self.view.addSubview(playerViewController.view)
                self.present(playerViewController, animated: true)
                {
                    playerViewController.player!.play()
                }
           }
      }
    
    // 11 June :-
    public func playerViewController(_ playerViewController: AVPlayerViewController, failedToStartPictureInPictureWithError error: Error) {
        print(error)
    }
   
    //MARK: Pagination Work
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
      }
    
    // Mark: Comvert Date
    func timeAgoSinceDate(_ date:Date,currentDate:Date, numericDates:Bool) -> String {
        let calendar = Calendar.current
        let now = currentDate
        let earliest = (now as NSDate).earlierDate(date)
        let latest = (earliest == now) ? date : now
        let components:DateComponents = (calendar as NSCalendar).components([NSCalendar.Unit.minute , NSCalendar.Unit.hour , NSCalendar.Unit.day , NSCalendar.Unit.weekOfYear , NSCalendar.Unit.month , NSCalendar.Unit.year , NSCalendar.Unit.second], from: earliest, to: latest, options: NSCalendar.Options())
        
        if (components.year! >= 2) {
            return "\(components.year!) years ago"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 year ago"
            } else {
                return "Last year"
            }
        } else if (components.month! >= 2) {
            return "\(components.month!) mon ago"
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 mon ago"
            } else {
                return "Last mon"
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!) weeks ago"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1 week ago"
            } else {
                return "Last week"
            }
        } else if (components.day! >= 2) {
            return "\(components.day!) days ago"
        } else if (components.day! >= 1){
            if (numericDates){
                return "1 day ago"
            } else {
                return "Yesterday"
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!) hours ago"
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1 hour ago"
            } else {
                return "An hour ago"
            }
        } else if (components.minute! >= 2) {
            return "\(components.minute!) min ago"
        } else if (components.minute! >= 1){
            if (numericDates){
                return "1 min ago"
            } else {
                return "A min ago"
            }
        } else if (components.second! >= 3) {
            return "\(components.second!) sec ago"
        } else {
            return "Just now"
        }
        
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
    }
    
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        if fromNavStr == "other"  {
            return 376
        }
        return 314
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if fromNavStr == "other"  {
            if Singleton.sharedInstance.userTypeStr == "place" {
                return 435
            }
            return 495
        }
        return 435
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
      
        var headerView1 = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 30))
        profileTbl.register(UINib(nibName: "ProfileHeader", bundle: nil), forCellReuseIdentifier: "ProfileHeader")
        
        let cell:ProfileHeader = profileTbl.dequeueReusableCell(withIdentifier: "ProfileHeader") as! ProfileHeader
        
    
        
        if fromNavStr == "other"  {
            if Singleton.sharedInstance.userTypeStr == "place" {
                headerView1 = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 495))
                cell.frame = CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 495)
                cell.contentView.frame = CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 495)
            } else {
                headerView1 = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 495))
                cell.frame = CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 495)
                cell.contentView.frame = CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 495)

            }
            
            if (Singleton.sharedInstance.userTypeStr == "member") {
                cell.sendView.isHidden = false
            }
            
            // 23 May :-
            if (Singleton.sharedInstance.userTypeStr == "place") || (Singleton.sharedInstance.userTypeStr == "artist") {
                cell.sendView.isHidden = false
            }
            
            
        } else {
            headerView1 = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 435))
            cell.frame = CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 435)
            cell.contentView.frame = CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 435)
            
           
            cell.sendView.isHidden = true
        }
        if showPostVideoBtn == true{
            
            cell.postVideoBtn.isHidden = true
            cell.postVideoBtn.layer.cornerRadius = 5
            cell.postVideoBtn.clipsToBounds = true
        }
        
        cell.postVideoBtn.addTarget(self, action: #selector(postVideo(_:)), for: .touchUpInside)
        cell.sendMsgBtn.addTarget(self, action: #selector(Message(_:)), for: .touchUpInside)
      
        
   
       
        
     // 20 May :-
        if(self.frndUnfrndStr == "1"){
        cell.addFriendImageView.image = UIImage(named: "remove.png")
//            self.indicator.stopAnimating()
//            self.indicator.isHidden = true
            
       }
     else {
        cell.addFriendImageView.image = UIImage(named: "add_friend.png")
//            self.indicator.stopAnimating()
//            self.indicator.isHidden = true
            
        }
        
        // 3 July (To check if there is option of frndrequest to its own or not.)
        if(Singleton.sharedInstance.userIdStr == self.memberProfileId) {
            cell.addFriendImageView.image = UIImage(named: "")
            cell.addFriendBtn.isHidden = true
         }
        
        cell.addFriendBtn.addTarget(self, action: #selector(AddFriend(_:)), for: .touchUpInside)
        
       // 17 May :-
       //  cell.sendFoodBtn.layer.cornerRadius=21
       //   cell.sendFoodBtn.clipsToBounds=true
        
        cell.sendFoodBtn.layer.borderWidth = 1
        cell.sendFoodBtn.layer.borderColor = cell.sendFoodBtn.titleLabel?.textColor.cgColor
        
        cell.sendFoodBtn.addTarget(self, action: #selector(SendDrinks(_:)), for: .touchUpInside)
        cell.mapView.delegate = self
        if fromNavStr != "other" {
            userImgStr = Singleton.sharedInstance.userImgStr
        }
        
        cell.nameLbl.text = nameStr
       //Mark: for visting the other user profile and updating the address
       
        if self.notificationSetting == "1" && fromNavStr == "other" && self.addressString != ""{

          print(self.addressString)
          cell.locationLbl.text = self.addressString

        }
        else if self.notificationSetting == "0" && fromNavStr == "other"{
  
          print(locationStr)
          cell.locationLbl.text = locationStr

        }
        
        //Mark: for showing address on the basis of user own profile
        else if self.locationValue == "1" && self.addressString != ""{
            // 6 June :-
            print(self.addressString)
            let fullNameArr = self.addressString.components(separatedBy: ",")

            if fullNameArr.count > 2 {
               // let count  = fullNameArr.count
                let str1: String = fullNameArr[0]
                let str2: String = fullNameArr[1]
                let str3: String = fullNameArr[2]
                let str4: String = fullNameArr.last ?? ""
                
                cell.locationLbl.text = str1+", "+str2+", "+str3+", "+str4
                
            }else {
                cell.locationLbl.text = self.addressString

            }
        }
        else{
            print(locationStr)
            cell.locationLbl.text = locationStr
            
        }
        
        cell.userImg.layer.cornerRadius = cell.userImg.frame.size.height/2
        cell.userImg.layer.borderColor = UIColor(red: 176/255, green: 176/255, blue: 176/255, alpha: 1).cgColor
        cell.userImg.layer.borderWidth = 1.5
        cell.userImg.clipsToBounds = true
        
        // 10 June :-
        // cell.userImg.sd_setImage(with: URL(string: userImgStr), placeholderImage: UIImage(named: "user"), options: .refreshCached)
        let urlStr : String = (userImgStr)
        let urlFormattedStr = urlStr.replacingOccurrences(of: " ", with: "%20")
        cell.userImg.sd_setImage(with: URL(string:urlFormattedStr), placeholderImage: UIImage(named: "user"), options: .refreshCached)
        
        
         let dropPin = MKPointAnnotation()
        
        
        
        // For other user profile - for showing lat long
        if fromNavStr == "other" && self.notificationSetting == "1" && self.addressString != "" {
            
            lat = DataManager.currentLat!
            long = DataManager.currentLong!
            let userLocation = CLLocationCoordinate2DMake(lat, long)
            dropPin.coordinate = userLocation
            dropPin.title = self.addressString
        }
        
        else if self.notificationSetting == "0" && fromNavStr == "other" {
            
            lat = (latitudeStr as NSString).doubleValue
            long = (longitudeStr as NSString).doubleValue
            let userLocation = CLLocationCoordinate2DMake(lat, long)
            dropPin.coordinate = userLocation
            dropPin.title = locationStr
       }
            
        else if fromNavStr == "other" && otherUserCheckin == true{
            
             lat = (checkInLat as NSString).doubleValue
              long = (checkInLong as NSString).doubleValue
             let  userLocation = CLLocationCoordinate2DMake(lat, long)
             dropPin.coordinate = userLocation
             dropPin.title = locationStr
        }
            // For my profile when location is on
       else if (self.locationValue == "1") && self.addressString != ""{
            
             lat = DataManager.currentLat!
             long = DataManager.currentLong!
            let userLocation = CLLocationCoordinate2DMake(lat, long)
            dropPin.coordinate = userLocation
            dropPin.title = self.addressString
            
            
        }
        // for my profile when location is off
        else{
          
             lat = (latitudeStr as NSString).doubleValue
             long = (longitudeStr as NSString).doubleValue
            let userLocation = CLLocationCoordinate2DMake(lat, long)
            dropPin.coordinate = userLocation
            dropPin.title = locationStr
            
        }
     if fromNavStr == "other" && otherUserCheckin == true {
            
            dropPin.title = checkInLoc
        }

        //(Singleton.sharedInstance.userTypeStr == "member") || (Singleton.sharedInstance.userTypeStr == "artist")
        
          if  (fromNavStr == "other")  {
        
                cell.mapView.addAnnotation(dropPin)
                let span = MKCoordinateSpanMake(0.010, 0.010)
                let region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: lat, longitude: long), span: span)
                cell.mapView.setRegion(region, animated: true)
        
       }
    else   {
        
           cell.mapView.addAnnotation(dropPin)
          let span = MKCoordinateSpanMake(0.010, 0.010)
          let region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: lat, longitude: long), span: span)
          cell.mapView.setRegion(region, animated: true)
        
        }

        cell.profileBackView.layer.cornerRadius = 4
        cell.profileBackView.layer.masksToBounds = false;
        cell.profileBackView.layer.shadowColor = UIColor.black.cgColor
        cell.profileBackView.layer.shadowOpacity = 0.3
        cell.profileBackView.layer.shadowRadius = 2
        cell.profileBackView.layer.shadowOffset = CGSize(width: 1, height: 1)
        
        
      //  cell.fbBtn.addTarget(self, action: #selector(ConnectedAction(_:)), for: .touchUpInside)
      //  cell.twitterBtn.addTarget(self, action: #selector(ConnectedAction(_:)), for: .touchUpInside)
        
        
        
        print(cell.frame.size.width)
        print(cell.contentView.frame.size.width)
        
        
        headerView1.addSubview(cell.contentView)
        return headerView1
    }
    
    
    //MARK: UIMapViewDelegates
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        // Don't want to show a custom image if the annotation is the user's location.
        guard !(annotation is MKUserLocation) else {
            return nil
        }
        
        // Better to make this class property
        let annotationIdentifier = "AnnotationIdentifier"
        
        var annotationView: MKAnnotationView?
        if let dequeuedAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier) {
            annotationView = dequeuedAnnotationView
            annotationView?.annotation = annotation
        }
        else {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
            annotationView?.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        }
        
        if let annotationView = annotationView {
            // Configure your annotation view here
            annotationView.canShowCallout = true
            
            
            let annView:UIView = UIView(frame: CGRect(x:0, y: 27, width: 185, height: 40))
            
            // Mark: For other user profile.
             if (fromNavStr == "other") {
                
                let annImg:UIImageView = UIImageView(frame: CGRect(x:5, y: 5, width: 30, height: 30))
                let annLbl:UILabel = UILabel(frame: CGRect(x:42, y: 0, width: 120, height: 40))
               //  annLbl.text="I am in \(self.checkInRest) club!"
                if self.notificationSetting == "1" && self.addressString != ""{
                    
                    annLbl.text = self.addressString
                    
                }
                    
                else{
                    
                    annLbl.text = self.locationStr
                    
                }
                annLbl.font=UIFont(name: "FuturaBT-Book", size: 10)!
                annLbl.textColor=UIColor.black
                annLbl.numberOfLines = 3
                annView.layer.cornerRadius = 20
                annView.layer.masksToBounds = false;
                annView.layer.shadowColor = UIColor.black.cgColor
                annView.layer.shadowOpacity = 0.3
                annView.layer.shadowRadius = 2
                annView.layer.shadowOffset = CGSize(width: 1, height: 1)
                
                annView.backgroundColor = UIColor.white
                // annImg.image =  UIImage(named: "me")
                
                // 10 June :-
                // annImg.sd_setImage(with: URL(string: self.userImgStr), placeholderImage: UIImage(named: "user"))
                let urlStr : String = (self.userImgStr)
               let urlFormattedStr = urlStr.replacingOccurrences(of: " ", with: "%20")
                annImg.sd_setImage(with: URL(string:urlFormattedStr), placeholderImage: UIImage(named: "user"))
                
                
                
                annImg.layer.cornerRadius = annImg.frame.height/2
                annImg.clipsToBounds = true
                
                //  annImg.contentMode = .scaleAspectFit
                annView .addSubview(annImg)
                annView .addSubview(annLbl)
                
                annotationView.addSubview(annView)
            }
                // Mark: User own profile
            else if (self.locationValue == "1") || (self.locationValue == "0"){
                
                let annImg:UIImageView = UIImageView(frame: CGRect(x:5, y: 5, width: 30, height: 30))
                let annLbl:UILabel = UILabel(frame: CGRect(x:42, y: 0, width: 120, height: 40))
                // annLbl.text="I am in \(self.checkInRest) club!"
                annLbl.font=UIFont(name: "FuturaBT-Book", size: 10)!
                annLbl.textColor=UIColor.black
                annLbl.numberOfLines = 0
                if self.locationValue == "1" && self.addressString != ""{
                    
                    annLbl.text = self.addressString
                    
                }
                    
                else{
                    
                    annLbl.text = self.locationStr
                    
                }
                
                annView.layer.cornerRadius = 20
                annView.layer.masksToBounds = false;
                annView.layer.shadowColor = UIColor.black.cgColor
                annView.layer.shadowOpacity = 0.3
                annView.layer.shadowRadius = 2
                annView.layer.shadowOffset = CGSize(width: 1, height: 1)
                
                annView.backgroundColor = UIColor.white
                // annImg.image =  UIImage(named: "me")
                
                // 10 June :-
               // annImg.sd_setImage(with: URL(string: self.userImgStr), placeholderImage: UIImage(named: "user"))
                let urlStr : String = (self.userImgStr)
                let urlFormattedStr = urlStr.replacingOccurrences(of: " ", with: "%20")
                annImg.sd_setImage(with: URL(string:urlFormattedStr), placeholderImage: UIImage(named: "user"))
                
                
                annImg.layer.cornerRadius = annImg.frame.height/2
                annImg.clipsToBounds = true
                
                //  annImg.contentMode = .scaleAspectFit
                annView .addSubview(annImg)
                annView .addSubview(annLbl)
                
                annotationView.addSubview(annView)
                
                
            }
                // Mark: for other useer who has checked in.
            else if (fromNavStr == "other") && (otherUserCheckin == true){
                
                
                let annImg:UIImageView = UIImageView(frame: CGRect(x:5, y: 5, width: 30, height: 30))
                let annLbl:UILabel = UILabel(frame: CGRect(x:42, y: 0, width: 120, height: 40))
                annLbl.text="I am in \(self.checkInRest) club!"
                annLbl.font=UIFont(name: "FuturaBT-Book", size: 11)!
                annLbl.textColor=UIColor.black
                annLbl.numberOfLines = 0
             
                
                annView.layer.cornerRadius = 20
                annView.layer.masksToBounds = false;
                annView.layer.shadowColor = UIColor.black.cgColor
                annView.layer.shadowOpacity = 0.3
                annView.layer.shadowRadius = 2
                annView.layer.shadowOffset = CGSize(width: 1, height: 1)
                
                annView.backgroundColor = UIColor.white
                // annImg.image =  UIImage(named: "me")
                
                // 10 June :-
               // annImg.sd_setImage(with: URL(string: self.userImgStr), placeholderImage: UIImage(named: "user"))
                let urlStr : String = (self.userImgStr)
                print(urlStr)
                let urlFormattedStr = urlStr.replacingOccurrences(of: " ", with: "%20")
                annImg.sd_setImage(with: URL(string:urlFormattedStr), placeholderImage: UIImage(named: "user"))
                
                
                
                
                annImg.layer.cornerRadius = annImg.frame.height/2
                annImg.clipsToBounds = true
                
                //  annImg.contentMode = .scaleAspectFit
                annView .addSubview(annImg)
                annView .addSubview(annLbl)
                
                annotationView.addSubview(annView)
            }
           else {
                
                
                annView.removeFromSuperview()
                annView.isHidden = true
            }
            

            annotationView.image = UIImage(named: "marker1")
        }
        
        return annotationView
    }
    
    
    
    
    // MARK: Button Actions
    
    
    func ConnectedAction(_ button: UIButton) {
        if fromNavStr != "other" {
            isConnected = true
            
            if (button.tag == 0){
                
                
//                Singleton.sharedInstance.socialLoginType = "1"
//                UserDefaults.standard.setValue(Sing leton.sharedInstance.socialLoginType, forKey: "socialLoginType")
//
//                if FBSDKAccessToken.current()?.tokenString != nil {
//                    self.indicator.startAnimating()
//                    self.indicator.isHidden = false
//                    self.fetchFacebookFeeds()
//                } else {
//                    let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
//                    fbLoginManager.logIn(withReadPermissions: ["public_profile", "email", "user_friends", "user_posts"], from: self) { (result, error) in
//                        if (error == nil){
//                            let fbloginresult : FBSDKLoginManagerLoginResult = result!
//                            if fbloginresult.grantedPermissions != nil {
//                                if(fbloginresult.grantedPermissions.contains("email"))
//                                {
//
//                                    // fbLoginManager.logOut()
//                                }
//
//                                self.indicator.startAnimating()
//                                self.indicator.isHidden = false
//                                self.fetchFacebookFeeds()
//
//                                print("token is \(FBSDKAccessToken.current().tokenString)")
//                                print("token is \(FBSDKAccessToken.current().userID)")
//
//
//                            }
//                        }
//                    }
//
//                }
                
            } else {
//
//                Singleton.sharedInstance.socialLoginType = "2"
//              UserDefaults.standard.setValue(Singleton.sharedInstance.socialLoginType, forKey: "socialLoginType")
//
//                if Twitter.sharedInstance().sessionStore.session()?.userID != nil {
//                    self.indicator.startAnimating()
//                    self.indicator.isHidden = false
//                    self.twitterMethod()
//                } else {
//                    Twitter.sharedInstance().logIn { session, error in
//                        if (session != nil) {
//                            print("signed in as \(session!.userName)");
//                            print("signed in as \(session!.userID)");
//
//                            self.indicator.startAnimating()
//                            self.indicator.isHidden = false
//                            self.twitterMethod()
//                        } else {
//                            print("error: \(String(describing: error?.localizedDescription))");
//                        }
//                    }
//                }
//
                
            }
        } else {
            if (button.tag == 0){
                Singleton.sharedInstance.socialLoginType = "1"
                UserDefaults.standard.setValue(Singleton.sharedInstance.socialLoginType, forKey: "socialLoginType")
                
                self.indicator.startAnimating()
                self.indicator.isHidden = false
                
                if self.fromNavStr == "other" {
                    self.memberProfileMethod()
                } else {
                    self.profileMethod()
                }
                
                
            } else {
                Singleton.sharedInstance.socialLoginType = "2"
                UserDefaults.standard.setValue(Singleton.sharedInstance.socialLoginType, forKey: "socialLoginType")
                
                self.indicator.startAnimating()
                self.indicator.isHidden = false
                
                if self.fromNavStr == "other" {
                    self.memberProfileMethod()
                } else {
                    self.profileMethod()
                }
                
            }
            
        }
    }
    
    
    @IBAction func Message(_ sender: Any) {
        
        let chatView = ChatViewController()
        chatView.navigationItem.title=self.nameStr
        chatView.recieverIdStr = memberProfileId
        chatView.senderIdStr = Singleton.sharedInstance.userIdStr
        chatView.recieverImgStr = userImgStr
        let chatNavigationController = UINavigationController(rootViewController: chatView)
        present(chatNavigationController, animated: true, completion: nil)
    }
    
    @IBAction func postVideo(_ sender: Any) {
        
        let cameraVC = storyboard!.instantiateViewController(withIdentifier: "CameraOverlay") as! CameraOverlay
       self.navigationController?.pushViewController(cameraVC, animated:true)
    
    }

    // 20 May :- AddFriend and Unfriend Button Action
    @IBAction func AddFriend(_ sender: Any) {
    
        let timestamp = NSDate().timeIntervalSince1970
        print(timestamp)
        print(String(format: "%.0f", (timestamp)))
        let timestampp = (String(format: "%.0f", (timestamp)))
        print(timestampp)
        
      let json = ["user_id":Singleton.sharedInstance.userIdStr,
                        "auth_token":Singleton.sharedInstance.userTokenStr,
                        "otheruser_id":self.memberProfileId,
                        "timestamp" : timestampp
                ] as [String : String]
            print(json)

        if(frndUnfrndStr == "1") {
        
            // 23 May :-
           self.indicator.isHidden = false
           self.indicator.startAnimating()
            
             Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/users/unfriend", method: .post, parameters: json, encoding: JSONEncoding.default)
                .responseJSON { response in
                    
                    print(response.result)   // result of response serialization
                    print(response)
                    if let jsonData = response.result.value {
                        print("JSON: \(jsonData)")
//                        let json = JSON(data: response.data! )
//                         print(json)
                        
                        let alert = UIAlertController(title: "", message: "Removed as Friend", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        self.frndUnfrndStr = "0"
                        
                        // 22 May :-
                        UserDefaults.standard.set(true, forKey: "ProfileViewFrnd")
                        self.profileTbl.reloadData()
                        
                     }
                    else if((response.error) != nil){

                        self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                        self.indicator.stopAnimating()
                        self.indicator.isHidden = true

                    }
            }
    } // ending If-Statement
   
        else {
            
            // 23 May :-
            self.indicator.isHidden = false
            self.indicator.startAnimating()
            
            Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/users/friend", method: .post, parameters: json, encoding: JSONEncoding.default)
                .responseJSON { response in
                    
                 print(response.result)   // result of response serialization
                    print(response)
                    if let jsonData = response.result.value {
                        print("JSON: \(jsonData)")
                        //                    let json = JSON(data: response.data! )
                        //                    print(json)
                        let alert = UIAlertController(title: "", message: "Added as Friend", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        self.frndUnfrndStr = "1"
                        
                        self.profileTbl.reloadData()
                        
                        
                        
                    } else if((response.error) != nil){
                        
                        self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                        self.indicator.stopAnimating()
                        self.indicator.isHidden = true
                        
                    }
            }
        } // ending else statement
        
}
   
 
   @IBAction func Block(_ sender: Any) {
        if fromNavStr == "other" {
            let alert = UIAlertController(title: nil, message: "Are you sure you want to block?", preferredStyle: UIAlertControllerStyle.actionSheet)
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            let blockAction = UIAlertAction(title: "Block", style: .destructive, handler: {(_ action: UIAlertAction) -> Void in
                self.indicator.startAnimating()
                self.indicator.isHidden = false
                self.blockMemberMethod()

            })
            let reportAction = UIAlertAction(title: "Report inappropriate", style: .destructive, handler: {(_ action: UIAlertAction) -> Void in
                self.indicator.startAnimating()
                self.indicator.isHidden = false
                self.blockMemberMethod()
                
            })
            alert.addAction(reportAction)
            alert.addAction(blockAction)
            alert.addAction(cancelAction)
            present(alert, animated: true)
            
            
        }
    }
    
    
    // MARK: Block Member Method
    
    func blockMemberMethod () {
        
        let  json = ["user_id":Singleton.sharedInstance.userIdStr,
                     "auth_token":Singleton.sharedInstance.userTokenStr,
                     "status":"1",
                     "block_id":memberProfileId
            ] as [String : Any]
        
        
        print(json)
        
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/members/blockuser", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    let json = JSON(data: response.data! )
                    if let descriptionStr = json["description"].string {
                        if descriptionStr == "Wrong auth token" {
                            UserDefaults.standard.setValue(nil, forKey: "auth_token")
                            UserDefaults.standard.setValue(nil, forKey: "user_id")
                            UserDefaults.standard.setValue(nil, forKey: "user_type")
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "Login") as! Login
                            vc.isExpired = true
                            self.navigationController?.pushViewController(vc,animated: true)
                            
                        } else if descriptionStr == "User Blocked Successfully" {
                          
                            print(Singleton.sharedInstance.blockArray.count)
                            Singleton.sharedInstance.blockArray = json["data"]["block_user"].arrayObject! as! [[String : Any]]
                            print(Singleton.sharedInstance.blockArray.count)

                            
                            
                            let alert = UIAlertController(title: "", message: "You have been successfully blocked this member.", preferredStyle: UIAlertControllerStyle.alert)
                            let okAction = UIAlertAction(title: "Ok", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                                if !self.fromViewControllerStr.isEmpty {
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let vc = storyboard.instantiateViewController(withIdentifier: self.fromViewControllerStr)
                                    self.navigationController?.pushViewController(vc,animated: true)
                                } else {
                                    _=self.navigationController?.popViewController(animated: true)
                                }
                            })
                            alert.addAction(okAction)
                            self.present(alert, animated: true)

                            
                            print("blocked")
                        }
                        //Now you got your value
                    }
                } else if((response.error) != nil){
                    
                    self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                    
                }
                
                
                self.indicator.stopAnimating()
                self.indicator.isHidden = true
                
        }
        
    }

    
    @IBAction func Back(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func SendDrinks(_ sender: Any) {
        
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SearchRestaurant") as! SearchRestaurant
        vc.recieverIdStr = memberProfileId
        vc.hideSkipTf = true
        navigationController?.pushViewController(vc,animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Compress Video Code
    func capture(outputFileURL:NSURL) {
       
        guard let data = NSData(contentsOf: outputFileURL as URL) else {
            return
        }
        
        print("File size before compression: \(Double(data.length / 1048576)) mb")
        let compressedURL = NSURL.fileURL(withPath: NSTemporaryDirectory() + NSUUID().uuidString + ".m4v")
        compressVideo(inputURL: outputFileURL as URL, outputURL: compressedURL) { (exportSession) in
            guard let session = exportSession else {
                return
            }
            
            switch session.status {
            case .unknown:
                break
            case .waiting:
                break
            case .exporting:
                break
            case .completed:
                guard let compressedData = NSData(contentsOf: compressedURL) else {
                    return
                }
                
                print("File size after compression: \(Double(compressedData.length / 1048576)) mb")
            case .failed:
                break
            case .cancelled:
                break
            }
        }
    }
    
    func compressVideo(inputURL: URL, outputURL: URL, handler:@escaping (_ exportSession: AVAssetExportSession?)-> Void) {
        
        let urlAsset = AVURLAsset(url: inputURL, options: nil)
        guard let exportSession = AVAssetExportSession(asset: urlAsset, presetName: AVAssetExportPresetMediumQuality) else {
            handler(nil)
            
            return
        }
        
        exportSession.outputURL = outputURL
        exportSession.outputFileType = AVFileTypeQuickTimeMovie
        exportSession.shouldOptimizeForNetworkUse = true
        exportSession.exportAsynchronously { () -> Void in
            handler(exportSession)
        }
    }

}


