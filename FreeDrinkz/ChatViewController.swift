//
//  ChatViewController.swift
//  SwiftExample
//
//  Created by Dan Leonard on 5/11/16.
//  Copyright © 2016 MacMeDan. All rights reserved.
//

import UIKit
import JSQMessagesViewController
import Firebase
import IQKeyboardManagerSwift
import Photos
import SDWebImage


class ChatViewController: JSQMessagesViewController, URLSessionDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    var nameStr = String()

    var messages = [JSQMessage]()
    let defaults = UserDefaults.standard
    var incomingBubble: JSQMessagesBubbleImage!
    var outgoingBubble: JSQMessagesBubbleImage!
    
    
    var senderIdStr = String()
    var recieverIdStr = String()
    var recieverImgStr = String()

    var senderImg = UIImage()
    var recieverImg = UIImage()
    var fcmTokenStr = String()
    var location = String()
    var isSend = Bool()
    var fromMessages = Bool()
    var isContain = Bool()

    let imagePicker = UIImagePickerController()

    
    var popUpView = UIView()
    var popUpImg = UIImageView()
    var scrollImg = UIScrollView()

    var isOn = Bool()
    
    fileprivate var displayName: String!
    
    // 3 July :-
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        if isContain {
            inputToolbar.contentView?.rightBarButtonItem?.isEnabled = false
            inputToolbar.contentView?.textView?.isUserInteractionEnabled = false
            inputToolbar.contentView?.leftBarButtonItem?.isEnabled = false
        
            let alert = UIAlertController(title: "", message: "The send message option is not available for this user.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)

        
        }
        

        popUpView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height))
        popUpImg = UIImageView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height))
        popUpView.backgroundColor = UIColor.black
        popUpImg.contentMode = .scaleAspectFit
        popUpImg.isUserInteractionEnabled = true
        popUpView.addSubview(popUpImg)
        self.view.addSubview(popUpView)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapBlurButton(_:)))
        popUpView.addGestureRecognizer(tapGesture)
        popUpView.isHidden = true
        
        popUpImg.image = UIImage(named: "user")

        
        scrollImg.delegate = self
        scrollImg.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
        scrollImg.backgroundColor = UIColor.clear
        scrollImg.alwaysBounceVertical = false
        scrollImg.alwaysBounceHorizontal = false
        scrollImg.showsVerticalScrollIndicator = false
        scrollImg.showsHorizontalScrollIndicator = false
  //    scrollImg.flashScrollIndicators()
        scrollImg.minimumZoomScale = 1.0
        scrollImg.maximumZoomScale = 10.0
        popUpView.addSubview(scrollImg)
        popUpImg.layer.cornerRadius = 11.0
        popUpImg.clipsToBounds = false
        scrollImg.addSubview(popUpImg)

        
        self.imagePicker.delegate = self
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
        indicator.frame = CGRect(x: (UIScreen.main.bounds.size.width-37)/2, y: 250, width: 37, height: 37)
        indicator.startAnimating()
        indicator.isHidden = false
        indicator.color = UIColor(red: 36/255, green: 69/255, blue: 138/255, alpha: 1)
        self.view.addSubview(indicator)
 
        IQKeyboardManager.sharedManager().enable = false

        // Setup navigation
        
        /*
         for (index, element) in self.facebookFeedsArray.enumerated() {
         print("Item \(index): \(element)")
         }
         */
        self.recieverImg = UIImage(named: "user")!
        self.senderImg = UIImage(named: "user")!


       DispatchQueue.global(qos: .default).async(execute: {() -> Void in
            
            if let url  = NSURL(string: Singleton.sharedInstance.userImgStr),
                let data = NSData(contentsOf: url as URL)
            {
                self.senderImg = UIImage(data: data as Data)!
            }
            DispatchQueue.main.sync(execute: {() -> Void in
                self.collectionView?.reloadData()
            })
        })

        DispatchQueue.global(qos: .default).async(execute: {() -> Void in
            
            if let url  = NSURL(string: self.recieverImgStr),
                let data = NSData(contentsOf: url as URL)
            {
                self.recieverImg = UIImage(data: data as Data)!
            }
            DispatchQueue.main.sync(execute: {() -> Void in
                self.collectionView?.reloadData()
            })
        })

        
        let userImg = UIImageView()
        userImg.sd_setImage(with: NSURL(string: Singleton.sharedInstance.userImgStr) as URL?, completed: { (image, error, cacheType, url) in
            
            if ((image) != nil) {
                self.senderImg = image!
            } else {
                self.senderImg = UIImage(named: "user")!
            }

        })
    

        
        let userImg1 = UIImageView()
        userImg1.sd_setImage(with: NSURL(string: recieverImgStr) as URL?, completed: { (image, error, cacheType, url) in
            
            if ((image) != nil) {
                self.recieverImg = image!
            } else {
                self.recieverImg = UIImage(named: "user")!
            }
        })

        
        
        FIRDatabase.database().reference().child("users").child(recieverIdStr).child("credentials").child("message_push").observe(.value, with: { (snapshot) in
            if snapshot.exists() {
                
                let message_push = snapshot.value! as! String
                if message_push == "1" {
                    self.isOn = true
                } else {
                    self.isOn = false
                }
            } else {
                self.isOn = true
            }
        })

        

        FIRDatabase.database().reference().child("users").child(recieverIdStr).child("credentials").child("fcm_token").observe(.value, with: { (snapshot) in
            if snapshot.exists() {
                self.fcmTokenStr=snapshot.value! as! String
            } else {
                self.fcmTokenStr = ""
            }
        })

        
        
        FIRDatabase.database().reference().child("users").child(senderIdStr).child("conversations").child(recieverIdStr).observe(.value, with: { (snapshot) in
            if snapshot.exists() {
                let data = snapshot.value as! [String: String]
                self.location = data["location"]!
               
                
                
                
                FIRDatabase.database().reference().child("users").child("conversations").child(self.location).observe(.childAdded, with: { (snap) in
                    if snap.exists() {
                        
                        
                        let receivedMessage = snap.value as! [String: Any]

                        
                        if receivedMessage["fromId"] == nil {
                            return
                        }
                        
                        var index = Int()
                        
                        
                        index = self.messages.count
                        
//   3 July                        FIRDatabase.database().reference().child("users").child("conversations").child(self.location).child(snap.key).updateChildValues(["isRead": "true"])

                        if receivedMessage["fromId"] as? String != Singleton.sharedInstance.userIdStr  && receivedMessage["isRead"] as? String == "false" {
                            
                            self.appDelegate.batchCount = self.appDelegate.batchCount - 1
                            FIRDatabase.database().reference().child("users").child("conversations").child(self.location).child(snap.key).updateChildValues(["isRead": "true"])
                        }
                        
                       
                        //let receivedMessage = snap.value as! [String: Any]
                        let messageType = receivedMessage["type"] as! String
                        let content = receivedMessage["content"] as! String
                        let fromID = receivedMessage["fromId"] as! String
                        let timestamp = receivedMessage["timestamp"] as! String
                        let datee = NSDate(timeIntervalSince1970: TimeInterval(timestamp)!)

                        
                     if messageType == "photo" {
                        
                            let userImg1 = UIImageView()
                            
                            let image = SDImageCache.shared().imageFromDiskCache(forKey: content)

                            if image != nil {
                            
                                if fromID == self.senderIdStr {
                                    let mediaItem = JSQPhotoMediaItem(image: image)
                                    let message = JSQMessage(senderId: self.senderIdStr, senderDisplayName: "Sender", date: datee as Date, media: mediaItem)
                                    if self.isSend == true {
                                        self.isSend = false
                                    } else {
                                        self.messages.append(message)
                                    }
                                } else {
                                    let mediaItem = JSQPhotoMediaItem(image: image)
                                    let message = JSQMessage(senderId: fromID, senderDisplayName: "", date: datee as Date, media: mediaItem)
                                    self.messages.append(message)
                                }
                                
                                self.collectionView?.reloadData()
                                self.scrollToBottom(animated: true)


                            } else {
                                
                                
                                userImg1.sd_setImage(with: NSURL(string: content) as URL?, completed: { (image, error, cacheType, url) in
 
                                })

                                
                                if self.isSend == true {
                                    self.isSend = false
                                } else {
                                    
                                    if fromID == self.senderIdStr {
                                        let mediaItem = JSQPhotoMediaItem(image: UIImage(named: "gray_background")!)
                                        let message = JSQMessage(senderId: self.senderIdStr, senderDisplayName: "Sender", date: datee as Date, media: mediaItem)
                                            self.messages.insert(message, at: index)
                                    } else {
                                        let mediaItem = JSQPhotoMediaItem(image: UIImage(named: "gray_background")!)
                                        let message = JSQMessage(senderId: fromID, senderDisplayName: "", date: datee as Date, media: mediaItem)
                                        self.messages.insert(message, at: index)
                                    }
                                    
                                    
                                    DispatchQueue.global(qos: .default).async(execute: {() -> Void in
                                        
                                        if let url  = NSURL(string: content) as URL?,
                                            let data = NSData(contentsOf: url as URL)
                                        {
                                            
                                            print(index)
                                            if fromID == self.senderIdStr {
                                                let mediaItem = JSQPhotoMediaItem(image: UIImage(data: data as Data)!)
                                                let message = JSQMessage(senderId: self.senderIdStr, senderDisplayName: "Sender", date: datee as Date, media: mediaItem)
                                                    self.messages.remove(at: index)
                                                    self.messages.insert(message, at: index)
                                            } else {
                                                let mediaItem = JSQPhotoMediaItem(image: UIImage(data: data as Data)!)
                                                let message = JSQMessage(senderId: fromID, senderDisplayName: "", date: datee as Date, media: mediaItem)
                                                self.messages.remove(at: index)
                                                self.messages.insert(message, at: index)
                                            }
                                        }
                                        DispatchQueue.main.sync(execute: {() -> Void in
                                            self.collectionView?.reloadData()
                                            self.scrollToBottom(animated: true)
                                        })
                                    })
                                }
                            }
                            
                            
//                            SDWebImageManager.shared.downloadImage(withURL: URL(string: avatarImageURL), options: 0, progress: nil, completed: {(_ image: UIImage, _ error: Error?, _ cacheType: SDImageCacheType, _ finished: Bool, _ imageURL: URL) -> Void in
//                                if error == nil {
//                                    userAvatarImage = JSQMessagesAvatarImageFactory.avatarImage(with: image, diameter: kJSQMessagesCollectionViewAvatarSizeDefault)
//                                }
//                            })

                        } else {
                            if fromID == self.senderIdStr {
                                let message = JSQMessage(senderId: self.senderIdStr, senderDisplayName: "Sender", date: datee as Date, text: content)
                                if self.messages.count == 1 && self.isSend == true {
                                    self.isSend = false
                                } else {
                                    self.messages.append(message)
                                }
                            } else {
                                let message = JSQMessage(senderId: fromID, senderDisplayName: "", date: datee as Date, text: content)
                                self.messages.append(message)
                            }
                        }
                        
                        
   
                        self.collectionView?.reloadData()
                        
                        /**
                         *  Scroll to actually view the indicator
                         */
                        self.scrollToBottom(animated: true)
                        self.collectionView?.reloadData()
                        

                    }
                    indicator.stopAnimating()
                    indicator.isHidden = true
                })
            } else {
                indicator.stopAnimating()
                indicator.isHidden = true
            }
        })

        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "red_background2"), for: .default)

        
        self.navigationController!.navigationBar.titleTextAttributes = [NSFontAttributeName: UIFont(name: "FuturaBT-Book", size: 24)!, NSForegroundColorAttributeName : UIColor.white];

//        let statusLbl = UILabel(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 20))
//        statusLbl.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.8)
//        let barButton = UIBarButtonItem.init(customView: statusLbl)
//        self.navigationItem.backBarButtonItem = barButton

        setupBackButton()
        
        /**
         *  Override point:
         *
         *  Example of how to cusomize the bubble appearence for incoming and outgoing messages.
         *  Based on the Settings of the user display two differnent type of bubbles.
         *
         */
        
        if defaults.bool(forKey: Setting.removeBubbleTails.rawValue) {
            // Make taillessBubbles
            incomingBubble = JSQMessagesBubbleImageFactory(bubble: UIImage.jsq_bubbleCompactTailless(), capInsets: UIEdgeInsets.zero, layoutDirection: UIApplication.shared.userInterfaceLayoutDirection).incomingMessagesBubbleImage(with: UIColor.jsq_messageBubbleBlue())
            outgoingBubble = JSQMessagesBubbleImageFactory(bubble: UIImage.jsq_bubbleCompactTailless(), capInsets: UIEdgeInsets.zero, layoutDirection: UIApplication.shared.userInterfaceLayoutDirection).outgoingMessagesBubbleImage(with: UIColor.lightGray)
        }
        else {
            // Bubbles with tails
            incomingBubble = JSQMessagesBubbleImageFactory().incomingMessagesBubbleImage(with: UIColor.jsq_messageBubbleBlue())
            outgoingBubble = JSQMessagesBubbleImageFactory().outgoingMessagesBubbleImage(with: UIColor.lightGray)
        }
        
        /**
         *  Example on showing or removing Avatars based on user settings.
         */
        
        if defaults.bool(forKey: Setting.removeAvatar.rawValue) {
            collectionView?.collectionViewLayout.incomingAvatarViewSize = .zero
            collectionView?.collectionViewLayout.outgoingAvatarViewSize = .zero
        } else {
            collectionView?.collectionViewLayout.incomingAvatarViewSize = CGSize(width: kJSQMessagesCollectionViewAvatarSizeDefault, height:kJSQMessagesCollectionViewAvatarSizeDefault )
            collectionView?.collectionViewLayout.outgoingAvatarViewSize = CGSize(width: kJSQMessagesCollectionViewAvatarSizeDefault, height:kJSQMessagesCollectionViewAvatarSizeDefault )
        }
        
        // Show Button to simulate incoming messages
  //      self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage.jsq_defaultTypingIndicator(), style: .plain, target: self, action: #selector(receiveMessagePressed))
        
        // This is a beta feature that mostly works but to make things more stable it is diabled.
        collectionView?.collectionViewLayout.springinessEnabled = false
        
        automaticallyScrollsToMostRecentMessage = true

        self.collectionView?.reloadData()
        self.collectionView?.layoutIfNeeded()
    }
    
    
//    override func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
//        print("Zoomscale \(scale)")
//        print("Zoom End!")
//    }
    
 
    
    override func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return popUpImg
    }

    func tapBlurButton(_ sender: UITapGestureRecognizer) {
        print("Please Help!")
        popUpView.isHidden = true
        inputToolbar.isHidden = false
        self.navigationController?.isNavigationBarHidden = false

    }
    
    override func viewDidDisappear(_ animated: Bool) {
        if Singleton.sharedInstance.isOpenPhotos != true {
            IQKeyboardManager.sharedManager().enable = true
            if location != "" {
                if fromMessages != true {
                    FIRDatabase.database().reference().child("users").child("conversations").child(location).removeAllObservers()
                }
            }
        } else {
            Singleton.sharedInstance.isOpenPhotos = false
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        IQKeyboardManager.sharedManager().enable = false
    }

    
    
    func setupBackButton() {
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "back.png"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(backButtonTapped), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 26, height: 26) //CGRectMake(0, 0, 30, 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        
        
//        let backButton = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonTapped))
//        navigationItem.leftBarButtonItem = backButton
    }
    func backButtonTapped() {
        dismiss(animated: true, completion: nil)
      //  _ = navigationController?.popViewController(animated: true)
    }
    
    /*
    func receiveMessagePressed(_ sender: UIBarButtonItem) {
        /**
         *  DEMO ONLY
         *
         *  The following is simply to simulate received messages for the demo.
         *  Do not actually do this.
         */
        
        /**
         *  Show the typing indicator to be shown
         */
        self.showTypingIndicator = !self.showTypingIndicator
        
        /**
         *  Scroll to actually view the indicator
         */
        self.scrollToBottom(animated: true)
        
        /**
         *  Copy last sent message, this will be the new "received" message
         */
        var copyMessage = self.messages.last?.copy()
        
        if (copyMessage == nil) {
            copyMessage = JSQMessage(senderId: AvatarIdJobs, displayName: getName(User.Jobs), text: "First received!")
        }
        
        var newMessage:JSQMessage!
        var newMediaData:JSQMessageMediaData!
        var newMediaAttachmentCopy:AnyObject?
        
        if (copyMessage! as AnyObject).isMediaMessage() {
            /**
             *  Last message was a media message
             */
            let copyMediaData = (copyMessage! as AnyObject).media
            
            switch copyMediaData {
            case is JSQPhotoMediaItem:
                let photoItemCopy = (copyMediaData as! JSQPhotoMediaItem).copy() as! JSQPhotoMediaItem
                photoItemCopy.appliesMediaViewMaskAsOutgoing = false
                
                newMediaAttachmentCopy = UIImage(cgImage: photoItemCopy.image!.cgImage!)
                
                /**
                 *  Set image to nil to simulate "downloading" the image
                 *  and show the placeholder view5017
                 */
                photoItemCopy.image = nil;
                
                newMediaData = photoItemCopy
            case is JSQLocationMediaItem:
                let locationItemCopy = (copyMediaData as! JSQLocationMediaItem).copy() as! JSQLocationMediaItem
                locationItemCopy.appliesMediaViewMaskAsOutgoing = false
                newMediaAttachmentCopy = locationItemCopy.location!.copy() as AnyObject?
                
                /**
                 *  Set location to nil to simulate "downloading" the location data
                 */
                locationItemCopy.location = nil;
                
                newMediaData = locationItemCopy;
            case is JSQVideoMediaItem:
                let videoItemCopy = (copyMediaData as! JSQVideoMediaItem).copy() as! JSQVideoMediaItem
                videoItemCopy.appliesMediaViewMaskAsOutgoing = false
                newMediaAttachmentCopy = (videoItemCopy.fileURL! as NSURL).copy() as AnyObject?
                
                /**
                 *  Reset video item to simulate "downloading" the video
                 */
                videoItemCopy.fileURL = nil;
                videoItemCopy.isReadyToPlay = false;
                
                newMediaData = videoItemCopy;
            case is JSQAudioMediaItem:
                let audioItemCopy = (copyMediaData as! JSQAudioMediaItem).copy() as! JSQAudioMediaItem
                audioItemCopy.appliesMediaViewMaskAsOutgoing = false
                newMediaAttachmentCopy = (audioItemCopy.audioData! as NSData).copy() as AnyObject?
                
                /**
                 *  Reset audio item to simulate "downloading" the audio
                 */
                audioItemCopy.audioData = nil;
                
                newMediaData = audioItemCopy;
            default:
                assertionFailure("Error: This Media type was not recognised")
            }
            
            newMessage = JSQMessage(senderId: senderIdStr, displayName: "Sender", media: newMediaData)
        }
        else {
            /**
             *  Last message was a text message
             */
            
            newMessage = JSQMessage(senderId: senderIdStr, displayName: "Sender", text: (copyMessage! as AnyObject).text)
        }
        
        /**
         *  Upon receiving a message, you should:
         *
         *  1. Play sound (optional)
         *  2. Add new JSQMessageData object to your data source
         *  3. Call `finishReceivingMessage`
         */
        

        self.messages.append(newMessage)
        self.finishReceivingMessage(animated: true)
        
        if newMessage.isMediaMessage {
            /**
             *  Simulate "downloading" media
             */
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)) {
                /**
                 *  Media is "finished downloading", re-display visible cells
                 *
                 *  If media cell is not visible, the next time it is dequeued the view controller will display its new attachment data
                 *
                 *  Reload the specific item, or simply call `reloadData`
                 */
                
                switch newMediaData {
                case is JSQPhotoMediaItem:
                    (newMediaData as! JSQPhotoMediaItem).image = newMediaAttachmentCopy as? UIImage
                    self.collectionView!.reloadData()
                case is JSQLocationMediaItem:
                    (newMediaData as! JSQLocationMediaItem).setLocation(newMediaAttachmentCopy as? CLLocation, withCompletionHandler: {
                        self.collectionView!.reloadData()
                    })
                case is JSQVideoMediaItem:
                    (newMediaData as! JSQVideoMediaItem).fileURL = newMediaAttachmentCopy as? URL
                    (newMediaData as! JSQVideoMediaItem).isReadyToPlay = true
                    self.collectionView!.reloadData()
                case is JSQAudioMediaItem:
                    (newMediaData as! JSQAudioMediaItem).audioData = newMediaAttachmentCopy as? Data
                    self.collectionView!.reloadData()
                default:
                    assertionFailure("Error: This Media type was not recognised")
                }
            }
        }
    }
 
 */
    
    // MARK: JSQMessagesViewController method overrides
    override func didPressSend(_ button: UIButton, withMessageText text: String, senderId: String, senderDisplayName: String, date: Date) {
        /**
         *  Sending a message. Your implementation of this method should do *at least* the following:
         *
         *  1. Play sound (optional)
         *  2. Add new id<JSQMessageData> object to your data source
         *  3. Call `finishSendingMessage`
         */
        

        if self.messages.count == 0 {
            isSend = true
            
            let timeStampStr = String(describing: Int(Date().timeIntervalSince1970))
            let datee = NSDate(timeIntervalSince1970: TimeInterval(timeStampStr)!)
            let message = JSQMessage(senderId: self.senderIdStr, senderDisplayName: "Sender", date: datee as Date, text: text)
            self.messages.append(message)
            self.scrollToBottom(animated: true)
            self.collectionView?.reloadData()
        }
        
    

    
    
     //   let values = ["type": "text", "content": text, "fromId": senderIdStr, "toId": recieverIdStr, "timestamp": Int(Date().timeIntervalSince1970), "isRead": false] as [String : Any]
    
        let timeStampStr = String(describing: Int(Date().timeIntervalSince1970))
        
        let values = ["type": "text", "content": text, "fromId": senderIdStr, "toId": recieverIdStr, "timestamp": timeStampStr, "isRead": "false"] as [String : Any]

        
        self.uploadMessage(values: values, completion: { (status) in
            print(status)
            if self.isOn {
                self.sendPushMessage(type: "text",message: text)
            }
            self.finishSendingMessage(animated: true)

        })

        
        
      

    }
    
    
    func sendPushMessage(type: String,message: String){
        
        let url  = NSURL(string: "https://fcm.googleapis.com/fcm/send")
        var token = String()
        if FIRInstanceID.instanceID().token() != nil {
            token = FIRInstanceID.instanceID().token()!
        } else {
            token = ""
        }
        let request = NSMutableURLRequest(url: url! as URL)
        request.setValue("key=AAAA05OLLkA:APA91bEBLeTfGaM2ARB-01YnKB7alDyBCq8yCm4VZBOc_xq0iqLMZ67Wnmm3smTETHlAnH8dP2Bik0j333TvwRl7RIcAqudbjZ36L6j9-BRjl6gATWwCm9n1c72tBODMnKeGnSVkcutX", forHTTPHeaderField:"Authorization") // your legacy server key
        request.setValue(token, forHTTPHeaderField:"Authentification") //  sender refreshedToken
        request.setValue("application/json", forHTTPHeaderField:"Content-Type")
        request.httpMethod = "POST"
        let sessionConfig = URLSessionConfiguration.default
        
        var json = [String : Any]()
        var name = String()
        if Singleton.sharedInstance.userTypeStr == "member" {
            name = Singleton.sharedInstance.firstNameStr
        } else {
            name = Singleton.sharedInstance.restaurantStr
        }
        
        if name == "" {
            name = Singleton.sharedInstance.firstNameStr
        }
        
        switch type {
        case "photo":
            json = ["to":fcmTokenStr,
                    "priority":"high",
                    "content_available":true,
                    "data":["title": "\(name) sent you a photo.","message":"","other_user_id":senderIdStr,"coming_from_about":"messages"],
                    "notification":["title": "\(name) sent you a photo.","time": Int(Date().timeIntervalSince1970)]] as [String : Any]
        case "text":
            
            
            json = ["to":fcmTokenStr,
                    "priority":"high",
                    "content_available":true,
                    "data":["title": "\(name) sent you a message.","message":"","other_user_id":senderIdStr,"coming_from_about":"messages"],
                    "notification":["title": "\(name) sent you a message.","body":message,"time": Int(Date().timeIntervalSince1970)]] as [String : Any]
        default: break
            
        }

        
        
//        [25/05/17, 4:35:40 PM] Vaneet Thakur: {
//            "to" : "e1w6hEbZn-8:APA91bEUIb2JewYCIiApsMu5JfI5Ak...",
//            "notification": {
//                "body": "Cool offers. Get them before expiring!",
//                "title": "Flat 80% discount",
//                "icon": "appicon"
//            },
//            "data" : {
//                "name" : "LG LED TV S15",
//                "product_id" : "123",
//                "final_price" : "2500"
//            }
//        }
//        [25/05/17, 4:36:00 PM] Vaneet Thakur: objectMessage.addProperty("title", "Message from "+PreferenceHandler.readString(this,PreferenceHandler.PREF_KEY_FIRST_NAME,""));
//        objectMessage.addProperty("message", mEdMessageText.getText().toString().trim());
//        objectMessage.addProperty("other_user_id", LoggedInUserId);
//        objectMessage.addProperty("coming_from_about","messages");
        
        
        
        
          print(json)
        
        do {
            
            let jsonData = try JSONSerialization.data(withJSONObject: json, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            request.httpBody = jsonData
            
        } catch let error as NSError {
            print(error)
        }
        
        // insert json data to the request
        
        let urlSession = URLSession(configuration: sessionConfig, delegate: self, delegateQueue: OperationQueue.main)
        
        
        
        let task = urlSession.dataTask(with: request as URLRequest, completionHandler: {
            (
            data, response, error) in
            
            guard let _:Data = data, let _:URLResponse = response  , error == nil else {
                print("error")
                return
            }
            
            let dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("\(String(describing: dataString))")
            
        })
        task.resume()
        
    }
    
    

    
    
    override func didPressAccessoryButton(_ sender: UIButton) {
        self.inputToolbar.contentView!.textView!.resignFirstResponder()

        Singleton.sharedInstance.isOpenPhotos = true
 
        
        
        let sheet = UIAlertController(title: "Media messages", message: nil, preferredStyle: .actionSheet)
        
        let photoAction = UIAlertAction(title: "Gallery", style: .default) { (action) in
            let status = PHPhotoLibrary.authorizationStatus()
            if (status == .authorized || status == .notDetermined) {
                self.imagePicker.sourceType = .savedPhotosAlbum;
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }
        
        let cameraAction = UIAlertAction(title: "Camera", style: .default) { (action) in

            let status: AVAuthorizationStatus = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo)
            if status == .authorized || status == .notDetermined {
                // authorized
                let picker = UIImagePickerController()
                picker.delegate = self
                picker.allowsEditing = true
                picker.sourceType = .camera
                self.present(picker, animated: true, completion: { _ in })
            }
        }
        
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        sheet.addAction(photoAction)
        sheet.addAction(cameraAction)
        sheet.addAction(cancelAction)
        
        self.present(sheet, animated: true, completion: nil)

        
        
        
        
        
        
        
        
        

        
        /*
        let sheet = UIAlertController(title: "Media messages", message: nil, preferredStyle: .actionSheet)
        
        let photoAction = UIAlertAction(title: "Send photo", style: .default) { (action) in
            /**
             *  Create fake photo
             */
            let photoItem = JSQPhotoMediaItem(image: UIImage(named: "goldengate"))
            self.addMedia(photoItem)
        }
        
        let locationAction = UIAlertAction(title: "Send location", style: .default) { (action) in
            /**
             *  Add fake location
             */
            let locationItem = self.buildLocationItem()
            
            self.addMedia(locationItem)
        }
        
        let videoAction = UIAlertAction(title: "Send video", style: .default) { (action) in
            /**
             *  Add fake video
             */
            let videoItem = self.buildVideoItem()
            
            self.addMedia(videoItem)
        }
        
        let audioAction = UIAlertAction(title: "Send audio", style: .default) { (action) in
            /**
             *  Add fake audio
             */
            let audioItem = self.buildAudioItem()
            
            self.addMedia(audioItem)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        sheet.addAction(photoAction)
        sheet.addAction(locationAction)
        sheet.addAction(videoAction)
        sheet.addAction(audioAction)
        sheet.addAction(cancelAction)
        
        self.present(sheet, animated: true, completion: nil)
 
 */
    }
 
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            let timeStampStr = String(describing: Int(Date().timeIntervalSince1970))
            let datee = NSDate(timeIntervalSince1970: TimeInterval(timeStampStr)!)
            let mediaItem = JSQPhotoMediaItem(image: pickedImage)
            let message = JSQMessage(senderId: self.senderIdStr, senderDisplayName: "Sender", date: datee as Date, media: mediaItem)
            self.isSend = true
            self.messages.append(message)
            self.collectionView?.reloadData()
            self.scrollToBottom(animated: true)
            
            postPhoto(photo: pickedImage)
        } else {
            let pickedImage = info[UIImagePickerControllerOriginalImage] as! UIImage
            let timeStampStr = String(describing: Int(Date().timeIntervalSince1970))
            let datee = NSDate(timeIntervalSince1970: TimeInterval(timeStampStr)!)
            let mediaItem = JSQPhotoMediaItem(image: pickedImage)
            let message = JSQMessage(senderId: self.senderIdStr, senderDisplayName: "Sender", date: datee as Date, media: mediaItem)
            self.isSend = true
            self.messages.append(message)
            self.collectionView?.reloadData()
            self.scrollToBottom(animated: true)

            postPhoto(photo: pickedImage)
        }
        if isOn {
            sendPushMessage(type: "photo", message: "")
        }
        picker.dismiss(animated: true, completion: nil)
    }

    func postPhoto(photo: UIImage) {
    
        let imageData = UIImageJPEGRepresentation(photo, 0.5)
        let child = UUID().uuidString
        FIRStorage.storage().reference().child("messagePics").child(child).put(imageData!, metadata: nil, completion: { (metadata, error) in
            if error == nil {
                let path = metadata?.downloadURL()?.absoluteString
               
                
                let timeStampStr = String(describing: Int(Date().timeIntervalSince1970))
                
                let values = ["type": "photo", "content": path!, "fromId": self.senderIdStr, "toId": self.recieverIdStr, "timestamp": timeStampStr, "isRead": "false"] as [String : Any]
                
                
                self.uploadMessage(values: values, completion: { (status) in
                    print(status)
                })
                
                
            }
        })
    }
    
    
     func uploadMessage(values: [String: Any], completion: @escaping (Bool) -> Swift.Void) {
        
        let currentUserID = self.senderIdStr
        
       print(values)
        FIRDatabase.database().reference().child("users").child(currentUserID).child("conversations").child(self.recieverIdStr).observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.exists() {
                let data = snapshot.value as! [String: String]
                let location = data["location"]!
                
                print(values)
                
                FIRDatabase.database().reference().child("users").child("conversations").child(location).childByAutoId().setValue(values, withCompletionBlock: { (error, _) in
                    if error == nil {
                        
                    } else {
                        
                    }
                    completion(true)
                })
            } else {
                FIRDatabase.database().reference().child("users").child("conversations").childByAutoId().childByAutoId().setValue(values, withCompletionBlock: { (error, reference) in
                    let data = ["location": reference.parent!.key]
                    FIRDatabase.database().reference().child("users").child(currentUserID).child("conversations").child(self.recieverIdStr).updateChildValues(data)
                    FIRDatabase.database().reference().child("users").child(self.recieverIdStr).child("conversations").child(currentUserID).updateChildValues(data)
                    completion(true)

                    
                })
            }
        })

    }
    
    
    func buildVideoItem() -> JSQVideoMediaItem {
        let videoURL = URL(fileURLWithPath: "file://")
        
        let videoItem = JSQVideoMediaItem(fileURL: videoURL, isReadyToPlay: true)
        
        return videoItem
    }
    
    func buildAudioItem() -> JSQAudioMediaItem {
        let sample = Bundle.main.path(forResource: "jsq_messages_sample", ofType: "m4a")
        let audioData = try? Data(contentsOf: URL(fileURLWithPath: sample!))
        
        let audioItem = JSQAudioMediaItem(data: audioData)
        
        return audioItem
    }
    
    func buildLocationItem() -> JSQLocationMediaItem {
        let ferryBuildingInSF = CLLocation(latitude: 37.795313, longitude: -122.393757)
        
        let locationItem = JSQLocationMediaItem()
        locationItem.setLocation(ferryBuildingInSF) {
            self.collectionView!.reloadData()
        }
        
        return locationItem
    }
    
    func addMedia(_ media:JSQMediaItem) {
        let message = JSQMessage(senderId: self.senderId(), displayName: self.senderDisplayName(), media: media)
        self.messages.append(message)
        
        //Optional: play sent sound
        
        self.finishSendingMessage(animated: true)
    }
    
    
    //MARK: JSQMessages CollectionView DataSource
    
    override func senderId() -> String {
        return senderIdStr
    }
    
    override func senderDisplayName() -> String {
        return "ABC"
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, messageDataForItemAt indexPath: IndexPath) -> JSQMessageData {
        return messages[indexPath.item]
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, messageBubbleImageDataForItemAt indexPath: IndexPath) -> JSQMessageBubbleImageDataSource {
        
        return messages[indexPath.item].senderId == self.senderId() ? outgoingBubble : incomingBubble
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, avatarImageDataForItemAt indexPath: IndexPath) -> JSQMessageAvatarImageDataSource? {
        let message = messages[indexPath.item]
     //   return getAvatar(message.senderId)
        if message.senderId == senderIdStr {
            return JSQMessagesAvatarImageFactory().avatarImage(withPlaceholder: senderImg)
        } else {
            return JSQMessagesAvatarImageFactory().avatarImage(withPlaceholder: recieverImg)

        }
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, didTapMessageBubbleAt indexPath: IndexPath) {
        print(indexPath.row)
        
        let message = self.messages[indexPath.item]

        if message.isMediaMessage {
            
            scrollImg.zoomScale = 1

            let mediaItem: JSQMessageMediaData? = message.media
            print("Tapped photo message bubble!")
            let photoItem: JSQPhotoMediaItem? = (mediaItem as? JSQPhotoMediaItem)
            popUpImg.image = photoItem?.image
            popUpView.isHidden = false
            inputToolbar.isHidden = true
            self.navigationController?.isNavigationBarHidden = true
            inputToolbar.contentView?.textView?.resignFirstResponder()
        }
        
    }
    
    
    
     override func collectionView(_ collectionView: JSQMessagesCollectionView, attributedTextForCellTopLabelAt indexPath: IndexPath) -> NSAttributedString? {
        /**
         *  This logic should be consistent with what you return from `heightForCellTopLabelAtIndexPath:`
         *  The other label text delegate methods should follow a similar pattern.
         *
         *  Show a timestamp for every 3rd message
         */
        if (indexPath.item % 3 == 0) {
            let message = self.messages[indexPath.item]
            
            return JSQMessagesTimestampFormatter.shared().attributedTimestamp(for: message.date)
        }
        
        return nil
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, attributedTextForMessageBubbleTopLabelAt indexPath: IndexPath) -> NSAttributedString? {
        let message = messages[indexPath.item]
        
        // Displaying names above messages
        //Mark: Removing Sender Display Name
        /**
         *  Example on showing or removing senderDisplayName based on user settings.
         *  This logic should be consistent with what you return from `heightForCellTopLabelAtIndexPath:`
         */
        if defaults.bool(forKey: Setting.removeSenderDisplayName.rawValue) {
            return nil
        }
        
        if message.senderId == self.senderId() {
            return nil
        }

        return NSAttributedString(string: message.senderDisplayName)
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout, heightForCellTopLabelAt indexPath: IndexPath) -> CGFloat {
        /**
         *  Each label in a cell has a `height` delegate method that corresponds to its text dataSource method
         */
        
        /**
         *  This logic should be consistent with what you return from `attributedTextForCellTopLabelAtIndexPath:`
         *  The other label height delegate methods should follow similarly
         *
         *  Show a timestamp for every 3rd message
         */
        if indexPath.item % 3 == 0 {
            return kJSQMessagesCollectionViewCellLabelHeightDefault
        }
        
        return 0.0
    }

    override func collectionView(_ collectionView: JSQMessagesCollectionView, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout, heightForMessageBubbleTopLabelAt indexPath: IndexPath) -> CGFloat {
        
        /**
         *  Example on showing or removing senderDisplayName based on user settings.
         *  This logic should be consistent with what you return from `attributedTextForCellTopLabelAtIndexPath:`
         */
        if defaults.bool(forKey: Setting.removeSenderDisplayName.rawValue) {
            return 0.0
        }
        
        /**
         *  iOS7-style sender name labels
         */
        let currentMessage = self.messages[indexPath.item]
        
        if currentMessage.senderId == self.senderId() {
            return 0.0
        }
        
        if indexPath.item - 1 > 0 {
            let previousMessage = self.messages[indexPath.item - 1]
            if previousMessage.senderId == currentMessage.senderId {
                return 0.0
            }
        }
        
        return kJSQMessagesCollectionViewCellLabelHeightDefault;
    }
    
}
