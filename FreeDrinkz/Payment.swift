//
//  Payment.swift
//  FreeDrinkz
//
//  Created by Brst-Pc109 on 28/03/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class Payment: UIViewController {

    @IBOutlet weak var paypalView: UIView!
    @IBOutlet weak var creditView: UIView!
    @IBOutlet weak var emailTxt: UITextField!
    
    @IBOutlet weak var holderNameTxt: UITextField!
    @IBOutlet weak var accNoTxt: UITextField!
    @IBOutlet weak var codeTxt: UITextField!

    
    @IBOutlet weak var addEmailBtn: UIButton!
    @IBOutlet weak var addBankBtn: UIButton!

    
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    var userId = String()
    var restroId = String()

    override func viewDidLoad() {
        super.viewDidLoad()

        paypalView.layer.cornerRadius = 4
        paypalView.layer.masksToBounds = false;
        paypalView.layer.shadowColor = UIColor.black.cgColor
        paypalView.layer.shadowOpacity = 0.3
        paypalView.layer.shadowRadius = 2
        paypalView.layer.shadowOffset = CGSize(width: 1, height: 1)
 
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border.frame = CGRect(x: 0, y: emailTxt.frame.size.height - width, width:  emailTxt.frame.size.width, height: emailTxt.frame.size.height)
        border.borderWidth = width
        emailTxt.layer.addSublayer(border)
        emailTxt.layer.masksToBounds = true

        
        
        let border1 = CALayer()
        let width1 = CGFloat(1.0)
        border1.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border1.frame = CGRect(x: 0, y: holderNameTxt.frame.size.height - width, width:  holderNameTxt.frame.size.width, height: holderNameTxt.frame.size.height)
        border1.borderWidth = width1
        holderNameTxt.layer.addSublayer(border1)
        holderNameTxt.layer.masksToBounds = true

        
        let border2 = CALayer()
        let width2 = CGFloat(1.0)
        border2.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border2.frame = CGRect(x: 0, y: accNoTxt.frame.size.height - width, width:  accNoTxt.frame.size.width, height: accNoTxt.frame.size.height)
        border2.borderWidth = width2
        accNoTxt.layer.addSublayer(border2)
        accNoTxt.layer.masksToBounds = true

        let border3 = CALayer()
        let width3 = CGFloat(1.0)
        border3.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border3.frame = CGRect(x: 0, y: codeTxt.frame.size.height - width, width:  codeTxt.frame.size.width, height: codeTxt.frame.size.height)
        border3.borderWidth = width3
        codeTxt.layer.addSublayer(border3)
        codeTxt.layer.masksToBounds = true

        
        getEmailMethod()
        getBankDetailsMethod()
    }

    
    // MARK: Get Payment Email
    
    func getEmailMethod () {
        
        self.indicator.startAnimating()
        self.indicator.isHidden = false
        
        
        let json = ["user_id":Singleton.sharedInstance.userIdStr,
                    "auth_token":Singleton.sharedInstance.userTokenStr,
                    ] as [String : Any]
        
        
        print(json)
        
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/users/getpaypalemail", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    let json = JSON(data: response.data! )
                    if let descriptionStr = json["description"].string {
                        print(descriptionStr)
                        if descriptionStr == "Wrong auth token" {
                            
                            
                            UserDefaults.standard.setValue(nil, forKey: "auth_token")
                            UserDefaults.standard.setValue(nil, forKey: "user_id")
                            UserDefaults.standard.setValue(nil, forKey: "user_type")
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "Login") as! Login
                            vc.isExpired = true
                            self.navigationController?.pushViewController(vc,animated: true)
                            
                        }  else if descriptionStr == "Success" {

                            
                            if json["data"]["email"].string != nil {
                               
                                self.emailTxt.text =  String(describing: json["data"]["email"].string!)
                                self.addEmailBtn.setTitle("Update", for: .normal)
                                
                            } else {
                                self.emailTxt.text =  ""
                                self.addEmailBtn.setTitle("Add", for: .normal)
                            }

                        }
                        
                        
                        
                        //Now you got your value
                    }
                } else if((response.error) != nil){
                    
                    self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                    
                }
                
                self.indicator.stopAnimating()
                self.indicator.isHidden = true
                
        }
        
    }
    

    // MARK: Get Bank Details
    
    func getBankDetailsMethod () {
        
        self.indicator.startAnimating()
        self.indicator.isHidden = false
        
        
        let json = ["user_id":Singleton.sharedInstance.userIdStr,
                    "auth_token":Singleton.sharedInstance.userTokenStr,
                    ] as [String : Any]
        
        
        print(json)
        
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/users/getbankaccount", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    let json = JSON(data: response.data! )
                    if let descriptionStr = json["description"].string {
                        print(descriptionStr)
                        if descriptionStr == "Wrong auth token" {
                            
                            
                            UserDefaults.standard.setValue(nil, forKey: "auth_token")
                            UserDefaults.standard.setValue(nil, forKey: "user_id")
                            UserDefaults.standard.setValue(nil, forKey: "user_type")
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "Login") as! Login
                            vc.isExpired = true
                            self.navigationController?.pushViewController(vc,animated: true)
                            
                        }  else if descriptionStr == "Success" {
                            
                            
                            if json["data"]["account_num"].string != nil {
                                
                                self.accNoTxt.text =  String(describing: json["data"]["account_num"].string!)
                                self.addBankBtn.setTitle("Update", for: .normal)
                                
                            } else {
                                self.accNoTxt.text =  ""
                                self.addBankBtn.setTitle("Add", for: .normal)
                            }
                            
                            
                            if json["data"]["username"].string != nil {
                                self.holderNameTxt.text =  String(describing: json["data"]["username"].string!)
                            } else {
                                self.holderNameTxt.text =  ""
                            }
                          
                            if json["data"]["card_num"].string != nil {
                                self.codeTxt.text =  String(describing: json["data"]["card_num"].string!)
                            } else {
                                self.codeTxt.text =  ""
                            }
                            
                        }
                        
                        
                        
                        //Now you got your value
                    }
                } else if((response.error) != nil){
                    
                    self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                    
                }
                
                self.indicator.stopAnimating()
                self.indicator.isHidden = true
                
        }
        
    }

    
    
    // MARK: AlertView
    
    func alertViewMethod(titleStr:String, messageStr:String) {
        let alert = UIAlertController(title: titleStr, message: messageStr, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

    
   // MARK: Button Actions
    
    @IBAction func AddEmail(_ sender: Any) {
      
        if ((sender as AnyObject).tag == 0) {
            if (emailTxt.text?.isEmpty)! {
                alertViewMethod(titleStr: "", messageStr: "Please enter the name on card.")
            } else if !isValidEmail(testStr: emailTxt.text!) {
                alertViewMethod(titleStr: "", messageStr: "Please enter the valid email.")
            }  else {
                indicator.startAnimating()
                indicator.isHidden = false
                addEmailMethod()
            }
        } else {

            if !(accNoTxt.text?.isEmpty)! {
                
                if addBankBtn.currentTitle == "Update" {
                  
                    indicator.startAnimating()
                    indicator.isHidden = false
                    updateBankDetailsMethod()

                } else {
                   
                    indicator.startAnimating()
                    indicator.isHidden = false
                    addBankDetailsMethod()
                }
                
            }

        }
    }
    
    // MARK: Email Validation
    
    func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }

    
    // MARK: Add Bank Details
    
    func addBankDetailsMethod () {
        
        self.indicator.startAnimating()
        self.indicator.isHidden = false
        
        
        let json = ["user_id":Singleton.sharedInstance.userIdStr,
                    "auth_token":Singleton.sharedInstance.userTokenStr,
                    "account_num":accNoTxt.text!,
                    "username":holderNameTxt.text!,
                    "card_num":codeTxt.text!
            ] as [String : Any]
        
        
        print(json)
        
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/users/addbankaccount", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    let json = JSON(data: response.data! )
                    if let descriptionStr = json["description"].string {
                        print(descriptionStr)
                        if descriptionStr == "Wrong auth token" {
                            
                            
                            UserDefaults.standard.setValue(nil, forKey: "auth_token")
                            UserDefaults.standard.setValue(nil, forKey: "user_id")
                            UserDefaults.standard.setValue(nil, forKey: "user_type")
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "Login") as! Login
                            vc.isExpired = true
                            self.navigationController?.pushViewController(vc,animated: true)
                            
                        }  else if descriptionStr == "Updated Successfully" {
                            
                            self.alertViewMethod(titleStr: "", messageStr: "Bank Details Updated Successfully")
                            
                        }  else if descriptionStr == "Added Successfully" {
                            
                            self.alertViewMethod(titleStr: "", messageStr: "Bank Details Added Successfully")
                            self.addEmailBtn.setTitle("Update", for: .normal)
                            
                        }
                        
                        
                        
                        //Now you got your value
                    }
                } else if((response.error) != nil){
                    
                    self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                    
                }
                
                self.indicator.stopAnimating()
                self.indicator.isHidden = true
                
        }
        
    }
    
    
    
    func updateBankDetailsMethod () {
        
        self.indicator.startAnimating()
        self.indicator.isHidden = false
        
        
        let json = ["user_id":Singleton.sharedInstance.userIdStr,
                    "auth_token":Singleton.sharedInstance.userTokenStr,
                    "account_num":accNoTxt.text!,
                    "username":holderNameTxt.text!,
                    "card_num":codeTxt.text!
            ] as [String : Any]
        
        
        print(json)
        
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/users/updatebankaccount", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    let json = JSON(data: response.data! )
                    if let descriptionStr = json["description"].string {
                        print(descriptionStr)
                        if descriptionStr == "Wrong auth token" {
                            
                            
                            UserDefaults.standard.setValue(nil, forKey: "auth_token")
                            UserDefaults.standard.setValue(nil, forKey: "user_id")
                            UserDefaults.standard.setValue(nil, forKey: "user_type")
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "Login") as! Login
                            vc.isExpired = true
                            self.navigationController?.pushViewController(vc,animated: true)
                            
                        }  else if descriptionStr == "Updated Successfully" {
                            
                            self.alertViewMethod(titleStr: "", messageStr: "Bank Details Updated Successfully")
                            
                        }  else if descriptionStr == "Added Successfully" {
                            
                            self.alertViewMethod(titleStr: "", messageStr: "Bank Details Added Successfully")
                            self.addEmailBtn.setTitle("Update", for: .normal)
                            
                        }
                        
                        
                        
                        //Now you got your value
                    }
                } else if((response.error) != nil){
                    
                    self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                    
                }
                
                self.indicator.stopAnimating()
                self.indicator.isHidden = true
                
        }
        
    }

    // MARK: Add Payment Email
    
    func addEmailMethod () {
        
        self.indicator.startAnimating()
        self.indicator.isHidden = false
        
        
        let json = ["user_id":Singleton.sharedInstance.userIdStr,
                    "auth_token":Singleton.sharedInstance.userTokenStr,
                    "paypal_email":emailTxt.text!
                    ] as [String : Any]
        
        
        print(json)
        
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/users/addpaypalemail", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    let json = JSON(data: response.data! )
                    if let descriptionStr = json["description"].string {
                        print(descriptionStr)
                        if descriptionStr == "Wrong auth token" {
                            
                            
                            UserDefaults.standard.setValue(nil, forKey: "auth_token")
                            UserDefaults.standard.setValue(nil, forKey: "user_id")
                            UserDefaults.standard.setValue(nil, forKey: "user_type")
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "Login") as! Login
                            vc.isExpired = true
                            self.navigationController?.pushViewController(vc,animated: true)
                            
                        }  else if descriptionStr == "Updated Successfully" {
                            
                            self.alertViewMethod(titleStr: "", messageStr: "Email Updated Successfully")
                            
                        }  else if descriptionStr == "Added Successfully" {
                            
                            self.alertViewMethod(titleStr: "", messageStr: "Email Added Successfully")
                            self.addEmailBtn.setTitle("Update", for: .normal)

                        }
                        
                        
                        
                        //Now you got your value
                    }
                } else if((response.error) != nil){
                    
                    self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                    
                }
                
                self.indicator.stopAnimating()
                self.indicator.isHidden = true
                
        }
        
    }

    
    @IBAction func Back(_ sender: Any) {
        if Singleton.sharedInstance.userTypeIdStr == "1" {
            print("member")
            Singleton.sharedInstance.userTypeStr = "member"
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "Search") as! Search
            self.navigationController?.pushViewController(vc,animated: true)
            
        } else if Singleton.sharedInstance.userTypeIdStr == "2" {
            print("place")
            Singleton.sharedInstance.userTypeStr = "place"
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "Search") as! Search
            self.navigationController?.pushViewController(vc,animated: true)
            
        }else if Singleton.sharedInstance.userTypeIdStr == "3" {
            print("artist")
            Singleton.sharedInstance.userTypeStr = "artist"
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ArtistProfile") as! ArtistProfile
            self.navigationController?.pushViewController(vc,animated: true)
        }
    }
    @IBAction func PaymentAction(_ sender: Any) {
//        if ((sender as AnyObject).tag == 1) {
//            creditView.layer.borderColor = UIColor(red: 36/255, green: 69/255, blue: 138/255, alpha: 0.7).cgColor
//            creditView.layer.borderWidth = 1
//            
//            paypalView.layer.borderColor = UIColor.white.cgColor
//            paypalView.layer.borderWidth = 1
//
//            
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let vc = storyboard.instantiateViewController(withIdentifier: "AddPayment") as! AddPayment
//            navigationController?.pushViewController(vc,animated: true)
//
//        }else {
//        
//            paypalView.layer.borderColor = UIColor(red: 36/255, green: 69/255, blue: 138/255, alpha: 0.7).cgColor
//            paypalView.layer.borderWidth = 1
//            
//            creditView.layer.borderColor = UIColor.white.cgColor
//            creditView.layer.borderWidth = 1
//
//        }
//        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
