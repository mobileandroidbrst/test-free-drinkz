//
//  ImBuyingFirstVC.swift
//  FreeDrinkz
//
//  Created by Tbi-Pc-22 on 17/04/18.
//  Copyright © 2018 Brst-Pc109. All rights reserved.
//

import UIKit
import CoreLocation
import Alamofire
import SwiftyJSON
import SDWebImage

class ImBuyingFirstVC: UIViewController,UITableViewDataSource,UITableViewDelegate,CLLocationManagerDelegate {
    
    
    //Variables:
    var locationManager = CLLocationManager()
    var restroListArray = NSMutableArray()
    var checkInStatus =  String()
    
    
    // Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var lblNoNearbyRestro: UILabel!

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.lblNoNearbyRestro.isHidden = true
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = 0.5
        locationManager.delegate = self as CLLocationManagerDelegate
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Mark: Location Updates
    func buildGeofenceData() {
        
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
        let json = ["user_id":Singleton.sharedInstance.userIdStr,
                    "auth_token":Singleton.sharedInstance.userTokenStr,
                    "lat":DataManager.currentLat ?? "",
                    "long":DataManager.currentLong ?? ""
            ] as [String : Any]
        print(json)
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/users/getiambuyingnearbyrestaurents", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON {  response in
                DispatchQueue.main.async {
                    
                    print(response)
                    if let jsonData = response.result.value {
                        print("JSON: \(jsonData)")
                    let json1 = jsonData as! NSDictionary
                    let dataListing = json1["data"] as! NSDictionary
                       print(dataListing)
                    let restroList = dataListing["nearby_restaurants"] as! NSArray
                        if restroList.count == 0{
                            
                            self.lblNoNearbyRestro.isHidden = false
                            
                        }
                        else{
                            
                         self.restroListArray = restroList.mutableCopy() as! NSMutableArray
                        }
                    }
                    if((response.error) != nil){
                        
                       // self.buildGeofenceData()
                        
                  }
                    
                    DispatchQueue.main.async {
                        
                     self.activityIndicator.isHidden = true
                    self.activityIndicator.stopAnimating()
                    self.tableView.reloadData()
                   
                    }
                    
                    
                    
                }
        }
    }
    
    
    //MARK: Location manager delegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        
        DataManager.currentLat = locations.last?.coordinate.latitude
        DataManager.currentLong = locations.last?.coordinate.longitude
        self.locationManager.stopUpdatingLocation()
        self.buildGeofenceData()
        return
   
    }
    
    // Mark:Table View Mthods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
     return self.restroListArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
      let cell = tableView.dequeueReusableCell(withIdentifier: "ImBuyingFirst", for: indexPath) as! ImBuyingCell
        
        // 22 May :-
        
        cell.mainBackgroundView.layer.borderWidth = 1
        cell.mainBackgroundView.layer.borderColor = UIColor.lightGray.cgColor
        cell.shadowView.addBottomBorder(color: .lightGray, width: 2)
 
//     cell.mainBackgroundView.layer.masksToBounds = false
//     cell.mainBackgroundView.layer.shadowOffset = CGSize(width: 0, height: 0)
//     cell.mainBackgroundView.layer.shadowRadius = 1
//     cell.mainBackgroundView.layer.shadowOpacity = 0.5

      let dict = restroListArray[indexPath.row] as! NSDictionary
      cell.imgViewRestro.layer.cornerRadius = 15 //cell.imgViewRestro.frame.size.height/2
      cell.imgViewRestro.clipsToBounds = true
      
       let restroName = dict.value(forKey: "resturant_name") as? String
        
        var name = ""
        if Singleton.sharedInstance.lastNameStr != "" && Singleton.sharedInstance.lastNameStr != " " {
            name = Singleton.sharedInstance.firstNameStr+" "+Singleton.sharedInstance.lastNameStr
        }
        else {
            name = Singleton.sharedInstance.firstNameStr
        }
      cell.restroAddress.text = dict.value(forKey: "address") as? String
      //cell.checkInPplStatus.text = dict.value(forKey: "total_user_checkedin") as? String
      let value = dict.value(forKey: "total_user_checkedin") as? String
        // 11 June :-
       // cell.checkInPplStatus.text = "User's Check In:" +  value!
          cell.checkInPplStatus.text = "User’s Checked In:" +  value!
        
        // 5 June :-
       // cell.imgViewRestro.sd_setImage(with: URL(string: dict.value(forKey: "profile_pic_url") as! String), placeholderImage: UIImage(named: "userSquare.jpg"))
        
        let urlStr : String = dict.value(forKey: "profile_pic_url") as! String
        let urlFormattedStr = urlStr.replacingOccurrences(of: " ", with: "%20")
        cell.imgViewRestro.sd_setImage(with: URL(string:urlFormattedStr), placeholderImage: UIImage(named: "user"))
        
        for _ in (0..<restroListArray.count)
        {
         
            self.checkInStatus = (dict.value(forKey: "user_check_in_status") as? String)!
              print(checkInStatus)
            UserDefaults.standard.setValue(checkInStatus, forKey: "checkIn_Status")
            if checkInStatus == "1"{

             break

            }
        

        }
        
        if self.checkInStatus == "1"{
            
            cell.restroName.text =  name + " " + "is checked in" + " " + restroName!

        }
        
        else{
        
            cell.restroName.text = restroName!
            
        }
        
        
        
     // Mark: Check In Out Handling
        cell.checkInBtn.tag = indexPath.row
        cell.checkInBtn.addTarget(self, action: #selector(handelCheckInOutEvent), for: UIControlEvents.touchUpInside)
        cell.checkInBtn.layer.cornerRadius = cell.checkInBtn.layer.frame.height/2
        cell.checkInBtn.clipsToBounds = true
        
        
        // Mark: Request Out Handling
        cell.requestButton.tag = indexPath.row
        cell.requestButton.addTarget(self, action: #selector(handelRequestInOutEvent), for: UIControlEvents.touchUpInside)
        cell.requestButton.layer.cornerRadius = cell.requestButton.layer.frame.size.height/2
         cell.requestButton.clipsToBounds = true
        
        cell.selectionStyle = .none
        
        return cell
   
    }
    
    
    // Mark: Check In Out Handling
    func handelCheckInOutEvent(sender: UIButton){
        
       
        let dict = restroListArray[sender.tag] as! NSDictionary
        let id  = dict.value(forKey: "id") as? String
        let name  = dict.value(forKey: "resturant_name") as? String
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "PlaceProfile") as! PlaceProfile
        vc.selectType = "1"
        vc.restaurantId = id!
        vc.fromNavStr = "other"
        vc.restroName = name!
        self.navigationController?.pushViewController(vc,animated: true)
        
        
    }
    
    
    // Mark: Check In Out Handling
    func handelRequestInOutEvent(sender: UIButton){

        let dict = restroListArray[sender.tag] as! NSDictionary
        print(dict)
        let id  = dict.value(forKey: "id") as? String
        
       // let status  = dict.value(forKey: "user_check_in_status") as? String
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ImBuying") as! ImBuying
        vc.restroId = id!
        vc.type = "all"
        
        vc.typeString = "push"
        print(vc.restroId)
        self.navigationController?.pushViewController(vc,animated: true)
        

        
    }
    // Mark: Alert View
    func alertViewMethod(titleStr:String, messageStr:String) {
        let alert = UIAlertController(title: titleStr, message: messageStr, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
}

// 22 May :-
extension UIView {
    @discardableResult func addBottomBorder(color: UIColor, width: CGFloat) -> UIView {
        let layer = CALayer()
        layer.borderColor = color.cgColor
        layer.borderWidth = width
        layer.frame = CGRect(x: 0, y: self.frame.size.height-width, width: self.frame.size.width, height: width)
        self.layer.addSublayer(layer)
        return self
    }
}
