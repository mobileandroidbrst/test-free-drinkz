//
//  Messages.swift
//  FreeDrinkz
//
//  Created by Brst-Pc109 on 27/03/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

//I'm buying request
//buy for me

import UIKit
import IQKeyboardManagerSwift
import Firebase
import Alamofire
import SwiftyJSON

class Messages: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var messageTbl: UITableView!
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    var messagesArray = [String: Any]()
    var locationArray = NSMutableArray()
    
    var fromPush = Bool()
    var recieverIdStr = String()
    
    var sortedArray = [(key: String, value: Any)]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.navigationController?.isNavigationBarHidden = true
     
        self.indicator.isHidden = false
        self.indicator.startAnimating()
    FIRDatabase.database().reference().child("users").child(Singleton.sharedInstance.userIdStr).child("conversations").observe(.childRemoved, with: { (snapshot) in
            if snapshot.exists() {
                print(snapshot)
                let values = snapshot.value as! [String: String]
                let location = values["location"]!
                self.messagesArray.removeValue(forKey: location)
                self.sortedArray = self.messagesArray.sorted { (dictOne, dictTwo) -> Bool in
                    let dict1 = dictOne.1 as! NSDictionary
                    let dict2 = dictTwo.1 as! NSDictionary
                    let d1 = dict1["timestamp"] as! String
                    let d2 = dict2["timestamp"] as! String
                    return d2 < d1
                }
                
                
                self.messageTbl.reloadData()
            }
        })


        let messagesRef = FIRDatabase.database().reference()
        let uid = "UserId1"
        let status = "unread"
        let queryString = Singleton.sharedInstance.userIdStr + "_" + status //results in a string UserId1_unread
        
        let ref = FIRDatabase.database().reference().child("users").child(Singleton.sharedInstance.userIdStr).child("conversations").queryEqual(toValue: "isRead == false")
        ref.observe(.value, with: { snapshot in
            if snapshot.exists() {
                let count = snapshot.childrenCount
                print("unread messages: \(count)")
            } else {
                print("no unread messages")
            }
        })
        
     //   FIRDatabase.database().reference().child("users").child(Singleton.sharedInstance.userIdStr).child("conversations").observe(.childRemoved, with: { (snapshot) in

        
        
        
        
        
        
         FIRDatabase.database().reference().child("users").child(Singleton.sharedInstance.userIdStr).child("conversations").observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.exists() {
                print(snapshot)
                self.statusLbl.isHidden = true
            } else {
                self.messagesArray.removeAll()
                self.messageTbl.reloadData()
                self.statusLbl.isHidden = false
            }
            self.indicator.isHidden = true
            self.indicator.stopAnimating()
            
        })
        

        FIRDatabase.database().reference().child("users").child(Singleton.sharedInstance.userIdStr).child("conversations").observe(.childAdded, with: { (snapshot) in
            if snapshot.exists() {
                print(snapshot)
                self.statusLbl.isHidden = true
                
                let fromID = snapshot.key
                let values = snapshot.value as! [String: String]
                let location = values["location"]!
                
                print(location)
                
                self.userInfo(forUserID: fromID, completion: { (userData) in
                    
                   
                    let userName = userData["name"]
                    if userName == nil{
                        
                    }
                    
                    else{
                        
                    let userName = userData["name"]
                    }
                    let image = userData["profile_pic_url"]!
                    var values = ["type": "text", "content": "Loading...", "from": userName, "fromID": fromID, "timestamp": "0", "isRead": "false", "profile_pic_url": image] as [String : Any]
                    
                    self.messagesArray[location] = values
                    self.locationArray.add(location)

                    self.sortedArray = self.messagesArray.sorted { (dictOne, dictTwo) -> Bool in
                        let dict1 = dictOne.1 as! NSDictionary
                        let dict2 = dictTwo.1 as! NSDictionary
                        
                        let d1 = dict1["timestamp"] as! String
                        let d2 = dict2["timestamp"] as! String
                        
                        
                        return d2 < d1
                    }

                    
                    self.messageTbl.reloadData()
                    print(values)
                    
                    if self.recieverIdStr == fromID && self.fromPush == true {
                        
                        
                        self.fromPush = false
                        
                        let chatView = ChatViewController()
                        chatView.navigationItem.title=userName
                        chatView.recieverIdStr = fromID
                        chatView.senderIdStr = Singleton.sharedInstance.userIdStr
                        chatView.recieverImgStr = image
                        chatView.fromMessages = true
                        let chatNavigationController = UINavigationController(rootViewController: chatView)
                        self.present(chatNavigationController, animated: true, completion: nil)
                        
                        
                    }
                    
                    
                    self.downloadLastMessage(forLocation: location, completion: { (receivedMessage) in
                        
                        let lastMsgStr = receivedMessage["content"]! as! String
                        let timestamp = receivedMessage["timestamp"] as! String
                        let fromID = receivedMessage["fromId"] as! String
                        let messageType = receivedMessage["type"] as! String
                        
                        values["content"] = lastMsgStr
                        values["timestamp"] = timestamp
                        
                        if fromID == Singleton.sharedInstance.userIdStr && messageType == "photo" {
                            values["content"] = "You sent a photo"
                        } else if messageType == "photo" {
                            values["content"] = "Sent you a photo"
                        }
                        values["keyForChild"] = receivedMessage["keyForChild"] as! String

                        self.messagesArray[location] = values

                        self.sortedArray = self.messagesArray.sorted { (dictOne, dictTwo) -> Bool in
                            let dict1 = dictOne.1 as! NSDictionary
                            let dict2 = dictTwo.1 as! NSDictionary
                            
                            let d1 = dict1["timestamp"] as! String
                            let d2 = dict2["timestamp"] as! String
                            
                            
                            return d2 < d1
                        }
                        
                        print(self.sortedArray)
                        
                        
                        self.messageTbl.reloadData()
                    })
                })
            } else {
                self.statusLbl.isHidden = false
            }
            self.indicator.isHidden = true
            self.indicator.stopAnimating()
        })
        
    }
    
    func downloadLastMessage(forLocation: String, completion: @escaping ([String: Any]) -> Swift.Void) {
        
        
        FIRDatabase.database().reference().child("users").child("conversations").child(forLocation).observe(.value, with: { (snapshot) in
            if snapshot.exists() {
                
                var receivedMessage = [String: Any]()
                for snap in snapshot.children {
                    print(snap)
                    receivedMessage = (snap as! FIRDataSnapshot).value as! [String: Any]
                    receivedMessage["keyForChild"] = (snap as! FIRDataSnapshot).key
                }
                completion(receivedMessage)
                
            }
        })
        
        
        
    }
    
    func userInfo(forUserID: String, completion: @escaping ([String: String]) -> Swift.Void) {
        
        FIRDatabase.database().reference().child("users").child(forUserID).child("credentials").observeSingleEvent(of: .value, with: { (snapshot) in
            if let data = snapshot.value as? [String: String] {
                completion(data)
                
            }
        })
        
    }
    
    
    @IBAction func Block(_ sender: Any) {
       // if fromNavStr == "other" {
            let alert = UIAlertController(title: nil, message: "Are you sure you want to block?", preferredStyle: UIAlertControllerStyle.actionSheet)
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            let blockAction = UIAlertAction(title: "Block", style: .destructive, handler: {(_ action: UIAlertAction) -> Void in
                self.indicator.startAnimating()
                self.indicator.isHidden = false
                self.blockMemberMethod()
                
            })
            let reportAction = UIAlertAction(title: "Report inappropriate", style: .destructive, handler: {(_ action: UIAlertAction) -> Void in
                self.indicator.startAnimating()
                self.indicator.isHidden = false
                self.blockMemberMethod()
                
            })
            alert.addAction(reportAction)
            alert.addAction(blockAction)
            alert.addAction(cancelAction)
            present(alert, animated: true)
            
            
      //  }
    }
    
    
    
    
    // MARK: Block Member Method
    
    func blockMemberMethod () {
        
        let  json = ["user_id":Singleton.sharedInstance.userIdStr,
                     "auth_token":Singleton.sharedInstance.userTokenStr,
                     "status":"1",
                     "block_id":Singleton.sharedInstance.userIdStr
            ] as [String : Any]
        
        
        print(json)
        
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/members/blockuser", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    let json = JSON(data: response.data! )
                    if let descriptionStr = json["description"].string {
                        if descriptionStr == "Wrong auth token" {
                            UserDefaults.standard.setValue(nil, forKey: "auth_token")
                            UserDefaults.standard.setValue(nil, forKey: "user_id")
                            UserDefaults.standard.setValue(nil, forKey: "user_type")
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "Login") as! Login
                            vc.isExpired = true
                            self.navigationController?.pushViewController(vc,animated: true)
                            
                        } else if descriptionStr == "User Blocked Successfully" {
                            
                            print(Singleton.sharedInstance.blockArray.count)
                            Singleton.sharedInstance.blockArray = json["data"]["block_user"].arrayObject! as! [[String : Any]]
                            print(Singleton.sharedInstance.blockArray.count)
                            
                            
                            
                            let alert = UIAlertController(title: "", message: "You have been successfully blocked this member.", preferredStyle: UIAlertControllerStyle.alert)
                            let okAction = UIAlertAction(title: "Ok", style: .default, handler: {(_ action: UIAlertAction) -> Void in
//                                if !self.fromViewControllerStr.isEmpty {
//                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                                    let vc = storyboard.instantiateViewController(withIdentifier: self.fromViewControllerStr)
//                                    self.navigationController?.pushViewController(vc,animated: true)
//                                } else {
//                                    _=self.navigationController?.popViewController(animated: true)
//                                }
                            })
                            alert.addAction(okAction)
                            self.present(alert, animated: true)
                            
                            
                            print("blocked")
                        }
                        //Now you got your value
                    }
                } else if((response.error) != nil){
                    
                  //  self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                    
                }
                
                
                self.indicator.stopAnimating()
                self.indicator.isHidden = true
                
        }
        
    }
    
    
    
    
    
    
    //MARK: UITableView Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return messagesArray.count
        //   return 10
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:MessagesCell = messageTbl.dequeueReusableCell(withIdentifier: "MessagesCell") as! MessagesCell
        
 
        let sortDict = self.sortedArray[indexPath.row]
        let location = sortDict.key

        
        
     //   let location = locationArray[indexPath.row] as! String
        let dict = messagesArray[location] as! [String : Any]
        
        cell.nameLbl.text = dict["from"] as? String
        if cell.nameLbl.text == nil || cell.nameLbl.text == "" {
            
            
        }
        
        else{
            
            
        }
        cell.descriptionLbl.text=dict["content"] as! String?
        
        // 7 June :-
        // cell.postImg.sd_setImage(with: URL(string: (dict["profile_pic_url"] as! String?)!), placeholderImage: UIImage(named: "user"))
         let urlStr : String = (dict["profile_pic_url"] as! String?)!
         let urlFormattedStr = urlStr.replacingOccurrences(of: " ", with: "%20")
         cell.postImg.sd_setImage(with: URL(string:urlFormattedStr), placeholderImage: UIImage(named: "user"))
        
        let timeStamp = dict["timestamp"] as! String?
        
        if timeStamp == "0" {
            cell.dateLbl.text = ""
        } else {
    
            // 25 May:-
//            let datee = NSDate(timeIntervalSince1970: TimeInterval(timeStamp!)!)
//            let dateString = timeAgoSinceDate(Date(), currentDate: datee as Date, numericDates: false)
//            cell.dateLbl.text = dateString
            let date = Date(timeIntervalSince1970: TimeInterval(timeStamp!)!)
            cell.dateLbl.text = date.setTimeAgoSinceDateSwift()
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.delete) {
            // handle delete (by removing the data from your array and updating the tableview)
            let alert = UIAlertController(title: nil, message: "Are you sure you want to delete?", preferredStyle: UIAlertControllerStyle.actionSheet)
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            let blockAction = UIAlertAction(title: "Delete", style: .destructive, handler: {(_ action: UIAlertAction) -> Void in
                
                print("delete:\(indexPath.row)")
                
                
                let sortDict = self.sortedArray[indexPath.row]
                let location = sortDict.key

                
             //   let location = self.locationArray[indexPath.row] as! String
                let dict = self.messagesArray[location] as! [String : Any]
                
            //    self.locationArray.removeObject(at: indexPath.row)
                self.sortedArray.remove(at: indexPath.row)
                self.messagesArray.removeValue(forKey: location)
                self.messageTbl.reloadData()
                
                FIRDatabase.database().reference().child("users").child("conversations").child(location).removeValue(completionBlock: { (error, reference) in
                    
                    if error == nil {
                        
                        
                        FIRDatabase.database().reference().child("users").child(Singleton.sharedInstance.userIdStr).child("conversations").child((dict["fromID"] as! String?)!).removeValue(completionBlock: { (error, reference) in
                            print("\(String(describing: error?.localizedDescription))")
                            
                        })
                        FIRDatabase.database().reference().child("users").child((dict["fromID"] as! String?)!).child("conversations").child(Singleton.sharedInstance.userIdStr).removeValue(completionBlock: { (error, reference) in
                            print("\(String(describing: error?.localizedDescription))")
                            
                        })
                        
                        
                    } else {
                        print("\(String(describing: error?.localizedDescription))")
                    }
                    
                })
                
                
            })
            alert.addAction(blockAction)
            alert.addAction(cancelAction)
            present(alert, animated: true)
            
        }
    }
    
    // 25 May :-
    // Mark: Comvert Date
    
//
//    func timeAgoSinceDate(_ date:Date,currentDate:Date, numericDates:Bool) -> String {
//
//
//        let timeZone = NSTimeZone.default
//        let dateFormatter = DateFormatter()
//        dateFormatter.timeZone = timeZone
//
//        dateFormatter.dateFormat = "dd/MM/yy"
//        let startDateStr: String = dateFormatter.string(from: currentDate as Date)
//        let startDate = dateFormatter.date(from: startDateStr )! as Date
//
//
//        dateFormatter.dateFormat = "dd/MM/yy"
//        let endDateStr: String = dateFormatter.string(from: date as Date)
//        let endDate = dateFormatter.date(from: endDateStr)! as Date
//
//
//
//        let calendar1 = Calendar.current
//        let now1 = startDate
//        let earliest1 = (now1 as NSDate).earlierDate(endDate)
//        let latest1 = (earliest1 == now1) ? endDate : now1
//        let components1:DateComponents = (calendar1 as NSCalendar).components([NSCalendar.Unit.minute , NSCalendar.Unit.hour , NSCalendar.Unit.day , NSCalendar.Unit.weekOfYear , NSCalendar.Unit.month , NSCalendar.Unit.year , NSCalendar.Unit.second], from: earliest1, to: latest1, options: NSCalendar.Options())
//
//
//
//        if (components1.year! >= 2) {
//            return "\(components1.year!) years ago"
//        } else if (components1.year! >= 1){
//
//            dateFormatter.dateFormat = "dd/MM/yy"
//            let localDateString: String = dateFormatter.string(from: currentDate as Date)
//            return localDateString
//
//        } else if (components1.month! >= 1){
//
//            dateFormatter.dateFormat = "dd/MM/yy"
//            let localDateString: String = dateFormatter.string(from: currentDate as Date)
//            return localDateString
//
//        } else if (components1.weekOfYear! >= 1){
//
//            dateFormatter.dateFormat = "dd/MM/yy"
//            let localDateString: String = dateFormatter.string(from: currentDate as Date)
//            return localDateString
//
//        } else if (components1.day! >= 8) {
//
//            dateFormatter.dateFormat = "dd/MM/yy"
//            let localDateString: String = dateFormatter.string(from: currentDate as Date)
//            return localDateString
//
//
//        }  else if (components1.day! >= 2) {
//
//            dateFormatter.dateFormat = "EEEE"
//            let localDateString: String = dateFormatter.string(from: currentDate as Date)
//            return localDateString
//
//
//        } else if (components1.day! >= 1){
//
//            return "Yesterday"
//
//        } else {
//
//
//            let calendar = Calendar.current
//            let now = currentDate
//            let earliest = (now as NSDate).earlierDate(date)
//            let latest = (earliest == now) ? date : now
//            let components:DateComponents = (calendar as NSCalendar).components([NSCalendar.Unit.minute , NSCalendar.Unit.hour , NSCalendar.Unit.day , NSCalendar.Unit.weekOfYear , NSCalendar.Unit.month , NSCalendar.Unit.year , NSCalendar.Unit.second], from: earliest, to: latest, options: NSCalendar.Options())
//
//
//            if (components.hour! >= 1){
//
//                dateFormatter.dateFormat = "hh:mm a"
//                let localDateString: String = dateFormatter.string(from: currentDate as Date)
//                return localDateString
//
//            } else if (components.minute! >= 1) {
//
//                dateFormatter.dateFormat = "hh:mm a"
//                let localDateString: String = dateFormatter.string(from: currentDate as Date)
//                return localDateString
//
//            } else if (components.second! >= 3) {
//
//                dateFormatter.dateFormat = "hh:mm a"
//                let localDateString: String = dateFormatter.string(from: currentDate as Date)
//                return localDateString
//            } else {
//                return "Just now"
//            }
//
//        }
//
//
//
//    }
    
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        let sortDict = self.sortedArray[indexPath.row]
        let location = sortDict.key

        let dict = messagesArray[location] as! [String : Any]
        
        
        if (dict["fromID"] as! String) != Singleton.sharedInstance.userIdStr {
         //   FIRDatabase.database().reference().child("users").child("conversations").child(location).child(dict["keyForChild"] as! String).updateChildValues(["isRead": "true"])
        }
        
        let chatView = ChatViewController()
        
        if let title = dict["from"] as? String {
            chatView.navigationItem.title = title
        }
        chatView.recieverIdStr = (dict["fromID"] as! String?)!
        
        let predicate = NSPredicate(format: "user_id == %d", Int(dict["fromID"] as! String)!)
        let array = Singleton.sharedInstance.blockArray.filter { predicate.evaluate(with: $0) }
        if array.count > 0 {
            chatView.isContain = true
        }
        
        chatView.senderIdStr = Singleton.sharedInstance.userIdStr
        chatView.recieverImgStr = (dict["profile_pic_url"] as! String?)!
        chatView.fromMessages = true
        let chatNavigationController = UINavigationController(rootViewController: chatView)
        present(chatNavigationController, animated: true, completion: nil)
        
        
    }
    
    // MARK: Buttons Action
    
    @IBAction func Back(_ sender: Any) {
        
        // 13 June :-
        let appDelegate: AppDelegate? = UIApplication.shared.delegate as? AppDelegate
        appDelegate!.firebaseMessagesLocations()
        
        
        if fromPush == true {
            
            if UserDefaults.standard.value(forKey: "auth_token") != nil {
                Singleton.sharedInstance.userTokenStr = UserDefaults.standard.value(forKey: "auth_token")! as! String
                Singleton.sharedInstance.userIdStr = UserDefaults.standard.value(forKey: "user_id")! as! String
                Singleton.sharedInstance.userTypeIdStr = UserDefaults.standard.value(forKey: "user_type")! as! String
                
                if Singleton.sharedInstance.userTypeIdStr == "1" {
                    print("member")
                    Singleton.sharedInstance.userTypeStr = "member"
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "Search") as! Search
                    self.navigationController?.pushViewController(vc,animated: true)
                    
                } else if Singleton.sharedInstance.userTypeIdStr == "2" {
                    print("place")
                    Singleton.sharedInstance.userTypeStr = "place"
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "Search") as! Search
                    self.navigationController?.pushViewController(vc,animated: true)
                    
                }else if Singleton.sharedInstance.userTypeIdStr == "3" {
                    print("artist")
                    Singleton.sharedInstance.userTypeStr = "artist"
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "ArtistProfile") as! ArtistProfile
                    self.navigationController?.pushViewController(vc,animated: true)
                }
            }
            
        } else {
          //  _ = navigationController?.popViewController(animated: true)
            if Singleton.sharedInstance.userTypeIdStr == "1" {
                print("member")
                Singleton.sharedInstance.userTypeStr = "member"
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "Search") as! Search
                self.navigationController?.pushViewController(vc,animated: true)
                
            } else if Singleton.sharedInstance.userTypeIdStr == "2" {
                print("place")
                Singleton.sharedInstance.userTypeStr = "place"
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "Search") as! Search
                self.navigationController?.pushViewController(vc,animated: true)
                
            }else if Singleton.sharedInstance.userTypeIdStr == "3" {
                print("artist")
                Singleton.sharedInstance.userTypeStr = "artist"
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "ArtistProfile") as! ArtistProfile
                self.navigationController?.pushViewController(vc,animated: true)
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
}
extension Date {
    
    func setTimeAgoSinceDateSwift() -> String {
        
        // From Time
        let fromDate = self
        
        // To Time
        let toDate = Date()
        
        // Estimation
        // Year
        if let interval = Calendar.current.dateComponents([.year], from: fromDate, to: toDate).year, interval > 0  {
            
            return interval == 1 ? "about \(interval)" + " " + "year ago" : "about \(interval)" + " " + "years ago"
        }
        
        // Month
        if let interval = Calendar.current.dateComponents([.month], from: fromDate, to: toDate).month, interval > 0  {
            
            return interval == 1 ? "\(interval)" + " " + "month ago" : "\(interval)" + " " + "months ago"
        }
        
        // Day
        if let interval = Calendar.current.dateComponents([.day], from: fromDate, to: toDate).day, interval > 0  {
            
            return interval == 1 ? "\(interval)" + " " + "day ago" : "\(interval)" + " " + "days ago"
        }
        
        // Hours
        if let interval = Calendar.current.dateComponents([.hour], from: fromDate, to: toDate).hour, interval > 0 {
            
            return interval == 1 ? "about \(interval)" + " " + "hour ago" : "about \(interval)" + " " + "hours ago"
        }
        
        // Minute
        if let interval = Calendar.current.dateComponents([.minute], from: fromDate, to: toDate).minute, interval > 0 {
            
            return interval == 1 ? "\(interval)" + " " + "minute ago" : "\(interval)" + " " + "minutes ago"
        }
        
        return "a moment ago"
    }
}
