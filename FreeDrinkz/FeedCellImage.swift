//
//  FeedCellImage.swift
//  FreeDrinkz
//
//  Created by brst on 02/01/19.
//  Copyright © 2019 Brst-Pc109. All rights reserved.
//

import UIKit

class FeedCellImage: UITableViewCell {
    
    
    @IBOutlet weak var feedImage: UIImageView!
    
    @IBOutlet weak var lblFeed: UILabel!
    
    @IBOutlet weak var feedOtherImg: UIImageView!
    
    @IBOutlet weak var lblInfo: UILabel!
    
    // 15 May :-
    @IBOutlet var lblDate: UILabel!
    
    // 5 June(For Member Profile) :-
   @IBOutlet var feedOtherImgBtn: UIButton!
    
    // 12 June(For Artist Profile) :-
    @IBOutlet var feedOtherImgBtn1: UIButton!
    @IBOutlet var artistLblDate: UILabel!
    
    @IBOutlet weak var btnVideoClick: UIButton!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
