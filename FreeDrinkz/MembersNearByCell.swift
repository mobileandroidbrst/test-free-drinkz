//
//  MembersNearByCell.swift
//  FreeDrinkz
//
//  Created by Brst-Pc109 on 29/03/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

import UIKit

class MembersNearByCell: UICollectionViewCell {
    
    @IBOutlet weak var memberImg: UIImageView!
    @IBOutlet weak var memberNameLbl: UILabel!
}
