//
//  RestaurantMemberCell.swift
//  FreeDrinkz
//
//  Created by Brst-Pc109 on 28/03/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

import UIKit

class RestaurantMemberCell: UICollectionViewCell {
    @IBOutlet weak var memberImg: UIImageView!
    @IBOutlet weak var MemberNameLbl: UILabel!
    @IBOutlet weak var sendMsgBtn: UIButton!
    @IBOutlet weak var sendDrinkBtn: UIButton!
    @IBOutlet weak var profileBtn: UIButton!
    @IBOutlet weak var locationLbl: UILabel!

}
