//
//  EditProfile.swift
//  FreeDrinkz
//
//  Created by Brst-Pc109 on 31/03/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AVFoundation
import Photos
import GooglePlaces

class EditProfile: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, GMSAutocompleteViewControllerDelegate,UIPickerViewDelegate {

    @IBOutlet weak var scrView: UIScrollView!
    @IBOutlet weak var firstNameTxt: UITextField!
    @IBOutlet weak var lastNameTxt: UITextField!
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var stateTxt: UITextField!
    @IBOutlet weak var countryTxt: UITextField!
    @IBOutlet weak var cityTxt: UITextField!
    @IBOutlet weak var zipCodeTxt: UITextField!
    @IBOutlet weak var ageTxt: UITextField!
    @IBOutlet weak var maleTxt: IQDropDownTextField!
    @IBOutlet weak var saveBtn: UIButton!


    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var userPlaceImg: UIImageView!
    @IBOutlet weak var userArtistImg: UIImageView!

    
    
    @IBOutlet weak var firstNameArtistTxt: UITextField!
    @IBOutlet weak var lastNameArtistTxt: UITextField!
    @IBOutlet weak var emailArtistTxt: UITextField!
    @IBOutlet weak var stateArtistTxt: UITextField!
    @IBOutlet weak var cityArtistTxt: UITextField!
    @IBOutlet weak var zipCodeArtistTxt: UITextField!
    @IBOutlet weak var ageArtistTxt: UITextField!
    @IBOutlet weak var bandNameArtistTxt: UITextField!
    @IBOutlet weak var countryArtistTxt: UITextField!
    @IBOutlet weak var maleArtistTxt: IQDropDownTextField!
    @IBOutlet weak var saveArtistBtn: UIButton!
    
    
    
    @IBOutlet weak var placeNameTxt: UITextField!
    @IBOutlet weak var emailPlaceTxt: UITextField!
    @IBOutlet weak var statePlaceTxt: UITextField!
    @IBOutlet weak var cityPlaceTxt: UITextField!
    @IBOutlet weak var zipCodePlaceTxt: UITextField!
    @IBOutlet weak var savePlaceBtn: UIButton!
    @IBOutlet weak var countryPlaceTxt: UITextField!

    
    
    
    @IBOutlet weak var memberView: UIView!
    @IBOutlet weak var artistView: UIView!
    @IBOutlet weak var placeView: UIView!
    @IBOutlet weak var backImg: UIImageView!
    @IBOutlet weak var backBtn: UIButton!

    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var titleLbl: UILabel!

    var uniquePhotoNameStr = String()
    var isSocial = Bool()

    
    
    @IBOutlet weak var addressTxt: UITextField!
    @IBOutlet weak var addressArtistTxt: UITextField!
    @IBOutlet weak var addressPlaceTxt: UITextField!

    var latitude = String()
    var longitude = String()
    var countryStr = String()
    var finalStateArray = [[String:Any]]()
    var finalCitiesArray = [[String:Any]]()
    var countryArray = [[String:Any]]()
    var stateArray = [[String:Any]]()
    var citiesArray = [[String:Any]]()

    private func readJson(resources: String) -> [[String:Any]] {
        var resultArray = [[String:Any]]()
        
        do {
            if let file = Bundle.main.url(forResource: resources, withExtension: "json") {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                if let object = json as? [String: Any] {
                    // json is a dictionary
                    resultArray = object[resources] as! [[String : Any]]
                    
                } else if json is [Any] {
                    // json is an array
                    
                } else {
                    print("JSON is invalid")
                }
                
                return resultArray
                
            } else {
                print("no file")
            }
        } catch {
            print(error.localizedDescription)
        }
        return resultArray
        
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()


        if (isSocial){
        
            titleLbl.text = "Complete Your Profile"
            saveBtn.setTitle("Proceed", for: .normal)
            saveArtistBtn.setTitle("Proceed", for: .normal)
            savePlaceBtn.setTitle("Proceed", for: .normal)

            backBtn.isHidden = true
            backImg.isHidden = true
        }
        
        DispatchQueue.global( qos: .default).async(execute: {() -> Void in
            self.countryArray = self.readJson(resources: "countries")
            self.finalStateArray = self.readJson(resources: "states")
            self.finalCitiesArray = self.readJson(resources: "cities")
            DispatchQueue.main.sync(execute: {() -> Void in
                
            })
        })
        
        
        let pickerView = UIPickerView()
        countryTxt.inputView = pickerView
        countryArtistTxt.inputView = pickerView
        countryPlaceTxt.inputView = pickerView
        pickerView.tag = 0
        pickerView.delegate = self
        
        
        let pickerView1 = UIPickerView()
        stateTxt.inputView = pickerView1
        statePlaceTxt.inputView = pickerView1
        stateArtistTxt.inputView = pickerView1
        pickerView1.tag = 1
        pickerView1.delegate = self
        
        let pickerView2 = UIPickerView()
        cityTxt.inputView = pickerView2
        cityPlaceTxt.inputView = pickerView2
        cityArtistTxt.inputView = pickerView2
        pickerView2.tag = 2
        pickerView2.delegate = self
        
        
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border.frame = CGRect(x: 0, y: firstNameTxt.frame.size.height - width, width:  firstNameTxt.frame.size.width, height: firstNameTxt.frame.size.height)
        border.borderWidth = width
        firstNameTxt.layer.addSublayer(border)
        firstNameTxt.layer.masksToBounds = true
        
        
        let border1 = CALayer()
        border1.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border1.frame = CGRect(x: 0, y: lastNameTxt.frame.size.height - width, width:  lastNameTxt.frame.size.width, height: lastNameTxt.frame.size.height)
        border1.borderWidth = width
        lastNameTxt.layer.addSublayer(border1)
        lastNameTxt.layer.masksToBounds = true
        
        
        
        let border2 = CALayer()
        border2.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border2.frame = CGRect(x: 0, y: emailTxt.frame.size.height - width, width:  emailTxt.frame.size.width, height: emailTxt.frame.size.height)
        border2.borderWidth = width
        emailTxt.layer.addSublayer(border2)
        emailTxt.layer.masksToBounds = true
        
        
        let border5 = CALayer()
        border5.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border5.frame = CGRect(x: 0, y: stateTxt.frame.size.height - width, width:  stateTxt.frame.size.width, height: stateTxt.frame.size.height)
        border5.borderWidth = width
        stateTxt.layer.addSublayer(border5)
        stateTxt.layer.masksToBounds = true
        
        
        let border6 = CALayer()
        border6.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border6.frame = CGRect(x: 0, y: cityTxt.frame.size.height - width, width:  cityTxt.frame.size.width, height: cityTxt.frame.size.height)
        border6.borderWidth = width
        cityTxt.layer.addSublayer(border6)
        cityTxt.layer.masksToBounds = true
        
        
        let border7 = CALayer()
        border7.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border7.frame = CGRect(x: 0, y: zipCodeTxt.frame.size.height - width, width:  zipCodeTxt.frame.size.width, height: zipCodeTxt.frame.size.height)
        border7.borderWidth = width
        zipCodeTxt.layer.addSublayer(border7)
        zipCodeTxt.layer.masksToBounds = true
        
        let border8 = CALayer()
        border8.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border8.frame = CGRect(x: 0, y: ageTxt.frame.size.height - width, width:  ageTxt.frame.size.width, height: ageTxt.frame.size.height)
        border8.borderWidth = width
        ageTxt.layer.addSublayer(border8)
        ageTxt.layer.masksToBounds = true
        
        let border9 = CALayer()
        border9.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border9.frame = CGRect(x: 0, y: maleTxt.frame.size.height - width, width:  maleTxt.frame.size.width, height: maleTxt.frame.size.height)
        border9.borderWidth = width
        maleTxt.layer.addSublayer(border9)
        maleTxt.layer.masksToBounds = true
        
        let border13 = CALayer()
        border13.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border13.frame = CGRect(x: 0, y: firstNameArtistTxt.frame.size.height - width, width:  firstNameArtistTxt.frame.size.width, height: firstNameArtistTxt.frame.size.height)
        border13.borderWidth = width
        firstNameArtistTxt.layer.addSublayer(border13)
        firstNameArtistTxt.layer.masksToBounds = true
        
        
        let border14 = CALayer()
        border14.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border14.frame = CGRect(x: 0, y: lastNameArtistTxt.frame.size.height - width, width:  lastNameArtistTxt.frame.size.width, height: lastNameArtistTxt.frame.size.height)
        border14.borderWidth = width
        lastNameArtistTxt.layer.addSublayer(border14)
        lastNameArtistTxt.layer.masksToBounds = true
        
        
        
        let border15 = CALayer()
        border15.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border15.frame = CGRect(x: 0, y: emailArtistTxt.frame.size.height - width, width:  emailArtistTxt.frame.size.width, height: emailArtistTxt.frame.size.height)
        border15.borderWidth = width
        emailArtistTxt.layer.addSublayer(border15)
        emailArtistTxt.layer.masksToBounds = true
        
        
        
        let border18 = CALayer()
        border18.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border18.frame = CGRect(x: 0, y: stateArtistTxt.frame.size.height - width, width:  stateArtistTxt.frame.size.width, height: stateArtistTxt.frame.size.height)
        border18.borderWidth = width
        stateArtistTxt.layer.addSublayer(border18)
        stateArtistTxt.layer.masksToBounds = true
        
        
        let border19 = CALayer()
        border19.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border19.frame = CGRect(x: 0, y: cityArtistTxt.frame.size.height - width, width:  cityArtistTxt.frame.size.width, height: cityArtistTxt.frame.size.height)
        border19.borderWidth = width
        cityArtistTxt.layer.addSublayer(border19)
        cityArtistTxt.layer.masksToBounds = true
        
        
        let border20 = CALayer()
        border20.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border20.frame = CGRect(x: 0, y: zipCodeArtistTxt.frame.size.height - width, width:  zipCodeArtistTxt.frame.size.width, height: zipCodeArtistTxt.frame.size.height)
        border20.borderWidth = width
        zipCodeArtistTxt.layer.addSublayer(border20)
        zipCodeArtistTxt.layer.masksToBounds = true
        
        let border21 = CALayer()
        border21.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border21.frame = CGRect(x: 0, y: ageArtistTxt.frame.size.height - width, width:  ageArtistTxt.frame.size.width, height: ageArtistTxt.frame.size.height)
        border21.borderWidth = width
        ageArtistTxt.layer.addSublayer(border21)
        ageArtistTxt.layer.masksToBounds = true
        
        let border22 = CALayer()
        border22.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border22.frame = CGRect(x: 0, y: maleArtistTxt.frame.size.height - width, width:  maleArtistTxt.frame.size.width, height: maleArtistTxt.frame.size.height)
        border22.borderWidth = width
        maleArtistTxt.layer.addSublayer(border22)
        maleArtistTxt.layer.masksToBounds = true
        
        let border23 = CALayer()
        border23.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border23.frame = CGRect(x: 0, y: bandNameArtistTxt.frame.size.height - width, width:  bandNameArtistTxt.frame.size.width, height: bandNameArtistTxt.frame.size.height)
        border23.borderWidth = width
        bandNameArtistTxt.layer.addSublayer(border23)
        bandNameArtistTxt.layer.masksToBounds = true
        
        
        let border24 = CALayer()
        border24.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border24.frame = CGRect(x: 0, y: placeNameTxt.frame.size.height - width, width:  placeNameTxt.frame.size.width, height: placeNameTxt.frame.size.height)
        border24.borderWidth = width
        placeNameTxt.layer.addSublayer(border24)
        placeNameTxt.layer.masksToBounds = true
        
        
        let border25 = CALayer()
        border25.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border25.frame = CGRect(x: 0, y: emailPlaceTxt.frame.size.height - width, width:  emailPlaceTxt.frame.size.width, height: emailPlaceTxt.frame.size.height)
        border25.borderWidth = width
        emailPlaceTxt.layer.addSublayer(border25)
        emailPlaceTxt.layer.masksToBounds = true
        
        let border28 = CALayer()
        border28.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border28.frame = CGRect(x: 0, y: statePlaceTxt.frame.size.height - width, width:  statePlaceTxt.frame.size.width, height: statePlaceTxt.frame.size.height)
        border28.borderWidth = width
        statePlaceTxt.layer.addSublayer(border28)
        statePlaceTxt.layer.masksToBounds = true
        
        
        let border29 = CALayer()
        border29.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border29.frame = CGRect(x: 0, y: cityPlaceTxt.frame.size.height - width, width:  cityPlaceTxt.frame.size.width, height: cityPlaceTxt.frame.size.height)
        border29.borderWidth = width
        cityPlaceTxt.layer.addSublayer(border29)
        cityPlaceTxt.layer.masksToBounds = true
        
        
        let border30 = CALayer()
        border30.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border30.frame = CGRect(x: 0, y: zipCodePlaceTxt.frame.size.height - width, width:  zipCodePlaceTxt.frame.size.width, height: zipCodePlaceTxt.frame.size.height)
        border30.borderWidth = width
        zipCodePlaceTxt.layer.addSublayer(border30)
        zipCodePlaceTxt.layer.masksToBounds = true
        
        
        
        let border31 = CALayer()
        border31.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border31.frame = CGRect(x: 0, y: addressTxt.frame.size.height - width, width:  addressTxt.frame.size.width, height: addressTxt.frame.size.height)
        border31.borderWidth = width
        addressTxt.layer.addSublayer(border31)
        addressTxt.layer.masksToBounds = true
        
        
        let border32 = CALayer()
        border32.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border32.frame = CGRect(x: 0, y: addressPlaceTxt.frame.size.height - width, width:  addressPlaceTxt.frame.size.width, height: addressPlaceTxt.frame.size.height)
        border32.borderWidth = width
        addressPlaceTxt.layer.addSublayer(border32)
        addressPlaceTxt.layer.masksToBounds = true
        
        
        let border33 = CALayer()
        border33.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border33.frame = CGRect(x: 0, y: addressArtistTxt.frame.size.height - width, width:  addressArtistTxt.frame.size.width, height: addressArtistTxt.frame.size.height)
        border33.borderWidth = width
        addressArtistTxt.layer.addSublayer(border33)
        addressArtistTxt.layer.masksToBounds = true
        
        
        let border34 = CALayer()
        border34.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border34.frame = CGRect(x: 0, y: countryTxt.frame.size.height - width, width:  countryTxt.frame.size.width, height: countryTxt.frame.size.height)
        border34.borderWidth = width
        countryTxt.layer.addSublayer(border34)
        countryTxt.layer.masksToBounds = true
        
        
        let border35 = CALayer()
        border35.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border35.frame = CGRect(x: 0, y: countryPlaceTxt.frame.size.height - width, width:  countryPlaceTxt.frame.size.width, height: countryPlaceTxt.frame.size.height)
        border35.borderWidth = width
        countryPlaceTxt.layer.addSublayer(border35)
        countryPlaceTxt.layer.masksToBounds = true
        
        
        let border36 = CALayer()
        border36.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border36.frame = CGRect(x: 0, y: countryArtistTxt.frame.size.height - width, width:  countryArtistTxt.frame.size.width, height: countryArtistTxt.frame.size.height)
        border36.borderWidth = width
        countryArtistTxt.layer.addSublayer(border36)
        countryArtistTxt.layer.masksToBounds = true

        maleTxt.keyboardDistanceFromTextField = 150;
        var itemLists = [NSString]()
        itemLists.append("Male")
        itemLists.append("Female")
        maleTxt.itemList = itemLists
        maleArtistTxt.keyboardDistanceFromTextField = 150;
        maleArtistTxt.itemList = itemLists
         placeView.isHidden = true
        artistView.isHidden = true
        memberView.isHidden = true
        

        if (Singleton.sharedInstance.userTypeStr == "member") {
            memberView.isHidden = false

            print(Singleton.sharedInstance.countryStr)
            firstNameTxt.text =  Singleton.sharedInstance.firstNameStr
            lastNameTxt.text = Singleton.sharedInstance.lastNameStr
            emailTxt.text = Singleton.sharedInstance.emailStr
            countryTxt.text! = Singleton.sharedInstance.countryStr
            stateTxt.text! = Singleton.sharedInstance.stateStr
            cityTxt.text = Singleton.sharedInstance.cityStr
            zipCodeTxt.text = Singleton.sharedInstance.zipcodeStr
            maleTxt.text = Singleton.sharedInstance.genderStr
            ageTxt.text = Singleton.sharedInstance.ageStr
            addressTxt.text = Singleton.sharedInstance.addressStr


            userImg.layer.cornerRadius = userImg.frame.size.height/2
            userImg.layer.borderColor = UIColor(red: 176/255, green: 176/255, blue: 176/255, alpha: 1).cgColor
            userImg.layer.borderWidth = 1.5
            userImg.clipsToBounds = true
            
            // 10 June :-
           // userImg.sd_setImage(with: URL(string: Singleton.sharedInstance.userImgStr), placeholderImage: UIImage(named: "user"))
            let urlStr : String = (Singleton.sharedInstance.userImgStr)
            let urlFormattedStr = urlStr.replacingOccurrences(of: " ", with: "%20")
            userImg.sd_setImage(with: URL(string:urlFormattedStr), placeholderImage: UIImage(named: "user"))
            
            

        } else if (Singleton.sharedInstance.userTypeStr == "artist") {
            if (UIScreen.main.bounds.size.height==568 || UIScreen.main.bounds.size.height==480) {
                scrView.contentSize = CGSize(width: 320,height: 550)
            }
            
            
            print(Singleton.sharedInstance.countryStr)
            artistView.isHidden = false
            firstNameArtistTxt.text =  Singleton.sharedInstance.firstNameStr
            lastNameArtistTxt.text = Singleton.sharedInstance.lastNameStr
            emailArtistTxt.text = Singleton.sharedInstance.emailStr
            countryArtistTxt.text = Singleton.sharedInstance.countryStr
            stateArtistTxt.text = Singleton.sharedInstance.stateStr
            cityArtistTxt.text = Singleton.sharedInstance.cityStr
            zipCodeArtistTxt.text = Singleton.sharedInstance.zipcodeStr
            maleArtistTxt.text = Singleton.sharedInstance.genderStr
            ageArtistTxt.text = Singleton.sharedInstance.ageStr
            bandNameArtistTxt.text = Singleton.sharedInstance.bandNameStr
            addressArtistTxt.text = Singleton.sharedInstance.addressStr
            
            userArtistImg.layer.cornerRadius = userArtistImg.frame.size.height/2
            userArtistImg.layer.borderColor = UIColor(red: 176/255, green: 176/255, blue: 176/255, alpha: 1).cgColor
            userArtistImg.layer.borderWidth = 1.5
            userArtistImg.clipsToBounds = true
            
            // 10 June :-
            // userArtistImg.sd_setImage(with: URL(string: Singleton.sharedInstance.userImgStr), placeholderImage: UIImage(named: "user"))
            let urlStr : String = (Singleton.sharedInstance.userImgStr)
            let urlFormattedStr = urlStr.replacingOccurrences(of: " ", with: "%20")
            userArtistImg.sd_setImage(with: URL(string:urlFormattedStr), placeholderImage: UIImage(named: "user"))
            
            
            
            
        } else if (Singleton.sharedInstance.userTypeStr == "place") {
            placeView.isHidden = false

            
            
            placeNameTxt.text =  Singleton.sharedInstance.restaurantStr
            emailPlaceTxt.text = Singleton.sharedInstance.emailStr
            statePlaceTxt.text = Singleton.sharedInstance.stateStr
            cityPlaceTxt.text = Singleton.sharedInstance.cityStr
            zipCodePlaceTxt.text = Singleton.sharedInstance.zipcodeStr
            addressPlaceTxt.text = Singleton.sharedInstance.addressStr
            countryPlaceTxt.text = Singleton.sharedInstance.countryStr
            userPlaceImg.layer.cornerRadius = userPlaceImg.frame.size.height/2
            userPlaceImg.layer.borderColor = UIColor(red: 176/255, green: 176/255, blue: 176/255, alpha: 1).cgColor
            userPlaceImg.layer.borderWidth = 1.5
            userPlaceImg.clipsToBounds = true
            
            // 10 June :-
            // userPlaceImg.sd_setImage(with: URL(string: Singleton.sharedInstance.userImgStr), placeholderImage: UIImage(named: "gallery"))
            let urlStr : String = (Singleton.sharedInstance.userImgStr)
            let urlFormattedStr = urlStr.replacingOccurrences(of: " ", with: "%20")
            userPlaceImg.sd_setImage(with: URL(string:urlFormattedStr), placeholderImage: UIImage(named: "gallery"))
            
            

        }
        
        
        self.latitude = Singleton.sharedInstance.latitudeStr
        self.longitude = Singleton.sharedInstance.longitudeStr
        
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.red
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(EditProfile.donePicker))
        
        
        toolBar.setItems([doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        countryTxt.inputAccessoryView = toolBar
        countryArtistTxt.inputAccessoryView = toolBar
        countryPlaceTxt.inputAccessoryView = toolBar
        stateTxt.inputAccessoryView = toolBar
        stateArtistTxt.inputAccessoryView = toolBar
        statePlaceTxt.inputAccessoryView = toolBar
        cityTxt.inputAccessoryView = toolBar
        cityArtistTxt.inputAccessoryView = toolBar
        cityPlaceTxt.inputAccessoryView = toolBar
        
        
        citiesArray = [[
            "id": "-8",
            "name": "Cities",
            "state_id": "-8"
            ]]
        
        
        stateArray = [[
            "id": "-8",
            "name": "State",
            "country_id": "-8"
            ]]

    }
    
    
    
    func donePicker(){
        
        countryTxt.resignFirstResponder()
        countryArtistTxt.resignFirstResponder()
        countryPlaceTxt.resignFirstResponder()
        stateTxt.resignFirstResponder()
        stateArtistTxt.resignFirstResponder()
        statePlaceTxt.resignFirstResponder()
        cityTxt.resignFirstResponder()
        cityArtistTxt.resignFirstResponder()
        cityPlaceTxt.resignFirstResponder()
        
        
    }

    //MARK: CheckCameraAuthorization
    
    func checkCameraAuthorization() {
        
        let status: AVAuthorizationStatus = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo)
        if status == .authorized {
            // authorized
            let picker = UIImagePickerController()
            picker.delegate = self
            picker.allowsEditing = true
            picker.sourceType = .camera
            present(picker, animated: true, completion: { _ in })
        } else if status == .denied {
            // denied
            if AVCaptureDevice.responds(to: #selector(AVCaptureDevice.requestAccess(forMediaType:completionHandler:))) {
                AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: {(_ granted: Bool) -> Void in
                    // Will get here on both iOS 7 & 8 even though camera permissions weren't required
                    // until iOS 8. So for iOS 7 permission will always be granted.
                    print("DENIED")
                    if granted {
                        // Permission has been granted. Use dispatch_async for any UI updating
                        // code because this block may be executed in a thread.
                        
                        DispatchQueue.main.async(execute: {() -> Void in
                            let picker = UIImagePickerController()
                            picker.delegate = self
                            picker.allowsEditing = true
                            picker.sourceType = .camera
                            self.present(picker, animated: true, completion: { _ in })
                        })
                        
                    }
                    else {
                        // Permission has been denied.
                        
                        
                        DispatchQueue.main.async(execute: {() -> Void in
                            self.accessMethod("Please go to Settings and enable the camera for this app to use this feature.")
                        })
                        
                        
                    }
                })
            }
        } else if status == .restricted {
            // restricted
            DispatchQueue.main.async(execute: {() -> Void in
                self.accessMethod("Please go to Settings and enable the camera for this app to use this feature.")
            })
        }
        else if status == .notDetermined {
            // not determined
            AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: {(_ granted: Bool) -> Void in
                if granted {
                    // Access has been granted ..do something
                    DispatchQueue.main.async(execute: {() -> Void in
                        let picker = UIImagePickerController()
                        picker.delegate = self
                        picker.allowsEditing = true
                        picker.sourceType = .camera
                        self.present(picker, animated: true, completion: { _ in })
                        
                    })
                    
                    
                }
                else {
                    // Access denied ..do something
                    DispatchQueue.main.async(execute: {() -> Void in
                        self.accessMethod("Please go to Settings and enable the camera for this app to use this feature.")
                    })
                    
                }
            })
        }
    }
    
    //MARK: CheckPhotoAuthorization
    
    func checkPhotoAuthorization()  {
        let status: PHAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
        if status == .authorized {
            let picker = UIImagePickerController()
            picker.delegate = self
            picker.allowsEditing = true
            picker.sourceType = .photoLibrary
            present(picker, animated: true, completion: { _ in })
        }
        else if status == .denied {
            DispatchQueue.main.async(execute: {() -> Void in
                self.accessMethod("Please go to Settings and enable the photos for this app to use this feature.")
            })
        } else if status == .notDetermined {
            // Access has not been determined.
            PHPhotoLibrary.requestAuthorization({(_ status: PHAuthorizationStatus) -> Void in
                if status == .authorized {
                    
                    DispatchQueue.main.async(execute: {() -> Void in
                        let picker = UIImagePickerController()
                        picker.delegate = self
                        picker.allowsEditing = true
                        picker.sourceType = .photoLibrary
                        self.present(picker, animated: true, completion: { _ in })
                    })
                    
                }
                else {
                    DispatchQueue.main.async(execute: {() -> Void in
                        self.accessMethod("Please go to Settings and enable the photos for this app to use this feature.")
                    })
                }
            })
        }
        else if status == .restricted {
            // Restricted access - normally won't happen.
        }
        
        
    }

    func accessMethod(_ message: String) {
        let alertController = UIAlertController(title: "Not Authorized", message: message, preferredStyle: .alert)
        let cancel = UIAlertAction(title: "OK", style: .default, handler: nil)
        let Settings = UIAlertAction(title: "Settings", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
        })
        alertController.addAction(Settings)
        alertController.addAction(cancel)
        present(alertController, animated: true, completion: { _ in })
    }
    
    //MARK: UIImagePickerControllerDelegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        var chosenImage: UIImage? = info[UIImagePickerControllerEditedImage] as! UIImage?
        chosenImage = resizeImage(image: chosenImage!, newWidth: 400)

        picker.dismiss(animated: true, completion: { _ in })

        if (Singleton.sharedInstance.userTypeStr == "member") {
            userImg.image = chosenImage
        } else if (Singleton.sharedInstance.userTypeStr == "artist") {
            userArtistImg.image = chosenImage
        } else if (Singleton.sharedInstance.userTypeStr == "place") {
            userPlaceImg.image = chosenImage
        }

        indicator.startAnimating()
        indicator.isHidden = false
        uploadImageDataMethod()

    }

    //MARK: Resize Image
    
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0,y: 0,width: newWidth,height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    

    // MARK: Button Actions
    
    @IBAction func Photo(_ sender: Any) {
        
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let camera = UIAlertAction(title: "Camera", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            self.checkCameraAuthorization()
        })
        let gallery = UIAlertAction(title: "Gallery", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            self.checkPhotoAuthorization()
        })
        alertController.addAction(gallery)
        alertController.addAction(camera)
        alertController.addAction(cancel)
        present(alertController, animated: true, completion: { _ in })
        
        
    }

    @IBAction func Back(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func Save(_ sender: Any) {
        
        
        if (Singleton.sharedInstance.userTypeStr == "member") {
            if firstNameTxt.text == "" || lastNameTxt.text == "" || maleTxt.text == "" || countryTxt.text == ""  || stateTxt.text == "" || cityTxt.text == "" || ageTxt.text == "" || zipCodeTxt.text == ""  || addressTxt.text == ""{
                alertViewMethod(titleStr: "", messageStr: "Please enter all fields.")
            } else {
                if case 16 ... 100 = Int(ageTxt.text!)! {
                    indicator.startAnimating()
                    indicator.isHidden = false
                    self.editProfileMethod()
                } else {
                    alertViewMethod(titleStr: "", messageStr: "User's age limit should be between 16 to 100 years.")
                }
                
            }
        } else if (Singleton.sharedInstance.userTypeStr == "artist") {
            if firstNameArtistTxt.text == "" || lastNameArtistTxt.text == "" || countryArtistTxt.text == "" || maleArtistTxt.text == "" || stateArtistTxt.text == "" || cityArtistTxt.text == "" || ageArtistTxt.text == "" || zipCodeArtistTxt.text == ""  || bandNameArtistTxt.text == "" || addressArtistTxt.text == ""{
                alertViewMethod(titleStr: "", messageStr: "Please enter all fields.")
            } else {
                
                if case 16 ... 100 = Int(ageArtistTxt.text!)! {
                    indicator.startAnimating()
                    indicator.isHidden = false
                    self.editProfileMethod()
                } else {
                    alertViewMethod(titleStr: "", messageStr: "User's age limit should be between 16 to 100 years.")
                }

                
                           }
        } else if (Singleton.sharedInstance.userTypeStr == "place") {
            
            if placeNameTxt.text == "" || statePlaceTxt.text == "" || cityPlaceTxt.text == "" || zipCodePlaceTxt.text == ""  || addressPlaceTxt.text == ""  || countryPlaceTxt.text == ""{
                alertViewMethod(titleStr: "", messageStr: "Please enter all fields.")
            } else {
                indicator.startAnimating()
                indicator.isHidden = false
                self.editProfileMethod()
            }
        }
        

        
        
        
//        if (Singleton.sharedInstance.userTypeStr == "member") {
//           
//        } else if (Singleton.sharedInstance.userTypeStr == "artist") {
//           
//        } else if (Singleton.sharedInstance.userTypeStr == "place") {
//          
//        }
//        
//        _ = navigationController?.popViewController(animated: true)
//
//        let alert = UIAlertController(title: "",message: "Profile has been successfully updated!",preferredStyle: UIAlertControllerStyle.alert)
//        let cancelAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
//        alert.addAction(cancelAction)
//        present(alert, animated: true)
//        
    }

    
    
    
    // MARK: Edit Profile Method
    
    // Upload Photo
    func uploadImageDataMethod()  {
        
        
        uniquePhotoNameStr = UUID().uuidString
        uniquePhotoNameStr = "http://beta.brstdev.com/freedrinkz/uploads/\(uniquePhotoNameStr).jpeg"
        print(uniquePhotoNameStr)

        
        let parameters = [
            "profile_pic": uniquePhotoNameStr,
            "image_type": "1",
            "user_id":Singleton.sharedInstance.userIdStr,
            "auth_token":Singleton.sharedInstance.userTokenStr,
            "is_register": "1"
        ]
        
        
        
        var userImg = UIImage()
        
        if (Singleton.sharedInstance.userTypeStr == "member") {
            userImg = self.userImg.image!
        } else if (Singleton.sharedInstance.userTypeStr == "artist") {
            userImg = self.userArtistImg.image!
        } else if (Singleton.sharedInstance.userTypeStr == "place") {
            userImg = self.userPlaceImg.image!
        }
        
        
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(UIImagePNGRepresentation(userImg)!, withName: "profile_pic", fileName: self.uniquePhotoNameStr, mimeType: "image/jpeg")
            for (key, value) in parameters {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
        }, to:"http://beta.brstdev.com/freedrinkz/api/web/v1/users/getprofilepic")
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    //Print progress
                    //  print(progress)
                    
                })
                
                upload.responseJSON { response in
                    //print response.result
                    print(response)
                    
                    if response.result.value != nil {
                        
                        let json = JSON(data: response.data! )
                        if let descriptionStr = json["description"].string {
                            print(descriptionStr)
                            if descriptionStr == "Image uploads Successfully" {
                                
                                let image = json["data"]["profile_pic_url"].string
                                self.uniquePhotoNameStr = image!
                                Singleton.sharedInstance.userImgStr = self.uniquePhotoNameStr
                                print(self.uniquePhotoNameStr)
                                
                            }
                        }
                        
                    }
                    
                    self.indicator.stopAnimating()
                    self.indicator.isHidden = true
                }
                
            case .failure(let encodingError):
                print(encodingError.localizedDescription)
                self.indicator.stopAnimating()
                self.indicator.isHidden = true
                break
                //print encodingError.description
                
            }
        }
    }
    
    
    // MARK: GeoCodeAddressString
    
    func getLatLongFromAddress(location: String)  {
        var address = ""
        print(location)
        if (Singleton.sharedInstance.userTypeStr == "member") {
            address = addressTxt.text!
        } else if (Singleton.sharedInstance.userTypeStr == "artist") {
            address = addressArtistTxt.text!
        } else if (Singleton.sharedInstance.userTypeStr == "place") {
            address = addressPlaceTxt.text!
        }
        
        let geocoder = CLGeocoder()
        geocoder.geocodeAddressString(location) { (placemarks, error) in
            if let validPlacemark = placemarks?[0] {
                print(validPlacemark)
                print(validPlacemark.location!.coordinate.latitude)
                print(validPlacemark.location!.coordinate.longitude)
                self.latitude = String(describing:validPlacemark.location!.coordinate.latitude)
                self.longitude = String(describing:validPlacemark.location!.coordinate.longitude)
            } else {
                self.latitude = "0.0"
                self.longitude = "0.0"
            }
            //self.editProfileMethod()
        }
    }

    
    
    func editProfileMethod () {
        
        var json = [String : Any]()
        
        
        Singleton.sharedInstance.countryStr = countryArtistTxt.text!
        if (Singleton.sharedInstance.userTypeStr == "member") {
            
            json = ["user_id":Singleton.sharedInstance.userIdStr,
                    "auth_token":Singleton.sharedInstance.userTokenStr,
                    "firstname":firstNameTxt.text!,
                    "lastname":lastNameTxt.text!,
                    "gender":maleTxt.text!,
                    "state":stateTxt.text!,
                    "city":cityTxt.text!,
                    "age":ageTxt.text!,
                    "zip_code":zipCodeTxt.text!,
                    "profile_pic": Singleton.sharedInstance.userImgStr,
                    "resturant_name": " ",
                    "band_name": " ",
                    "user_type":"1",
                    "address": addressTxt.text!,
                    "latitude": latitude,
                    "longitude": longitude,
                    "country": countryTxt.text!
                ] as [String : Any]
            
            
        } else if (Singleton.sharedInstance.userTypeStr == "artist") {
            
            json = ["user_id":Singleton.sharedInstance.userIdStr,
                    "auth_token":Singleton.sharedInstance.userTokenStr,
                    "firstname":firstNameArtistTxt.text!,
                    "lastname":lastNameArtistTxt.text!,
                    "gender":maleArtistTxt.text!,
                    "state":stateArtistTxt.text!,
                    "city":cityArtistTxt.text!,
                    "age":ageArtistTxt.text!,
                    "zip_code":zipCodeArtistTxt.text!,
                    "profile_pic": Singleton.sharedInstance.userImgStr,
                    "resturant_name": " ",
                    "band_name": bandNameArtistTxt.text!,
                    "user_type":"3",
                    "address": addressArtistTxt.text!,
                    "latitude": latitude,
                    "longitude": longitude,
                    "country": countryArtistTxt.text!
                ] as [String : Any]
            
            
        } else if (Singleton.sharedInstance.userTypeStr == "place") {
            
            json = ["user_id":Singleton.sharedInstance.userIdStr,
                    "auth_token":Singleton.sharedInstance.userTokenStr,
                    "firstname":" ",
                    "lastname":" ",
                    "gender":" ",
                    "state":statePlaceTxt.text!,
                    "city":cityPlaceTxt.text!,
                    "age":"16",
                    "zip_code":zipCodePlaceTxt.text!,
                    "profile_pic": Singleton.sharedInstance.userImgStr,
                    "resturant_name": placeNameTxt.text!,
                    "band_name": " ",
                    "user_type":"2",
                    "address": addressPlaceTxt.text!,
                    "latitude": latitude,
                    "longitude": longitude,
                    "country": countryPlaceTxt.text!
                
                ] as [String : Any]
            
            
        }

        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/users/updateprofile", method: .put, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    let json = JSON(data: response.data! )
                    if let descriptionStr = json["description"].string {
                        print(descriptionStr)
                        if descriptionStr == "User already exists!" {
                            self.alertViewMethod(titleStr: "", messageStr: "The email address is already in use by another account.")
                        } else if descriptionStr == "Profile Update Successfully" {
                            
                            let userId = String(describing: json["data"]["user_id"].int!)
                            Singleton.sharedInstance.userIdStr = userId
                            
                            
                            let auth_token = json["data"]["auth_token"].string
                            Singleton.sharedInstance.userTokenStr = auth_token!
                            
                            let userType = json["data"]["user_type"].string
                            
                            Singleton.sharedInstance.userTypeIdStr = userType!
                            
                            UserDefaults.standard.setValue(auth_token!, forKey: "auth_token")
                            UserDefaults.standard.setValue(userId, forKey: "user_id")
                            UserDefaults.standard.setValue(userType!, forKey: "user_type")

                            
                            print(userType!)
                            if userType == "1" {
                                print("member")
                                Singleton.sharedInstance.userTypeStr = "member"
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let vc = storyboard.instantiateViewController(withIdentifier: "Profile") as! Profile
                                self.navigationController?.pushViewController(vc,animated: true)
                                
                            } else if userType == "2" {
                                print("place")
                                Singleton.sharedInstance.userTypeStr = "place"
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let vc = storyboard.instantiateViewController(withIdentifier: "PlaceProfile") as! PlaceProfile
                                self.navigationController?.pushViewController(vc,animated: true)
                                
                                
                            }else if userType == "3" {
                                print("artist")
                                Singleton.sharedInstance.userTypeStr = "artist"
                                
                                
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let vc = storyboard.instantiateViewController(withIdentifier: "ArtistProfile") as! ArtistProfile
                                self.navigationController?.pushViewController(vc,animated: true)
                                
                            }
                            
                            
                            
                            
                        } else if descriptionStr == "Wrong auth token" {
                            
                            UserDefaults.standard.setValue(nil, forKey: "auth_token")
                            UserDefaults.standard.setValue(nil, forKey: "user_id")
                            UserDefaults.standard.setValue(nil, forKey: "user_type")
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "Login") as! Login
                            vc.isExpired = true
                            self.navigationController?.pushViewController(vc,animated: true)
                            
                        }

                        //Now you got your value
                    }
                } else if((response.error) != nil){
                    
                    self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                    
                }

                
                self.indicator.stopAnimating()
                self.indicator.isHidden = true
                
        }
        
    }
    

    
    // MARK: AlertView
    
    func alertViewMethod(titleStr:String, messageStr:String) {
        let alert = UIAlertController(title: titleStr, message: messageStr, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    

    
    // MARK: Text Field Delegate
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        firstNameTxt.resignFirstResponder()
        lastNameTxt.resignFirstResponder()
        emailTxt.resignFirstResponder()
        stateTxt.resignFirstResponder()
        cityTxt.resignFirstResponder()
        zipCodeTxt.resignFirstResponder()
        ageTxt.resignFirstResponder()
        firstNameArtistTxt.resignFirstResponder()
        lastNameArtistTxt.resignFirstResponder()
        emailArtistTxt.resignFirstResponder()
        stateArtistTxt.resignFirstResponder()
        cityArtistTxt.resignFirstResponder()
        ageArtistTxt.resignFirstResponder()
        bandNameArtistTxt.resignFirstResponder()
        placeNameTxt.resignFirstResponder()
        emailPlaceTxt.resignFirstResponder()
        statePlaceTxt.resignFirstResponder()
        cityPlaceTxt.resignFirstResponder()
        zipCodePlaceTxt.resignFirstResponder()
        
    }
    
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool  {
        textField.resignFirstResponder()
        return true;
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == addressTxt || textField == addressPlaceTxt || textField == addressArtistTxt {
            //   Present the Autocomplete view controller when the button is pressed.
           // let autocompleteController = GMSAutocompleteViewController()
           // autocompleteController.delegate = self
           // present(autocompleteController, animated: true, completion: nil)
        }

        scrView.isScrollEnabled = false
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        scrView.isScrollEnabled = true
    }


    //MARK: Goole Place Delegate Methods
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        
        // Get the address components.
        if let addressLines = place.addressComponents {
            // Populate all of the address fields we can find.
            for field in addressLines {
                switch field.type {
                case kGMSPlaceTypeCountry:
                 countryStr = field.name
                    
                // Print the items we aren't using.
                default:
                    print("Type: \(field.type), Name: \(field.name)")
                }
            }
        }

        if (Singleton.sharedInstance.userTypeStr == "member") {
               // addressTxt.text = place.name

            } else if (Singleton.sharedInstance.userTypeStr == "artist") {
               // addressArtistTxt.text = place.name

            } else if (Singleton.sharedInstance.userTypeStr == "place") {
               // addressPlaceTxt.text = place.name

            }
        self.latitude = String(describing:place.coordinate.latitude)
        self.longitude = String(describing:place.coordinate.longitude)
        
        print("Place name: \(place.name)")
        print("Place name: \(place.coordinate)")
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: UIPickerView Methods
    
    // returns the number of 'columns' to display.
    func numberOfComponentsInPickerView(pickerView: UIPickerView!) -> Int{
        return 1
    }
    
    // returns the # of rows in each component..
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        if pickerView.tag == 0 {
            return countryArray.count
        } else if (pickerView.tag == 1) {
            return stateArray.count
        }
        return citiesArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 0 {
            return (countryArray[row]["name"] as! String)
        } else if (pickerView.tag == 1) {
            return (stateArray[row]["name"] as! String)
        }
        return (citiesArray[row]["name"] as! String)
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 0 {
            
            if (countryArray[row]["name"] as! String) != "Country" {
                countryTxt.text = (countryArray[row]["name"] as! String)
                countryPlaceTxt.text = (countryArray[row]["name"] as! String)
                countryArtistTxt.text = (countryArray[row]["name"] as! String)
                Singleton.sharedInstance.countryStr = countryTxt.text!
                
            } else {
                countryTxt.text = ""
                countryPlaceTxt.text = ""
                countryArtistTxt.text = ""
                
            }
            
            
            let predicate = NSPredicate(format: "country_id == %@", (countryArray[row]["id"] as! String))
            let array = NSMutableArray(array: finalStateArray.filter { predicate.evaluate(with: $0) } )
            print(array)
            stateArray = array as! [[String : Any]];
            
            let pickerView11 = UIPickerView()
            stateTxt.inputView = pickerView11
            stateArtistTxt.inputView = pickerView11
            statePlaceTxt.inputView = pickerView11
            pickerView11.tag = 1
            pickerView11.delegate = self
            
            let pickerView12 = UIPickerView()
            cityTxt.inputView = pickerView12
            cityArtistTxt.inputView = pickerView12
            cityPlaceTxt.inputView = pickerView12
            pickerView12.tag = 2
            pickerView12.delegate = self
            
            
            stateTxt.text = ""
            stateArtistTxt.text = ""
            statePlaceTxt.text = ""
            cityTxt.text = ""
            cityArtistTxt.text = ""
            cityPlaceTxt.text = ""
            
            stateArray.insert([
                "id": "-8",
                "name": "State",
                "country_id": "-8"
                ], at: 0)
            citiesArray = [[
                "id": "-8",
                "name": "Cities",
                "state_id": "-8"
                ]]
            
        } else if (pickerView.tag == 1) {
            if (stateArray[row]["name"] as! String) != "State" {
                stateTxt.text = (stateArray[row]["name"] as! String)
                stateArtistTxt.text = (stateArray[row]["name"] as! String)
                statePlaceTxt.text = (stateArray[row]["name"] as! String)
            } else {
                stateTxt.text = ""
                stateArtistTxt.text = ""
                statePlaceTxt.text = ""
            }
            
            let predicate = NSPredicate(format: "state_id == %@", (stateArray[row]["id"] as! String))
            let array = NSMutableArray(array: finalCitiesArray.filter { predicate.evaluate(with: $0) } )
            print(array)
            citiesArray = array as! [[String : Any]];
            
            let pickerView2 = UIPickerView()
            cityTxt.inputView = pickerView2
            pickerView2.tag = 2
            pickerView2.delegate = self as! UIPickerViewDelegate
            
            cityTxt.text = ""
            cityArtistTxt.text = ""
            cityPlaceTxt.text = ""
            
            citiesArray.insert([
                "id": "-8",
                "name": "Cities",
                "state_id": "-8"
                ], at: 0)
        } else {
            
            if (citiesArray[row]["name"] as! String) != "Cities" {
                cityTxt.text = (citiesArray[row]["name"] as! String)
                cityArtistTxt.text = (citiesArray[row]["name"] as! String)
                cityPlaceTxt.text = (citiesArray[row]["name"] as! String)
                
                self.getLatLongFromAddress(location: Singleton.sharedInstance.countryStr+","+stateTxt.text!)
            } else {
                cityTxt.text = ""
                cityArtistTxt.text = ""
                cityPlaceTxt.text = ""
            }
            
            
        }
    }
    
    
    
}
