//
//  WhosBuying.swift
//  FreeDrinkz
//
//  Created by Brst-Pc109 on 04/04/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class WhosBuying: UIViewController,UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate {
    
    //Outlets
    @IBOutlet weak var imBuyimgTbl: UITableView!
    @IBOutlet weak var detailTbl: UITableView!
    @IBOutlet weak var detailView: UIView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var addView: UIView!
    
    
    
    //Variables
    var requestArray = [JSON]()
    var requestDetailsArray = [JSON]()
    var requestIdStr = String()
    var isChecked = Int()
    var type = String()
    var restroId = String()
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if type == "all"{
            
            indicator.startAnimating()
            indicator.isHidden = false
            getRequestMethod()
            
        }
        

        if (Singleton.sharedInstance.userTypeStr == "member") {
            
            if Singleton.sharedInstance.checkInStatus == true{
                indicator.startAnimating()
                indicator.isHidden = false
                getRequestMethod()
            } else {
                //statusLbl.isHidden = false
            }
            
        } else if(Singleton.sharedInstance.userTypeStr == "place") {
            
            addView.isHidden = true
            indicator.startAnimating()
            indicator.isHidden = false
            getRequestMethod()
            
        }
        
        
        detailView.isHidden = true
        
        isChecked = 0
       
        self.LongPressGesture()
        
    }
    
    func LongPressGesture() {
        //Long Press
        let longPressGesture:UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
        longPressGesture.minimumPressDuration = 0.5
        longPressGesture.delegate = self
        self.imBuyimgTbl.addGestureRecognizer(longPressGesture)
    }

    func handleLongPress(longPressGesture:UILongPressGestureRecognizer) {
        
        let p = longPressGesture.location(in: self.imBuyimgTbl)
        let indexPath = self.imBuyimgTbl.indexPathForRow(at: p)
        
        let alert = UIAlertController(title:"Are you sure you want to delete?", message:"", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
            
            print("Long press on row, at \(indexPath!.row)")
            
            if indexPath != nil {
                print("Long press on table view, not row.")
                let id =  self.requestArray[(indexPath?.row)!]["id"].int
                self.iamBuyingMethod(requestId:id!)
                
            }
            
            
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler:nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    // Mark: Pop Back
    
    @IBAction func popBack(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    
    // MARK: Get Requests
    
    func getRequestMethod () {
        
        
        var json = [String : Any]()
        
        
        if(Singleton.sharedInstance.userTypeStr == "place") {
            json = ["user_id":Singleton.sharedInstance.userIdStr,
                    "auth_token":Singleton.sharedInstance.userTokenStr,
                    "resturant_id":Singleton.sharedInstance.userIdStr
                ] as [String : Any]
            print(json)
            
        } else {
            json = ["user_id":Singleton.sharedInstance.userIdStr,
                    "auth_token":Singleton.sharedInstance.userTokenStr,
                    "resturant_id":Singleton.sharedInstance.checkInRestId
                ] as [String : Any]
            print(json)
        }
        
        print(json)
        
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/members/whosbuyinglisting", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    let json = JSON(data: response.data! )
                    if let descriptionStr = json["description"].string {
                        print(descriptionStr)
                        if descriptionStr == "Wrong auth token" {
                            
                            
                            UserDefaults.standard.setValue(nil, forKey: "auth_token")
                            UserDefaults.standard.setValue(nil, forKey: "user_id")
                            UserDefaults.standard.setValue(nil, forKey: "user_type")
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "Login") as! Login
                            vc.isExpired = true
                            self.navigationController?.pushViewController(vc,animated: true)
                            
                        }  else if descriptionStr == "Who_Buying_listing" {
                            
                            self.requestArray = json["data"]["Who_Buying_listing"].array!
                            if self.requestArray.count == 0 {
                                self.statusLbl.isHidden = false;
                                self.statusLbl.text = "There is no request posted in Who's Buying Request section. Click on button + to post new request."

                                if(Singleton.sharedInstance.userTypeStr == "place") {
                                    self.statusLbl.text = "There is no request posted in Who's Buying Request section."
                                }

                            } else {
                                self.statusLbl.isHidden = true;
                            }
                            print(self.requestArray.count)
                        }
                        
                        self.imBuyimgTbl.reloadData()
                        //Now you got your value
                    }
                } else if((response.error) != nil){
                    
                    self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                    
                }
                
                self.indicator.stopAnimating()
                self.indicator.isHidden = true
                
        }
        
    }
    
    
    // MARK: Get Own Request Details
    
    func viewDetailsMethod () {
        
        
        let json  = ["user_id":Singleton.sharedInstance.userIdStr,
                     "auth_token":Singleton.sharedInstance.userTokenStr,
                     "request_id":requestIdStr
            ] as [String : Any]
        
        
        print(json)
        
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/members/whotestlisting", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    let json = JSON(data: response.data! )
                    if let descriptionStr = json["description"].string {
                        print(descriptionStr)
                        if descriptionStr == "Wrong auth token" {
                            
                            
                            UserDefaults.standard.setValue(nil, forKey: "auth_token")
                            UserDefaults.standard.setValue(nil, forKey: "user_id")
                            UserDefaults.standard.setValue(nil, forKey: "user_type")
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "Login") as! Login
                            vc.isExpired = true
                            self.navigationController?.pushViewController(vc,animated: true)
                            
                        }  else if descriptionStr == "Who_Buying_send_listing" {
                            
                            self.requestDetailsArray = json["data"]["Who_Buying_send_listing"].array!
                            if self.requestDetailsArray.count == 0 {
                                self.statusLbl.isHidden = false;
                                self.statusLbl.text = "There is no respond corresponding to this request."
                            } else {
                                self.statusLbl.isHidden = true;
                            }
                            print(self.requestDetailsArray.count)
                        }
                        
                        self.detailTbl.reloadData()
                        //Now you got your value
                    }
                } else if((response.error) != nil){
                    
                    self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                    
                }
                
                self.indicator.stopAnimating()
                self.indicator.isHidden = true
                
        }
        
    }
    

    
    //MARK: UITableView Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.tag == 1 {
            return requestDetailsArray.count
            
        }
        return self.requestArray.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if tableView.tag == 1 {
            
            let cell:BuyingDetailCell = detailTbl.dequeueReusableCell(withIdentifier: "BuyingDetailCell") as! BuyingDetailCell
            
            // 3 July :-
           // if requestDetailsArray[indexPath.row]["user_type"].string! == "1" {
              if requestDetailsArray[indexPath.row]["user_type"].string! == "1" || requestDetailsArray[indexPath.row]["user_type"].string! == "3"   {
                
                cell.memberNameLbl.text = "\(requestDetailsArray[indexPath.row]["firstname"].string!) \(requestDetailsArray[indexPath.row]["lastname"].string!)"
                
            } else if requestDetailsArray[indexPath.row]["user_type"].string! == "2" {
                
                cell.memberNameLbl.text = requestDetailsArray[indexPath.row]["resturant_name"].string!
            }

            // 10 June :-
           // cell.memberImg.sd_setImage(with: URL(string: requestDetailsArray[indexPath.row]["profile_pic_url"].string!), placeholderImage: UIImage(named: "user"))
            let urlStr : String = (requestDetailsArray[indexPath.row]["profile_pic_url"].string!)
            let urlFormattedStr = urlStr.replacingOccurrences(of: " ", with: "%20")
            cell.memberImg.sd_setImage(with: URL(string:urlFormattedStr), placeholderImage: UIImage(named: "user"))
            
            
            
            cell.locationTxt.text = requestDetailsArray[indexPath.row]["address"].string!
            let city = requestDetailsArray[indexPath.row]["city"].string!
            let state = requestDetailsArray[indexPath.row]["state"].string!
            let zipCode = requestDetailsArray[indexPath.row]["zip_code"]
            cell.locationTxt.text = "\(cell.locationTxt.text!), \(city), \(state), \(zipCode)"
            

            cell.profileBtn.addTarget(self, action: #selector(viewProfileDetails(_:)), for: .touchUpInside)
            cell.profileBtn.tag = indexPath.row

            
            return cell
            
        }

        let cell:ImBuyingCell = imBuyimgTbl.dequeueReusableCell(withIdentifier: "ImBuyingCell") as! ImBuyingCell
        
        cell.requestBtn.setTitle("I'll Buy", for: UIControlState.normal)
        cell.requestBtn.backgroundColor = UIColor(red: 36/255, green: 69/255, blue: 138/255, alpha: 1)

        cell.memberNameLbl.text = "\(requestArray[indexPath.row]["firstname"].string!) \(requestArray[indexPath.row]["lastname"].string!)"
        
        // 10 June :-
       // cell.memberImg.sd_setImage(with: URL(string: requestArray[indexPath.row]["profile_pic_url"].string!), placeholderImage: UIImage(named: "user"))
        let urlStr : String = (requestArray[indexPath.row]["profile_pic_url"].string!)
        let urlFormattedStr = urlStr.replacingOccurrences(of: " ", with: "%20")
        cell.memberImg.sd_setImage(with: URL(string:urlFormattedStr), placeholderImage: UIImage(named: "user"))
        
        

        cell.locationTxt.text = requestArray[indexPath.row]["address"].string!
        let city = requestArray[indexPath.row]["city"].string!
        let state = requestArray[indexPath.row]["state"].string!
        let zipCode = requestArray[indexPath.row]["zip_code"]
        cell.locationTxt.text = "\(cell.locationTxt.text!), \(city), \(state), \(zipCode)"
        
     
        if String(describing: requestArray[indexPath.row]["user_id"].int!) == Singleton.sharedInstance.userIdStr {
            cell.requestBtn.setTitle("View Details", for: UIControlState.normal)
        }

        cell.profileBtn.addTarget(self, action: #selector(viewProfile(_:)), for: .touchUpInside)
        cell.profileBtn.tag = indexPath.row


        cell.requestBtn.addTarget(self, action: #selector(BuyAction(_:)), for: .touchUpInside)
        cell.requestBtn.tag = indexPath.row

        
        return cell
    }
    
//    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool{
//
//        if String(describing: requestArray[indexPath.row]["user_id"].int!) == Singleton.sharedInstance.userIdStr {
//
//            return true
//
//
//        }
//
//        return false
//    }
//
//
//    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
//
//
//       if String(describing: requestArray[indexPath.row]["user_id"].int!) == Singleton.sharedInstance.userIdStr{
//
//            if (editingStyle == UITableViewCellEditingStyle.delete) {
//
//                let alert = UIAlertController(title:"", message:"Are you sure you want to delete?", preferredStyle: UIAlertControllerStyle.alert)
//                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: {(action:UIAlertAction!) in
//
//                    let id =  self.requestArray[indexPath.row]["id"].int
//                    self.iamBuyingMethod(requestId:id!)
//
//                }))
//                alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil))
//                self.present(alert, animated: true, completion: nil)
//
//            }
//
//        }
//
    //}
    
    //MARK: deleting i am buying method
    func iamBuyingMethod (requestId: Int) {
        
        self.view.isUserInteractionEnabled = false
        
        let json = ["user_id":Singleton.sharedInstance.userIdStr,
                    "auth_token":Singleton.sharedInstance.userTokenStr,
                    "delete_id":requestId
            ] as [String : Any]
        
        print(json)
        
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/members/deletewhoisbuying", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
                self.view.isUserInteractionEnabled = true
                print(response.result)   // result of response serialization
                print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    self.getRequestMethod ()
                    
                    if((response.error) != nil){
                        self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                    }
                    self.indicator.stopAnimating()
                    self.indicator.isHidden = true
                }
                
        }
        
    }
    
    
    // MARK: TableView Buttons Action
     func viewProfileDetails(_ button: UIButton) {
        
        
        if requestDetailsArray[button.tag]["user_type"].string! == "1" {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "Profile") as! Profile
            if String(describing: requestDetailsArray[button.tag]["user_id"].int!) != Singleton.sharedInstance.userIdStr {
                vc.fromNavStr = "other"
                vc.fromViewControllerStr = "WhosBuying"
            }
            vc.memberProfileId = String(describing: requestDetailsArray[button.tag]["user_id"].int!)
            navigationController?.pushViewController(vc,animated: true)
            
        } else if requestDetailsArray[button.tag]["user_type"].string! == "2" {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "PlaceProfile") as! PlaceProfile
            if String(describing: requestDetailsArray[button.tag]["user_id"].int!) != Singleton.sharedInstance.userIdStr {
                vc.fromNavStr = "other"
            }
            vc.restaurantId = String(describing: requestDetailsArray[button.tag]["user_id"].int!)
            navigationController?.pushViewController(vc,animated: true)
        }

    }

    
    func BuyAction(_ button: UIButton) {
        
        
        if String(describing: requestArray[button.tag]["user_id"].int!) != Singleton.sharedInstance.userIdStr {
            
            // 21 June :-
            UserDefaults.standard.set(true, forKey: "whosbuying")
            UserDefaults.standard.synchronize()
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "Menu") as! Menu
            
            
            
            // 21 June :-
           // vc.recieverIdStr = recieverIdStr
            
            
            if(Singleton.sharedInstance.userTypeStr == "place") {
                vc.otherRestaurantId = Singleton.sharedInstance.userIdStr
            } else {
                vc.otherRestaurantId = Singleton.sharedInstance.checkInRestId
            }
            vc.isOther = true
            vc.isSendDrink = true
            vc.isAcceptWhos = true
            
          // 21 June :- (To Check which is reciever-id and in who's Buying case login id is user-id)
            vc.requestIdStr = String(describing: requestArray[button.tag]["id"].int!)
            print(vc.requestIdStr)
            
            vc.recieverIdStr = String(describing: requestArray[button.tag]["user_id"].int!)
            print(vc.recieverIdStr)
            
            // 21 June :-
            vc.recieverIdStrW = String(describing: requestArray[button.tag]["user_id"].int!)
            
           
            // saving value in nsuserdefaults
            UserDefaults.standard.set(isChecked, forKey: "ThreeKey")
            UserDefaults.standard.synchronize()
            
            // 25 June :-
           
            let cart_idd = UserDefaults.standard.value(forKey: "cart_id")
            
            if cart_idd != nil  {
                
                let alert = UIAlertController(title: "You have already saved cart", message: "Click OK to view cart.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                    
                    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                    let CartDetailVC = storyBoard.instantiateViewController(withIdentifier: "CartDetailVC") as! CartDetailVC
                    self.navigationController?.pushViewController(CartDetailVC,animated: true)
                } )); self.present(alert, animated: true)
            }
                
                
            else {
                navigationController?.pushViewController(vc,animated: true)
            }
            // 25 June :-
            // navigationController?.pushViewController(vc,animated: true)
        }
        
        else {
            indicator.isHidden = false
            indicator.startAnimating()
            requestIdStr = String(describing: requestArray[button.tag]["id"].int!)
            viewDetailsMethod()
            detailView.isHidden = false
        }

    }

    
    func viewProfile(_ button: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "Profile") as! Profile
        if String(describing: requestArray[button.tag]["user_id"].int!) != Singleton.sharedInstance.userIdStr {
            vc.fromNavStr = "other"
            vc.fromViewControllerStr = "WhosBuying"
        }
        vc.memberProfileId = String(describing: requestArray[button.tag]["user_id"].int!)
        navigationController?.pushViewController(vc,animated: true)
    }
    
   //MARK: Button Actions
    @IBAction func Cancel(_ sender: Any) {
        detailView.isHidden = true
        statusLbl.isHidden=true
    }

    @IBAction func Add(_ sender: Any) {
        
        if (Singleton.sharedInstance.checkInRestId) == (restroId){
            
            postRequestMethod()
            indicator.startAnimating()
            indicator.isHidden = false
            
        }
        else{
            
            self.alertViewMethod(titleStr: "", messageStr: "You have not checked in a restaurant")
            
        }
    }
    
    
    // MARK: Post Request Method
    
    func postRequestMethod () {
        
        self.view.isUserInteractionEnabled = false
        
        let json = ["user_id":Singleton.sharedInstance.userIdStr,
                    "auth_token":Singleton.sharedInstance.userTokenStr,
                    "resturant_id":Singleton.sharedInstance.checkInRestId
                ] as [String : Any]
        print(json)
        
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/members/whosbuying", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
                self.view.isUserInteractionEnabled = true
                print(response.result)   // result of response serialization
                print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    let json = JSON(data: response.data! )
                    if let descriptionStr = json["description"].string {
                        print(descriptionStr)
                        if descriptionStr == "Wrong auth token" {
                            
                            
                            UserDefaults.standard.setValue(nil, forKey: "auth_token")
                            UserDefaults.standard.setValue(nil, forKey: "user_id")
                            UserDefaults.standard.setValue(nil, forKey: "user_type")
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "Login") as! Login
                            vc.isExpired = true
                            self.navigationController?.pushViewController(vc,animated: true)
                            
                        }  else if descriptionStr == "You have already posted the request" {
                            self.alertViewMethod(titleStr: "", messageStr: "You have already posted the request!")
                        }   else if descriptionStr == "You have posted the request" {
                          //  self.alertViewMethod(titleStr: "", messageStr: "You have been successfully posted!")
                            self.indicator.startAnimating()
                            self.indicator.isHidden = false
                            self.getRequestMethod()
                            
                        }
                        
                        self.imBuyimgTbl.reloadData()
                        //Now you got your value
                    }
                } else if((response.error) != nil){
                    
                    self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                    
                }
                
                self.indicator.stopAnimating()
                self.indicator.isHidden = true
                
        }
        
    }
    
    // MARK: AlertView
    
    func alertViewMethod(titleStr:String, messageStr:String) {
        let alert = UIAlertController(title: titleStr, message: messageStr, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    }
