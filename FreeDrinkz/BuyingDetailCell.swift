//
//  BuyingDetailCell.swift
//  FreeDrinkz
//
//  Created by Brst-Pc109 on 04/04/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

import UIKit

class BuyingDetailCell: UITableViewCell {
    @IBOutlet weak var memberImg: UIImageView!
    @IBOutlet weak var memberNameLbl: UILabel!
    @IBOutlet weak var acceptBtn: UIButton!
    @IBOutlet weak var rejectBtn: UIButton!
    @IBOutlet weak var profileBtn: UIButton!
    @IBOutlet weak var locationTxt: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
