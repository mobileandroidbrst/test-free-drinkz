//
//  CardDetailTableViewCell.swift
//  FreeDrinkz
//
//  Created by Brst981 on 04/10/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

import UIKit

class CardDetailTableViewCell: UITableViewCell {

    
    
    @IBOutlet var lblCardNumberLabel: UILabel!
    
    @IBOutlet var lblCardNumber: UILabel!
    
    
    @IBOutlet var lblCardTypeLabel: UILabel!
    
    
    @IBOutlet var lblCardName: UILabel!
    
   @IBOutlet var lblCardTypeAddPayment: UILabel!
    
    @IBOutlet var lblCardNumAddPayment: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
