//
//  SettingsCell.swift
//  FreeDrinkz
//
//  Created by Brst-Pc109 on 13/07/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

import UIKit

class SettingsCell: UITableViewCell {
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var onSwitch: UISwitch!
    @IBOutlet weak var arrowImg: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
