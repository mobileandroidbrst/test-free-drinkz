//
//  Singleton.swift
//  FreeDrinkz
//
//  Created by Brst-Pc109 on 28/03/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

import UIKit
import SwiftyJSON
import CoreLocation


class Singleton: NSObject{
    var userTypeStr = String()
    var userIdStr = String()
    var userTokenStr = String()
    var userImgStr = String()
    var firstNameStr = String()
    var lastNameStr = String()
    var emailStr = String()
    var stateStr = String()
    var cityStr = String()
    var countryStr = String()
    var zipcodeStr = String()
    var ageStr = String()
    var genderStr = String()
    var restaurantStr = String()
    var bandNameStr = String()
    var addressStr = String()
    var latitudeStr = String()
    var longitudeStr = String()
    var userTypeIdStr = String()
    var deviceTokenStr = String()
    var isOpenPhotos = Bool()
    var checkInStatus = Bool()
    var checkInRestId = String()
    var socialLoginType = String()
    var blockArray =  [[String: Any]]()
    var categoryArray =  [[String: Any]]()
    var editPerformTime = Int()
    var editRestroId = Int()
    var editRestroName = String()
    var editSchduleId = Int()

    var isLocationOn = Bool()
    
   var isCallRestroWebservice = true
   var restroMutableList = [NSMutableDictionary]()

    
    var payFromCreditCard = String()
    var cardNoStr = String()
    var expDateStr = String()

    
    class var sharedInstance: Singleton {
        struct Static {
            static let instance: Singleton = Singleton()
        }
        return Static.instance
    }
    
    
    
}
