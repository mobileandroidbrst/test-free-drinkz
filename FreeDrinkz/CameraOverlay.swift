//
//  CameraOverlay.swift
//  Ludi
//
//  Created by Mrinal Khullar on 07/12/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

import UIKit
//import MBProgressHUD
import PhotosUI
import SDWebImage
//import PhotoEditorSDK
import Alamofire
import AVFoundation
import SwiftyJSON

class CameraOverlay: UIViewController, UIVideoEditorControllerDelegate, UITextViewDelegate, UIGestureRecognizerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
   
//   // Outlets
    @IBOutlet var crossBtnCamera: UIButton!
    @IBOutlet var captureBtn: UIButton!
    @IBOutlet var capturedImage: UIImageView!
    @IBOutlet var downloadBtnCameraView: UIButton!
    @IBOutlet var playBtn: UIButton!
    @IBOutlet weak var timeCountLbl: UILabel!
    @IBOutlet var cameraView: UIView!
    @IBOutlet weak var cameraFlipButton: UIButton!
    @IBOutlet weak var editButton: UIButton!

//    // Variables
    var cameraController = CameraController()
    var capturedTime = Float()
    var timer: Timer!
    var videoPlayer = AVPlayer()
    var progressCircle = CAShapeLayer()
    var media_uploadedFrom = String()
    var crossCount = Int()
    var videoPath = NSURL()
    var video_duration = Float()
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    var mediaType = String()
    var parentClass = UIViewController()
    var thumbnail = UIImage()
    var campValue = String()
    var isRecording = false
//
    override func viewDidLoad() {
        super.viewDidLoad()

      //  self.customLayer(layer: sendBtnCameraView.layer)
        self.initCameraOverlay()
        
        self.crossCount = 1
        self.mediaType = "public.image"
        self.playBtn.isHidden = true
        self.timeCountLbl.isHidden = true

        self.checkCameraAuthorization()

        PHPhotoLibrary.requestAuthorization({(newStatus) in })
        try! AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, with: [])
        var prefersStatusBarHidden: Bool { return true }
        
        self.captureBtn.addTarget(self, action:#selector(singleTap(sender:)), for:UIControlEvents.touchUpInside)
        
    

        let tap = UITapGestureRecognizer(target: self, action: #selector(DoubleTap(sender:)))
        tap.numberOfTapsRequired = 2
        self.cameraView.isUserInteractionEnabled = true
        self.cameraView.addGestureRecognizer(tap)
        self.cameraView.backgroundColor = UIColor.black

        self.capturedImage.backgroundColor = UIColor.black
        self.capturedImage.contentMode = .scaleAspectFit
        self.capturedImage.frame = CGRect(x: 0, y: UIScreen.main.bounds.height/10, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.width)

        self.activityIndicator.isHidden = true
        
    
        

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    override func viewWillDisappear(_ animated: Bool) {

        self.videoPlayer.pause()
        self.videoPlayer.isMuted = true
        NotificationCenter.default.removeObserver(self, name: .AVPlayerItemDidPlayToEndTime, object: self.videoPlayer)

    }


    override func viewWillAppear(_ animated: Bool) {

        self.navigationController?.isNavigationBarHidden = true

    }

    //MARK: Custom Camera functions

    func startTimer() {

        if self.timer == nil {
            capturedTime = 0

            self.timeCountLbl.text = "00:0\(Int(capturedTime))"
            self.timeCountLbl.isHidden = false

            self.timer = Timer.scheduledTimer(timeInterval: 0.25, target: self, selector: #selector(self.runTimedCode), userInfo: nil, repeats: true)
        }

    }

    func stopTimer() {
        print("1")
        self.timeCountLbl.isHidden = true

        if self.timer != nil {
            self.timer?.invalidate()
            self.timer = nil
            print("2")
        }
    }

    func runTimedCode() {
        print("timmerEnd")

        if capturedTime < 9.5{
            capturedTime += 0.25

            if capturedTime < 9 {
                self.timeCountLbl.text = "00:0\(Int(capturedTime) + 1)"
            }
            else{
                self.timeCountLbl.text = "00:\(Int(capturedTime) + 1)"
            }

            return
        }

        capturedTime += 0.25
        self.timeCountLbl.text = "00:\(Int(capturedTime))"
        self.cameraController.stopRecording()
        self.crossCount = 0
        self.progressCircle.removeFromSuperlayer()
      //  self.captureBtn.setImage(UIImage(named: "circle2"), for: .normal)
        self.stopTimer()
    }

    func configureCameraController() {

        cameraController.prepare {(error) in

            if let error = error {
                print(error)
            }

            try? self.cameraController.displayPreview(on: self.cameraView)

        }

    }

    func initCameraOverlay()  {

        self.playBtn.isHidden = true
      //  self.downloadBtnCameraView.isHidden = true
     ///   self.editButton.isHidden = true
        self.cameraFlipButton.isHidden = false
        self.capturedImage.isHidden = true
        self.captureBtn.isHidden = false
        self.progressCircle.removeFromSuperlayer()
        self.captureBtn.setImage(UIImage(named: "ic_video"), for: .normal)
        self.videoPlayer.pause()
        self.videoPlayer.isMuted = true
        self.playBtn.isSelected = false

        self.capturedImage.layer.sublayers?.forEach {
            $0.removeFromSuperlayer()
        }
        NotificationCenter.default.removeObserver(self, name: .AVPlayerItemDidPlayToEndTime, object: self.videoPlayer)

    }

    func finalizeCameraOverlay()  {

        self.capturedImage.isHidden = false
       // self.downloadBtnCameraView.isHidden = false

        if self.mediaType == "public.image"{
           //self.editButton.isHidden = false
        }

        self.cameraFlipButton.isHidden = true
        self.captureBtn.isHidden = true

    }


    //MARK: Gesture Recognizers

    func DoubleTap(sender: UITapGestureRecognizer?) {

        if isRecording == false {
        do {
            try cameraController.switchCameras()
        }
        catch{

        }
        }

    }

    //MARK: Start and Save the Video
    func singleTap(sender:UIButton) {

           // self.captureBtn.setTitle("Done", for:UIControlState.normal)
           // self.captureBtn.setImage(UIImage(named:""), for:UIControlState.normal)
           // self.captureBtn.addTarget(self, action:#selector(doneAction(sender:)), for:UIControlEvents.touchUpInside)
        
        
        
        isRecording = !isRecording
        
        if isRecording == false {
            cameraController.stopRecording()
        } else {
            cameraController.startVideoRecord(completion: { (url, error) in
                
                if((error) != nil){
                    print(error as Any)
                }
                else{
                    
                    // 15 May :-
                    UserDefaults.standard.set(true, forKey: "search") //Bool
                    UserDefaults.standard.synchronize()
                    
                    self.mediaType = "public.movie"
                    let asset : AVURLAsset = AVURLAsset(url: url!)
                    self.video_duration = (Float(asset.duration.seconds))
                    print("Selected video_duration-> \(self.video_duration)")
                    self.videoPath = asset.url as NSURL
                    self.playBtn.isHidden = false
                    
                    self.videoPlayer = AVPlayer(url: self.videoPath as URL)
                    let playerLayer = AVPlayerLayer(player: self.videoPlayer)
                    playerLayer.frame = self.capturedImage.bounds
                    self.videoPlayer.play()
                    self.videoPlayer.isMuted = false
                    self.capturedImage.image = nil
                    self.capturedImage.backgroundColor = UIColor.black
                    self.capturedImage.layer.addSublayer(playerLayer)
                    self.capturedImage.layoutIfNeeded()
                    NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: self.videoPlayer.currentItem, queue: nil, using: { (_) in
                        DispatchQueue.main.async {
                            self.videoPlayer.seek(to: kCMTimeZero)
                            self.videoPlayer.play()
                            self.videoPlayer.isMuted = false
                            self.playBtn.isSelected  = true
                        }
                    })
                    
                    self.media_uploadedFrom = "Camera"
                    self.finalizeCameraOverlay()
                    let imageDataDict:[String:NSURL] = ["video":self.videoPath]
                    NotificationClass.sharedInstance.handelNotification(videoPath:imageDataDict)
                    
                }
            })
            
            self.cameraFlipButton.isUserInteractionEnabled = false
            self.startTimer()
        }
    }

    //MARK:Done Action on video
    func doneAction(sender:UIButton){
        
        self.cameraController.stopRecording()
        self.crossCount = 0
        self.progressCircle.removeFromSuperlayer()
        self.stopTimer()
        self.cameraFlipButton.isUserInteractionEnabled = true
         let imageDataDict:[String:NSURL] = ["video":self.videoPath ]
      NotificationClass.sharedInstance.handelNotification(videoPath:imageDataDict)
    
        
    }
    
    
    
    @IBAction func cancelBtnFromKeyboardClicked(sender: Any) {
        view.endEditing(true)
    }
    
    
    @IBAction func flipCamera(_ sender: Any) {

        do {
            try cameraController.switchCameras()
        }
        catch{

        }

    }


    @IBAction func imageCaptureAction(_ sender: Any) {

       
//        cameraController.captureImage { (image, error) in
//
//            self.mediaType = "public.image"
//            self.capturedImage.layer.sublayers?.forEach { $0.removeFromSuperlayer() }
//            self.playBtn.isHidden = true
//            self.capturedImage.image = image
//
//            self.crossCount = 0
//            self.media_uploadedFrom = "Camera"
//            self.finalizeCameraOverlay()
//
//
//        }

    }
    
    @IBAction func saveImageInGallery(_ sender: Any) {


        if mediaType  == "public.image" {
        }

        if mediaType == "public.movie" {
            self.videoPlayer.pause()
            self.videoPlayer.isMuted = true
            self.playBtn.isSelected = false

        }
    }

    func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            let ac = UIAlertController(title: "Save error", message: error.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            self.present(ac, animated: true)
        } else {


            let ac = UIAlertController(title: "Saved!", message: "Captured image has been saved to your photos.", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            self.present(ac, animated: true)
        }
    }

    @IBAction func cameraCrossAction(_ sender: Any) {

//        crossCount = crossCount + 1
//
//        if(crossCount == 2){
//            dismiss(animated: true, completion: nil)
//        }
//        else{
//            initCameraOverlay()
//            dismiss(animated: true, completion: nil)
//        }
        
//        dismiss(animated: true, completion: nil)

self.navigationController?.popViewController(animated: true)
        
    }


    @IBAction func playVideo(_ sender: Any){

        if(playBtn.isSelected){
            self.videoPlayer.pause()
            self.videoPlayer.isMuted = true
            self.playBtn.isSelected = false
        }
        else{
            self.playBtn.isSelected = true
            self.videoPlayer.play()
            self.videoPlayer.isMuted = false
        }

    }


    //MARK: Edit Image Action

    @IBAction func editImage(_ sender: Any) {

        if self.capturedImage.image != nil {

        }


    }




    //MARK: Camera Authorization

   func checkCameraAuthorization() {

        let status: AVAuthorizationStatus = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo)

        if status == .authorized {

            self.configureCameraController()

        } else if status == .denied {

            if AVCaptureDevice.responds(to: #selector(AVCaptureDevice.requestAccess(forMediaType:completionHandler:))) {
                AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: {(_ granted: Bool) -> Void in

                    if granted {
                        DispatchQueue.main.async(execute: {() -> Void in
                            self.configureCameraController()
                        })
                    }
                    else {
                        DispatchQueue.main.async(execute: {() -> Void in
                            self.accessMethod("Please go to Settings and enable the camera for this app to use this feature.")
                        })

                    }
                })
            }
        } else if status == .restricted {

            DispatchQueue.main.async(execute: {() -> Void in
                self.accessMethod("Please go to Settings and enable the camera for this app to use this feature.")
            })
        }
        else if status == .notDetermined {

            AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: {(_ granted: Bool) -> Void in
                if granted {
                    DispatchQueue.main.async(execute: {() -> Void in
                        self.configureCameraController()

                    })
                }
                else {
                    DispatchQueue.main.async(execute: {() -> Void in
                        self.accessMethod("Please go to Settings and enable the camera for this app to use this feature.")
                    })

                }
            })
        }
    }


    func accessMethod(_ message: String) {
        let alertController = UIAlertController(title: "Not Authorized", message: message, preferredStyle: .alert)
        let cancel = UIAlertAction(title: "Ok", style: .default) { (action) in
            self.dismiss(animated: true, completion: nil)
        }
        let Settings = UIAlertAction(title: "Settings", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!, options: ["":""], completionHandler: nil)
        })
        alertController.addAction(Settings)
        alertController.addAction(cancel)
        present(alertController, animated: true, completion: { _ in })
    }



    func checkPhotoAuthorization()  {
        let status: PHAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
        if status == .authorized {
            let localPicker = UIImagePickerController()
            localPicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
            localPicker.videoMaximumDuration = 15.0
            localPicker.allowsEditing = true
            localPicker.delegate = self
            self.present(localPicker, animated: true, completion: { _ in })

        }
        else if status == .denied {
            DispatchQueue.main.async(execute: {() -> Void in
                self.accessMethodPhotos("Please go to Settings and enable the photos for this app to use this feature.")
            })
        } else if status == .notDetermined {
            PHPhotoLibrary.requestAuthorization({(_ status: PHAuthorizationStatus) -> Void in
                if status == .authorized {

                    DispatchQueue.main.async(execute: {() -> Void in
                        let localPicker = UIImagePickerController()
                        localPicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
                        localPicker.videoMaximumDuration = 15.0
                        localPicker.allowsEditing = true
                        localPicker.delegate = self
                        self.present(localPicker, animated: true, completion: { _ in })
                    })

                }
                else {
                    DispatchQueue.main.async(execute: {() -> Void in
                        self.accessMethodPhotos("Please go to Settings and enable the photos for this app to use this feature.")
                    })
                }
            })
        }
        else if status == .restricted {
        }


    }

    func accessMethodPhotos(_ message: String) {
        let alertController = UIAlertController(title: "Not Authorized", message: message, preferredStyle: .alert)
        let cancel = UIAlertAction(title: "Ok", style: .default) { (action) in

        }
        let Settings = UIAlertAction(title: "Settings", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!, options: ["":""], completionHandler: nil)
        })
        alertController.addAction(Settings)
        alertController.addAction(cancel)
        present(alertController, animated: true, completion: { _ in })
    }

    //MARK: UIImagePickerControllerDelegate

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {

        if let mediaType = info[UIImagePickerControllerMediaType] as? String {

            self.mediaType = mediaType

            capturedImage.layer.sublayers?.forEach {
                $0.removeFromSuperlayer()
            }

            if mediaType  == "public.image" {
                print("Image Selected")

                if let image = info[UIImagePickerControllerEditedImage] as? UIImage {

                    self.playBtn.isHidden = true
                    self.capturedImage.image = image

                }
            }

            if mediaType == "public.movie" {
                print("Video Selected")

                if let videoURL = info[UIImagePickerControllerMediaURL] as? NSURL {


                    let asset : AVURLAsset = AVURLAsset(url: videoURL as URL)
                    self.video_duration = (Float(asset.duration.seconds))
                    print("Selected video_duration-> \(self.video_duration)")

                    self.videoPath = asset.url as NSURL
                    self.playBtn.isHidden = false

                    self.videoPlayer = AVPlayer(url: videoURL as URL)
                    let playerLayer = AVPlayerLayer(player: self.videoPlayer)
                    playerLayer.frame = self.capturedImage.bounds
                    self.videoPlayer.play()
                    self.videoPlayer.isMuted = false

                    self.capturedImage.image = nil
                    self.capturedImage.backgroundColor = UIColor.black
                    self.capturedImage.layer.addSublayer(playerLayer)

                    NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: self.videoPlayer.currentItem, queue: nil, using: { (_) in
                        DispatchQueue.main.async {
                            self.videoPlayer.seek(to: kCMTimeZero)
                            self.videoPlayer.play()
                            self.videoPlayer.isMuted = false
                            self.playBtn.isSelected  = true
                        }
                    })

                }
            }

            self.finalizeCameraOverlay()
        }

        if(picker.sourceType == .photoLibrary){

            self.media_uploadedFrom = "Gallery"
            picker.dismiss(animated: false) {
                if self.mediaType  == "public.image" {
                    if self.capturedImage.image != nil{
                     //   self.presentPhotoEditorViewController(image: self.capturedImage.image!)
                    }
                }
            }
        }

    }


    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {



        self.crossCount = 1
        picker.dismiss(animated: false, completion: nil)

    }


    func alertView(title: String, message: String){

        let alertView = UIAlertController(title: title, message:message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in

        })
        alertView.addAction(action)
        self.present(alertView, animated: true, completion: nil)

    }

    func submissionPreUploadProcess(){


        var mimeType = String()
        var fileName = String()
        var imageData = Data()
        var videoData = Data()

        if mediaType  == "public.movie" {
            mimeType = "video/mp4"
            fileName = "video.mp4"

            do{
                videoData =  try NSData(contentsOfFile: (self.videoPath.relativePath)!, options: NSData.ReadingOptions.alwaysMapped) as Data
                self.uploadMediaForSubmission( videoData: videoData , fileName: fileName, mimeType: mimeType)
            }
            catch{
                //    self.AlertMessage(title: "Alert", message: "Unable to process the selected video. Please try again.", buttonTitle: "Try Again")
            }
        }
        else if mediaType  == "public.image"{

            mimeType = "image/jpg"
            fileName = "file.jpg"

            imageData = UIImageJPEGRepresentation(self.capturedImage.image!,1)!
            self.uploadMediaForSubmission( imageData: imageData, fileName: fileName, mimeType: mimeType)

        }

    }


    // Mark: Upload video
    func uploadMediaForSubmission(videoData:Data  , fileName:String  , mimeType:String ) {




        let valueId = UserDefaults.standard.value(forKey: "runForOfficeKey") as? String
        if valueId != nil{

            self.campValue = valueId!

        }

        else{

            self.campValue = ""

        }

        DispatchQueue.main.async {

            self.activityIndicator.isHidden = false
            self.activityIndicator.startAnimating()
        }


        let parameters: Parameters =
            ["auth_token":UserDefaults.standard.value(forKey:"authToken")!,
             "user_id":UserDefaults.standard.value(forKey: "userId")!,
             "runforoffice_id":self.campValue
        ]

        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(videoData, withName: "media",fileName:fileName ,mimeType:mimeType )
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        },to:"http://beta.brstdev.com/politicsplay/api/web/v1/users/add_story")
        { (result) in
            switch result {
            case .success(let upload, _, _):

                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })

                upload.responseString(completionHandler: { (respo) in
                    print(respo)
                })

                upload.responseJSON { response in

                    print(response)
                    if let json = response.result.value as? NSDictionary {

                        if(json.value(forKey: "success") as! String == "200"){

                            let storyId = json.value(forKey: "story_id") as! Int
                            let userId = UserDefaults.standard.value(forKey: "userId") as! String
                            let authToken = UserDefaults.standard.value(forKey: "authToken") as! String

                            self.uploadVideoAgain(userId: userId,authToken: authToken,storyIdValue: storyId)

                        }
                        else{

                        }

                    }

                    if response.result.isFailure {


                        self.alertView(title: "", message: (response.error?.localizedDescription)!)
                        self.activityIndicator.isHidden = true
                        self.activityIndicator.stopAnimating()
                    }

                }

            case .failure(let encodingError):
                print(encodingError)
            }

        }
    }


//    // Mark: Upload Thumbnail
    func uploadVideoAgain(userId: String,authToken: String,storyIdValue: Int){



        DispatchQueue.main.async{

            self.activityIndicator.isHidden = false
            self.activityIndicator.startAnimating()


        }
        do {
            // for creating thumbnail for video url
            let asset = AVURLAsset(url: videoPath as URL , options: nil)
            let imgGenerator = AVAssetImageGenerator(asset: asset)
            imgGenerator.appliesPreferredTrackTransform = true
            let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(0, 1), actualTime: nil)
            let uiImage = UIImage(cgImage: cgImage)
            thumbnail = uiImage
        }

        catch{

            print("*** Error generating thumbnail: \(error.localizedDescription)")
        }

        let dataImage = UIImagePNGRepresentation(thumbnail) as NSData?
        let image : UIImage = UIImage(data: dataImage! as Data)!



        let parameters = ["post_id":String(describing: storyIdValue)] as  [String : Any]


        print(parameters)

        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(UIImagePNGRepresentation(image)!, withName: "thumbnail", fileName: "profilepic.jpg", mimeType: "image/jpg")
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to:"http://beta.brstdev.com/politicsplay/api/web/v1/users/upload_thumb")
        { (result) in
            switch result {
            case .success(let upload, _, _):

                upload.uploadProgress(closure: { (progress) in
                    //Print progress
                    //  print(progress)

                })

                upload.responseJSON { response in
                    //print response.result
                    print(response)

                    if response.result.value != nil {

                        DispatchQueue.main.async {

                            let json = JSON(data: response.data! )

                            if let descriptionStr = json["message"].string {
                                print(descriptionStr)

                        if descriptionStr == "Post Added Successfully"{


                                self.alertView(title: "", message: "Story Added Successfully")
                                self.activityIndicator.isHidden = true
                                self.activityIndicator.stopAnimating()
                                self.initCameraOverlay()

                            }

                        else{
                            
                            self.alertView(title: "", message: (response.error?.localizedDescription)!)
                               self.activityIndicator.isHidden = true
                               self.activityIndicator.stopAnimating()
                           }

                          if ((response.error) != nil){
                                    let alertView = UIAlertController(title: "", message:(response.error?.localizedDescription)! , preferredStyle: .alert)
                                    let action = UIAlertAction(title: "OK", style: .default, handler:nil)
                                    alertView.addAction(action)
                                    self.present(alertView, animated: true, completion: nil)

                                }

                            }
                        }

                        DispatchQueue.main.async{

                            self.activityIndicator.isHidden = true
                            self.activityIndicator.stopAnimating()

                        }
                    }
                }

            case .failure(let encodingError):



                break

            }

        }
    }



    // Mark: Upload image
    func uploadMediaForSubmission(imageData:Data  , fileName:String  , mimeType:String ) {


        let valueId = UserDefaults.standard.value(forKey: "runForOfficeKey") as? String
        if valueId != nil{

            self.campValue = valueId!

        }

        else{

            self.campValue = ""

        }


        DispatchQueue.main.async {

            self.activityIndicator.isHidden = false
            self.activityIndicator.startAnimating()
        }


        let parameters: Parameters =
            ["auth_token":UserDefaults.standard.value(forKey:"authToken")!,
             "user_id":UserDefaults.standard.value(forKey: "userId")!,
             "runforoffice_id":self.campValue
        ]

        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(imageData, withName: "media",fileName:fileName ,mimeType:mimeType )
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        },to:"http://beta.brstdev.com/politicsplay/api/web/v1/users/add_story")
        { (result) in
            switch result {
            case .success(let upload, _, _):

                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })

                upload.responseString(completionHandler: { (respo) in
                    print(respo)
                })

                upload.responseJSON { response in

                    print(response)
                    if let json = response.result.value as? NSDictionary {

                        if(json.value(forKey: "success") as! String == "200"){
                           self.alertView(title: "", message: "Story Added Successfully")
                            self.activityIndicator.isHidden = true
                            self.activityIndicator.stopAnimating()
                            self.initCameraOverlay()
                        }
                        else{

                             self.alertView(title: "", message: (response.error?.localizedDescription)!)
                             self.activityIndicator.isHidden = true
                             self.activityIndicator.stopAnimating()
                        }

                    }

                    if response.result.isFailure {


                        self.alertView(title: "", message: (response.error?.localizedDescription)!)
                        self.activityIndicator.isHidden = true
                        self.activityIndicator.stopAnimating()
                    }

                }

            case .failure(let encodingError):
                print(encodingError)
              }

         }
    }


    //MARK: Common Methods.

    func customLayer(layer:CALayer){

        layer.cornerRadius = 8
        layer.masksToBounds = false;
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowRadius = 3
        layer.shadowOffset = CGSize(width: 1, height: 1)

    }


    @IBAction func toggleLeft(_ sender: Any) {


    }



}

extension UIView {

    func takeSnapshot() -> UIImage {

        print(self.bounds)
        print(bounds.size)
        print(UIScreen.main.scale)

        UIGraphicsBeginImageContextWithOptions(CGSize(width: (previewLayer?.frame.size.width)!, height: (previewLayer?.frame.size.width)!), true, UIScreen.main.scale)
        drawHierarchy(in: CGRect(origin: CGPoint(x: 0, y: -UIScreen.main.bounds.height/10 ), size: self.bounds.size), afterScreenUpdates: true)

        let image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()


        return image
    }
}


