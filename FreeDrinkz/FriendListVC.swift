//
//  FriendListVC.swift
//  FreeDrinkz
//
//  Created by tbi-pc-57 on 5/16/19.
//  Copyright © 2019 Brst-Pc109. All rights reserved.
//

// 20 May :-
import UIKit
import Alamofire
import SwiftyJSON

class FriendListVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UISearchResultsUpdating {
 
// Outlets :-
@IBOutlet var tableView: UITableView!
@IBOutlet var searchImageView: UIImageView!
@IBOutlet var searchBtn: UIButton!
@IBOutlet var backView: UIView!

 var resultSearchController = UISearchController()
    
    var filteredTableData = [[String:Any]]()
    var friendListArray = [[String: Any]]()
    var friendListArrayOriginal = [[String: Any]]()
    var dict = [String:Any]()

    var pageno = String()
    var totalPages = Int()
    var nextPage = Int()
    var userId = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.searchBtn.isHidden = true
        self.searchImageView.isHidden = true
        
        resultSearchController = ({
            let controller = UISearchController(searchResultsController: nil)
            controller.searchResultsUpdater = self as UISearchResultsUpdating
            controller.dimsBackgroundDuringPresentation = false
            controller.searchBar.sizeToFit()

            tableView.tableHeaderView = controller.searchBar

            return controller
        })()
    
        self.nextPage = 1
        
     backView.isHidden = true
     self.tableView.rowHeight = 100
     self.tableView.separatorStyle = .none
        
        getFrndList(nextPage: nextPage)
 
        
 }
    
    // 22 May :-
    override func viewWillAppear(_ animated: Bool) {
        if UserDefaults.standard.bool(forKey: "ProfileViewFrnd") == true{
            self.nextPage = 1
            getFrndList(nextPage: nextPage)
            UserDefaults.standard.set(false, forKey: "ProfileViewFrnd")
        }
    }
    
    
// Getting Frnd List of User :-
    func getFrndList(nextPage:Int)  {
    
// Converting nextPage Integer to String
  let nextpagee = String(nextPage)

    let json = ["user_id": Singleton.sharedInstance.userIdStr,
    "auth_token": Singleton.sharedInstance.userTokenStr,
    "page": nextpagee
     ] as [String : String]
    
    print(json)
    
    Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/users/getfriendlist", method: .post, parameters: json, encoding: JSONEncoding.default)
    .responseJSON { response in
    print(response.result)   // result of response serialization
    print(response)
      if let jsonData = response.result.value {
              print("JSON: \(jsonData)")
               let json = JSON(data: response.data! )
               print(json)

              if let descriptionStr = json["description"].string {
                print(descriptionStr)
                if descriptionStr == "Missing input data" {
                    self.backView.isHidden = false
                }
              }
                
            // If user have some Friends.
                else {
                let responseDict = json["data"].dictionary
                print(responseDict!)
                self.friendListArrayOriginal = responseDict?["Friendlist"]?.arrayObject as! [[String : Any]]
                self.friendListArray = self.friendListArrayOriginal
                let paginationDict =  responseDict!["Pagination"]?.dictionary
              //  print(paginationDict!)
                self.totalPages = (paginationDict?["totalpages"]?.int)!
              //  print(self.totalPages)
                
               
                print(self.friendListArray)
                self.tableView.reloadData()
               }
        
      }
                
            else if((response.error) != nil) {
       }
        
    }
    
}
    // Table-View Data Sources and Delegates :-
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
       if  (resultSearchController.isActive) {
             return friendListArray.count
           }
        
        return friendListArrayOriginal.count
        
    }
    
    
func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    if (resultSearchController.isActive) {
        print(friendListArray[indexPath.row])
        dict = friendListArray[indexPath.row] as? NSDictionary as! [String : Any]
        print(dict)
    }
    
    else {
        dict = friendListArrayOriginal[indexPath.row] as? NSDictionary as! [String : Any]
        print(dict)
        
    }
    
    
//// Fetching Dictionary From Array according to index-path.
//        print(friendListArray[indexPath.row])
//        let dict = friendListArray[indexPath.row] as? NSDictionary
//        print(dict!)
    
        let cell = tableView.dequeueReusableCell(withIdentifier: "FriendListTVC") as! FriendListTVC

 // If user-image comes from Backend.
    
    let profilepic: String = (dict["profile_pic"] as? String)!
   
    // 10 June :-
    //    cell.userImage.sd_setImage(with:URL(string:profilepic), placeholderImage:UIImage(named:"default_profile.png"))
    let urlStr : String = (profilepic)
    let urlFormattedStr = urlStr.replacingOccurrences(of: " ", with: "%20")
    cell.userImage.sd_setImage(with: URL(string:urlFormattedStr), placeholderImage: UIImage(named: "default_profile.png"))
    
    let firstName: String = (dict["firstname"] as? String)!
    let lastName: String = (dict["lastname"] as? String)!
         let fullName:String = firstName + " " + lastName
         cell.userNameLbl.text = fullName
        
    let loc1: String = (dict["city"] as? String)!
    let loc2: String = (dict["state"] as? String)!
    let loc3: String = (dict["zip_code"] as? String)!
        let loc:String = loc1 + " " + loc2 + ", " + loc3
        cell.locationLbl.text = loc
    
    print((dict["timestamp"] as? String)!)
    if let timestamp = dict["timestamp"] as? String {
        let timestampDouble = Double(timestamp)
        let date = Date(timeIntervalSince1970: timestampDouble!)
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "dd-MMMM-yyyy hh:mm a"
        let strDate = dateFormatter.string(from: date)
        cell.dateLbl.text =  "Date:" + strDate
        
    }
    
        return cell
}
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")
        
        print(friendListArray[indexPath.row])
        let dict = friendListArray[indexPath.row] as? NSDictionary
        print(dict!)
        self.userId = (dict?.value(forKey:"id") as? String)!
        let userType = (dict?.value(forKey:"usertype") as? String)!
        print(userType)
        
        if userType == "1" {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "Profile") as! Profile
            vc.fromNavStr = "other"
            vc.showPostVideoBtn = true
            //  vc.fromViewControllerStr = "Search"
            vc.memberProfileId = userId
           // vc.isLocation = allUsersArray[button.tag]["notification_setting"].string!
          //  print(vc.isLocation)
            
            navigationController?.pushViewController(vc,animated: true)
        }
            
        else if userType == "2" {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "PlaceProfile") as! PlaceProfile
            vc.fromNavStr = "other"
            vc.restaurantId = userId
            navigationController?.pushViewController(vc,animated: true)
        }
            
        else if userType == "3" {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ArtistProfile") as! ArtistProfile
            vc.fromNavStr = "other"
            vc.artistId = userId
            navigationController?.pushViewController(vc,animated: true)
        }
            
        else if userType == "4" {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "PlaceProfile") as! PlaceProfile
            vc.fromNavStr = "other"
            vc.restaurantId = userId
            navigationController?.pushViewController(vc,animated: true)
        }
            
        else if userType == "5" {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "PlaceProfile") as! PlaceProfile
            vc.fromNavStr = "other"
            vc.restaurantId = userId
            navigationController?.pushViewController(vc,animated: true)
        }
            
        else if userType == "6" {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "PlaceProfile") as! PlaceProfile
            vc.fromNavStr = "other"
            vc.restaurantId = userId
            navigationController?.pushViewController(vc,animated: true)
        }
            
        else if userType == "7" {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "PlaceProfile") as! PlaceProfile
            vc.fromNavStr = "other"
            vc.restaurantId = userId
            navigationController?.pushViewController(vc,animated: true)
         }
    }

// For Pagination :-
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.nextPage += 1
        if nextPage <= totalPages {
            let bottomEdge: Float = Float(self.tableView.contentOffset.y + tableView.frame.size.height)
            if bottomEdge >= Float(tableView.contentSize.height) && friendListArray.count > 0 {
                getFrndList(nextPage: nextPage)  ///    do your action
                print("we are at end of table")
            }
        }
    
    }
  
 // Buttons Actions :-
   @IBAction func backAction(_ sender: UIButton) {
      _ = navigationController?.popViewController(animated: true)
     }
    
    @IBAction func searchAction(_ sender: UIButton) {
    }
   
    
 // Top Searching in TableView :-
    func updateSearchResults(for searchController: UISearchController) {
        filteredTableData.removeAll(keepingCapacity: false)

//        let searchPredicate = NSPredicate(format: "SELF CONTAINS[c] %@", searchController.searchBar.text!)
//        let array = (friendListArray as NSArray).filtered(using: searchPredicate)
//        filteredTableData = array as! [[String:Any]]
//
//        self.tableView.reloadData()
        
         self.friendListArray = (self.friendListArrayOriginal as NSArray).filtered(using: NSPredicate(format: "firstname CONTAINS[C] %@",searchController.searchBar.text! )) as! [[String : Any]]
        self.tableView.reloadData()
        
        
    }
   
    
    
    
    
    
    
    
    
    
    
}

