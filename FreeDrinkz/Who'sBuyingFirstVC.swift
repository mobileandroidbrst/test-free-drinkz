//
//  Who'sBuyingFirstVC.swift
//  FreeDrinkz
//
//  Created by Tbi-Pc-22 on 18/04/18.
//  Copyright © 2018 Brst-Pc109. All rights reserved.
//

import UIKit
import CoreLocation
import Alamofire
import SwiftyJSON



class Who_sBuyingFirstVC: UIViewController,UITableViewDataSource,UITableViewDelegate,CLLocationManagerDelegate {
    
    
    //Variables:
    var locationManager = CLLocationManager()
    var restroListArray = NSMutableArray()
    var checkInStatus =  String()
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // Mark: Location Manager
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = 0.5
        locationManager.delegate = self as CLLocationManagerDelegate
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
   
        // 29 April :-
      //  buildGeofenceData()
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Mark: Location Updates
    func buildGeofenceData() {
        
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
        
        // print (UserDefaults.value(forKey: "currentlatitude")!)
        
   let json = ["user_id":Singleton.sharedInstance.userIdStr,
                    "auth_token":Singleton.sharedInstance.userTokenStr,
                    "lat":DataManager.currentLat ?? "",
                    "long":DataManager.currentLong ?? ""
            ] as [String : Any]
        print(json)
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/users/getiambuyingnearbyrestaurents", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON {  response in
                DispatchQueue.main.async {
                    
                    print(response)
                    if let jsonData = response.result.value {
                        print("JSON: \(jsonData)")
                        let json1 = jsonData as! NSDictionary
                        let dataListing = json1["data"] as! NSDictionary
                        print(dataListing)
                        let restroList = dataListing["nearby_restaurants"] as! NSArray
                        self.restroListArray = restroList.mutableCopy() as! NSMutableArray
                        
                        // 29 April :-
                    //   self.locationManager.startUpdatingLocation()
                    //     self.locationManager.startUpdatingLocation()
                    }
                    if((response.error) != nil){
                        
                        // 29 April :-
                       // self.buildGeofenceData()
                      //    self.buildGeofenceData()
                        
                    }
                     
                        // 29 April :-
                    else {
                      //  print(response.error!)
                    }
                    
                    DispatchQueue.main.async {
                        
                        self.activityIndicator.isHidden = true
                        self.activityIndicator.stopAnimating()
                        self.tableView.reloadData()
                        
                    }
                         
                }
        }
    }
    //MARK: Location manager delegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        
        DataManager.currentLat = locations.last?.coordinate.latitude
        DataManager.currentLong = locations.last?.coordinate.longitude
        
        // 30 April :-
        print(DataManager.currentLat!)
        print(DataManager.currentLong!)
        
        self.locationManager.stopUpdatingLocation()
        self.buildGeofenceData()
        return
        
    }
    

    // Mark:Table View Mthods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.restroListArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "WhosBuyingFirst", for: indexPath) as! ImBuyingCell
        
        // 22 May :-
        
        cell.mainBackgroundView.layer.borderWidth = 1
        cell.mainBackgroundView.layer.borderColor = UIColor.lightGray.cgColor
        cell.shadowView.addBottomBorder(color: .lightGray, width: 2)
       
        
        print(restroListArray)
        
        let dict = restroListArray[indexPath.row] as! NSDictionary
        _ = dict.value(forKey: "user_check_in_status") as? String
        
        cell.imgViewRestro.layer.cornerRadius = 15 //cell.imgViewRestro.frame.size.height/2
        cell.imgViewRestro.clipsToBounds = true
        
        cell.restroAddress.text = dict.value(forKey: "address") as? String
        //cell.checkInPplStatus.text = dict.value(forKey: "total_user_checkedin") as? String
        let value = dict.value(forKey: "total_user_checkedin") as? String
        // 11 June :-
        // cell.checkInPplStatus.text = "User's Check In:" +  value!
           cell.checkInPplStatus.text = "User’s Checked In:" +  value!
        
        let restroName = dict.value(forKey: "resturant_name") as? String
        
        var name = ""
        
        
        if Singleton.sharedInstance.lastNameStr != "" && Singleton.sharedInstance.lastNameStr != " " {
            name = Singleton.sharedInstance.firstNameStr+" "+Singleton.sharedInstance.lastNameStr
        }
        else {
            name = Singleton.sharedInstance.firstNameStr
        }
        for _ in (0..<restroListArray.count)
        {
            
            self.checkInStatus = (dict.value(forKey: "user_check_in_status") as? String)!
            print(checkInStatus)
            UserDefaults.standard.setValue(checkInStatus, forKey: "checkIn_Status")
            if checkInStatus == "1"{
                
                break
                
            }
            
            
        }
        
        if self.checkInStatus == "1"{
            
            cell.restroName.text =  name + " " + "is checked in" + " " + restroName!
            
        }
            
        else{
            
            cell.restroName.text = restroName!
            
        }
        
        // 10 June :-
      // cell.imgViewRestro.sd_setImage(with: URL(string: dict.value(forKey: "profile_pic_url") as! String), placeholderImage: UIImage(named: "userSquare.jpg"))
        let urlStr : String = (dict.value(forKey: "profile_pic_url") as! String)
        let urlFormattedStr = urlStr.replacingOccurrences(of: " ", with: "%20")
        cell.imgViewRestro.sd_setImage(with: URL(string:urlFormattedStr), placeholderImage: UIImage(named: "userSquare.jpg"))
        
        
        
        // Mark: Check In Out Handling
        cell.checkInBtn.tag = indexPath.row
        cell.checkInBtn.addTarget(self, action: #selector(handelCheckInOutEvent), for: UIControlEvents.touchUpInside)
        cell.checkInBtn.layer.cornerRadius = cell.checkInBtn.frame.size.height/2;
        cell.checkInBtn.clipsToBounds = true;
        
        
        // Mark: Request Out Handling
        cell.requestButton.tag = indexPath.row
        cell.requestButton.addTarget(self, action: #selector(handelRequestInOutEvent), for: UIControlEvents.touchUpInside)
        cell.requestButton.layer.cornerRadius = cell.checkInBtn.frame.size.height/2;
        cell.requestButton.clipsToBounds = true;
        cell.selectionStyle = .none

        return cell
        
    }
    
    // Mark: Check In Out Handling
    func handelCheckInOutEvent(sender: UIButton){
        
        let dict = restroListArray[sender.tag] as! NSDictionary
        let id  = dict.value(forKey: "id") as? String
        let name  = dict.value(forKey: "resturant_name") as? String
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "PlaceProfile") as! PlaceProfile
        vc.selectType = "1"
        vc.fromNavStr = "other"
        vc.restaurantId = id!
        vc.restroName = name!
        self.navigationController?.pushViewController(vc,animated: true)
        
        
    }
    
    
    // Mark: Check In Out Handling
    func handelRequestInOutEvent(sender: UIButton) {
        
        
        let dict = restroListArray[sender.tag] as! NSDictionary
        let id  = dict.value(forKey: "id") as? String
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "WhosBuying") as! WhosBuying
        vc.restroId = id!
        vc.type = "all"
        print(vc.restroId)
        self.navigationController?.pushViewController(vc,animated: true)
        
        
    }
    
   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
       // let storyboard = UIStoryboard(name: "Main", bundle: nil)
      //  let vc = storyboard.instantiateViewController(withIdentifier: "WhosBuying") as! WhosBuying
       // self.navigationController?.pushViewController(vc,animated: true)
    
    }
    
 
    // Mark: Alert View
    func alertViewMethod(titleStr:String, messageStr:String) {
        let alert = UIAlertController(title: titleStr, message: messageStr, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    //MARK: SetUpSlideMenu
    
//    fileprivate func setupSideMenu() {
//        // Define the menus
//        SideMenuManager.menuLeftNavigationController = storyboard!.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as? UISideMenuNavigationController
//        
//        // Enable gestures. The left and/or right menus must be set up above for these to work.
//        // Note that these continue to work on the Navigation Controller independent of the View Controller it displays!
//        SideMenuManager.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
//        SideMenuManager.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
//        
//        
//        // Set up a cool background image for demo purposes
//        //     SideMenuManager.menuAnimationBackgroundColor = UIColor(patternImage: UIImage(named: "background")!)
//        SideMenuManager.menuAnimationBackgroundColor = UIColor(white: 1, alpha: 0.0)
//    }
//
    

}

//// 22 May :-
//extension UIView {
//       func addBottomBorder(color: UIColor, width: CGFloat) -> UIView {
//        let layer = CALayer()
//        layer.borderColor = color.cgColor
//        layer.borderWidth = width
//        layer.frame = CGRect(x: 0, y: self.frame.size.height-width, width: self.frame.size.width, height: width)
//        self.layer.addSublayer(layer)
//        return self
//    }
//}
