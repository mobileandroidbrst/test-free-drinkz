//
//  CartCell.swift
//  FreeDrinkz
//
//  Created by tbi-pc-57 on 6/20/19.
//  Copyright © 2019 Brst-Pc109. All rights reserved.
//

// 20 June :-

import UIKit

class CartCell: UITableViewCell {

    // Mark :- Outlets
    @IBOutlet var cartLbl: UILabel!
    @IBOutlet var receiverNameLbl: UILabel!
    @IBOutlet var deleteBtn: UIButton!
    
     override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
 }

}
