//
//  Slidebar.swift
//  FreeDrinkz
//
//  Created by Brst-Pc109 on 25/03/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import TwitterKit
import Firebase
import Alamofire
import SwiftyJSON



class Slidebar: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var slidebarTbl: UITableView!
    
    var nameArray = [[String : String]]()
    var airports = [[String : String]]()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate

    override func viewDidLoad() {
        
        
        print(Singleton.sharedInstance.restaurantStr)
        super.viewDidLoad()

        // Do any additional setup after loading the view.
       
        self.profileMethod()
        
        getMenuData()
        
        
        // Register to receive notification
        NotificationCenter.default.addObserver(self, selector: #selector(Slidebar.reloadTable(notification:)), name: NSNotification.Name(rawValue: Constant().FD_UpdateMessageBedge), object: nil)

        
       
    }
    func reloadTable(notification: Notification) {
        DispatchQueue.main.async {
            self.slidebarTbl.reloadData()

        }
    }
    func getMenuData() {
        var firstName = ""
        if Singleton.sharedInstance.firstNameStr != "" && Singleton.sharedInstance.firstNameStr != " " {
            if Singleton.sharedInstance.lastNameStr != "" && Singleton.sharedInstance.lastNameStr != " "{
                firstName = Singleton.sharedInstance.firstNameStr+" "+Singleton.sharedInstance.lastNameStr
            }else {
                firstName = Singleton.sharedInstance.firstNameStr
            }
        }else {
           firstName = "Me"
        }
       
        // 11 June(If Login with Restaurant,Bar or Place then on top of Side Bar Shows its name) :-
        var restrName = Singleton.sharedInstance.restaurantStr
        
        
        // 16 May :- New Feature Friend-List is added in users and artists.
        if Singleton.sharedInstance.userTypeStr == "member" {
            nameArray = [["name": firstName, "image": "me"],
                         ["name": "Search", "image": "search_nav"],
                         ["name": "Friendlist","image": "group_friend"],
                         ["name": "My Orders", "image": "orders"],
                         ["name": "Messages", "image": "messages"],
                         ["name": "I'm Buying Request", "image": "i'mbuy"],
                         ["name": "Who's Buying Request", "image": "who'sbuy"],
                         ["name": "Contact", "image": "contact"],
                         ["name": "About", "image": "about"],
                         ["name": "Settings", "image": "settings_menu"],
                         ["name": "Logout", "image": "logout"]]
            
            // 22 May :- New Feature Friend-List is added in bars/restaurants.
            // 11 June :- restrName(Restaurant,Bar,Place name is showing on top of side bar)
        } else if Singleton.sharedInstance.userTypeStr == "place" {
            nameArray = [["name": restrName,  "image": "me"],
                         ["name": "Search", "image": "search_nav"],
                         ["name": "Friendlist","image": "group_friend"],
                         ["name": "Menu", "image": "menu"],
                         ["name": "My Orders", "image": "orders"],
                         ["name": "Messages", "image": "messages"],
                         ["name": "I'm Buying Request", "image": "i'mbuy"],
                         ["name": "Payment", "image": "payment"],
                         ["name": "Contact", "image": "contact"],
                         ["name": "About", "image": "about"],
                         ["name": "Logout", "image": "logout"]]
            
        } else if Singleton.sharedInstance.userTypeStr == "artist" {
            nameArray = [["name": firstName, "image": "me"],
                         ["name": "Search", "image": "search_nav"],
                         ["name": "Friendlist","image": "group_friend"],
                         ["name": "My Orders", "image": "orders"],
                         ["name":"Messages","image": "messages"],
                         ["name": "Contact", "image": "contact"],
                         ["name": "About", "image": "about"],
                         ["name": "I'm Buying Request", "image": "i'mbuy"],
                         ["name": "Who's Buying Request", "image": "who'sbuy"],
                         ["name": "Settings", "image": "settings_menu"],
                         ["name": "Logout", "image": "logout"]]
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
         self.view.isUserInteractionEnabled = true
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return nameArray.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:SlidebarCell = slidebarTbl.dequeueReusableCell(withIdentifier: "Slidebar") as! SlidebarCell
        cell.backgroundColor = UIColor.init(red: 36/255, green: 69/255, blue: 138/255, alpha: 1)
        cell.nameLbl.text = nameArray[indexPath.row]["name"]
        cell.lblBatchNo.isHidden = true
        if indexPath.row == 0 {
        
            cell.itemImg.layer.cornerRadius = cell.itemImg.frame.size.height/2
            cell.itemImg.layer.borderColor = UIColor.white.cgColor
            cell.itemImg.layer.borderWidth = 1.5
            cell.itemImg.clipsToBounds = true
            
            // 10 June :-
            // cell.itemImg.sd_setImage(with: URL(string: Singleton.sharedInstance.userImgStr), placeholderImage: UIImage(named: "user"))
            let urlStr : String = (Singleton.sharedInstance.userImgStr)
            let urlFormattedStr = urlStr.replacingOccurrences(of: " ", with: "%20")
            cell.itemImg.sd_setImage(with: URL(string:urlFormattedStr), placeholderImage: UIImage(named: "user"))
            
            
            

        } else {
            cell.itemImg.image = UIImage(named: nameArray[indexPath.row]["image"]!)
            cell.itemImg.layer.cornerRadius = 0
            cell.itemImg.layer.borderColor = UIColor.clear.cgColor
            cell.itemImg.layer.borderWidth = 0
            cell.itemImg.clipsToBounds = true

        }
        
        // 13 June :-
        if nameArray[indexPath.row]["name"] == "Messages" && appDelegate.batchCount > 0 {
            cell.lblBatchNo.isHidden = false
            cell.lblBatchNo.text = "\(appDelegate.batchCount)"
            cell.lblBatchNo.layer.cornerRadius = 18 // 21
            cell.lblBatchNo.layer.masksToBounds = true
        }
        
        
        return cell
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        tableView.reloadData()
        let cell = tableView.cellForRow(at: indexPath as IndexPath) as! SlidebarCell
        cell.backgroundColor = UIColor.init(red: 26/255, green: 45/255, blue: 115/255, alpha: 1)
        if (cell.nameLbl.text == "Logout") {
            print("logout")
            
            let dialogMessage = UIAlertController(title: "Confirm", message: "Are you sure you want to Logout?", preferredStyle: .alert)
            
            let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
                print("Ok button click...")
                self.logoutConfirm()
            })
            
            let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) -> Void in
                print("Cancel button click...")
            }
            
            dialogMessage.addAction(ok)
            dialogMessage.addAction(cancel)
            
            self.present(dialogMessage, animated: true, completion: nil)
            
           
        } else if (cell.nameLbl.text == "My Orders") {
           
      
            if (Singleton.sharedInstance.userTypeStr == "place") {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "MyPlacesOrder") as! MyPlacesOrder
                navigationController?.pushViewController(vc,animated: true)
            } else {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "MyOrders") as! MyOrders
                navigationController?.pushViewController(vc,animated: true)

            }
            self.view.isUserInteractionEnabled = false
    }
       
    // 11 June (Changing the condition of "Me" to Particular Restaurant Name i.e "Singleton.sharedInstance.restaurantStr") :-
            
        else if (cell.nameLbl.text! == Singleton.sharedInstance.restaurantStr || cell.nameLbl.text!.contains(Singleton.sharedInstance.firstNameStr)) {
            if (Singleton.sharedInstance.userTypeStr == "member") {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "Profile") as! Profile
                navigationController?.pushViewController(vc,animated: true)
            } else if (Singleton.sharedInstance.userTypeStr == "artist") {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "ArtistProfile") as! ArtistProfile
                navigationController?.pushViewController(vc,animated: true)
            }
            else if (Singleton.sharedInstance.userTypeStr == "place") {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "PlaceProfile") as! PlaceProfile
                navigationController?.pushViewController(vc,animated: true)
            }
            self.view.isUserInteractionEnabled = false

        }
            
//        else if (cell.nameLbl.text == Singleton.sharedInstance.restaurantStr) {
//          
////            if (Singleton.sharedInstance.userTypeStr == "member") {
////                let storyboard = UIStoryboard(name: "Main", bundle: nil)
////                let vc = storyboard.instantiateViewController(withIdentifier: "Profile") as! Profile
////                navigationController?.pushViewController(vc,animated: true)
////            } else if (Singleton.sharedInstance.userTypeStr == "artist") {
////                let storyboard = UIStoryboard(name: "Main", bundle: nil)
////                let vc = storyboard.instantiateViewController(withIdentifier: "ArtistProfile") as! ArtistProfile
////                navigationController?.pushViewController(vc,animated: true)
////            }
//            
//            if (Singleton.sharedInstance.userTypeStr == "place") {
//                let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                let vc = storyboard.instantiateViewController(withIdentifier: "PlaceProfile") as! PlaceProfile
//                navigationController?.pushViewController(vc,animated: true)
//            }
//            self.view.isUserInteractionEnabled = false
//            
//        }
            
        else if (cell.nameLbl.text == "Messages") {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "Messages") as! Messages
            navigationController?.pushViewController(vc,animated: true)
            self.view.isUserInteractionEnabled = false

        } else if (cell.nameLbl.text == "Contact") {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "Contact") as! Contact
            navigationController?.pushViewController(vc,animated: true)
            self.view.isUserInteractionEnabled = false

        } else if (cell.nameLbl.text == "Menu") {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "Menu") as! Menu
            navigationController?.pushViewController(vc,animated: true)
            self.view.isUserInteractionEnabled = false

        }
      
            // 16 May :-
        else if (cell.nameLbl.text == "Friendlist") {
          
            // 17 May :-
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "FriendList") as! FriendListVC
            navigationController?.pushViewController(vc,animated: true)
            self.view.isUserInteractionEnabled = false
            
       }
            
      // 24 April :-
        else if (cell.nameLbl.text == "Payment") {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "StripeButtonVC") as! StripeButtonVC
            navigationController?.pushViewController(vc,animated: true)
            self.view.isUserInteractionEnabled = false

        } else if (cell.nameLbl.text == "About") {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "About") as! About
            navigationController?.pushViewController(vc,animated: true)
            self.view.isUserInteractionEnabled = false
            
        } else if (cell.nameLbl.text == "I'm Buying Request") {
            
//            if (Singleton.sharedInstance.userTypeStr == "member")  Singleton.sharedInstance.checkInStatus == false {
//
//                let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                let vc = storyboard.instantiateViewController(withIdentifier: "ImBuyingFirstVC") as! ImBuyingFirstVC
//                navigationController?.pushViewController(vc,animated: true)
//                self.view.isUserInteractionEnabled = false
//
//            }
            if (Singleton.sharedInstance.userTypeStr == "member") ||  (Singleton.sharedInstance.userTypeStr == "place") ||  (Singleton.sharedInstance.userTypeStr == "artist") {
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "ImBuyingFirstVC") as! ImBuyingFirstVC
                navigationController?.pushViewController(vc,animated: true)
                self.view.isUserInteractionEnabled = false
                
            }
            
        
        } else if (cell.nameLbl.text == "Who's Buying Request")   {
//            if (Singleton.sharedInstance.userTypeStr == "member") && Singleton.sharedInstance.checkInStatus == false {
//                let alert = UIAlertController(title: "", message: "You are not checked-in any restaurant.", preferredStyle: UIAlertControllerStyle.alert)
//                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
//                self.present(alert, animated: true, completion: nil)
//            } else {
//                let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                let vc = storyboard.instantiateViewController(withIdentifier: "WhosBuying") as! WhosBuying
//                navigationController?.pushViewController(vc,animated: true)
//                self.view.isUserInteractionEnabled = false
//            }
            
            
            if (Singleton.sharedInstance.userTypeStr == "member") ||  (Singleton.sharedInstance.userTypeStr == "artist")    {
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "Who_sBuyingFirstVC") as! Who_sBuyingFirstVC
                navigationController?.pushViewController(vc,animated: true)
                self.view.isUserInteractionEnabled = false
                
            }
            
        } else if (cell.nameLbl.text == "Search") {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "Search") as! Search
            navigationController?.pushViewController(vc,animated: true)
            self.view.isUserInteractionEnabled = false
        } else if (cell.nameLbl.text == "Settings") {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "Settings") as! Settings
            navigationController?.pushViewController(vc,animated: true)
            self.view.isUserInteractionEnabled = false
        }
    }
    
    
    // MARK: Get Offline Method
    
    func getOfflineMethod (onlieStatus: String) {
        
        let json = ["user_id":Singleton.sharedInstance.userIdStr,
                    "auth_token":Singleton.sharedInstance.userTokenStr,
                    "online_status":onlieStatus
            ] as [String : Any]
        
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/users/getonline", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
        }
        
    }

    
    // MARK: Get Offline Method
    
    
    func logoutConfirm() {
        Singleton.sharedInstance.socialLoginType = ""
        UserDefaults.standard.setValue(nil, forKey: "socialLoginType")
        
        if Singleton.sharedInstance.userTypeStr == "member" {
            getOfflineMethod(onlieStatus: "0")
        }
        
        Singleton.sharedInstance.blockArray =  [[String: Any]]()
        print(Singleton.sharedInstance.blockArray.count)
        
        
        logoutAPI()
        
        let loginManager = FBSDKLoginManager()
        loginManager.logOut()
        GIDSignIn.sharedInstance().signOut()
        
        let userID = Twitter.sharedInstance().sessionStore.session()?.userID
        if ((userID) != nil) {
            Twitter.sharedInstance().sessionStore.logOutUserID(userID!)
        }
        
        
        let values = ["fcm_token": NSNull()]
        FIRDatabase.database().reference().child("users").child(Singleton.sharedInstance.userIdStr).child("credentials").updateChildValues(values, withCompletionBlock: { (errr, _) in
            if errr == nil {
                
            }
        })
        
        
        UserDefaults.standard.setValue(nil, forKey: "auth_token")
        UserDefaults.standard.setValue(nil, forKey: "user_id")
        UserDefaults.standard.setValue(nil, forKey: "user_type")
        UserDefaults.standard.setValue(nil, forKey: "latitude")
        UserDefaults.standard.setValue(nil, forKey: "longitude")
        
        Singleton.sharedInstance.userImgStr = ""
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "Login") as! Login
        navigationController?.pushViewController(vc,animated: true)
        self.view.isUserInteractionEnabled = false
    }
    
    func logoutAPI() {
       
        let json = ["user_id":Singleton.sharedInstance.userIdStr,
                    "auth_token":Singleton.sharedInstance.userTokenStr
            ] as [String : Any]

        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/members/logout", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                
                // 25 June :-
                UserDefaults.standard.removeObject(forKey: "cart_id")
                UserDefaults.standard.synchronize()
                
        }
        
    }
    
    
    // MARK: Get Profile Method
    
    func profileMethod () {
        
        let json = ["user_id":Singleton.sharedInstance.userIdStr,
                    "auth_token":Singleton.sharedInstance.userTokenStr
            ] as [String : Any]
        
        
        
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/users/getprofile", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    let json = JSON(data: response.data! )
                    if let descriptionStr = json["description"].string {
                        print(descriptionStr)
                        if descriptionStr == "Wrong auth_token" {
                            
                            
//                            UserDefaults.standard.setValue(nil, forKey: "auth_token")
//                            UserDefaults.standard.setValue(nil, forKey: "user_id")
//                            UserDefaults.standard.setValue(nil, forKey: "user_type")
//                            
//                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                            let vc = storyboard.instantiateViewController(withIdentifier: "Login") as! Login
//                            vc.isExpired = true
//                            self.navigationController?.pushViewController(vc,animated: true)
                            
                        } else if descriptionStr == "User Profile" {
                            
                            
                            if (Singleton.sharedInstance.userTypeStr == "member") {
                               Singleton.sharedInstance.blockArray = json["data"]["block_user"].arrayObject! as! [[String : Any]]
                                print(Singleton.sharedInstance.blockArray.count)

                            }
                            
                            
                            if json["data"]["profile_pic_url"].string != nil {
                                Singleton.sharedInstance.userImgStr =  String(describing: json["data"]["profile_pic_url"].string!)
                                Singleton.sharedInstance.userImgStr =  Singleton.sharedInstance.userImgStr.replacingOccurrences(of: "_normal", with: "")
                                
                            } else {
                                Singleton.sharedInstance.userImgStr =  ""
                                
                            }
                            if json["data"]["email"].string != nil {
                                Singleton.sharedInstance.emailStr =  String(describing: json["data"]["email"].string!)
                            } else {
                                Singleton.sharedInstance.emailStr =  ""
                            }

                            if json["data"]["latitude"].string != nil {
                                Singleton.sharedInstance.latitudeStr =  String(describing: json["data"]["latitude"].string!)
                                UserDefaults.standard.setValue(Singleton.sharedInstance.latitudeStr, forKey: "latitude")
                            } else {
                                Singleton.sharedInstance.latitudeStr =  "0.0"
                            }
                            
                            if json["data"]["longitude"].string != nil {
                                Singleton.sharedInstance.longitudeStr =  String(describing: json["data"]["longitude"].string!)
                                UserDefaults.standard.setValue(Singleton.sharedInstance.longitudeStr, forKey: "longitude")
                                
                            } else {
                                Singleton.sharedInstance.longitudeStr =  "0.0"
                            }
                            

                            if json["data"]["address"].string != nil {
                                Singleton.sharedInstance.addressStr =  String(describing: json["data"]["address"].string!)
                            } else {
                                Singleton.sharedInstance.addressStr =  ""
                            }
                            
                            if json["data"]["country"].string != nil {
                                Singleton.sharedInstance.cityStr =  String(describing: json["data"]["country"].string!)
                            } else {
                                Singleton.sharedInstance.countryStr =  ""
                            }
                            
                            if json["data"]["city"].string != nil {
                                Singleton.sharedInstance.cityStr =  String(describing: json["data"]["city"].string!)
                            } else {
                                Singleton.sharedInstance.cityStr =  ""
                            }

                            if json["data"]["state"].string != nil {
                                Singleton.sharedInstance.stateStr =  String(describing: json["data"]["state"].string!)
                            } else {
                                Singleton.sharedInstance.stateStr =  ""
                            }

                            if json["data"]["zip_code"].string != nil {
                                Singleton.sharedInstance.zipcodeStr =  String(describing: json["data"]["zip_code"].string!)
                            } else {
                                Singleton.sharedInstance.zipcodeStr =  ""
                            }

                            
                            if json["data"]["check_in_status"].int != nil {
                                let status =  json["data"]["check_in_status"].int!
                                if status == 0 {
                                    Singleton.sharedInstance.checkInStatus =  false
                                } else {
                                    Singleton.sharedInstance.checkInStatus =  true
                                }
                            } else {
                                
                                Singleton.sharedInstance.checkInStatus =  false
                            }
                            
                            if json["data"]["resturant_id"].int != nil {
                                Singleton.sharedInstance.checkInRestId =  String(describing: json["data"]["resturant_id"].int!)
                            } else {
                                Singleton.sharedInstance.checkInRestId =  ""
                                
                            }
                            
                            var nameStr = String()
                            
                            // 11 June(Applied condition to check which type of place id is login and its name is saved accordingly.) :-
                            if Singleton.sharedInstance.userTypeIdStr == "2"
                             {
                                if json["data"]["resturant_name"].string != nil {
                                    Singleton.sharedInstance.restaurantStr =  String(describing: json["data"]["resturant_name"].string!)
                                } else {
                                    Singleton.sharedInstance.restaurantStr =  ""
                                }
                                 nameStr = Singleton.sharedInstance.restaurantStr

                            }
                                
                    else if Singleton.sharedInstance.userTypeIdStr == "4"
                            {
                                if json["data"]["resturant_name"].string != nil {
                                    Singleton.sharedInstance.restaurantStr =  String(describing: json["data"]["resturant_name"].string!)
                                } else {
                                    Singleton.sharedInstance.restaurantStr =  ""
                                }
                                nameStr = Singleton.sharedInstance.restaurantStr
                                
                            }
                            
                    else if Singleton.sharedInstance.userTypeIdStr == "5"
                            {
                                if json["data"]["resturant_name"].string != nil {
                                    Singleton.sharedInstance.restaurantStr =  String(describing: json["data"]["resturant_name"].string!)
                                } else {
                                    Singleton.sharedInstance.restaurantStr =  ""
                                }
                                nameStr = Singleton.sharedInstance.restaurantStr
                           }
                                
                    else if Singleton.sharedInstance.userTypeIdStr == "6"
                            {
                                if json["data"]["resturant_name"].string != nil {
                                    Singleton.sharedInstance.restaurantStr =  String(describing: json["data"]["resturant_name"].string!)
                                } else {
                                    Singleton.sharedInstance.restaurantStr =  ""
                                }
                                nameStr = Singleton.sharedInstance.restaurantStr
                            }
                                
                    else if Singleton.sharedInstance.userTypeIdStr == "7"
                            {
                                if json["data"]["resturant_name"].string != nil {
                                    Singleton.sharedInstance.restaurantStr =  String(describing: json["data"]["resturant_name"].string!)
                                } else {
                                    Singleton.sharedInstance.restaurantStr =  ""
                                }
                                nameStr = Singleton.sharedInstance.restaurantStr
                            }
                            
                            
                            
                            else {
                                  if json["data"]["firstname"].string != nil {
                                    Singleton.sharedInstance.firstNameStr =  String(describing: json["data"]["firstname"].string!)
                                } else {
                                    Singleton.sharedInstance.firstNameStr =  ""
                                }
                                
                                
                                if json["data"]["lastname"].string != nil {
                                    Singleton.sharedInstance.lastNameStr =  String(describing: json["data"]["lastname"].string!)
                                } else {
                                    Singleton.sharedInstance.lastNameStr =  ""
                                }
                                 nameStr = "\(Singleton.sharedInstance.firstNameStr) \(Singleton.sharedInstance.lastNameStr)"

                            }
                            
                            if Singleton.sharedInstance.firstNameStr == " " {
                                Singleton.sharedInstance.firstNameStr = String(describing: json["data"]["resturant_name"].string!)
                            }

                            var token = String()
                            if FIRInstanceID.instanceID().token() != nil {
                                token = FIRInstanceID.instanceID().token()!
                            } else {
                                token = ""
                            }
                            let values = ["name": nameStr, "userid": Singleton.sharedInstance.userIdStr as String, "fcm_token": token, "profile_pic_url": Singleton.sharedInstance.userImgStr] as [String : Any]
                            FIRDatabase.database().reference().child("users").child(Singleton.sharedInstance.userIdStr).child("credentials").updateChildValues(values, withCompletionBlock: { (errr, _) in
                                if errr == nil {
                                    
                                }
                            })
                            
                            self.getMenuData()
                            self.slidebarTbl.reloadData()
                            
                        }
                        //Now you got your value
                    }
                    
                } else if((response.error) != nil){

                    
                }
        }
        
    }
    

    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
}
