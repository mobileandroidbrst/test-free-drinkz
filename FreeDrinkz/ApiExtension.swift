//
//  ApiExtension.swift
//  FreeDrinkz
//
//  Created by brst on 31/12/18.
//  Copyright © 2018 Brst-Pc109. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import CoreLocation

//Callbacks
typealias JSONDictionary = [String:Any]

class ApiExtension {
    
    let base_Url = "http://beta.brstdev.com/freedrinkz/api/web/v1/members/"
    let share = "addfeeds"
    let feeds = "getfeeds"
    public static let sharedInstance = ApiExtension()
   
    func shareOrderScreen(parameter: [String:Any],completionHandler: @escaping (_ result: NSDictionary) -> Void) {
        
        Alamofire.request(base_Url+share, method: .post, parameters:parameter, encoding: JSONEncoding.default).responseJSON { (response:DataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if let data = response.result.value{
                    
                    if let data_dic = data as? NSDictionary
                    {
                        completionHandler(data_dic)
                    }
                    
                    break
                }
            case .failure(_):
                print(response.result.error as Any)
                completionHandler(["Error":response.result.error as Any])
                break
            }
        }
        
    }

    
    func getFeeds(parameter: [String:Any],completionHandler:@escaping(_ result: NSDictionary)->Void){
        
        
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/users/getfeeds", method: .post, parameters:parameter, encoding: JSONEncoding.default).responseJSON { (response:DataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if let data = response.result.value{
                    
                    if let data_dic = data as? NSDictionary
                    {
                        completionHandler(data_dic)
                    }
                    
                    break
                }
            case .failure(_):
                print(response.result.error as Any)
                completionHandler(["Error":response.result.error as Any])
                break
            }
        }
        
    }
    
    func addVideo(videoPath:NSURL ,parameters:[String:Any],completionHandler:@escaping(_ result:NSDictionary) -> Void ){
        
        print(videoPath)
        var videoData =  Data()
        do {
            videoData =  try NSData(contentsOfFile: (videoPath.relativePath!), options: NSData.ReadingOptions.alwaysMapped) as Data
            print(videoData)
        }
        catch {
            
        }
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(videoData, withName: "video", fileName: "video.mp4", mimeType: "video/mp4")
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to:"http://beta.brstdev.com/freedrinkz/api/web/v1/members/addvideofeed")
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    
                })
                
                upload.responseJSON { response in
                    print(response)
                    if let data = response.result.value{
                        
                        if let data_dic = data as? NSDictionary
                        {
                            completionHandler(data_dic)
                        }
                        
                        
                    }
            }
                
            case .failure( _):
               // completionHandler(["Error" as Any])
            break
                
            }
            
        }
        }

    func newPassword(paramters:[String:Any],completionHandler:@escaping(_ _result:NSDictionary)-> Void){
        
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/members/resetpassword", method: .post, parameters:paramters, encoding:URLEncoding.default).responseJSON { (response:DataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if let data = response.result.value{
                    
                    if let data_dic = data as? NSDictionary
                    {
                        completionHandler(data_dic)
                    }
                    
                    break
                }
            case .failure(_):
                print(response.result.error as Any)
                completionHandler(["Error":response.result.error as Any])
                break
            }
        }
    
    }
    
    
}
