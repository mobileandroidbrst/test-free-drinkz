
//
//  AddPayment.swift
//  FreeDrinkz
//
//  Created by Brst-Pc109 on 28/03/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

import UIKit
import QuartzCore
import Alamofire
import SwiftyJSON
import Stripe

class AddPayment: UIViewController, UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate {

    
    // Outlets
    @IBOutlet weak var payBtn: UIButton!
    @IBOutlet weak var cardNoTxt: UITextField!
    @IBOutlet weak var expiryTxt: UITextField!
    @IBOutlet weak var cvvTxt: UITextField!
    @IBOutlet var cardDetailTV: UITableView!
    @IBOutlet var activityIndicatorView: UIActivityIndicatorView!
    @IBOutlet var buttonImageChange: UIButton!
    @IBOutlet var btnBack: UIButton!
    
    
    
    // Variables
    var paymentArray  = NSMutableArray()
    var creditArray  = NSMutableArray()
    var profileDict  = NSDictionary()
    var user_Id = String()
    var restro_Id = String()
    var product_Id = String()
    var price = String()
    var selected = "0"
    var profileId = NSString()
    var cardid = NSString()
    var cardType = NSArray()
    var stripeToken = String()
    var requestIdStr = String()
    var recieverId = String()
    var whosBuyingValue = Int()
    var searchRestroValue = Int()
    var iAmBuyingPaymentValue = Int()
    var restoCheckIn = Int()

    // 21 June :-
    var cart_Id = String()
   
    // 26 June :-
    var card_id = String()
    var profile_id = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

       print(cart_Id)
        
        self.activityIndicatorView.stopAnimating()
        self.activityIndicatorView.isHidden = true

        
        
      // code for calling the get details method
        showSavedCardInfo()
        
    // code for calling the customization method
        customization()
        
        print(product_Id)
        print(price)
        print(restro_Id)
        
}
    
 
    // MARK: Button Actions
    @IBAction func Back(_ sender: Any) {
        Singleton.sharedInstance.payFromCreditCard = "no"
        
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func Payment(_ sender: Any) {
        
//        if (nameTxt.text?.isEmpty)! {
//            alertViewMethod(titleStr: "", messageStr: "Please enter the name on card.")
//        } 
         if (cardNoTxt.text?.isEmpty)! {
            alertViewMethod(titleStr: "Error", messageStr: "Please enter the card number")
        } else if (expiryTxt.text?.isEmpty)! {
            alertViewMethod(titleStr: "Error", messageStr: "Please enter the expiry date")
        } else if (cvvTxt.text?.isEmpty)! {
            alertViewMethod(titleStr: "Error", messageStr: "Please enter the cvv number")
        } else {
//            Singleton.sharedInstance.payFromCreditCard = "yes"
//            Singleton.sharedInstance.cardNoStr = cardNoTxt.text!
//            Singleton.sharedInstance.expDateStr = expiryTxt.text!
//            Singleton.sharedInstance.expDateStr = Singleton.sharedInstance.expDateStr.replacingOccurrences(of: "/", with: "")
//            _ = navigationController?.popViewController(animated: true)
            generateToken(completionHandler: { success in
                print(success)
            })
            
      }
        
  }
    
    
    
    // code for customizing the fields
    func customization(){
        
        buttonImageChange.layer.borderWidth = 2.0;
        buttonImageChange.layer.borderColor = UIColor.red.cgColor

        // code for opening the keyboard in number pad
        self.cardNoTxt.keyboardType = UIKeyboardType.numberPad
        self.cvvTxt.keyboardType = UIKeyboardType.numberPad
        
        // code for showing the encyrypted cvv no
        cvvTxt.isSecureTextEntry = true
        
    }
    
    // 10 June :-
    func isValidDate(dateString: String) -> Bool {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "YYYY-MM"
        if let _ = dateFormatterGet.date(from: dateString) {
            //date parsing succeeded, if you need to do additional logic, replace _ with some variable name i.e date
            return true
        } else {
            // Invalid date
            return false
        }
    }
    
  
   // code for generating stripe token

    func generateToken(completionHandler:@escaping (Bool) -> ()) {
        
        self.activityIndicatorView.isHidden = false
        self.activityIndicatorView.startAnimating()
        let stripCard = STPCard()
        
        // Split the expiration date to extract Month & Year
        if self.expiryTxt.text?.isEmpty == false {
            // 10 June :-
            if  isValidDate(dateString: expiryTxt.text!) {
            let expirationDate = self.expiryTxt.text?.components(separatedBy:"-")
            let expMonth = UInt((expirationDate?[1])!)
            let expYear = UInt((expirationDate?[0])!)
            
            // Send the card info to Strip to get the token
            stripCard.number = self.cardNoTxt.text
            stripCard.cvc = self.cvvTxt.text
            stripCard.expMonth = expMonth!
            stripCard.expYear = expYear!
            
            }
// 10 June :-
            else {
                let alert = UIAlertController(title: "Alert", message: "Please enter Valid Card Expiry date i.e (YYYY-MM)", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    
        // validate card
        do{
         
            let data  = try  stripCard.validateReturningError()
            print(data)
           }
        catch{
            
            print(error.localizedDescription)
            let objAlertController = UIAlertController(title: "" ,message: error.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
            let objAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler:nil)
            objAlertController.addAction(objAction)
            self.present(objAlertController, animated: true, completion: nil)
            self.activityIndicatorView.isHidden = true
            self.activityIndicatorView.stopAnimating()
            
            completionHandler(false)

            return
        }
        
        STPAPIClient.shared().createToken(with: stripCard, completion: { (token, error) -> Void in
         guard let  stripeTokenALocal = token
                else {
                    NSLog("Error creating token: %@", error!.localizedDescription);
                    return
            }
            self.stripeToken = stripeTokenALocal.tokenId
            print(self.stripeToken)
            
            // code for search
            self.searchRestroValue = (UserDefaults.standard.integer(forKey: "ThreeKey"))
            if self.searchRestroValue == 3{
            // code for fetching the details method
                self.cardDetailTV.isUserInteractionEnabled = false
                // 21 June :-
                // self.performPayment()
                self.newPerformPayment()
            }
            
            
           // code for whos buying
            self.whosBuyingValue = (UserDefaults.standard.integer(forKey: "ThreeKey"))
            print(self.whosBuyingValue)
            if self.whosBuyingValue == 0{
                self.cardDetailTV.isUserInteractionEnabled = false
                // 21 June :-
                // self.paymentWhosMethod()
                self.newPerformPayment()
           
            }
            
           // code for i am buying
            self.iAmBuyingPaymentValue = (UserDefaults.standard.integer(forKey: "ThreeKey"))
            print(self.iAmBuyingPaymentValue)
            if self.iAmBuyingPaymentValue == 2{
                self.cardDetailTV.isUserInteractionEnabled = false
                // 21 June :-
                // self.paymentIAmBuying()
                self.newPerformPayment()
                print(self.stripeToken)
            
            }
            
            completionHandler(true)

      })
    
    }

    // 9 May :-
    
    func getCurrentTimeStampWOMiliseconds(dateToConvert: NSDate) -> String {
        let objDateformat: DateFormatter = DateFormatter()
        objDateformat.dateFormat = "yyyy-MM-dd"
        let strTime: String = objDateformat.string(from: dateToConvert as Date)
        let objUTCDate: NSDate = objDateformat.date(from: strTime)! as NSDate
        let milliseconds: Int64 = Int64(objUTCDate.timeIntervalSince1970)
        let strTimeStamp: String = "\(milliseconds)"
        return strTimeStamp
    }
   
    // 21 June :- (New Payment-Api Implementation)
    func newPerformPayment() {
       
        self.activityIndicatorView.isHidden = false
        self.activityIndicatorView.startAnimating()
        
        self.cvvTxt.isUserInteractionEnabled = false
        self.expiryTxt.isUserInteractionEnabled = false
        self.cardNoTxt.isUserInteractionEnabled = false
        self.btnBack.isEnabled = false
        self.payBtn.isEnabled = false
        self.cardDetailTV.isUserInteractionEnabled = false
        self.buttonImageChange.isUserInteractionEnabled = false
        print(price)
        print(product_Id)
        print(recieverId)
        print(restro_Id)
        
        let timestamp = NSDate().timeIntervalSince1970
        print(timestamp)
        print(String(format: "%.0f", (timestamp)))
        let nowTimeStamp = (String(format: "%.0f", (timestamp)))
        print(nowTimeStamp)
        
// 26 June :-
//        let p : Parameters = ["user_id":Singleton.sharedInstance.userIdStr,"auth_token":Singleton.sharedInstance.userTokenStr,"cart_id":"\(cart_Id)","save_card":selected,"profile_id":"","card_id":"","stripe_token":stripeToken,"timestamp":nowTimeStamp,"device_type":"iphone","device_token":""]
   
        // 26 June :-
let p : Parameters = ["user_id":Singleton.sharedInstance.userIdStr,"auth_token":Singleton.sharedInstance.userTokenStr,"cart_id":"\(cart_Id)","save_card":selected,"profile_id":self.profile_id,"card_id":self.card_id,"stripe_token":stripeToken,"timestamp":nowTimeStamp,"device_type":"iphone","device_token":""]
        
        
       print(p)
        
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/users/cartpayment", method: .post, parameters: p, encoding: JSONEncoding.default)
            .responseString(completionHandler: { (response) in
                
               print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    let json = JSON(data: response.data!)
                    print(json)
                    let message = json["description"].string
                    print(message)
                    
                    // 25 June :-
                    if (UserDefaults.standard.bool(forKey: "senddrinksscreen")) == true {
                       UserDefaults.standard.set(false, forKey:"senddrinksscreen")
                       UserDefaults.standard.synchronize()
                        
                        if message == "Drink/Food successfully ordered."  {
                            
                           UserDefaults.standard.removeObject(forKey: "restroid")
                           UserDefaults.standard.synchronize()
                           UserDefaults.standard.removeObject(forKey: "cart_id")
                           UserDefaults.standard.synchronize()
                            
                            
                        }
                   }
                    
                    
                   let objAlertController = UIAlertController(title: message ,message: "", preferredStyle: UIAlertControllerStyle.alert)
                    let objAction = UIAlertAction(title: "OK",style: UIAlertActionStyle.default, handler:{ (alert) in
                        DispatchQueue.main.async {
                            //self.navigationController?.popViewController(animated: true)
                            for controller in self.navigationController!.viewControllers as Array {
                                if controller.isKind(of: Menu.self) {
                                    self.navigationController!.popToViewController(controller, animated: true)
                                    break
                                }
                            }
                        }
                    })
                    
                   objAlertController.addAction(objAction)
                    self.present(objAlertController, animated: true, completion: nil)
                    DispatchQueue.main.async {
                        self.activityIndicatorView.stopAnimating()
                        self.activityIndicatorView.isHidden = true
                        self.cardDetailTV.reloadData()
                        self.cvvTxt.isUserInteractionEnabled = true
                        self.expiryTxt.isUserInteractionEnabled = true
                        self.cardNoTxt.isUserInteractionEnabled = true
                        self.btnBack.isEnabled = true
                        self.payBtn.isEnabled = true
                        self.cvvTxt.text = ""
                        self.expiryTxt.text = ""
                        self.cardNoTxt.text = ""
                        self.cardDetailTV.isUserInteractionEnabled = true
                        self.buttonImageChange.isUserInteractionEnabled = true
                        //   let menu = self.storyboard?.instantiateViewController(withIdentifier: "MenuViewController") as! Menu
                        // menu.paymentView.isHidden = true
                        //  .paymentView.isHidden = true
                        
                        
                    }
                    
                }
                
            })
   
    }
  
  //Mark: Perform payment through search scenario
    func performPayment() {
        
        
  //      DispatchQueue.main.async{
            
         self.activityIndicatorView.isHidden = false
         self.activityIndicatorView.startAnimating()
        
        
         self.cvvTxt.isUserInteractionEnabled = false
         self.expiryTxt.isUserInteractionEnabled = false
         self.cardNoTxt.isUserInteractionEnabled = false
         self.btnBack.isEnabled = false
         self.payBtn.isEnabled = false
         self.cardDetailTV.isUserInteractionEnabled = false
         self.buttonImageChange.isUserInteractionEnabled = false
        print(price)
        print(product_Id)
        print(recieverId)
        print(restro_Id)
        
        
//            let p : Parameters = ["user_id":Singleton.sharedInstance.userIdStr,"auth_token":Singleton.sharedInstance.userTokenStr,"receiver_id":self.recieverId,"resturant_id": self.restro_Id, "amount":self.price,"product_id":self.product_Id,"save_card":self.selected,"profile_id":"","card_id":"","stripe_token":self.stripeToken]
//            print(p)
            
 //       }

         // 9 May :-
        // let now = NSDate()
        // let nowTimeStamp = self.getCurrentTimeStampWOMiliseconds(dateToConvert: now)
        // print(nowTimeStamp)
        
        // 6 June :-
        let timestamp = NSDate().timeIntervalSince1970
        print(timestamp)
        print(String(format: "%.0f", (timestamp)))
        let nowTimeStamp = (String(format: "%.0f", (timestamp)))
        print(nowTimeStamp)
        
       
        let p : Parameters = ["user_id":Singleton.sharedInstance.userIdStr,"auth_token":Singleton.sharedInstance.userTokenStr,"receiver_id":recieverId,"resturant_id": restro_Id, "amount":price,"product_id":product_Id,"save_card":selected,"profile_id":"","card_id":"","stripe_token":stripeToken,"timestamp":nowTimeStamp]
        
        
        print(p)
        
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/members/sendproductdrinkauthrize", method: .post, parameters: p, encoding: JSONEncoding.default)
            .responseString(completionHandler: { (response) in
                
                
                print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    let json = JSON(data: response.data!)
                    print(json)
                    let message = json["description"].string
                    print(message)
                    let objAlertController = UIAlertController(title: message ,message: "", preferredStyle: UIAlertControllerStyle.alert)
                    let objAction = UIAlertAction(title: "OK",style: UIAlertActionStyle.default, handler:{ (alert) in
                        DispatchQueue.main.async {
                            self.navigationController?.popViewController(animated: true)
                        }
                    })
                    
                    
                    objAlertController.addAction(objAction)
                    self.present(objAlertController, animated: true, completion: nil)
                    DispatchQueue.main.async {
                        self.activityIndicatorView.stopAnimating()
                        self.activityIndicatorView.isHidden = true
                        self.cardDetailTV.reloadData()
                        self.cvvTxt.isUserInteractionEnabled = true
                        self.expiryTxt.isUserInteractionEnabled = true
                        self.cardNoTxt.isUserInteractionEnabled = true
                        self.btnBack.isEnabled = true
                        self.payBtn.isEnabled = true
                        self.cvvTxt.text = ""
                        self.expiryTxt.text = ""
                        self.cardNoTxt.text = ""
                        self.cardDetailTV.isUserInteractionEnabled = true
                        self.buttonImageChange.isUserInteractionEnabled = true
                     //   let menu = self.storyboard?.instantiateViewController(withIdentifier: "MenuViewController") as! Menu
                       // menu.paymentView.isHidden = true
                      //  .paymentView.isHidden = true
                    
                    
                    }
                
                }
            
            })

    }
    
    
    
    // Mark: Perform payment through who is buying scenario
    func paymentWhosMethod() {
        
        
        self.cardDetailTV.isUserInteractionEnabled = false
        self.cvvTxt.isUserInteractionEnabled = false
        self.expiryTxt.isUserInteractionEnabled = false
        self.cardNoTxt.isUserInteractionEnabled = false
        self.btnBack.isEnabled = false
        self.payBtn.isEnabled = false
        self.buttonImageChange.isUserInteractionEnabled = false

        
        
        
       let p : Parameters = ["user_id":Singleton.sharedInstance.userIdStr,"auth_token":Singleton.sharedInstance.userTokenStr,"receiver_id":recieverId,"resturant_id": restro_Id, "amount":price,"product_id":product_Id,"save_card":selected,"profile_id":"","card_id":"","stripe_token":stripeToken,"request_id":requestIdStr]
        
        print (p)
        
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/members/whobuyingformeacceptauthorize", method: .post, parameters: p, encoding: JSONEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    let json = JSON(data: response.data! )
                   let message = json["description"].string
                    let objAlertController = UIAlertController(title: message ,message: "", preferredStyle: UIAlertControllerStyle.alert)
                    let objAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler:{ (alert) in
                        DispatchQueue.main.async {
                            self.navigationController?.popViewController(animated: true)
                        }
                    })
                    
                    
                    objAlertController.addAction(objAction)
                    self.present(objAlertController, animated: true, completion: nil)
                    self.cardDetailTV.isUserInteractionEnabled = true
                    self.cvvTxt.isUserInteractionEnabled = true
                    self.expiryTxt.isUserInteractionEnabled = true
                    self.cardNoTxt.isUserInteractionEnabled = true
                    self.btnBack.isEnabled = true
                    self.payBtn.isEnabled = true
                    self.buttonImageChange.isUserInteractionEnabled = true
                    self.cvvTxt.text = ""
                    self.expiryTxt.text = ""
                    self.cardNoTxt.text = ""
                }
                else if((response.error) != nil){
                    self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                }
                
                //  self.indicator.stopAnimating()
                //self.indicator.isHidden = true
                
        }
        
    }
    
    
    // perform payment through Iambuying scenario
    func paymentIAmBuying() {
        
        
          self.cardDetailTV.isUserInteractionEnabled = false
          self.cvvTxt.isUserInteractionEnabled = false
          self.expiryTxt.isUserInteractionEnabled = false
          self.cardNoTxt.isUserInteractionEnabled = false
          self.btnBack.isEnabled = false
          self.payBtn.isEnabled = false
          self.buttonImageChange.isUserInteractionEnabled = false
        
        // 9 May :-
        
        let now = NSDate()
        let nowTimeStamp = self.getCurrentTimeStampWOMiliseconds(dateToConvert: now)
        print(nowTimeStamp)
        

        // 9 May :- Time-Stamp value is added.
        let  json = ["user_id":Singleton.sharedInstance.userIdStr,
                     "auth_token":Singleton.sharedInstance.userTokenStr,
                     "request_id":requestIdStr,
                     "status":"2",
                     "product_id":product_Id,
                     "resturant_id":restro_Id,
                     "amount":price,
                    "receiver_id":recieverId,
                    "card_id":"",
                    "profile_id":"",
                    "stripe_token":stripeToken,
                    "save_card":selected,
                    "timestamp":nowTimeStamp
            
                     
            
            ] as [String : Any]
        
        //  restruant_id
        
        
        print(json)
        
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/members/updatebuystatusauthorize", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    let json = JSON(data: response.data! )
                    let message = json["description"].string
                 //   print(message)
                    let objAlertController = UIAlertController(title: message ,message: "", preferredStyle: UIAlertControllerStyle.alert) 
                    let objAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler:{ (alert) in
                        DispatchQueue.main.async {
                            self.navigationController?.popViewController(animated: true)
                        }
                    })
                    
                    
                    objAlertController.addAction(objAction)
                    self.present(objAlertController, animated: true, completion: nil)
                    self.cardDetailTV.isUserInteractionEnabled = true
                    self.cvvTxt.isUserInteractionEnabled = true
                    self.expiryTxt.isUserInteractionEnabled = true
                    self.cardNoTxt.isUserInteractionEnabled = true
                    self.btnBack.isEnabled = true
                    self.payBtn.isEnabled = true
                    self.buttonImageChange.isUserInteractionEnabled = true
                    self.cvvTxt.text = ""
                    self.expiryTxt.text = ""
                    self.cardNoTxt.text = ""


                    
           } else if((response.error) != nil){
                    self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                }
           //     self.indicator.stopAnimating()
              //  self.indicator.isHidden = true
                
        }
        
    }
    
    //Mark: Table View did select scenario
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
 
        
        // perform payment through search scenario again
        self.searchRestroValue = (UserDefaults.standard.integer(forKey: "ThreeKey"))
        if self.searchRestroValue == 3{
            let dict = self.paymentArray[indexPath.row] as! NSDictionary
            print(dict)
            self.cardid = dict.value(forKey: "id")  as! NSString
            print(self.cardid)
            self.profileId = dict.value(forKey: "customer")  as!  NSString
            print(self.profileId)
            let objAlertController = UIAlertController(title:"Do you want to do payment now?",message: "", preferredStyle: UIAlertControllerStyle.alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (alert) in
                
                // 21 June :-
//                self.generateToken(completionHandler: { success in
//
//                    if success == true {
//                        self.performPaymentThroughRestroLogin(profileIdData: self.profileId as String, cardIdData: self.cardid as String)
//                    }
//                })
                
                // 26 June :-
                self.newPerformPayment()
                
                
            })
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: { (alert) in
                
            })
            
            objAlertController.addAction(okAction)
            objAlertController.addAction(cancelAction)
            self.present(objAlertController, animated: true, completion: nil)
            
        }
    
        // perform payment through whos buying scenario
         self.whosBuyingValue = (UserDefaults.standard.integer(forKey: "ThreeKey"))
        if whosBuyingValue == 0{
            
       
            let dict = self.paymentArray[indexPath.row] as! NSDictionary
            print(dict)
            self.cardid = dict.value(forKey: "id")  as! NSString
            print(self.cardid)
            self.profileId = dict.value(forKey: "customer")  as!  NSString
            print(self.profileId)
            let objAlertController = UIAlertController(title:"Do you want to do payment now?",message: "", preferredStyle: UIAlertControllerStyle.alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (alert) in
                
                // 21 June :-
                // 3 July :-
                
              //  self.generateToken(completionHandler: { success in

             //       if success == true {
                        
                        self.performPaymentThroughWhosBuying(profileIdData: self.profileId as String , cardIdData: self.cardid as String)
                        
             //       }
             //   })
                
            })
            
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: { (alert) in
                
            })
            
            objAlertController.addAction(okAction)
            objAlertController.addAction(cancelAction)
            self.present(objAlertController, animated: true, completion: nil)
  
        }
        
        
        // perform payment through i am buying scenario
     self.iAmBuyingPaymentValue = (UserDefaults.standard.integer(forKey: "ThreeKey"))
        if iAmBuyingPaymentValue == 2{
            
            
            let dict = self.paymentArray[indexPath.row] as! NSDictionary
            print(dict)
            self.cardid = dict.value(forKey: "id")  as! NSString
            print(self.cardid)
            self.profileId = dict.value(forKey: "customer")  as!  NSString
            print(self.profileId)
            let objAlertController = UIAlertController(title:"Do you want to do payment now?",message: "", preferredStyle: UIAlertControllerStyle.alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (alert) in
                
                // 4 June :-
                // 3 July :-
             //   self.generateToken(completionHandler: { success in
             //       print(success)
              //      if success == true {
                        self.performPaymentThroughIAmBuying(profileIdData: self.profileId as String , cardIdData: self.cardid as String)
               //     }
              //  })
                
                
                
              
            })
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: { (alert) in
                
            })
            
            objAlertController.addAction(okAction)
            objAlertController.addAction(cancelAction)
            self.present(objAlertController, animated: true, completion: nil)
            
        }
        
    }

    
    //Mark: Perform payment through restro Login on did select
    func performPaymentThroughRestroLogin(profileIdData: String, cardIdData: String) {
        
         self.activityIndicatorView.isHidden = false
        self.activityIndicatorView.startAnimating()
        self.cvvTxt.isUserInteractionEnabled = false
        self.expiryTxt.isUserInteractionEnabled = false
        self.cardNoTxt.isUserInteractionEnabled = false
        self.btnBack.isEnabled = false
        self.payBtn.isEnabled = false
        self.buttonImageChange.isEnabled = false
        
        // 21 June :-
        // let p : Parameters = ["user_id":Singleton.sharedInstance.userIdStr,"auth_token":Singleton.sharedInstance.userTokenStr,"receiver_id":recieverId,"resturant_id": restro_Id, "amount":price,"product_id":product_Id,"save_card":selected,"profile_id":profileId,"card_id":cardid,"stripe_token":stripeToken]
        
        let timestamp = NSDate().timeIntervalSince1970
        print(timestamp)
        print(String(format: "%.0f", (timestamp)))
        let nowTimeStamp = (String(format: "%.0f", (timestamp)))
        print(nowTimeStamp)
        
        let p : Parameters = ["user_id":Singleton.sharedInstance.userIdStr,"auth_token":Singleton.sharedInstance.userTokenStr,"save_card":selected,"profile_id":profileId,"card_id":cardid,"stripe_token":stripeToken,"cart_id":"\(cart_Id)","device_type":"iphone","device_token":"","timestamp":nowTimeStamp]
        
        print(p)
        
        // 21 June :- Previous Api :- "http://beta.brstdev.com/freedrinkz/api/web/v1/members/sendproductdrinkauthrize"
        
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/users/cartpayment", method: .post, parameters: p, encoding: JSONEncoding.default)
            .responseString(completionHandler: { (response) in
                print(response)
                if let jsonData = response.result.value {
                    print("JSON:  ?? <#default value#>\(jsonData)")
                    let json = JSON(data: response.data!)
                    let message = json["description"].string
                    
                   
                     let objAlertController = UIAlertController(title: message ,message: "", preferredStyle: UIAlertControllerStyle.alert)
                    let objAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (alert) in
                        
                        // 21 June :-
                       // self.navigationController?.popViewController(animated: true)
                        
                        // 3 July :-
                        for controller in self.navigationController!.viewControllers as Array {
                            if controller.isKind(of: Menu.self) {
                                self.navigationController!.popToViewController(controller, animated: true)
                                break
                            }
                        }
                    })
                    
                    objAlertController.addAction(objAction)
                    self.present(objAlertController, animated: true, completion: nil)
                    DispatchQueue.main.async {
                        self.activityIndicatorView.stopAnimating()
                        self.activityIndicatorView.isHidden = true
                        self.cardDetailTV.reloadData()
                        self.cvvTxt.isUserInteractionEnabled = true
                        self.expiryTxt.isUserInteractionEnabled = true
                        self.cardNoTxt.isUserInteractionEnabled = true
                        self.btnBack.isEnabled = true
                        self.payBtn.isEnabled = true
                        self.cvvTxt.text = ""
                        self.expiryTxt.text = ""
                        self.cardNoTxt.text = ""
                        self.buttonImageChange.isEnabled = true
                        
                        
                        
                    }
                    
                    
                }
            })
    }
    

    //Mark: Perform payment through whos method on did select
    func performPaymentThroughWhosBuying(profileIdData: String, cardIdData: String) {
        
        self.activityIndicatorView.isHidden = false
        self.activityIndicatorView.startAnimating()
        self.cvvTxt.isUserInteractionEnabled = false
        self.expiryTxt.isUserInteractionEnabled = false
        self.cardNoTxt.isUserInteractionEnabled = false
        self.btnBack.isEnabled = false
        self.payBtn.isEnabled = false
        self.buttonImageChange.isEnabled = false
        self.cardDetailTV.isUserInteractionEnabled = false
        
// 21 June :-
//        let p : Parameters = ["user_id":Singleton.sharedInstance.userIdStr,"auth_token":Singleton.sharedInstance.userTokenStr,"receiver_id":recieverId,"resturant_id": restro_Id, "amount":price,"product_id":product_Id,"save_card":selected,"profile_id":profileId,"card_id":cardid,"stripe_token":"","request_id":requestIdStr]
        
        let timestamp = NSDate().timeIntervalSince1970
        print(timestamp)
        print(String(format: "%.0f", (timestamp)))
        let nowTimeStamp = (String(format: "%.0f", (timestamp)))
        print(nowTimeStamp)
        
        let p : Parameters = ["user_id":Singleton.sharedInstance.userIdStr,"auth_token":Singleton.sharedInstance.userTokenStr,"save_card":selected,"profile_id":profileId,"card_id":cardid,"stripe_token":stripeToken,"cart_id":"\(cart_Id)","device_type":"iphone","device_token":"","timestamp":nowTimeStamp]
        
       print(p)
        
      // 21 June :- Previous Api :- "http://beta.brstdev.com/freedrinkz/api/web/v1/members/whobuyingformeacceptauthorize"
        
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/users/cartpayment", method: .post, parameters: p, encoding: JSONEncoding.default)
            .responseString(completionHandler: { (response) in
                print(response)
                if let jsonData = response.result.value {
                    print("JSON:  ?? <#default value#>\(jsonData)")
                    let json = JSON(data: response.data!)
                    let message = json["description"].string
                    
                    // 3 July :-
                    if message == "Drink/Food successfully ordered." {
                        UserDefaults.standard.removeObject(forKey: "cart_id")
                        UserDefaults.standard.synchronize()
                    }
                    
                   let objAlertController = UIAlertController(title: message ,message: "", preferredStyle: UIAlertControllerStyle.alert)
                    let objAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (alert) in
                        
                        // 3 July :-
                       // self.navigationController?.popViewController(animated: true)
                        for controller in self.navigationController!.viewControllers as Array {
                            if controller.isKind(of: Menu.self) {
                                self.navigationController!.popToViewController(controller, animated: true)
                                break
                            }
                        }
                        
                     })
                    objAlertController.addAction(objAction)
                    self.present(objAlertController, animated: true, completion: nil)
                    DispatchQueue.main.async {
                        self.activityIndicatorView.stopAnimating()
                        self.activityIndicatorView.isHidden = true
                        self.cardDetailTV.reloadData()
                        self.cvvTxt.isUserInteractionEnabled = true
                        self.expiryTxt.isUserInteractionEnabled = true
                        self.cardNoTxt.isUserInteractionEnabled = true
                        self.btnBack.isEnabled = true
                        self.payBtn.isEnabled = true
                        self.cvvTxt.text = ""
                        self.expiryTxt.text = ""
                        self.cardNoTxt.text = ""
                        self.buttonImageChange.isEnabled = true
                        self.cardDetailTV.isUserInteractionEnabled = true
                        
                        
                        
                    }
                    
                    
                }
            })
    }
    

    
    //Mark: Perform payment through I am buying on did select
    func performPaymentThroughIAmBuying(profileIdData: String, cardIdData: String) {
        
        print(stripeToken)
        
        self.activityIndicatorView.isHidden = false
        self.activityIndicatorView.startAnimating()
        self.cvvTxt.isUserInteractionEnabled = false
        self.expiryTxt.isUserInteractionEnabled = false
        self.cardNoTxt.isUserInteractionEnabled = false
        self.btnBack.isEnabled = false
        self.payBtn.isEnabled = false
        self.buttonImageChange.isEnabled = false
  
// 21 June :-
//        // 9 May :-
//        let now = NSDate()
//        let nowTimeStamp = self.getCurrentTimeStampWOMiliseconds(dateToConvert: now)
//        print(nowTimeStamp)
//
//       // 9 May :- time-stamp is added.
//        // 4 June(Added Stripe-Token):-
//        let p : Parameters = ["user_id":Singleton.sharedInstance.userIdStr,"auth_token":Singleton.sharedInstance.userTokenStr,"receiver_id":requestIdStr,"resturant_id": restro_Id, "amount":price,"product_id":product_Id,"save_card":selected,"profile_id":profileId,"card_id":cardid,"stripe_token":stripeToken,"timestamp":nowTimeStamp]
//
//        print(p)
//        print(stripeToken)
 
      
        let timestamp = NSDate().timeIntervalSince1970
        print(timestamp)
        print(String(format: "%.0f", (timestamp)))
        let nowTimeStamp = (String(format: "%.0f", (timestamp)))
        print(nowTimeStamp)
        
        let p : Parameters = ["user_id":Singleton.sharedInstance.userIdStr,"auth_token":Singleton.sharedInstance.userTokenStr,"save_card":selected,"profile_id":profileId,"card_id":cardid,"stripe_token":stripeToken,"cart_id":"\(cart_Id)","device_type":"iphone","device_token":"","timestamp":nowTimeStamp]
        
        print(p)
        
        // 21 June :- Previous Api :- "http://beta.brstdev.com/freedrinkz/api/web/v1/members/whobuyingformeacceptauthorize"
        
       Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/users/cartpayment", method: .post, parameters: p, encoding: JSONEncoding.default)
            .responseString(completionHandler: { (response) in
                print(response)
                if let jsonData = response.result.value {
                    print("JSON:  ?? <#default value#>\(jsonData)")
                    let json = JSON(data: response.data!)
                    let message = json["description"].string
                    
                    // 3 July :-
                    if message == "Drink/Food successfully ordered." {
                        UserDefaults.standard.removeObject(forKey: "cart_id")
                        UserDefaults.standard.synchronize()
                    }
                    
                   let objAlertController = UIAlertController(title: message ,message: "", preferredStyle: UIAlertControllerStyle.alert)
                    let objAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (alert) in
                        
                        // 3 July :-
                       // self.navigationController?.popViewController(animated: true)
                        for controller in self.navigationController!.viewControllers as Array {
                            if controller.isKind(of: Menu.self) {
                                self.navigationController!.popToViewController(controller, animated: true)
                                break
                            }
                        }
                        
                    })
                    objAlertController.addAction(objAction)
                    self.present(objAlertController, animated: true, completion: nil)
                    DispatchQueue.main.async {
                        self.activityIndicatorView.stopAnimating()
                        self.activityIndicatorView.isHidden = true
                        self.cardDetailTV.reloadData()
                        self.cvvTxt.isUserInteractionEnabled = true
                        self.expiryTxt.isUserInteractionEnabled = true
                        self.cardNoTxt.isUserInteractionEnabled = true
                        self.btnBack.isEnabled = true
                        self.payBtn.isEnabled = true
                        self.cvvTxt.text = ""
                        self.expiryTxt.text = ""
                        self.cardNoTxt.text = ""
                        self.buttonImageChange.isEnabled = true
                        
                        
                        
                    }
                    
                    
                }
            })
    }
    
// MARK: AlertView
    
    func alertViewMethod(titleStr:String, messageStr:String) {
        let alert = UIAlertController(title: titleStr, message: messageStr, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

    
    
    // MARK: Text Field Delegate
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool { // return NO to not change text
        
        
        if textField == cvvTxt {
            
            guard let text = textField.text else { return true }
            let newLength = text.characters.count + string.characters.count - range.length
         //   let newLengthAgain = cvvTxt.text
            if (newLength > 3) {
              
                let objAlertController = UIAlertController(title: "" ,message: "You card's security code is invalid", preferredStyle: UIAlertControllerStyle.alert)
                let objAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler:nil)
                objAlertController.addAction(objAction)
                self.present(objAlertController, animated: true, completion: nil)
            }
        }
        
        
        if textField == expiryTxt{
            
            
            
            var strText: String? = expiryTxt.text
            if strText == nil {
                strText = ""
            }
        
            
         strText = strText?.replacingOccurrences(of: "-", with: "")
            
            
            if strText!.characters.count > 1 && strText!.characters.count % 4 == 0 && string != "" {
                textField.text = "\(expiryTxt.text!)-\(string)"
                return false
            }
            
        }
        
        if textField == cardNoTxt {
            
            guard let text = textField.text else { return true }
            let newLength = text.characters.count + string.characters.count - range.length
            if (newLength > 16) {
                
                
                let objAlertController = UIAlertController(title: "" ,message: "You cannot enter card more than 16 digits", preferredStyle: UIAlertControllerStyle.alert)
                let objAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler:nil)
                objAlertController.addAction(objAction)
                self.present(objAlertController, animated: true, completion: nil)
              //  return newLength <= 19
            }
       
        }

        return true
        
    }
    

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
      //  nameTxt.resignFirstResponder()
        cardNoTxt.resignFirstResponder()
        cvvTxt.resignFirstResponder()
        expiryTxt.resignFirstResponder()
        
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool  {
        textField.resignFirstResponder()
        return true;
    }
    

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // showing the savedCreditCardList
    
    func showSavedCardInfo(){

        self.activityIndicatorView.isHidden = false
        self.activityIndicatorView.startAnimating()
        self.cvvTxt.isUserInteractionEnabled = false
        self.expiryTxt.isUserInteractionEnabled = false
        self.cardNoTxt.isUserInteractionEnabled = false
        self.btnBack.isEnabled = false
        self.payBtn.isEnabled = false
        self.buttonImageChange.isEnabled = false
        
        let p : Parameters = ["user_id":Singleton.sharedInstance.userIdStr,"auth_token":Singleton.sharedInstance.userTokenStr]
        print(Singleton.sharedInstance.userIdStr)
        print(Singleton.sharedInstance.userTokenStr)
        
        
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/members/getcustomerprofile", method: .post, parameters: p, encoding: JSONEncoding.default)
            .responseString(completionHandler: { (response) in
                print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    let json = JSON(data: response.data!)
                    if let jsonDict = json.dictionaryObject {
                        print(jsonDict)
                        if let sources = jsonDict["sources"] as? NSDictionary {
                            print(sources)
                          let paymentarr = sources["data"] as! NSArray
                            print(paymentarr)
                            self.paymentArray = paymentarr.mutableCopy() as! NSMutableArray
                            print(self.paymentArray)
                       //     let dict = self.paymentArray as! NSMutableDictionary
                         //   print(dict)
                           self.cardType = self.paymentArray.value(forKey: "brand") as! NSArray
                            print(self.cardType)
                            let cardNum = self.paymentArray.value(forKey: "last4") as! NSArray
                              print(cardNum)
                           
                            DispatchQueue.main.async {
                                self.activityIndicatorView.stopAnimating()
                                self.activityIndicatorView.isHidden = true
                                self.cardDetailTV.reloadData()
                                self.cvvTxt.isUserInteractionEnabled = true
                                self.expiryTxt.isUserInteractionEnabled = true
                                self.cardNoTxt.isUserInteractionEnabled = true
                                self.btnBack.isEnabled = true
                                self.payBtn.isEnabled = true
                                self.buttonImageChange.isEnabled = true

                            }
           
                        }
                            
                        else{
                           
                           
                            self.activityIndicatorView.stopAnimating()
                            self.activityIndicatorView.isHidden = true
                            self.cardDetailTV.isHidden = true
                            
                        }
                        
                        // 9 May :-
                        self.activityIndicatorView.stopAnimating()
                        self.activityIndicatorView.isHidden = true
                        self.cardDetailTV.reloadData()
                        self.cvvTxt.isUserInteractionEnabled = true
                        self.expiryTxt.isUserInteractionEnabled = true
                        self.cardNoTxt.isUserInteractionEnabled = true
                        self.btnBack.isEnabled = true
                        self.payBtn.isEnabled = true
                        self.buttonImageChange.isEnabled = true
                        
                        
                  }
                    
                }
            })

        // 9 May :-
        btnBack.isEnabled = true
     
        
        
    }
    
    
    func convertToDictionaryCreditList(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    // table view methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return paymentArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:CardDetailTableViewCell = cardDetailTV.dequeueReusableCell(withIdentifier: "cardDetail") as! CardDetailTableViewCell
        
        print(paymentArray)
        let dict =  paymentArray[indexPath.row] as! NSDictionary
        print(dict)
        let cardNum = dict["last4"] as? String
        let cardString = "XXXX XXXX XXXX"
        cell.lblCardNumAddPayment.text = cardString + " "+cardNum!
        cell.lblCardTypeAddPayment.text = dict["brand"] as? String
        
        // 26 June :-
        self.card_id = dict["id"] as! String
        self.profile_id = dict["customer"] as! String
        
        
        
        return cell
        
        
        
    }
    

    @IBAction func checkMarkOnButton(_ sender: Any) {
        
        print(buttonImageChange.isSelected)
        if (sender as AnyObject).tag == 1 {
           
            buttonImageChange.setImage(UIImage(named: ""), for: .normal)
            buttonImageChange.tintColor = UIColor.red
            buttonImageChange.tag = 0
            selected = "0"
        
        }
        else{
        buttonImageChange.setImage(UIImage(named: "checkmarkagain"), for: .normal)
        buttonImageChange.tintColor = UIColor.red
        buttonImageChange.tag = 1
           selected = "1"
       
        }
    
  
}
    
  
}
