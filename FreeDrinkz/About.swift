//
//  About.swift
//  FreeDrinkz
//
//  Created by Brst-Pc109 on 31/03/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

import UIKit

class About: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    // MARK: Button Actions
    
   
    @IBAction func Back(_ sender: Any) {
        if Singleton.sharedInstance.userTypeIdStr == "1" {
            print("member")
            Singleton.sharedInstance.userTypeStr = "member"
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "Search") as! Search
            self.navigationController?.pushViewController(vc,animated: true)
            
        } else if Singleton.sharedInstance.userTypeIdStr == "2" {
            print("place")
            Singleton.sharedInstance.userTypeStr = "place"
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "Search") as! Search
            self.navigationController?.pushViewController(vc,animated: true)
            
        }else if Singleton.sharedInstance.userTypeIdStr == "3" {
            print("artist")
            Singleton.sharedInstance.userTypeStr = "artist"
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ArtistProfile") as! ArtistProfile
            self.navigationController?.pushViewController(vc,animated: true)
        }    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
