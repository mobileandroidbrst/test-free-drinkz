//
//  ProfileHeader.swift
//  FreeDrinkz
//
//  Created by Brst-Pc109 on 30/03/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

import UIKit
import MapKit

class ProfileHeader: UITableViewCell {
    @IBOutlet weak var sendMsgBtn: UIButton!
    @IBOutlet weak var sendFoodBtn: UIButton!
  
   // 17 May :- (AddFriend and UnFriend Button is added)
    @IBOutlet var addFriendBtn: UIButton!
    @IBOutlet var addFriendImageView: UIImageView!
    
    @IBOutlet weak var sendView: UIView!
  
    @IBOutlet weak var nameLbl: UILabel!
     @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var userImg: UIImageView!

    @IBOutlet weak var fbBtn: UIButton!
    @IBOutlet weak var twitterBtn: UIButton!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var profileBackView: UIView!
    @IBOutlet weak var postVideoBtn: UIButton!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
