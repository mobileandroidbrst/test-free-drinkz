//
//  ImBuyingCell.swift
//  FreeDrinkz
//
//  Created by Brst-Pc109 on 04/04/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

import UIKit

class ImBuyingCell: UITableViewCell {
    @IBOutlet weak var memberImg: UIImageView!
    @IBOutlet weak var memberNameLbl: UILabel!
    @IBOutlet weak var requestBtn: UIButton!
    @IBOutlet weak var profileBtn: UIButton!
    @IBOutlet weak var locationTxt: UILabel!
    @IBOutlet weak var restroName: UILabel!
    @IBOutlet weak var restroAddress: UILabel!
    @IBOutlet weak var checkInPplStatus: UILabel!
    @IBOutlet weak var checkInBtn: UIButton!
    @IBOutlet weak var imgViewRestro: UIImageView!
     @IBOutlet weak var requestButton: UIButton!
   
    // 22 May :-
    
    @IBOutlet var mainBackgroundView: UIView!
    @IBOutlet var shadowView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
