//
//  ArtistHeader.swift
//  FreeDrinkz
//
//  Created by Brst-Pc109 on 28/03/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

import UIKit
import MapKit

class ArtistHeader: UITableViewCell {

    
    @IBOutlet weak var postLbl: UILabel!
    @IBOutlet weak var detailsLbl: UILabel!
    @IBOutlet weak var aboutLbl: UITextView!
    @IBOutlet weak var postBtn: UIButton!
    @IBOutlet weak var detailsBtn: UIButton!
    // 24 May :-
    @IBOutlet var addFrndBtn: UIButton!
    
    @IBOutlet weak var changeTxt: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var userImg: UIImageView!
    
    @IBOutlet weak var fbBtn: UIButton!
    @IBOutlet weak var twitterBtn: UIButton!
    @IBOutlet weak var editAboutBtn: UIButton!
    @IBOutlet weak var editPerformanceBtn: UIButton!
    @IBOutlet weak var aboutArtistView: UIView!
    @IBOutlet weak var editAboutImg: UIImageView!
    @IBOutlet weak var editPerformanceImg: UIImageView!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var postVideoBtn: UIButton!
    @IBOutlet weak var feedsLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
