//
//  CartDrinksVC.swift
//  FreeDrinkz
//
//  Created by tbi-pc-57 on 6/20/19.
//  Copyright © 2019 Brst-Pc109. All rights reserved.
//
// 20 June :-


import UIKit
import Alamofire
import SwiftyJSON

class CartDrinksVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

    // Mark :- Outlets
    @IBOutlet var cartDrinksTableView: UITableView!
    var drinksArray = NSMutableArray()
    var productId = String()
    var recievr_Name = String()
    
    // 21 June :-
    var cart_Id = String()
    
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        print(drinksArray)
        print(recievr_Name)
        print(cart_Id)
        
        self.cartDrinksTableView.rowHeight = 125
        
        
      //  cartDrinksTableView.reloadData()
        
    }
    
    // 21 June :-
    
    override func viewWillAppear(_ animated: Bool) {
         cartDrinksTableView.reloadData()
    }
    
    
    // Mark :- Tableview - Delegates and DataSources.
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return drinksArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:CartDrinksCell = cartDrinksTableView.dequeueReusableCell(withIdentifier: "CartDrinksCell") as! CartDrinksCell
        
        cell.selectionStyle = .none
        
        let dict = drinksArray[indexPath.row] as! NSDictionary
        
        if let restName = dict["resturant_name"] as? String {
            if let drinkName = dict["drink_name"] as? String {
               cell.drinkRestNameLbl.text = drinkName + "@" + " " + restName
             }
         }
        
      
        if let drinkImage = dict["drink_image"] as? String {
          //  cell.drinkImageView.sd_setImage(with: URL(string:drinkImage), placeholderImage: nil)
            let urlStr : String = (drinkImage)
            let urlFormattedStr = urlStr.replacingOccurrences(of: " ", with: "%20")
            cell.drinkImageView.sd_setImage(with: URL(string:urlFormattedStr), placeholderImage: UIImage(named: ""), options: .refreshCached)
      
        }
        
        if let restAddress = dict["resturant_address"] as? String {
            cell.restAddressLbl.text = restAddress
        }
       
        // 27 June :-
       // self.productId = String(describing: dict["product_id"] as! Int)
        self.productId = String(describing: dict["drink_id"] as! Int)
        
        
        if let timestamp = dict["timestamp"] as? String {
            let timestampDouble = Double(timestamp)
            let date = Date(timeIntervalSince1970: timestampDouble!)
            let dateFormatter = DateFormatter()
            dateFormatter.locale = NSLocale.current
            dateFormatter.dateFormat = "dd-MMMM-yyyy hh:mm a"
            let strDate = dateFormatter.string(from: date)
            cell.dateTimeLbl.text =  "Date:" + strDate
        }
        
       // if let receiverName = dict["reciever_name"] as? String {
       //     cell.recievrNameLbl.text = receiverName
       // }
        
        cell.recievrNameLbl.text = recievr_Name
        
        cell.deleteBtn.tag = indexPath.row
        cell.deleteBtn.addTarget(self, action: #selector(deleteAction(_:)), for: .touchUpInside)
       
        return cell
    }

    // Mark :- Delete-Button Action in Table-View
    @IBAction func deleteAction(_ sender: UIButton) {
      
        // 27 June :-
        drinksArray.removeObject(at: sender.tag)
        
      //  self.cartDrinksTableView.reloadData()
        deleteDrink()
    }
    
    func deleteDrink() {
        let json = ["user_id":Singleton.sharedInstance.userIdStr,
                    "auth_token":Singleton.sharedInstance.userTokenStr,
                    "product_id": (self.productId)
            ] as [String : Any]
        
        print(json)
        
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/users/removespecificitem", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
                
                print(response.result)   // result of response serialization
                print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    
                    let json = JSON(data: response.data! )
                    print(json)
                    if let dataDict = json["data"].dictionaryObject {
                        print(dataDict)
                        let message = dataDict["message"] as? String
                        if message == "successfully remove item" {
                            
                            let alert = UIAlertController(title: "", message: "Drink is removed", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                         
                            self.cartDrinksTableView.reloadData()
                            
                        }
                    }
                }
                
                // self.imBuyimgTbl.reloadData()
                if((response.error) != nil){
                    self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                }
               // self.indicator.stopAnimating()
              //  self.indicator.isHidden = true
        }
        
        
    }
    
    // MARK: AlertView
    func alertViewMethod(titleStr:String, messageStr:String) {
        let alert = UIAlertController(title: titleStr, message: messageStr, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    @IBAction func backBtnAction(_ sender: UIButton) {
       _ = navigationController?.popViewController(animated: true)
     }
    
    // 21 June :-
    @IBAction func sendBtnAction(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AddPayment") as! AddPayment
        // 21 June :-
        vc.cart_Id = self.cart_Id
        
        // 25 June :-(To-Check if payment screen is opening from senddrinks screen.)
        UserDefaults.standard.set(true, forKey: "senddrinksscreen")
        UserDefaults.standard.synchronize()
        
         navigationController?.pushViewController(vc,animated: true)
    }
    
    
    
}
