//
//  Contact.swift
//  FreeDrinkz
//
//  Created by Brst-Pc109 on 27/03/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Alamofire
import SwiftyJSON

class Contact: UIViewController, UITextViewDelegate {
    @IBOutlet var messageTxt: IQTextView!
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var phoneTxt: UITextField!
    @IBOutlet weak var nameTxt: UITextField!
    @IBOutlet weak var sendBtn: UIButton!
    @IBOutlet weak var indicator: UIActivityIndicatorView!

    var messageLbl = UILabel()

    override func viewDidLoad() {
        super.viewDidLoad()


        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border.frame = CGRect(x: 0, y: nameTxt.frame.size.height - width, width:  nameTxt.frame.size.width, height: nameTxt.frame.size.height)
        border.borderWidth = width
        nameTxt.layer.addSublayer(border)
        nameTxt.layer.masksToBounds = true
        
        
        let border1 = CALayer()
        border1.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border1.frame = CGRect(x: 0, y: phoneTxt.frame.size.height - width, width:  phoneTxt.frame.size.width, height: phoneTxt.frame.size.height)
        border1.borderWidth = width
        phoneTxt.layer.addSublayer(border1)
        phoneTxt.layer.masksToBounds = true
        
        
        
        let border2 = CALayer()
        border2.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border2.frame = CGRect(x: 0, y: emailTxt.frame.size.height - width, width:  emailTxt.frame.size.width, height: emailTxt.frame.size.height)
        border2.borderWidth = width
        emailTxt.layer.addSublayer(border2)
        emailTxt.layer.masksToBounds = true

        
        messageLbl = UILabel(frame: CGRect(x: messageTxt.frame.origin.x+5, y: messageTxt.frame.origin.y, width: messageTxt.frame.size.width - 40, height: 40))
        messageLbl.text = "Your message"
        messageLbl.font = messageTxt.font
        messageLbl.textColor = UIColor.lightGray
        self.view.addSubview(messageLbl)
        messageTxt.text = ""
        
        messageTxt.textColor = phoneTxt.textColor
        

    }
    
    //MARK: Button Actions

    @IBAction func Contact(_ sender: Any) {
        
        if nameTxt.text == "" || messageTxt.text == "" || phoneTxt.text == "" || emailTxt.text == "" {
            alertViewMethod(titleStr: "", messageStr: "Please enter the all fields.")
        } else if !isValidEmail(testStr: emailTxt.text!) {
            alertViewMethod(titleStr: "", messageStr: "Please enter the valid email.")
        } else {
            indicator.startAnimating()
            indicator.isHidden = false
            contactUsMethod()
        }
    }
    
    @IBAction func Back(_ sender: Any) {
        if Singleton.sharedInstance.userTypeIdStr == "1" {
            print("member")
            Singleton.sharedInstance.userTypeStr = "member"
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "Search") as! Search
            self.navigationController?.pushViewController(vc,animated: true)
            
        } else if Singleton.sharedInstance.userTypeIdStr == "2" {
            print("place")
            Singleton.sharedInstance.userTypeStr = "place"
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "Search") as! Search
            self.navigationController?.pushViewController(vc,animated: true)
            
        } else if Singleton.sharedInstance.userTypeIdStr == "3" {
            print("artist")
            Singleton.sharedInstance.userTypeStr = "artist"
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ArtistProfile") as! ArtistProfile
            self.navigationController?.pushViewController(vc,animated: true)
        }
    }

    
    // MARK: Contact Us Method
    
    func contactUsMethod () {
        
        let  json = ["user_id":Singleton.sharedInstance.userIdStr,
                     "auth_token":Singleton.sharedInstance.userTokenStr,
                     "name":nameTxt.text!,
                     "email":emailTxt.text!,
                     "phone":phoneTxt.text!,
                     "message":messageTxt.text!
            ] as [String : Any]
        
        
        
        print(json)
        
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/members/contactus", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    let json = JSON(data: response.data! )
                    if let descriptionStr = json["description"].string {
                        if descriptionStr == "Wrong auth token" {
                            UserDefaults.standard.setValue(nil, forKey: "auth_token")
                            UserDefaults.standard.setValue(nil, forKey: "user_id")
                            UserDefaults.standard.setValue(nil, forKey: "user_type")
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "Login") as! Login
                            vc.isExpired = true
                            self.navigationController?.pushViewController(vc,animated: true)
                            
                        } else if descriptionStr == "We will contact you soon!" {                            
                            self.messageTxt.text = ""
                            self.phoneTxt.text = ""
                            self.nameTxt.text = ""
                            self.emailTxt.text = ""
                            self.alertViewMethod(titleStr: "", messageStr: "We will contact you soon!")
                        }
                        //Now you got your value
                    }
                } else if((response.error) != nil){
                    
                    self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                    
                }
                self.indicator.stopAnimating()
                self.indicator.isHidden = true
                
        }
        
    }
    // MARK: Email Validation
    
    func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }

    
    // MARK: AlertView
    
    func alertViewMethod(titleStr:String, messageStr:String) {
        let alert = UIAlertController(title: titleStr, message: messageStr, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    

    
    // MARK: Text Field Delegate
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        emailTxt.resignFirstResponder()
        phoneTxt.resignFirstResponder()
        nameTxt.resignFirstResponder()
        messageLbl.resignFirstResponder()

    }
    
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool  {
        textField.resignFirstResponder()
        return true;
    }

    //MARK: UITextView Delegates
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        print("textViewDidBeginEditing")
        
        if(messageTxt.text == "") {
            messageLbl.isHidden = false
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        print("textViewDidEndEditing")
        if(messageTxt.text == "") {
            messageLbl.isHidden = false
        }
        
    }
    func textViewDidChange(_ textView: UITextView) {
        
        if(messageTxt.text == "") {
            
            messageLbl.isHidden = false
        } else {
            
            messageLbl.isHidden = true
            
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n")
        {
            textView.endEditing(true)
            return false
        }
        else
        {
            
            let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
            let numberOfChars = newText.characters.count
            if numberOfChars < 351 {
                print(numberOfChars)
            }
            return numberOfChars < 351
        }

    
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

   
}
