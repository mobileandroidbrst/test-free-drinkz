//
//  StripeButtonVC.swift
//  FreeDrinkz
//
//  Created by tbi-pc-57 on 4/24/19.
//  Copyright © 2019 Brst-Pc109. All rights reserved.
//

import UIKit
import Alamofire

class StripeButtonVC: UIViewController {

 // Outlets of Buttons :-
    @IBOutlet var stripeConnectBtn: UIButton!
    
    // 2 May :-
    @IBOutlet var updateBtn: UIButton!
    
 
    let label = UILabel(frame: CGRect(x: 0, y: 0, width: 400, height: 40))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    // 30 April :-
        getStatus()
    
        
        label.center = self.view.center
        label.textAlignment = .center
        label.text = "Your Account is already set up with stripe"
        label.textColor = UIColor.lightGray
        self.view.addSubview(label)
      
        label.isHidden = true
        
        
        
 }
   
    // 30 April :-
    func getStatus() {
        
    // Fetching values from UserDefaults to get current login User's user-id and auth_token.
         let userId:String = UserDefaults.standard.value(forKey: "userId") as! String
         let authToken:String = UserDefaults.standard.value(forKey: "auth_token") as! String
        
        let parameters: [String: String] = [
            "user_id" : userId ,
            "auth_token" : authToken ,
            "res_id" : userId ,
            
            ]
        
   let url: String = "http://beta.brstdev.com/freedrinkz/api/web/v1/members/checkrestaurantstatus"

        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON {  response in
                DispatchQueue.main.async {
                    print(response)
                    if let jsonData = response.result.value {
                        print("JSON: \(jsonData)")
                          let JSON = jsonData as! NSDictionary
                            print(JSON)
                          let description:String = JSON.value(forKey: "description") as! String
                              if description == "Restaurant already account"
                              {
                                self.stripeConnectBtn.isHidden = true

//                                let label = UILabel(frame: CGRect(x: 0, y: 0, width: 400, height: 40))
//                                label.center = self.view.center
//                                label.textAlignment = .center
//                                label.text = "Your Account is already set up with stripe"
//                                label.textColor = UIColor.lightGray
//                                self.view.addSubview(label)
                                
                                self.label.isHidden = false
                               
                                
                           }
                        else
                              {
                        //      self.stripeConnectBtn.isHidden = false
                           
                           }
                        
                }
                    else if((response.error) != nil){
                        print(response.error!)
                    }
                    
                }
       
        }

    }
    
    // Back-Button Action :-
    @IBAction func backBtnAction(_ sender: UIButton)
    {
      _ = navigationController?.popViewController(animated: true)
    }
   
    @IBAction func stripeConnectBtnAction(_ sender: UIButton)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "StripeConnectVC") as! StripeConnectVC
        navigationController?.pushViewController(vc,animated: true)
    }
   
    // 2 May :-
    @IBAction func updateBtnAction(_ sender: UIButton) {
        
        UserDefaults.standard.set(true, forKey: "update_payment")
        UserDefaults.standard.synchronize()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "StripeConnectVC") as! StripeConnectVC
        navigationController?.pushViewController(vc,animated: true)

}

    
    
    override func viewWillAppear(_ animated: Bool) {
        
        let paymentBool:Bool = UserDefaults.standard.bool(forKey: "payment_done")
        print(paymentBool)
        let updatePaymentBool:Bool = UserDefaults.standard.bool(forKey: "update_payment")
        print(updatePaymentBool)
        
        
      //  || updatePaymentBool == true
        if paymentBool == true
        {
            UserDefaults.standard.set(false, forKey: "payment_done")
            UserDefaults.standard.synchronize()
//            UserDefaults.standard.set(false, forKey: "update_payment")
//            UserDefaults.standard.synchronize()
//
            self.stripeConnectBtn.isHidden = true
//            let label = UILabel(frame: CGRect(x: 0, y: 0, width: 400, height: 40))
//            label.center = self.view.center
//            label.textAlignment = .center
//            label.text = "Your Account is already added"
//            label.textColor = UIColor.lightGray
//            self.view.addSubview(label)
              self.label.isHidden = false
            
        }
        
        if updatePaymentBool == true {
            UserDefaults.standard.set(false, forKey: "update_payment")
            UserDefaults.standard.synchronize()
            self.stripeConnectBtn.isHidden = true
            self.label.isHidden = false
          
            let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/7 , y: self.view.frame.size.height-100, width: 300, height: 35))
            toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
            toastLabel.textColor = UIColor.white
            toastLabel.textAlignment = .center;
            toastLabel.font = UIFont(name: "Montserrat-Light", size: 12.0)
            toastLabel.text = "Your Account-Id is updated"
            toastLabel.alpha = 1.0
            toastLabel.layer.cornerRadius = 10;
            toastLabel.clipsToBounds  =  true
            self.view.addSubview(toastLabel)
            UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
                toastLabel.alpha = 0.0
            }, completion: {(isCompleted) in
                toastLabel.removeFromSuperview()
            })
        }
        
}
        
        
    
        
}
    

 

    
    
    
    

