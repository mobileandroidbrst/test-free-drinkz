//
//  SearchRestaurantCell.swift
//  FreeDrinkz
//
//  Created by Brst-Pc109 on 03/05/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

import UIKit

class SearchRestaurantCell: UITableViewCell {

    @IBOutlet weak var restaurantImg: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var locationTxt: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
