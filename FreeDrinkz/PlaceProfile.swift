//
//  PlaceProfile.swift
//  FreeDrinkz
//
//  Created by Brst-Pc109 on 28/03/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

import UIKit
import SideMenu
import MapKit
import Alamofire
import SwiftyJSON
import TwitterKit
import FBSDKCoreKit.FBSDKGraphRequest
import FBSDKLoginKit
import Firebase
import IQKeyboardManagerSwift
import CoreLocation

class PlaceProfile: UIViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, MKMapViewDelegate, UITextViewDelegate,CLLocationManagerDelegate {
    
    @IBOutlet weak var profileTbl: UITableView!
    @IBOutlet weak var postLbl: UILabel!
    @IBOutlet weak var detailsLbl: UILabel!
    @IBOutlet weak var membersLbl: UILabel!
    
    @IBOutlet weak var memberCollection: UICollectionView!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var restaurantBackView: UIView!
    @IBOutlet weak var sendMsgBtn: UIButton!
    @IBOutlet weak var checkInBtn: UIButton!
    
    // 23 May :-
    @IBOutlet var addFrndBtn: UIButton!
    
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var locationLbl: UITextView!
    @IBOutlet weak var restaurantImg: UIImageView!
    @IBOutlet weak var routeView: UIView!
    
    var typeStr = String()
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var searchView: UIView!
    
    @IBOutlet weak var detailTbl: UITableView!
    
    
    var fromNavStr = String()
    var restroName = String()

    var nameStr = String()
    var locationStr = String()
    var userImgStr = String()
    
    
    var facebookFeedsArray = NSMutableArray()
    var postArray = NSMutableArray()
    var twitterFeedsArray = [JSON]()
    
    var restaurantId = String()
    var checkInStatusStr = String()
    
    var socialPostsArray = [JSON]()
    var membersArray = [JSON]()
    
    var isConnected = Bool()
    
    var latitudeStr = String()
    var longitudeStr = String()
    
    var socialFeedsArray = NSMutableArray()
    var isEdit = Bool()
    var performanceArray = [JSON]()
    var ViewForDoneButtonOnKeyboard = UIToolbar()
    var aboutStr = String()
    var uploadAboutTxt = String()
    var isChecked = Int()
    var locationManager =  CLLocationManager()
    var selectType = String()
   
    // 23 May :-
    var frndUnfrndStr = String()
    var myAnnotation: MKPointAnnotation = MKPointAnnotation()

    override func viewWillAppear(_ animated: Bool) {
        
        // 3 July :- (To check if there is option of frndrequest to its own or not.)
        if(Singleton.sharedInstance.userIdStr == restaurantId)
        {
            self.addFrndBtn.isHidden = true
        }
        
        
        profileTbl.reloadData()
        memberCollection.reloadData()
     }
    
    override func viewWillDisappear(_ animated: Bool) {
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        isChecked = 3
        
        print(Singleton.sharedInstance.userTypeIdStr)
        print(restaurantId)
        
        if Singleton.sharedInstance.socialLoginType == "" {
            Singleton.sharedInstance.socialLoginType = "1"
            UserDefaults.standard.setValue(Singleton.sharedInstance.socialLoginType, forKey: "socialLoginType")
            
        }
        
        
        self.mapView.delegate = self
        
        
        profileTbl.estimatedSectionHeaderHeight = 700
        profileTbl.register(UINib(nibName: "PlaceHeader", bundle: nil), forCellReuseIdentifier: "PlaceHeader")
        detailTbl.register(UINib(nibName: "PlaceHeader", bundle: nil), forCellReuseIdentifier: "PlaceHeader")
        
        ViewForDoneButtonOnKeyboard.sizeToFit()
        
        let btnDoneOnKeyboard3 = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneBtnFromKeyboardClicked))
        
        let btnDoneOnKeyboard1 = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelBtnFromKeyboardClicked))
        let btnDoneOnKeyboard2 = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        ViewForDoneButtonOnKeyboard.items = [btnDoneOnKeyboard1, btnDoneOnKeyboard2, btnDoneOnKeyboard3]
        
        
        
        self.navigationController?.isNavigationBarHidden = true
        
        setupSideMenu()
        
        restaurantBackView.layer.cornerRadius = 4
        restaurantBackView.layer.masksToBounds = false;
        restaurantBackView.layer.shadowColor = UIColor.black.cgColor
        restaurantBackView.layer.shadowOpacity = 0.3
        restaurantBackView.layer.shadowRadius = 2
        restaurantBackView.layer.shadowOffset = CGSize(width: 1, height: 1)

        sendMsgBtn.layer.borderWidth = 0.8
        sendMsgBtn.layer.borderColor = sendMsgBtn.titleLabel?.textColor.cgColor
        sendMsgBtn.layer.cornerRadius = 12
        sendMsgBtn.clipsToBounds = true
        
        checkInBtn.layer.borderWidth = 0.8
        checkInBtn.layer.borderColor = checkInBtn.titleLabel?.textColor.cgColor
        checkInBtn.layer.cornerRadius = 12
        checkInBtn.clipsToBounds = true
        
        
        memberCollection.register(UICollectionReusableView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader,withReuseIdentifier: "HeaderView");
        
        
        if fromNavStr == "other" {
           
            
            backView.isHidden = false
            searchView.isHidden = true
            
            if (Singleton.sharedInstance.userTypeStr == "member") || (Singleton.sharedInstance.userTypeStr == "artist"){
                sendMsgBtn.isHidden=false
                checkInBtn.isHidden=false
                
            }
            
            restaurantProfileMethod()
            
        } else {
            // profileMethod()
            restaurantId = Singleton.sharedInstance.userIdStr
            twitterMethod()
            fetchFacebookFeeds()
            restaurantProfileMethod()
            routeView.isHidden=true
        }
        
        // Location Manager
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = 0.5
        locationManager.delegate = self
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        
       // postLbl.isHidden = true
        
    }
    
    
    // Mark: Location Manager
     func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        
        
        
        if typeStr != "member" {
            for anno in self.mapView.annotations {
                self.mapView.removeAnnotation( anno )
                
            }
            
            return
        }

        
        let annotationsToRemove = mapView.annotations.filter { $0 !== mapView.userLocation }
        mapView.removeAnnotations( annotationsToRemove )
        
        for anno in self.mapView.annotations {
            self.mapView.removeAnnotation( anno )

        }
        
        //MARK:- Show user location
        self.showUserLocation(userLocation: locations[0])
  
      
    }
    
    // 4 July :- (To show backend data on Mapview.)
    func showUserLocation(userLocation: CLLocation) {
        
        var latitute = Double()
        var longitute = Double()
        
        //MARK:- Show backend Location
        if fromNavStr.contains("other") {
            latitute = Double(latitudeStr) ?? 0.0
            longitute = Double(longitudeStr) ?? 0.0
        }else {
            latitute = userLocation.coordinate.latitude
            longitute = userLocation.coordinate.longitude
        }
        
        
        
        let center = CLLocationCoordinate2D(latitude: latitute, longitude: longitute)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05))
        
        mapView.setRegion(region, animated: true)
        mapView.removeAnnotation(myAnnotation)
        // Drop a pin at user's Current Location
        //  myAnnotation: MKPointAnnotation = MKPointAnnotation()
       
        myAnnotation.coordinate = CLLocationCoordinate2DMake(latitute, longitute);
        let dropPin = MKPointAnnotation()
        print(restroName)
        myAnnotation.title = restroName
        
        //mapView.removeAnnotation(myAnnotation)
        mapView.addAnnotation(myAnnotation)
        dropPin.title = restroName
    }
    // MARK: Facebook Method
    
    func fetchFacebookFeeds() {
        let params: [AnyHashable: Any] = ["fields": "id, name, story, message, from, picture, object_id, created_time, description"]
        
        /* make the API call */
        let request : FBSDKGraphRequest  = FBSDKGraphRequest(graphPath: "/me/feed", parameters: params, httpMethod: "GET")
        
        
        request.start(completionHandler: { (connection, result,  error) in
            // Handle the result
            if (result != nil) {
                let dictFromJSON = result as! NSDictionary
                print("result: \(dictFromJSON)")
                self.facebookFeedsArray = (dictFromJSON["data"]! as! NSArray).mutableCopy() as! NSMutableArray
                print("result: \(self.facebookFeedsArray)")
                print("count: \(self.facebookFeedsArray.count)")
                if self.facebookFeedsArray.count>0 {
                    self.saveSocialMediaFeeds()
                } else {
                    if self.isConnected == true {
                        self.restaurantProfileMethod()
                    }
                }
                self.profileTbl.reloadData()
                
                
            }
            
            
            self.indicator.stopAnimating()
            self.indicator.isHidden = true
            
            
        })
        
    }
    
    // MARK: Twitter Method
    
    
    func twitterMethod() {
        let userID = Twitter.sharedInstance().sessionStore.session()?.userID
        let client = TWTRAPIClient(userID: userID)
        //        let statusesShowEndpoint = "https://api.twitter.com/1.1/statuses/home_timeline.json"
        //        let params = ["exclude_replies":"true","include_entities":"false","count": "15"]
        
        let statusesShowEndpoint = "https://api.twitter.com/1.1/statuses/user_timeline.json"
        let params = ["count": "5"]
        
        var clientError : NSError?
        
        let request = client.urlRequest(withMethod: "GET", url: statusesShowEndpoint, parameters: params, error: &clientError)
        
        client.sendTwitterRequest(request) { (response, data, connectionError) -> Void in
            if connectionError != nil {

            } else {
                let jsonVal = JSON(data: data! )
                self.twitterFeedsArray = jsonVal.array!
                print(self.twitterFeedsArray)
                if self.twitterFeedsArray.count>0 {
                    self.saveTwitterFeeds()
                }
                
                self.profileTbl.reloadData()
            }
            
            self.indicator.stopAnimating()
            self.indicator.isHidden = true
            
        }
        
    }
    
    // MARK: Social Media Feeds Method
    
    
    func saveTwitterFeeds() {
        
        
        let array = NSMutableArray()
        
        for item in twitterFeedsArray {
            let stringg = item["text"].string!
            let result = stringg.replacingOccurrences(of: "[^\\x00-\\x7F]", with: "", options: NSString.CompareOptions.regularExpression, range: nil)
            print(result)
            
            
            let nameStrr = item["user"]["name"].string!
            let nameResult = nameStrr.replacingOccurrences(of: "[^\\x00-\\x7F]", with: "", options: NSString.CompareOptions.regularExpression, range: nil)
            
            
            let image = item["user"]["profile_image_url_https"].string!
            let matchInfo: NSDictionary = ["id": String(describing: item["id"].int!), "created_time": item["created_at"].string!, "description": result, "from": ["name": nameResult, "id": image]]
            
            
            array.add(matchInfo)
            
        }
        
        
        //        let array1 = Array(array.prefix(1))
        //        print(array1.count)
        //
        
        let json = ["user_id":Singleton.sharedInstance.userIdStr,
                    "auth_token":Singleton.sharedInstance.userTokenStr,
                    "feeds_type":"2",
                    "data": array
            ] as NSDictionary
        
        print(json)
        
        
        
        let postData: Data? = try? JSONSerialization.data(withJSONObject: json, options: [])
        
        let request = NSMutableURLRequest(url: NSURL(string: "http://beta.brstdev.com/freedrinkz/api/web/v1/users/savefeeds")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.httpBody = postData! as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {

            } else {
                
                let dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)

                
                if dataString != nil {
                    let json = JSON(data: data! )
                    if let descriptionStr = json["description"].string {
                        print(descriptionStr)
                        if descriptionStr == "Feed added Successfully" {
                            print("added")
                            if self.isConnected == true {
                                self.restaurantProfileMethod()
                            } else {
                                self.restaurantProfileMethod()
                            }
                            
                        }
                        //Now you got your value
                    }
                }
                
                
                
            }
        })
        
        dataTask.resume()
        
    }
    
    
    
    
    
    func saveSocialMediaFeeds() {
        
        for (index, element) in facebookFeedsArray.enumerated() {
            
            var dict = element as!  [String: Any]
            
            if dict["description"] != nil {
                let stringg = dict["description"]! as! String
                let result = stringg.replacingOccurrences(of: "[^\\x00-\\x7F]", with: "", options: NSString.CompareOptions.regularExpression, range: nil)
                dict["description"] = result
            }
            if dict["story"] != nil {
                let stringg = dict["story"]! as! String
                let result = stringg.replacingOccurrences(of: "[^\\x00-\\x7F]", with: "", options: NSString.CompareOptions.regularExpression, range: nil)
                dict["story"] = result
            }
            
            if dict["message"] != nil {
                let stringg = dict["message"]! as! String
                let result = stringg.replacingOccurrences(of: "[^\\x00-\\x7F]", with: "", options: NSString.CompareOptions.regularExpression, range: nil)
                dict["message"] = result
            }
            facebookFeedsArray.replaceObject(at: index, with: dict)
            
        }
        let json = ["user_id":Singleton.sharedInstance.userIdStr,
                    "auth_token":Singleton.sharedInstance.userTokenStr,
                    "feeds_type":"1",
                    "data": facebookFeedsArray
            ] as NSDictionary
        
        print(json)
        
        let postData: Data? = try? JSONSerialization.data(withJSONObject: json, options: [])
        
        
        
        
        let request = NSMutableURLRequest(url: NSURL(string: "http://beta.brstdev.com/freedrinkz/api/web/v1/users/savefeeds")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.httpBody = postData! as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {

            } else {

                
                
                let dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)

                
                if dataString != nil {
                    let json = JSON(data: data! )
                    if let descriptionStr = json["description"].string {
                        print(descriptionStr)
                        if descriptionStr == "Feed added Successfully" {
                            print("added")
                            if self.isConnected == true {
                                self.restaurantProfileMethod()
                            } else {
                                self.restaurantProfileMethod()
                            }
                            
                        }
                        //Now you got your value
                    }
                }
                
                
                
            }
        })
        
        dataTask.resume()
        
    }
    
    
    // MARK: Get Restaurant Profile Method
    
    func restaurantProfileMethod () {
        
        let value = String(Singleton.sharedInstance.userTypeIdStr)
        print(value ?? String.self)
        
        
        let json = ["user_id":Singleton.sharedInstance.userIdStr,
                    "auth_token":Singleton.sharedInstance.userTokenStr,
                   "other_user_id":restaurantId,
                   "user_type":Singleton.sharedInstance.userTypeIdStr
            ] as [String : Any]
        
        print(json)
        
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/members/resturantprofile", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    let json = JSON(data: response.data! )
                    if let descriptionStr = json["description"].string {
                        print(descriptionStr)
                        if descriptionStr == "Wrong auth token" {
                            
                            
                            UserDefaults.standard.setValue(nil, forKey: "auth_token")
                            UserDefaults.standard.setValue(nil, forKey: "user_id")
                            UserDefaults.standard.setValue(nil, forKey: "user_type")
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "Login") as! Login
                            vc.isExpired = true
                            self.navigationController?.pushViewController(vc,animated: true)
                            
                        }  else if descriptionStr == "Resturant Profile" {
                            
                            if self.fromNavStr == "other" {
                                
                                if json["data"]["User_data"][0]["profile_pic_url"].string != nil {
                                    self.userImgStr =  String(describing: json["data"]["User_data"][0]["profile_pic_url"].string!)
                                    
                                } else {
                                    self.userImgStr = ""
                                    
                                }
                                
                                if json["data"]["User_data"][0]["resturant_name"].string != nil {
                                    self.nameStr =  String(describing: json["data"]["User_data"][0]["resturant_name"].string!)
                                    
                                    // 22 May :-
                                   self.restroName = self.nameStr
                                    
                                } else {
                                    self.nameStr = ""
                                }
                                
  
                                // 23 May :- (Checking is that particular Frnd is user's frnd or not)
                                if json["data"]["User_data"][0]["Friend_Unfriend"] == 1
                                {
                                    self.frndUnfrndStr = "1"
                                    let image = UIImage(named: "remove.png")
                                    self.addFrndBtn.setBackgroundImage(image, for: .normal)
                                }
                                else {
                                    self.frndUnfrndStr = "0"
                                    let image = UIImage(named: "add_friend.png")
                                   self.addFrndBtn.setBackgroundImage(image, for: .normal)
                                }
                           
                                // 3 June :- (Adding Complete Address in Restaurant View.)
                            if json["data"]["User_data"][0]["address"].string != nil {
                               self.locationStr =  String(describing: json["data"]["User_data"][0]["address"].string!)
                                    } else {
                                        self.locationStr =  ""
                                    }
                                
                                
                                if json["data"]["User_data"][0]["city"].string != nil {
                                    // 3 June :-
                                     self.locationStr =  "\(self.locationStr),\(String(describing: json["data"]["User_data"][0]["city"].string!))" //\(self.locationStr),
                                }
                                
                                if json["data"]["User_data"][0]["state"].string != nil {
                                    self.locationStr =  "\(self.locationStr), \(String(describing: json["data"]["User_data"][0]["state"].string!))"
                                }
                                
                                if json["data"]["User_data"][0]["zip_code"].string != nil {
                                    self.locationStr =  "\(self.locationStr), \(String(describing: json["data"]["User_data"][0]["zip_code"].string!))"
                                }
                                
                                
                                if json["data"]["User_data"][0]["latitude"].string != nil {
                                    self.latitudeStr =  String(describing: json["data"]["User_data"][0]["latitude"].string!)
                                } else {
                                    self.latitudeStr =  "0.0"
                                }
                                
                                if json["data"]["User_data"][0]["longitude"].string != nil {
                                    self.longitudeStr =  String(describing: json["data"]["User_data"][0]["longitude"].string!)
                                } else {
                                    self.longitudeStr =  "0.0"
                                }
                                
                                
                                if json["data"]["User_data"][0]["check_in_status"].int != nil {
                                    self.checkInStatusStr =  String(describing: json["data"]["User_data"][0]["check_in_status"].int!)
                                } else {
                                    self.checkInStatusStr =  "0"
                                }
                                
                                if self.checkInStatusStr == "1" {
                                    self.checkInBtn.setTitle("Check-out", for: .normal)
                                } else {
                                    self.checkInBtn.setTitle("Check-in", for: .normal)
                                }
                                
                                
                            }
                                // Mark: Other work:
                            else {
                                
                                if json["data"]["User_data"][0]["address"].string != nil {
                                    Singleton.sharedInstance.addressStr =  String(describing: json["data"]["User_data"][0]["address"].string!)
                                    self.locationStr = Singleton.sharedInstance.addressStr
                                    
                                } else {
                                    Singleton.sharedInstance.addressStr =  ""
                                    self.locationStr = ""
                                    
                                }
                                
                                if json["data"]["User_data"][0]["city"].string != nil {
                                    Singleton.sharedInstance.cityStr =  String(describing: json["data"]["User_data"][0]["city"].string!)
                                    
                                    self.locationStr =  "\(self.locationStr), \(String(describing: json["data"]["User_data"][0]["city"].string!))"
                                    
                                    
                                } else {
                                    Singleton.sharedInstance.cityStr =  ""
                                }
                                
                                
                                if json["data"]["User_data"][0]["state"].string != nil {
                                    Singleton.sharedInstance.stateStr =  String(describing: json["data"]["User_data"][0]["state"].string!)
                                    
                                    self.locationStr =  "\(self.locationStr), \(String(describing: json["data"]["User_data"][0]["state"].string!))"
                                    
                                } else {
                                    Singleton.sharedInstance.stateStr =  ""
                                }
                                
                                
                                
                                if json["data"]["User_data"][0]["zip_code"].string != nil {
                                    Singleton.sharedInstance.zipcodeStr =  String(describing: json["data"]["User_data"][0]["zip_code"].string!)
                                    
                                    self.locationStr =  "\(self.locationStr), \(String(describing: json["data"]["User_data"][0]["zip_code"].string!))"
                                    
                                } else {
                                    Singleton.sharedInstance.zipcodeStr =  ""
                                }
                                
                                if json["data"]["User_data"][0]["profile_pic_url"].string != nil {
                                    Singleton.sharedInstance.userImgStr =  String(describing: json["data"]["User_data"][0]["profile_pic_url"].string!)
                                    Singleton.sharedInstance.userImgStr =  Singleton.sharedInstance.userImgStr.replacingOccurrences(of: "_normal", with: "")

                                    self.userImgStr = Singleton.sharedInstance.userImgStr
                                    
                                } else {
                                    Singleton.sharedInstance.userImgStr =  ""
                                    self.userImgStr = ""
                                    
                                }
                                if json["data"]["User_data"][0]["email"].string != nil {
                                    Singleton.sharedInstance.emailStr =  String(describing: json["data"]["User_data"][0]["email"].string!)
                                } else {
                                    Singleton.sharedInstance.emailStr =  ""
                                }
                                
                                if json["data"]["User_data"][0]["resturant_name"].string != nil {
                                    Singleton.sharedInstance.restaurantStr =  String(describing: json["data"]["User_data"][0]["resturant_name"].string!)
                                    self.nameStr = Singleton.sharedInstance.restaurantStr
                                } else {
                                    Singleton.sharedInstance.restaurantStr =  ""
                                    self.nameStr = Singleton.sharedInstance.restaurantStr
                                }
                                
                                
                                if json["data"]["User_data"][0]["latitude"].string != nil {
                                    Singleton.sharedInstance.latitudeStr =  String(describing: json["data"]["User_data"][0]["latitude"].string!)
                                    self.latitudeStr = Singleton.sharedInstance.latitudeStr
                                    UserDefaults.standard.setValue(Singleton.sharedInstance.latitudeStr, forKey: "latitude")

                                    
                                } else {
                                    Singleton.sharedInstance.latitudeStr =  "0.0"
                                    self.latitudeStr = "0.0"
                                }
                                
                                if json["data"]["User_data"][0]["longitude"].string != nil {
                                    Singleton.sharedInstance.longitudeStr =  String(describing: json["data"]["User_data"][0]["longitude"].string!)
                                    self.longitudeStr = Singleton.sharedInstance.longitudeStr
                                    UserDefaults.standard.setValue(Singleton.sharedInstance.longitudeStr, forKey: "longitude")

                                    
                                } else {
                                    Singleton.sharedInstance.longitudeStr =  "0.0"
                                    self.longitudeStr = "0.0"
                                }
                                
                                
                                
                                var token = String()
                                if FIRInstanceID.instanceID().token() != nil {
                                    token = FIRInstanceID.instanceID().token()!
                                } else {
                                    token = ""
                                }
                                let values = ["name": Singleton.sharedInstance.restaurantStr as String, "userid": Singleton.sharedInstance.userIdStr as String, "fcm_token": token, "profile_pic_url": Singleton.sharedInstance.userImgStr] as [String : Any]
                                FIRDatabase.database().reference().child("users").child(Singleton.sharedInstance.userIdStr).child("credentials").updateChildValues(values, withCompletionBlock: { (errr, _) in
                                    if errr == nil {
                                        
                                    }
                                })
                                
                                
                            }
                          if json["data"]["User_data"][0]["about_text"].string != nil {
                                self.aboutStr =  String(describing: json["data"]["User_data"][0]["about_text"].string!)
                            } else {
                                self.aboutStr =  "There is no description about bar."
                            }
                            
                            if self.aboutStr == "" {
                                self.aboutStr =  "There is no description about bar."
                            }
                            
                            self.uploadAboutTxt = self.aboutStr
                            
                            self.performanceArray = json["data"]["artistperformtoday"].array!
                            print(self.performanceArray)
                            
                            
                            //                            if self.mapView.annotations.count>0 {
                            //                                self.mapView.removeAnnotations(self.mapView.annotations)
                            //                            }
                            
                            let annotationsToRemove = self.mapView.annotations.filter { $0 !== self.mapView.userLocation }
                            self.mapView.removeAnnotations( annotationsToRemove )
                            
                            
                            
                            let lat = (self.latitudeStr as NSString).doubleValue
                            let long = (self.longitudeStr as NSString).doubleValue
                            let userLocation = CLLocationCoordinate2DMake(lat, long)
                            let dropPin = MKPointAnnotation()
                            dropPin.coordinate = userLocation
                            dropPin.title = self.restroName
                            
                            if self.checkInStatusStr == "1" {
                                self.mapView.addAnnotation(dropPin)
                                
                            }
                            
                            
                            let span = MKCoordinateSpanMake(0.010, 0.010)
                            let region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: lat, longitude: long), span: span)
                            self.mapView.setRegion(region, animated: true)
                            
                            
                            
                            self.nameLbl.text = self.nameStr
                            self.locationLbl.text = self.locationStr
                            
                            // 10 June:-
                           // self.restaurantImg.sd_setImage(with: URL(string: self.userImgStr), placeholderImage: UIImage(named: "gallery.png"))
                            
                            let urlStr : String = (self.userImgStr)
                            let urlFormattedStr = urlStr.replacingOccurrences(of: " ", with: "%20")
                            self.restaurantImg.sd_setImage(with: URL(string:urlFormattedStr), placeholderImage: UIImage(named: "gallery.png"))
                            
                            
//                            self.socialPostsArray = json["data"]["feed_list"].array!
//
//
//                            //      let array1 = json["data"]["feed_list"].arrayObject as! NSMutableArray
//                            var array1 = NSMutableArray(array: json["data"]["feed_list"].arrayObject!)
//
//                            let predicate = NSPredicate(format: "feeds_type == %d", Int(Singleton.sharedInstance.socialLoginType)!)
//                            array1 = NSMutableArray(array: array1.filter { predicate.evaluate(with: $0) } )
//
//                            let array2  = NSMutableArray()
//                            for (_, element) in array1.enumerated() {
//
//                                var dict = element as!  [String: Any]
//
//
//                                if dict["feeds_type"]  as! Int == 1 {
//
//
//                                    let dateStr = dict["created_time"]!
//                                    print(dateStr)
//                                    let dateFormatter = DateFormatter()
//                                    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZ" //twitter
//                                    dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
//                                    let date = dateFormatter.date(from: dateStr as! String)
//                                    print(date!)
//                                    dict["created_time"] = date!
//
//
//                                } else {
//
//
//                                    let dateStr = dict["created_time"]!
//                                    print(dateStr)
//                                    let dateFormatter = DateFormatter()
//                                    dateFormatter.dateFormat = "eee MMM dd HH:mm:ss ZZZZ yyyy" //twitter
//                                    dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
//                                    let date = dateFormatter.date(from: dateStr as! String)
//                                    print(date!)
//                                    dict["created_time"] = date!
//
//                                }
//
//                                array2.add(dict)
//
//                            }
//
//
//
//                            let descriptor: NSSortDescriptor = NSSortDescriptor(key: "created_time", ascending: false)
//                            let sortedarray = array2.sortedArray(using: [descriptor])
//                            self.socialFeedsArray = NSMutableArray(array: Array(sortedarray.prefix(5))) //Will convert [Any] to NSArray
//                            print(self.socialFeedsArray)
                            self.membersArray = json["data"]["Member_list"].array!
                            print(self.membersArray.count)
                            
                            self.profileTbl.reloadData()
                            self.memberCollection.reloadData()
                            
                            
                        }
                        
                        
                        
                        //Now you got your value
                    
                    }
                } else if((response.error) != nil){
                    
                    self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                    
                }
                self.indicator.stopAnimating()
                self.indicator.isHidden = true
                
        }
        
    }
    
    
    
    // MARK: AlertView
    
    func alertViewMethod(titleStr:String, messageStr:String) {
        let alert = UIAlertController(title: titleStr, message: messageStr, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    fileprivate func setupSideMenu() {
        // Define the menus
        SideMenuManager.menuLeftNavigationController = storyboard!.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as? UISideMenuNavigationController
        
        // Enable gestures. The left and/or right menus must be set up above for these to work.
        // Note that these continue to work on the Navigation Controller independent of the View Controller it displays!
        SideMenuManager.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
        SideMenuManager.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
        
        
        // Set up a cool background image for demo purposes
        //     SideMenuManager.menuAnimationBackgroundColor = UIColor(patternImage: UIImage(named: "background")!)
        SideMenuManager.menuAnimationBackgroundColor = UIColor(white: 1, alpha: 0.0)
        
        typeStr = "member"
    }
    
    //MARK: UIMapViewDelegates
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        // Don't want to show a custom image if the annotation is the user's location.
        
        print(Singleton.sharedInstance.userTypeStr)
        
        guard !(annotation is MKUserLocation) else {
            return nil
        }
      
        // Better to make this class property
        let annotationIdentifier = "AnnotationIdentifier"
        
        var annotationView: MKAnnotationView?
        if let dequeuedAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier) {
            annotationView = dequeuedAnnotationView
            annotationView?.annotation = annotation
        }
        else {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
            annotationView?.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        }
        
        if let annotationView = annotationView {
            // Configure your annotation view here
            annotationView.canShowCallout = true
            
            let annView:UIView = UIView(frame: CGRect(x:0, y: 27, width: 160, height: 40))
            
            if fromNavStr == "other" && checkInStatusStr == "1" && (Singleton.sharedInstance.userTypeStr == "member") || (Singleton.sharedInstance.userTypeStr == "artist")  || Singleton.sharedInstance.userTypeIdStr == "1"   {
                
                let annImg:UIImageView = UIImageView(frame: CGRect(x:5, y: 5, width: 30, height: 30))
                let annLbl:UILabel = UILabel(frame: CGRect(x:42, y: 5, width: 100, height: 30))
               /// annLbl.text="I am in this club!"
                
                // 23 May :-
                if self.checkInStatusStr == "1" {
                    annLbl.text = "I am in this club!"
                } else {
                    annLbl.text = self.restroName
                }
                
                
              //  annLbl.text = self.restroName
                annLbl.font=sendMsgBtn.titleLabel?.font
                annLbl.textColor=UIColor.black
                
                
                
                annView.layer.cornerRadius = 20
                annView.layer.masksToBounds = false;
                annView.layer.shadowColor = UIColor.black.cgColor
                annView.layer.shadowOpacity = 0.3
                annView.layer.shadowRadius = 2
                annView.layer.shadowOffset = CGSize(width: 1, height: 1)
                annView.backgroundColor = UIColor.white
                // annImg.image =  UIImage(named: "me")
                
                // 10 June :-
                //annImg.sd_setImage(with: URL(string: Singleton.sharedInstance.userImgStr), placeholderImage: UIImage(named: "user"))
                let urlStr3 : String = (Singleton.sharedInstance.userImgStr)
                let urlFormattedStr3 = urlStr3.replacingOccurrences(of: " ", with: "%20")
                annImg.sd_setImage(with: URL(string:urlFormattedStr3), placeholderImage: UIImage(named: "user"))
                
               annImg.layer.cornerRadius = annImg.frame.height/2
                annImg.clipsToBounds = true
                
                //  annImg.contentMode = .scaleAspectFit
                annView .addSubview(annImg)
                annView .addSubview(annLbl)
                
                // 4 July :- (If user is checked in Resort then image of user and name of restaurant shows otherwise only pin shows on map.)
                if self.checkInStatusStr == "1" {
                annotationView.addSubview(annView)
                }
                //self.restaurantProfileMethod()
            }
            else if Singleton.sharedInstance.userTypeIdStr == "4" ||  Singleton.sharedInstance.userTypeIdStr == "2" || Singleton.sharedInstance.userTypeIdStr == "5" || Singleton.sharedInstance.userTypeIdStr == "6" || Singleton.sharedInstance.userTypeIdStr == "7" {

                
                let annImg:UIImageView = UIImageView(frame: CGRect(x:5, y: 5, width: 30, height: 30))
                let annLbl:UILabel = UILabel(frame: CGRect(x:42, y: 5, width: 100, height: 30))
                annLbl.text="I am in this club!"
                annLbl.font=sendMsgBtn.titleLabel?.font
                annLbl.textColor=UIColor.black
                
                
                
                annView.layer.cornerRadius = 20
                annView.layer.masksToBounds = false;
                annView.layer.shadowColor = UIColor.black.cgColor
                annView.layer.shadowOpacity = 0.3
                annView.layer.shadowRadius = 2
                annView.layer.shadowOffset = CGSize(width: 1, height: 1)
                
                
                
                
                annView.backgroundColor = UIColor.white
                // annImg.image =  UIImage(named: "me")
                
                // 10 June :-
                // annImg.sd_setImage(with: URL(string: Singleton.sharedInstance.userImgStr), placeholderImage: UIImage(named: "user"))
                let urlStr4 : String = (Singleton.sharedInstance.userImgStr)
                let urlFormattedStr4 = urlStr4.replacingOccurrences(of: " ", with: "%20")
                annImg.sd_setImage(with: URL(string:urlFormattedStr4), placeholderImage: UIImage(named: "user"))
                
                
                
                
                annImg.layer.cornerRadius = annImg.frame.height/2
                annImg.clipsToBounds = true
                
                //  annImg.contentMode = .scaleAspectFit
                annView .addSubview(annImg)
                annView .addSubview(annLbl)
                
                annotationView.addSubview(annView)
                
            }
            else {
                
                
                annView.removeFromSuperview()
                annView.isHidden = true
            }
        
            
            annotationView.image = UIImage(named: "marker1")
            
            
        }
        
        return annotationView
    }
    
    // MARK: Check-in Method
    
    func checkInMethod () {
        
           self.view.isUserInteractionEnabled = false
            
            if checkInStatusStr == "0" {
                checkInStatusStr="1"
            } else {
                checkInStatusStr="0"
            }
            
            let json = ["user_id":Singleton.sharedInstance.userIdStr,
                        "auth_token":Singleton.sharedInstance.userTokenStr,
                        "place_id":restaurantId,
                        "check_in_status":checkInStatusStr
                ] as [String : Any]
            
            print(json)
            
            Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/members/checkin", method: .post, parameters: json, encoding: JSONEncoding.default)
                .responseJSON { response in
                    self.view.isUserInteractionEnabled = true
                    print(response.result)   // result of response serialization
                    print(response)
                    if let jsonData = response.result.value {
                        print("JSON: \(jsonData)")
                        let json = JSON(data: response.data! )
                        if let descriptionStr = json["description"].string {
                            print(descriptionStr)
                            if descriptionStr == "Wrong auth token" {
                                
                                
                                UserDefaults.standard.setValue(nil, forKey: "auth_token")
                                UserDefaults.standard.setValue(nil, forKey: "user_id")
                                UserDefaults.standard.setValue(nil, forKey: "user_type")
                                
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let vc = storyboard.instantiateViewController(withIdentifier: "Login") as! Login
                                vc.isExpired = true
                                self.navigationController?.pushViewController(vc,animated: true)
                                
                            }  else if descriptionStr == "User Checkin successfully!" {
                                self.checkInStatusStr = "1"
                                if self.checkInStatusStr == "1" {
                                    self.checkInBtn.setTitle("Checkout", for: .normal)
                                    // 23 May
                                    
                                    
                                    
                                } else {
                                    self.checkInBtn.setTitle("Check-in", for: .normal)
                                }
                                
                                
                                Singleton.sharedInstance.checkInRestId = self.restaurantId
                                Singleton.sharedInstance.checkInStatus = true
                                
                                self.restaurantProfileMethod()
                                
                            }   else if descriptionStr == "User Checkout successfully!" {
                                self.checkInStatusStr = "0"
                                if self.checkInStatusStr == "1" {
                                    self.checkInBtn.setTitle("Checkout", for: .normal)
                                } else {
                                    self.checkInBtn.setTitle("Check-in", for: .normal)
                                }
                                
                                Singleton.sharedInstance.checkInStatus = false
                                Singleton.sharedInstance.checkInRestId = ""
                                
                                self.restaurantProfileMethod()
                            }
                            
                            self.profileTbl.reloadData()
                            //Now you got your value
                        }
                    } else if((response.error) != nil){
                        
                        self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                        
                    }
                    
                    self.indicator.stopAnimating()
                    self.indicator.isHidden = true
                    
            }
            
        
   }
    
    // MARK: Button Actions
    
    
    func AboutEdit(_ button: UIButton) {
        
        let indexPath = IndexPath(row: 0, section: 0)
        let cell = detailTbl.cellForRow(at: indexPath)  as! ArtistPerformanceCell
        
        
        if isEdit == false {
            isEdit = true
            cell.aboutTxt.becomeFirstResponder()
            IQKeyboardManager.sharedManager().enableAutoToolbar = false
            
        } else {
            isEdit = false
            cell.aboutTxt.resignFirstResponder()
            IQKeyboardManager.sharedManager().enableAutoToolbar = true
            
        }
        
    }
    
    
    @IBAction func OpenGoogleMaps(_ button: UIButton) {
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
            UIApplication.shared.openURL(NSURL(string:
                "comgooglemaps://?saddr=&daddr=\(Float(latitudeStr)!),\(Float(longitudeStr)!)&directionsmode=driving")! as URL)
            
        } else {
            alertViewMethod(titleStr: "", messageStr: "Google Maps not installed. Please install Google Maps.")
            NSLog("Can't use comgooglemaps://");
        }
    }
    
    @IBAction func CheckIn(_ button: UIButton) {
        
        if Singleton.sharedInstance.checkInStatus == true && Singleton.sharedInstance.checkInRestId != restaurantId {
            
            let alert = UIAlertController(title: "", message: "\(Singleton.sharedInstance.firstNameStr) you have already checked-in in another restaurant.Are you sure you want to checkout from that restaurant?", preferredStyle: UIAlertControllerStyle.alert)
            let cancelAction = UIAlertAction(title: "No", style: .default, handler: nil)
            let okAction = UIAlertAction(title: "Yes", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "PlaceProfile") as! PlaceProfile
                vc.fromNavStr = "other"
                vc.restaurantId = Singleton.sharedInstance.checkInRestId
                self.navigationController?.pushViewController(vc,animated: true)
                
            })
            alert.addAction(okAction)
            alert.addAction(cancelAction)
            present(alert, animated: true)
            
            
        } else {
            indicator.startAnimating()
            indicator.isHidden = false
            checkInMethod()
        }
    }
    
    // 23 May :-
    @IBAction func addFrndAction(_ sender: UIButton) {
      
        let timestamp = NSDate().timeIntervalSince1970
        print(timestamp)
        print(String(format: "%.0f", (timestamp)))
        let timestampp = (String(format: "%.0f", (timestamp)))
        print(timestampp)
        
        let json = ["user_id":Singleton.sharedInstance.userIdStr,
                    "auth_token":Singleton.sharedInstance.userTokenStr,
                    "otheruser_id":self.restaurantId,
                    "timestamp" : timestampp
            ] as [String : String]
        print(json)
        
        if(frndUnfrndStr == "1") {
            
            self.indicator.isHidden = false
            self.indicator.startAnimating()
            
           Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/users/unfriend", method: .post, parameters: json, encoding: JSONEncoding.default)
                .responseJSON { response in
                    
                    // 23 May :-
                    self.indicator.isHidden = false
                    self.indicator.startAnimating()
                    
                    print(response.result)   // result of response serialization
                    print(response)
                    if let jsonData = response.result.value {
                        print("JSON: \(jsonData)")
                        //                        let json = JSON(data: response.data! )
                        //                         print(json)
                        
                        let alert = UIAlertController(title: "", message: "Removed as Friend", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        self.frndUnfrndStr = "0"
                        
                         // 22 May :-
                        UserDefaults.standard.set(true, forKey: "ProfileViewFrnd")
                        self.profileTbl.reloadData()
                        
                        // 23 May :-
                        let image = UIImage(named: "add_friend.png")
                        self.addFrndBtn.setBackgroundImage(image, for: .normal)

                        self.indicator.stopAnimating()
                        self.indicator.isHidden = true
                        
                     }  else if((response.error) != nil){
                        
                        self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                        self.indicator.stopAnimating()
                        self.indicator.isHidden = true
                        
                    }
            }
        } // ending If-Statement
            
        else {
            
            self.indicator.isHidden = false
            self.indicator.startAnimating()
            
             Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/users/friend", method: .post, parameters: json, encoding: JSONEncoding.default)
                .responseJSON { response in
                    
                    print(response.result)   // result of response serialization
                    print(response)
                    if let jsonData = response.result.value {
                        print("JSON: \(jsonData)")
                        //                    let json = JSON(data: response.data! )
                        //                    print(json)
                        let alert = UIAlertController(title: "", message: "Added as Friend", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        self.frndUnfrndStr = "1"
                        
                        self.profileTbl.reloadData()
                      
                        let image = UIImage(named: "remove.png")
                        self.addFrndBtn.setBackgroundImage(image, for: .normal)

                        self.indicator.stopAnimating()
                        self.indicator.isHidden = true
                        
                        }
                    else if((response.error) != nil){
                        
                        self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                        self.indicator.stopAnimating()
                        self.indicator.isHidden = true
                        
                    }
            }
        } // ending else
        
}

    
    @IBAction func ConnectedAction(_ button: UIButton) {
        if fromNavStr != "other" {
            isConnected = true
            
            if (button.tag == 0){
                
//
//                Singleton.sharedInstance.socialLoginType = "1"
//                UserDefaults.standard.setValue(Singleton.sharedInstance.socialLoginType, forKey: "socialLoginType")
//
//                if FBSDKAccessToken.current()?.tokenString != nil {
//                    self.indicator.startAnimating()
//                    self.indicator.isHidden = false
//                    self.fetchFacebookFeeds()
//                } else {
//                    let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
//                    fbLoginManager.logIn(withReadPermissions: ["public_profile", "email", "user_friends", "user_posts"], from: self) { (result, error) in
//                        if (error == nil){
//                            let fbloginresult : FBSDKLoginManagerLoginResult = result!
//                            if fbloginresult.grantedPermissions != nil {
//                                if(fbloginresult.grantedPermissions.contains("email"))
//                                {
//
//                                    // fbLoginManager.logOut()
//                                }
//
//                                self.indicator.startAnimating()
//                                self.indicator.isHidden = false
//                                self.fetchFacebookFeeds()
//
//                                print("token is \(FBSDKAccessToken.current().tokenString)")
//                                print("token is \(FBSDKAccessToken.current().userID)")
//
//
//                            }
//                        }
//                    }
//
//                }
                
            } else {
                
//                Singleton.sharedInstance.socialLoginType = "2"
//                UserDefaults.standard.setValue(Singleton.sharedInstance.socialLoginType, forKey: "socialLoginType")
//
//                if Twitter.sharedInstance().sessionStore.session()?.userID != nil {
//                    self.indicator.startAnimating()
//                    self.indicator.isHidden = false
//                    self.twitterMethod()
//                } else {
//                    Twitter.sharedInstance().logIn { session, error in
//                        if (session != nil) {
//                            print("signed in as \(session!.userName)");
//                            print("signed in as \(session!.userID)");
//
//                            self.indicator.startAnimating()
//                            self.indicator.isHidden = false
//                            self.twitterMethod()
//                        } else {
//                            print("error: \(String(describing: error?.localizedDescription))");
//                        }
//                    }
//                }
//
                
            }
        } else {
            if (button.tag == 0){
                Singleton.sharedInstance.socialLoginType = "1"
                UserDefaults.standard.setValue(Singleton.sharedInstance.socialLoginType, forKey: "socialLoginType")
                
                self.indicator.startAnimating()
                self.indicator.isHidden = false
                restaurantProfileMethod()
                
                
            } else {
                Singleton.sharedInstance.socialLoginType = "2"
                UserDefaults.standard.setValue(Singleton.sharedInstance.socialLoginType, forKey: "socialLoginType")
                
                self.indicator.startAnimating()
                self.indicator.isHidden = false
                restaurantProfileMethod()
                
                
            }
            
        }    }
    
    @IBAction func RestaurantMenu(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "Menu") as! Menu
        vc.otherRestaurantId = restaurantId
        print(vc.otherRestaurantId)
        vc.isOther = true                                              
        navigationController?.pushViewController(vc,animated: true)
        
    
    }
    @IBAction func MemberMessage(_ sender: Any) {
        
      
        let buttonPosition = (sender as AnyObject).convert(CGPoint(), to:memberCollection)
        let indexPath = memberCollection.indexPathForItem(at:buttonPosition)
        let cell = memberCollection.cellForItem(at: indexPath!)  as! RestaurantMemberCell
        let chatView = ChatViewController()
        chatView.navigationItem.title=cell.MemberNameLbl.text
        chatView.recieverIdStr = String(describing: membersArray[(sender as AnyObject).tag]["user_id"].int!)
        chatView.senderIdStr = Singleton.sharedInstance.userIdStr
        chatView.recieverImgStr = membersArray[(sender as AnyObject).tag]["profile_pic_url"].string!
        let chatNavigationController = UINavigationController(rootViewController: chatView)
        present(chatNavigationController, animated: true, completion: nil)
        
        
        
        
        //        let chatView = ChatViewController()
        //      //  chatView.messages = makeNormalConversation()
        //        chatView.navigationItem.title="Excelente Bar"
        //
        //        if ((sender as AnyObject).tag%5==0) {
        //            chatView.navigationItem.title="Simona"
        //        } else if ((sender as AnyObject).tag%5==1) {
        //            chatView.navigationItem.title="Dianna"
        //        } else if ((sender as AnyObject).tag%5==2) {
        //            chatView.navigationItem.title="Clarke"
        //        } else if ((sender as AnyObject).tag%5==3) {
        //            chatView.navigationItem.title="Garry"
        //        } else {
        //            chatView.navigationItem.title="Elle Waston"
        //        }
        //        let chatNavigationController = UINavigationController(rootViewController: chatView)
        //        present(chatNavigationController, animated: true, completion: nil)
        //
    }
    
    
    @IBAction func Message(_ sender: Any) {
        
        let chatView = ChatViewController()
        chatView.navigationItem.title=self.nameStr
        chatView.recieverIdStr = restaurantId
        chatView.senderIdStr = Singleton.sharedInstance.userIdStr
        chatView.recieverImgStr = userImgStr
        let chatNavigationController = UINavigationController(rootViewController: chatView)
        present(chatNavigationController, animated: true, completion: nil)
        
    }
    @IBAction func Back(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func PostDetailAction(_ sender: Any) {
        
        
      //  postLbl.isHidden = true
        
        if ((sender as AnyObject).tag == 0) {
            membersLbl.textColor = UIColor.black
         //  postLbl.textColor = UIColor.darkGray
            detailsLbl.textColor = UIColor.darkGray
            
            typeStr = "member"
            memberCollection.isHidden = false
            profileTbl.isHidden = true
            detailTbl.isHidden = true
            
            memberCollection.reloadData()
            
        } else if ((sender as AnyObject).tag == 1) {
            
            membersLbl.textColor = UIColor.darkGray
          //  postLbl.textColor = UIColor.black
            detailsLbl.textColor = UIColor.darkGray

            memberCollection.isHidden = true
            profileTbl.isHidden = false
            detailTbl.isHidden = true

            typeStr = "post"
            profileTbl.reloadData()
            
            
        } else if ((sender as AnyObject).tag == 2) {
            
         //   postLbl.isHidden = true
            membersLbl.textColor = UIColor.darkGray
          //  postLbl.textColor = UIColor.darkGray
            detailsLbl.textColor = UIColor.black
            
            memberCollection.isHidden = true
            profileTbl.isHidden = true
            detailTbl.isHidden = false
            
            typeStr = "details"
            detailTbl.reloadData()
            
        }
    }
    
    
    // MARK: UICollectionView Methods
   func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return membersArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RestaurantMemberCell", for: indexPath) as! RestaurantMemberCell
        cell.memberImg.layer.cornerRadius=cell.memberImg.frame.size.height/2
        cell.memberImg.clipsToBounds=true
       
     
        if Singleton.sharedInstance.userTypeStr != "member"{
            
            // 4 July :-
           // cell.sendMsgBtn.isHidden = true
           // cell.sendDrinkBtn.isHidden = true
            cell.sendMsgBtn.isHidden = false
            cell.sendDrinkBtn.isHidden = false
            
         }
        
        
       if Singleton.sharedInstance.userIdStr == String(describing: membersArray[indexPath.row]["user_id"].int!) {
            cell.sendMsgBtn.isEnabled = true
            cell.sendDrinkBtn.isEnabled = true
            cell.sendDrinkBtn.alpha = 1;
            cell.sendMsgBtn.alpha = 1;
        }
        
        cell.MemberNameLbl.text = "\(membersArray[indexPath.row]["firstname"].string!) \(membersArray[indexPath.row]["lastname"].string!)"
        
        // 10 June :-
//        cell.memberImg.sd_setImage(with: URL(string: membersArray[indexPath.row]["profile_pic_url"].string!), placeholderImage: UIImage(named: "user"))
        
        let urlStr : String = (membersArray[indexPath.row]["profile_pic_url"].string!)
        print(urlStr)
        let urlFormattedStr = urlStr.replacingOccurrences(of: " ", with: "%20")
        cell.memberImg.sd_setImage(with: URL(string:urlFormattedStr), placeholderImage: UIImage(named: "user"))
        
        // 3 July :-
      
         print(membersArray)
         cell.locationLbl.text = "\(membersArray[indexPath.row]["city"].string!), \(membersArray[indexPath.row]["state"].string!)"
        
        cell.sendDrinkBtn.layer.cornerRadius=11
        cell.sendDrinkBtn.clipsToBounds=true
        
        cell.sendMsgBtn.layer.cornerRadius=11
        cell.sendMsgBtn.layer.borderWidth = 1
        cell.sendMsgBtn.layer.borderColor = cell.sendMsgBtn.titleLabel?.textColor.cgColor
        cell.sendMsgBtn.clipsToBounds=true
        cell.sendMsgBtn.addTarget(self, action: #selector(MemberMessage(_:)), for: .touchUpInside)
        cell.sendMsgBtn.tag=indexPath.row
        cell.profileBtn.addTarget(self, action: #selector(viewProfile(_:)), for: .touchUpInside)
        cell.profileBtn.tag = indexPath.row
        
        cell.sendDrinkBtn.addTarget(self, action: #selector(SendDrinkz(_:)), for: .touchUpInside)
        cell.sendDrinkBtn.tag = indexPath.row
        
        
        cell.layer.cornerRadius = 4
        cell.layer.masksToBounds = false;
        cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.shadowOpacity = 0.3
        cell.layer.shadowRadius = 2
        cell.layer.shadowOffset = CGSize(width: 1, height: 1)
        
        
        return cell
    }
    
    // MARK: TableView Buttons Action
    func SendDrinkz(_ button: UIButton) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "Menu") as! Menu
        vc.otherRestaurantId = restaurantId
        vc.isOther = true
        vc.isSendDrink = true
        vc.recieverIdStr = String(describing: membersArray[button.tag]["user_id"].int!)
        UserDefaults.standard.set(isChecked, forKey: "ThreeKey")
        UserDefaults.standard.synchronize()

        navigationController?.pushViewController(vc,animated: true)

    }
    
    
    
    func viewProfile(_ button: UIButton) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "Profile") as! Profile
        if Singleton.sharedInstance.userIdStr != String(describing: membersArray[button.tag]["user_id"].int!) {
            vc.fromNavStr = "other"
        }
        vc.memberProfileId = String(describing: membersArray[button.tag]["user_id"].int!)
        navigationController?.pushViewController(vc,animated: true)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var height = Int()
        
        // 4 July :- (Client Changed the requirement.)
//        if Singleton.sharedInstance.userTypeStr == "member"{
//            height = 187
//        } else {
//            height = 126
//        }
        
        height = 187
        
        if (UIScreen.main.bounds.size.height==568 || UIScreen.main.bounds.size.height==480) {
            return CGSize(width: 145,height: height)
        }
        if (UIScreen.main.bounds.size.height==736 ) {
            return CGSize(width: 125,height: height)
        }
        return CGSize(width: 112,height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,referenceSizeForHeaderInSection section: Int) -> CGSize {
        
        return headerView.frame.size
    }
    
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let headerView2 = UICollectionReusableView()
        switch kind {
            
        case UICollectionElementKindSectionHeader:
            
            let headerView1 = collectionView.dequeueReusableSupplementaryView(ofKind: kind,withReuseIdentifier: "HeaderView", for: indexPath)
            headerView1.frame=headerView.frame;
            headerView1.addSubview(headerView)
            
            memberCollection.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            
            
            return headerView1
        default:
            
            assert(false, "Unexpected element kind")
        }
        
        return headerView2
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 0, left: 10, bottom: 10, right: 10)
        
    }
    
    // MARK: UITableView Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if typeStr == "post" {
            if fromNavStr == "other" {
                
                return socialFeedsArray.count;
            }
            return socialFeedsArray.count;
            
        } else if typeStr == "details" {
            if performanceArray.count == 0 {
                return 2
            }
            
            return performanceArray.count+1
        }
        
        return 0
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if typeStr == "details" {
            
            
            let cell:ArtistPerformanceCell = detailTbl.dequeueReusableCell(withIdentifier: "ArtistPerformanceCell") as! ArtistPerformanceCell
            
            if indexPath.row == 0 {
                cell.aboutView.isHidden = false
                cell.aboutTxt.text = aboutStr
                
                if fromNavStr == "other" {
                    cell.editAboutBtn.isHidden = true
                    cell.editAboutImg.isHidden = true
                    cell.editPerformanceBtn.isHidden = true
                    cell.editPerformanceImg.isHidden = true
                    cell.aboutTxt.isEditable = false
                    cell.aboutTxt.isSelectable = false
                }
                cell.editAboutBtn.addTarget(self, action: #selector(AboutEdit(_:)), for: .touchUpInside)
                
                cell.aboutTxt.delegate = self
                cell.aboutTxt.inputAccessoryView = ViewForDoneButtonOnKeyboard
                
                cell.editAboutBtn.tag = indexPath.row;
                
                
                
                
            } else {
                
                
                cell.aboutView.isHidden = true
                
                
                if performanceArray.count == 0 {
                    cell.performanceLbl.text = "There is no artist performing today."
                    
                } else {
                    
                    print(performanceArray)
                    
                    let artistName = performanceArray[indexPath.row-1]["artist_name"].string!
                    let bandName = performanceArray[indexPath.row-1]["band_name"].string!
                    
                    var strDate = String()
                    if performanceArray[indexPath.row-1]["gender"].string !=  nil {
                        var gender = performanceArray[indexPath.row-1]["gender"].string!
                        gender = gender.lowercased()
                        
                        // 21 May(Placing date value along performance description) :-
                        
                        if performanceArray[indexPath.row-1]["time"].int != nil {
                            let timestamp = performanceArray[indexPath.row-1]["time"].int
                            let timestampDouble = Double(timestamp!)
                            let date = Date(timeIntervalSince1970: timestampDouble)
                            let dateFormatter = DateFormatter()
                            dateFormatter.locale = NSLocale.current
                            dateFormatter.dateFormat = "dd-MMMM-yyyy hh:mm a"
                            strDate = dateFormatter.string(from: date)
                        }
                        
                        
                        if gender == "male" {
                            cell.performanceLbl.text = "\(artistName) with his \(bandName) band performing today at \(nameStr)" + " " + "on" + " " + strDate
                            
                        } else {
                            cell.performanceLbl.text = "\(artistName) with her \(bandName) band performing today at \(nameStr)" + " " + "on" + " " + strDate
                        }
                        
                    } else {
                        cell.performanceLbl.text = "\(artistName) with his/her \(bandName) band performing today at \(nameStr)" + "on" + " " + strDate
                    }
                    
                    
                }
                
                
                print(indexPath.row)
                
            }
            
            
            
            return cell
            
        }
        
        
        let cell:PostCell = profileTbl.dequeueReusableCell(withIdentifier: "PostCell") as! PostCell
        let dict = socialFeedsArray[indexPath.row] as! NSDictionary
        if dict["feeds_type"]  as! Int == 1 {
           // cell.postTypeImg.image = UIImage(named: "fbgray")
            cell.postImg.sd_setImage(with: URL(string: "http://graph.facebook.com/\(dict["from_id"]!)/picture?type=large"), completed: {(image,  error,  cacheType, imageURL) -> Void in
                
            })
        } else {
            
          //  cell.postTypeImg.image = UIImage(named: "twittergray")
            
            
            var image = dict["from_id"]  as! String
            image = image.replacingOccurrences(of: "_normal", with: "")
            
            cell.postImg.sd_setImage(with: URL(string: image), completed: {(image,  error,  cacheType, imageURL) -> Void in
            })
            
        }
        
        let date = dict["created_time"]! as! Date
        let dateString = timeAgoSinceDate(Date(), currentDate: date, numericDates: true)
        print(dateString)
        cell.timeLbl.text = dateString
        
        
        cell.nameLbl.text = dict["name"] as? String
        
        
        if dict["description"] as! String != "" {
            cell.descriptionLbl.text = dict["description"]! as? String
        } else if dict["message"] as! String != "" {
            cell.descriptionLbl.text = dict["message"] as? String
        } else if dict["name"]  as! String != "" {
            cell.descriptionLbl.text = dict["name"] as? String
            if dict["story"]  as! String != "" {
                cell.descriptionLbl.text = "\(cell.descriptionLbl.text!)- \(dict["story"]!)"
            }
        } else if dict["story"]  as! String != "" {
            cell.descriptionLbl.text = dict["story"] as? String
        } else {
            cell.descriptionLbl.text = ""
        }
        
        
        
        return cell
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if typeStr == "post" {
            return 95
        }
        
        if indexPath.row == 0 {
            let cell:ArtistPerformanceCell = detailTbl.dequeueReusableCell(withIdentifier: "ArtistPerformanceCell") as! ArtistPerformanceCell
            cell.aboutTxt.text = aboutStr
            let fixedWidth = cell.aboutTxt.frame.size.width
            cell.aboutTxt.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            let newSize = cell.aboutTxt.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            return newSize.height+80
            
        }
        
        return 36
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if typeStr == "post" {
            return 474
        }
        return 474
        
        //     return 699
        
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
       
        var headerView1 = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 30))
        
        let cell:PlaceHeader = profileTbl.dequeueReusableCell(withIdentifier: "PlaceHeader") as! PlaceHeader
        
        cell.sendView.isHidden = true
        if typeStr == "post" {
           
            
            
            headerView1 = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 474))
            cell.frame = CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 474)
            cell.contentView.frame = CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 474)

            cell.membersLbl.textColor = UIColor.darkGray
          //  cell.postLbl.textColor = UIColor.black
            cell.detailsLbl.textColor = UIColor.darkGray
            cell.changeTxt.text = "Social Media Posts"
            
            
        } else if typeStr == "details" {
        
            
            headerView1 = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 474))
            cell.frame = CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 474)
            cell.contentView.frame = CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 474)
            
            cell.membersLbl.textColor = UIColor.darkGray
          // cell.postLbl.textColor = UIColor.darkGray
            cell.detailsLbl.textColor = UIColor.black
            
            cell.changeTxt.text = "Details"
            
            
        } else if typeStr == "member" {
            
            
            headerView1 = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 474))
            cell.frame = CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 474)
            cell.contentView.frame = CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 474)
            
            cell.membersLbl.textColor = UIColor.black
          // cell.postLbl.textColor = UIColor.darkGray
            cell.detailsLbl.textColor = UIColor.darkGray
            
          cell.changeTxt.text = "Social Media Posts"
            
        }
        
        
        cell.membersBtn.addTarget(self, action: #selector(PostDetailAction(_:)), for: .touchUpInside)
      //  cell.postBtn.addTarget(self, action: #selector(PostDetailAction(_:)), for: .touchUpInside)
        cell.detailsBtn.addTarget(self, action: #selector(PostDetailAction(_:)), for: .touchUpInside)
        cell.sendMsgBtn.addTarget(self, action: #selector(Message(_:)), for: .touchUpInside)
        cell.checkInBtn.addTarget(self, action: #selector(CheckIn(_:)), for: .touchUpInside)
        
        // 23 May :-
         cell.addFrndBtn.addTarget(self, action: #selector(addFrndAction(_:)), for: .touchUpInside)
        
       // 23 May :-
        if(self.frndUnfrndStr == "1") {
           // cell.addFrndBtn.image = UIImage(named: "remove.png")
            let image = UIImage(named: "remove.png")
            cell.addFrndBtn.setBackgroundImage(image, for: .normal)
        }
        else {
             // cell.addFrndBtn.image = UIImage(named: "add_friend.png")
            let image = UIImage(named: "add_friend.png")
            cell.addFrndBtn.setBackgroundImage(image, for: .normal)
        }
       
        // 3 July :- (To check if there is option of frndrequest to its own or not.)
        if (Singleton.sharedInstance.userIdStr == restaurantId)
        {
            cell.addFrndBtn.isHidden = true
            cell.addFrndBtn.setBackgroundImage(nil, for: .normal)
            
        }
        
        
       //  cell.fbBtn.addTarget(self, action: #selector(ConnectedAction(_:)), for: .touchUpInside)
     //   cell.twitterBtn.addTarget(self, action: #selector(ConnectedAction(_:)), for: .touchUpInside)
        cell.routeBtn.addTarget(self, action: #selector(OpenGoogleMaps(_:)), for: .touchUpInside)
        
        
        cell.restaurantBackView.layer.cornerRadius = 4
        cell.restaurantBackView.layer.masksToBounds = false;
        cell.restaurantBackView.layer.shadowColor = UIColor.black.cgColor
        cell.restaurantBackView.layer.shadowOpacity = 0.3
        cell.restaurantBackView.layer.shadowRadius = 2
        cell.restaurantBackView.layer.shadowOffset = CGSize(width: 1, height: 1)
        
        cell.sendMsgBtn.layer.borderWidth = 0.8
        cell.sendMsgBtn.layer.borderColor = sendMsgBtn.titleLabel?.textColor.cgColor
        cell.sendMsgBtn.layer.cornerRadius = 12
        cell.sendMsgBtn.clipsToBounds = true
        
        cell.checkInBtn.layer.borderWidth = 0.8
        cell.checkInBtn.layer.borderColor = checkInBtn.titleLabel?.textColor.cgColor
        cell.checkInBtn.layer.cornerRadius = 12
        cell.checkInBtn.clipsToBounds = true
        

        cell.routeView.layer.cornerRadius = 6
        cell.routeView.clipsToBounds = true
        
        if self.checkInStatusStr == "1" {
            cell.checkInBtn.setTitle("Checkout", for: .normal)
        } else {
            cell.checkInBtn.setTitle("Check-in", for: .normal)
        }
        
        if fromNavStr != "other" {
            userImgStr = Singleton.sharedInstance.userImgStr
            cell.routeView.isHidden = true
        }
        
        cell.nameLbl.text = nameStr
        cell.locationLbl.text = locationStr
       
        // 10 June :-
       // cell.restaurantImg.sd_setImage(with: URL(string: userImgStr), placeholderImage: UIImage(named: "gallery.png"), options: .refreshCached)
        let urlStr : String = (userImgStr)
       let urlFormattedStr = urlStr.replacingOccurrences(of: " ", with: "%20")
        cell.restaurantImg.sd_setImage(with: URL(string:urlFormattedStr), placeholderImage: UIImage(named: "gallery.png"), options: .refreshCached)
     
        // 10 June :-
       // restaurantImg.sd_setImage(with: URL(string: userImgStr), placeholderImage: UIImage(named: "gallery.png"), options: .refreshCached)
        restaurantImg.sd_setImage(with: URL(string:urlFormattedStr), placeholderImage: UIImage(named: "gallery.png"), options: .refreshCached)
        
        
        
        if fromNavStr == "other" &&  (Singleton.sharedInstance.userTypeStr == "member") || (Singleton.sharedInstance.userTypeStr == "artist"){
            cell.sendMsgBtn.isHidden=false
            cell.checkInBtn.isHidden=false
        // 23 May :-
            cell.addFrndBtn.isHidden = false
            cell.sendView.isHidden = false
        }
     
        // 23 May :-
        
        
        for anno in cell.mapView.annotations {
            mapView.removeAnnotation( anno )
            
        }
        cell.mapView.delegate = self
        
        let lat = (latitudeStr as NSString).doubleValue
        let long = (longitudeStr as NSString).doubleValue
        let newYorkLocation = CLLocationCoordinate2DMake(lat, long)
        let dropPin = MKPointAnnotation()
        dropPin.coordinate = newYorkLocation
        dropPin.title = restroName
        cell.mapView.addAnnotation(dropPin)
        
        let span = MKCoordinateSpanMake(0.010, 0.010)
        let region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: lat, longitude: long), span: span)
        cell.mapView.setRegion(region, animated: true)
        print(cell.frame.size.width)
        print(cell.contentView.frame.size.width)
        
        
        headerView1.addSubview(cell.contentView)
        return headerView1
    }
    
    
    // Mark: Comvert Date
    
    func timeAgoSinceDate(_ date:Date,currentDate:Date, numericDates:Bool) -> String {
        let calendar = Calendar.current
        let now = currentDate
        let earliest = (now as NSDate).earlierDate(date)
        let latest = (earliest == now) ? date : now
        let components:DateComponents = (calendar as NSCalendar).components([NSCalendar.Unit.minute , NSCalendar.Unit.hour , NSCalendar.Unit.day , NSCalendar.Unit.weekOfYear , NSCalendar.Unit.month , NSCalendar.Unit.year , NSCalendar.Unit.second], from: earliest, to: latest, options: NSCalendar.Options())
        
        if (components.year! >= 2) {
            return "\(components.year!) years ago"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 year ago"
            } else {
                return "Last year"
            }
        } else if (components.month! >= 2) {
            return "\(components.month!) mon ago"
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 mon ago"
            } else {
                return "Last mon"
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!) weeks ago"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1 week ago"
            } else {
                return "Last week"
            }
        } else if (components.day! >= 2) {
            return "\(components.day!) days ago"
        } else if (components.day! >= 1){
            if (numericDates){
                return "1 day ago"
            } else {
                return "Yesterday"
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!) hours ago"
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1 hour ago"
            } else {
                return "An hour ago"
            }
        } else if (components.minute! >= 2) {
            return "\(components.minute!) min ago"
        } else if (components.minute! >= 1){
            if (numericDates){
                return "1 min ago"
            } else {
                return "A min ago"
            }
        } else if (components.second! >= 3) {
            return "\(components.second!) sec ago"
        } else {
            return "Just now"
        }
        
    }
    
    @IBAction func cancelBtnFromKeyboardClicked(sender: Any) {
        view.endEditing(true)
        detailTbl.reloadData()
        
    }
    
    @IBAction func doneBtnFromKeyboardClicked(sender: Any) {
        detailTbl.reloadData()
        view.endEditing(true)
        indicator.startAnimating()
        indicator.isHidden = false
        aboutApiMethod()
    }
    
    
    // MARK: About API Method
    
    func aboutApiMethod () {
        
        let  json = ["user_id":Singleton.sharedInstance.userIdStr,
                     "auth_token":Singleton.sharedInstance.userTokenStr,
                     "about_text":uploadAboutTxt,
                     ] as [String : Any]
        
        
        print(json)
        
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/members/aboutartist", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    let json = JSON(data: response.data! )
                    if let descriptionStr = json["description"].string {
                        print(descriptionStr)
                        if descriptionStr == "About Text Save  Successfully" {
                            //   self.alertViewMethod(titleStr: "", messageStr: "Product has been successfully added.")
                            
                            
                            self.aboutStr = self.uploadAboutTxt
                            
                        } else if descriptionStr == "Wrong auth token"  || descriptionStr == "Something wrong"{
                            
                            UserDefaults.standard.setValue(nil, forKey: "auth_token")
                            UserDefaults.standard.setValue(nil, forKey: "user_id")
                            UserDefaults.standard.setValue(nil, forKey: "user_type")
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "Login") as! Login
                            vc.isExpired = true
                            self.navigationController?.pushViewController(vc,animated: true)
                            
                        }
                        //Now you got your value
                    }
                    else if let messageStr = json["message"].string {
                        
                        if messageStr == "Missing parameter" {
                            
                            UserDefaults.standard.setValue(nil, forKey: "auth_token")
                            UserDefaults.standard.setValue(nil, forKey: "user_id")
                            UserDefaults.standard.setValue(nil, forKey: "user_type")
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "Login") as! Login
                            vc.isExpired = true
                            self.navigationController?.pushViewController(vc,animated: true)
                            
                        }
                        
                    }
                } else if((response.error) != nil){
                    
                    self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                    
                }
                
                self.detailTbl.reloadData()
                self.indicator.stopAnimating()
                self.indicator.isHidden = true
                
        }
        
    }
    
    
    //MARK: UITextField Delegates
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        
    }
    func textViewDidChange(_ textView: UITextView) {
        print(textView.text)
        uploadAboutTxt = textView.text
        
        //    headerCell.aboutLbl.text = textView.text
        //        let fixedWidth = headerCell.aboutLbl.frame.size.width
        //        headerCell.aboutLbl.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        //        let newSize = headerCell.aboutLbl.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        //        var newFrame = headerCell.aboutLbl.frame
        //        newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
        //        headerCell.aboutLbl.frame = newFrame;
        //
        //        print(headerCell.aboutArtistView.frame)
        //
        //        var newFrame1 = headerCell.aboutArtistView.frame
        //        newFrame1.size = CGSize(width: newFrame1.width, height: 80+newFrame1.height)
        //        headerCell.aboutArtistView.frame = newFrame1
        //        
        //        print(headerCell.aboutArtistView.frame)
        
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
}
