//
//  OtpVC.swift
//  FreeDrinkz
//
//  Created by brst on 27/02/19.
//  Copyright © 2019 Brst-Pc109. All rights reserved.
//

import UIKit

class OtpVC: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var textFieldOne: UITextField!
    @IBOutlet weak var textFieldTwo: UITextField!
    @IBOutlet weak var textFieldThree: UITextField!
    @IBOutlet weak var textFieldFour: UITextField!
    @IBOutlet weak var newPasswordTF: UITextField!
    @IBOutlet weak var confirmPasswordTF: UITextField!
    @IBOutlet weak var btnSubmit: UIButton!
    
    // Variables
     var id = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textFieldOne.delegate = self
        textFieldTwo.delegate = self
        textFieldThree.delegate = self
        textFieldFour.delegate = self
        textFieldOne.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControlEvents.editingChanged)
        textFieldTwo.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControlEvents.editingChanged)
        textFieldThree.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControlEvents.editingChanged)
        textFieldFour.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControlEvents.editingChanged)
        newPasswordTF.isEnabled = false
        confirmPasswordTF.isEnabled = false
        btnSubmit.isEnabled = false
        btnSubmit.layer.cornerRadius = btnSubmit.frame.size.height/2
        btnSubmit.clipsToBounds = true
    }
    func textFieldDidChange(textField: UITextField){
        
        let text = textField.text
        if (text?.utf16.count)! >= 1{
            switch textField{
            case textFieldOne:
                textFieldTwo.becomeFirstResponder()
            case textFieldTwo:
                textFieldThree.becomeFirstResponder()
            case textFieldThree:
                textFieldFour.becomeFirstResponder()
            case textFieldFour:
                textFieldFour.resignFirstResponder()
                newPasswordTF.isEnabled = true
                confirmPasswordTF.isEnabled = true
                btnSubmit.isEnabled = true
            default:
                break
            }
        }else{
            
        }
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        newPasswordTF.resignFirstResponder()
        confirmPasswordTF.resignFirstResponder()
    }
    
    
    func submitClick(){
        
        let passwordStr = confirmPasswordTF.text
        var parameterDict = JSONDictionary()
        parameterDict["id"] = id
        parameterDict["password"] = passwordStr
        ApiExtension.sharedInstance.newPassword(paramters:parameterDict, completionHandler:{(result) in
           print(result)
         let description = result.value(forKey:"description") as? String
         self.alertViewMethod(titleStr:"", messageStr:description!)
          
        })
    }


    @IBAction func btnSubmit(_ sender: Any) {
        
        if (newPasswordTF.text != confirmPasswordTF.text) {
           
        self.alertViewMethod(titleStr:"", messageStr:"The confirm password and new passowrd are not same")
      
        } else {
           
            submitClick()

        }
        
    }
    
    // MARK: Buttons Action
    
    @IBAction func Back(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
        
    }
    
    
    
    // MARK: AlertView
    
    func alertViewMethod(titleStr:String, messageStr:String) {
        let alert = UIAlertController(title: titleStr, message: messageStr, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: {(action:UIAlertAction!) in
          if messageStr != "The confirm password and new passowrd are not same"{
             
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "Login") as! Login
                self.navigationController?.pushViewController(vc,animated: true)
                
            }
        }))
        self.present(alert, animated: true, completion: nil)
    }

}
