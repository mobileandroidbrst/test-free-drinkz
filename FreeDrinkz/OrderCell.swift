//
//  OrderCell.swift
//  FreeDrinkz
//
//  Created by Brst-Pc109 on 27/03/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

import UIKit

class OrderCell: UITableViewCell {

    @IBOutlet weak var drinkLbl: UILabel!
    @IBOutlet weak var drinkImg: UIImageView!
    @IBOutlet weak var completedView: UIView!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var usernameLbl: UILabel!
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var fromToLbl: UILabel!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var backView1: UIView!
    @IBOutlet weak var coupanViewBtn: UIButton!
    @IBOutlet weak var coupanView: UIView!
    @IBOutlet weak var completeBtn: UIButton!
    @IBOutlet weak var shareImg: UIImageView!
    @IBOutlet weak var shareBtn: UIButton!
    @IBOutlet weak var profileBtn: UIButton!
    
    // 6 June :-
    @IBOutlet var shareBtnImg: UIImageView!
    
    @IBOutlet weak var dateTimeLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
