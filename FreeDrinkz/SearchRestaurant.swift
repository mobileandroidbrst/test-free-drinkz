//
//  SearchRestaurant.swift
//  FreeDrinkz
//
//  Created by Brst-Pc109 on 03/05/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import CoreLocation

class SearchRestaurant: UIViewController, UITableViewDelegate, UITableViewDataSource, UIPickerViewDelegate,CLLocationManagerDelegate {

    @IBOutlet weak var searchTxt: UITextField!
    @IBOutlet weak var restaurantTbl: UITableView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!

    var ViewForDoneButtonOnKeyboard = UIToolbar()

    @IBOutlet weak var pickerView: UIView!
    @IBOutlet weak var pickerView1: UIView!
    @IBOutlet weak var datePickerView: UIDatePicker!
    var searchArray = [[String: Any]]()
    var finalArray = [[String: Any]]()
    var recieverIdStr = String()
    var isPerformance = Bool()
    var restaurantId = String()
    var restaurantName = String()

    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var countryTxt: UITextField!
    @IBOutlet weak var stateTxt: UITextField!
    @IBOutlet weak var citiesTxt: UITextField!

    
    var countryArray = [[String:Any]]()
    var stateArray = [[String:Any]]()
    var citiesArray = [[String:Any]]()
    
    var finalStateArray = [[String:Any]]()
    var finalCitiesArray = [[String:Any]]()
    var performanceTime = Int()
    var performanceRestroId = Int()
    var performanceRestroName = String()
    var performanceSchduleId = Int()
    var videoUrl = NSURL()
    let locationManager = CLLocationManager()
    var addressString : String = ""
    var textFieldString  = UITextField()
    var locationBool = Bool()
    @IBOutlet weak var btnSkipSearch: UIButton!
    var hideSkipTf = Bool()
    
    private func readJson(resources: String) -> [[String:Any]] {
        var resultArray = [[String:Any]]()
        
        do {
            if let file = Bundle.main.url(forResource: resources, withExtension: "json") {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                if let object = json as? [String: Any] {
                    // json is a dictionary
                    resultArray = object[resources] as! [[String : Any]]
                    
                } else if json is [Any] {
                    // json is an array
                    
                } else {
                    print("JSON is invalid")
                }
                
                return resultArray
                
            } else {
                print("no file")
            }
        } catch {
            print(error.localizedDescription)
        }
        return resultArray
        
    }

    
    override func viewDidLoad() {
       
        
        super.viewDidLoad()

        if hideSkipTf == true{
            
          btnSkipSearch.isHidden = true
            
        }
        
        
        else{
            
            btnSkipSearch.isHidden = false
         }
        

        DispatchQueue.global(qos: .default).async(execute: {() -> Void in
            self.countryArray = self.readJson(resources: "countries")
            self.finalStateArray = self.readJson(resources: "states")
            self.finalCitiesArray = self.readJson(resources: "cities")
            DispatchQueue.main.sync(execute: {() -> Void in
                
            })
        })
        
        
        
        citiesArray = [[
            "id": "-8",
            "name": "Cities",
            "state_id": "-8"
            ]]
        
        
        stateArray = [[
            "id": "-8",
            "name": "State",
            "country_id": "-8"
            ]]
        
        
        let pickerView11 = UIPickerView()
        countryTxt.inputView = pickerView11
        pickerView11.tag = 0
        pickerView11.delegate = self
        
        
        let pickerView12 = UIPickerView()
        stateTxt.inputView = pickerView12
        pickerView12.tag = 1
        pickerView12.delegate = self
        
        let pickerView13 = UIPickerView()
        citiesTxt.inputView = pickerView13
        pickerView13.tag = 2
        pickerView13.delegate = self
        
        pickerView.isHidden = true
        datePickerView.minimumDate = Date()
        
        searchTxt.becomeFirstResponder()
        // Do any additional setup after loading the view.
       
        
        getRestaurants()
        
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
        
        locationBool = true
        
        // 6 June :-
        self.doneButton()
    }
    
    // 6 June :- Done Button on top of picker-view.
    func doneButton(){
        
       
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        //toolBar.tintColor = UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(SearchRestaurant.donePicker))
     
     
        
        toolBar.setItems([doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        citiesTxt.inputAccessoryView = toolBar
        
        stateTxt.inputAccessoryView = toolBar
        
        countryTxt.inputAccessoryView = toolBar
        
    }
    func donePicker() {
        
        // 6 June :-
        //  pickerView.isHidden = true
        // 6 June :-
        self.view.endEditing(true)

    }
    
    //MARK: Location Manager Delegate Method

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        
        DataManager.currentLat = locations.last?.coordinate.latitude
        DataManager.currentLong = locations.last?.coordinate.longitude
        self.locationManager.stopUpdatingLocation()
        if locationBool == true{
            
          self.getAddressFromLatLon(pdblLatitude:DataManager.currentLat! , withLongitude:  DataManager.currentLong! )
            locationBool = false
        }
        
            return
        
   
    }
    
    
    
    func getAddressFromLatLon(pdblLatitude:Double, withLongitude pdblLongitude: Double) {
       
        
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(pdblLatitude)")!
        let lon: Double = Double("\(pdblLongitude)")!
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                let pm = placemarks! as [CLPlacemark]
                if pm.count > 0 {
                    let pm = placemarks![0]
                 
                    if pm.subLocality != nil {
                        self.addressString = self.addressString + pm.subLocality! + ", "
                    }
                    if pm.thoroughfare != nil {
                        self.addressString = self.addressString + pm.thoroughfare! + ", "
                    }
                    if pm.locality != nil {
                        self.addressString = self.addressString + pm.locality! + ", "
                    }
                    if pm.country != nil {
                        self.addressString = self.addressString + pm.country! + ", "
                    }
                    if pm.postalCode != nil {
                        self.addressString = self.addressString + pm.postalCode! + " "
                    }
                    
                    
                    print(self.addressString)
                }
        })
        
        
    }
    //MARK: Call Video Api Method
    // 15 May :- Adding Time-Stamp in Posting Video to API and This method is called when restaurant is selected then restaurant id will get posted in API and restaurant name reflected on Profile-screen.
    func videoApi(textFieldString:String,restaurantIdString:String){
        
      let userId = Singleton.sharedInstance.userIdStr
      let authToken = Singleton.sharedInstance.userTokenStr
      var parametersDict = JSONDictionary()
      parametersDict["user_id"] = userId
      parametersDict["auth_token"] = authToken
      parametersDict["location"] = self.addressString
      parametersDict["restaurant_id"] = restaurantIdString
      print(parametersDict)
      print(restaurantIdString)
      parametersDict["notes"] = textFieldString
      
     // 15 may :-
        
        let timestamp = NSDate().timeIntervalSince1970
        print(timestamp)
        print(String(format: "%.0f", (timestamp)))
        let timestampp = (String(format: "%.0f", (timestamp)))
        parametersDict["timestamp"] = timestampp
        
        print(parametersDict)
    ApiExtension.sharedInstance.addVideo(videoPath:self.videoUrl, parameters:parametersDict, completionHandler:{(result) in
        print(result)
        let statusCode = result.value(forKey:"status_code") as? String
        if statusCode == "200"{
         let desp = result.value(forKey:"description") as? String
         self.alertViewMethod(titleStr:"", messageStr: desp!)
        }
        else{
            self.alertViewMethod(titleStr:"", messageStr:"Error")
        }
    
      })
    }
  
// 17 May :- New Method is added Which is called When Skip-Restaurant Button is Pressed.
    func videoApi(textFieldString:String){
        
        let userId = Singleton.sharedInstance.userIdStr
        let authToken = Singleton.sharedInstance.userTokenStr
        var parametersDict = JSONDictionary()
        parametersDict["user_id"] = userId
        parametersDict["auth_token"] = authToken
        parametersDict["location"] = self.addressString
        parametersDict["restaurant_id"] = restaurantId
        parametersDict["notes"] = textFieldString
        
        let timestamp = NSDate().timeIntervalSince1970
        print(timestamp)
        print(String(format: "%.0f", (timestamp)))
        let timestampp = (String(format: "%.0f", (timestamp)))
        parametersDict["timestamp"] = timestampp
        
        ApiExtension.sharedInstance.addVideo(videoPath:self.videoUrl, parameters:parametersDict, completionHandler:{(result) in
            print(result)
            let statusCode = result.value(forKey:"status_code") as? String
            if statusCode == "200"{
                let desp = result.value(forKey:"description") as? String
                self.alertViewMethod(titleStr:"", messageStr: desp!)
            }
            else{
                self.alertViewMethod(titleStr:"", messageStr:"Error")
            }
            
        })
    }
    
  
    //MARK: On click of skip restaurant button
    @IBAction func btnNoteAction(_ sender: Any) {
   
       searchTxt.resignFirstResponder()
        let alertController = UIAlertController(title: "Video Note", message: "Enter note below(optional)", preferredStyle: .alert)
        alertController.addTextField { (textField : UITextField!) -> Void in
             self.textFieldString = alertController.textFields![0] as UITextField
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: { alert -> Void in

        })
        let doneAction = UIAlertAction(title: "Done", style: .default, handler: { (action : UIAlertAction!) -> Void in
            
            // 17 May :- Method is called When Skip Restaurant Button is Pressed.
          self.videoApi(textFieldString:self.textFieldString.text!)
            
        })

        alertController.addAction(cancelAction)
        alertController.addAction(doneAction)
        self.present(alertController, animated: true, completion: nil)
    
    
    }

    
    // MARK: Get Restaurants Method
    
    func getRestaurants () {
        
        let json = ["user_id":Singleton.sharedInstance.userIdStr,
                    "auth_token":Singleton.sharedInstance.userTokenStr,
                    "rest_id":""
            ] as [String : Any]
        
        
        
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/members/searchresturant", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    let json = JSON(data: response.data! )
                    if let descriptionStr = json["description"].string {
                        print(descriptionStr)
                        if descriptionStr == "Wrong auth token" {
                            
                            
                            UserDefaults.standard.setValue(nil, forKey: "auth_token")
                            UserDefaults.standard.setValue(nil, forKey: "user_id")
                            UserDefaults.standard.setValue(nil, forKey: "user_type")
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "Login") as! Login
                            vc.isExpired = true
                            self.navigationController?.pushViewController(vc,animated: true)
                            
                        }  else if descriptionStr == "Resturant List" {
                          
                            self.finalArray = json["data"]["resturant_list"].arrayObject as! [[String : Any]]
                            print(self.finalArray.count)
                            self.restaurantTbl.reloadData()
                        }
                        //Now you got your value
                    }
                } else if((response.error) != nil){
                    
                    self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                    
                }
                
        }
        
    }
    
    
    // MARK: Edit Method
    func editRestaurants(){

        // 23 May :-
//        let json = ["user_id":Singleton.sharedInstance.userIdStr,
//                    "auth_token":Singleton.sharedInstance.userTokenStr,
//                    "resturant_id":performanceRestroId,
//                    "resturant_name":performanceRestroName,
//                    "performance_time":datePickerView.date.timeIntervalSince1970,
//                    "schdule_id":performanceSchduleId
//            ] as [String : Any]
//        print(json)
       
                let json = ["user_id":Singleton.sharedInstance.userIdStr,
                            "auth_token":Singleton.sharedInstance.userTokenStr,
                            "resturant_id":performanceRestroId,
                            "resturant_name":restaurantName,
                            "performance_time":datePickerView.date.timeIntervalSince1970,
                            "schdule_id":performanceSchduleId
                    ] as [String : Any]
                print(json)
        
        
     Alamofire.request("http://192.168.1.13/freedrinkz/api/web/v1/members/editartist", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    let json = JSON(data: response.data! )
                    let status = json["status_code"].string
                    if let descriptionStr = json["description"].string {
                        print(descriptionStr)
                        if descriptionStr == "Wrong auth token" {
                            
                            
                            UserDefaults.standard.setValue(nil, forKey: "auth_token")
                            UserDefaults.standard.setValue(nil, forKey: "user_id")
                            UserDefaults.standard.setValue(nil, forKey: "user_type")
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "Login") as! Login
                            vc.isExpired = true
                            self.navigationController?.pushViewController(vc,animated: true)
                            
                       }

                    }
                    
                    if status == "200"{

                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "ArtistProfile") as! ArtistProfile
                        print(self.performanceRestroName)
                        print(self.datePickerView.date.timeIntervalSince1970)
                        self.navigationController?.pushViewController(vc,animated: true)
                       
                        
                     }
                } else if((response.error) != nil){
                    
                    self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                    
                }
                
        }
        
    }
    
    // MARK: AlertView
    
    func alertViewMethod(titleStr:String, messageStr:String) {
        let alert = UIAlertController(title: titleStr, message: messageStr, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    

    // MARK: UIPickerView Methods
    
    // returns the number of 'columns' to display.
    func numberOfComponentsInPickerView(pickerView: UIPickerView!) -> Int{
        return 1
    }
    
    // returns the # of rows in each component..
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        if pickerView.tag == 0 {
            return countryArray.count
        } else if (pickerView.tag == 1) {
            return stateArray.count
        }
        return citiesArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView.tag == 0 {
            return (countryArray[row]["name"] as! String)
        } else if (pickerView.tag == 1) {
            return (stateArray[row]["name"] as! String)
        }
        return (citiesArray[row]["name"] as! String)
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 0 {
            
            if (countryArray[row]["name"] as! String) != "Country" {
                countryTxt.text = (countryArray[row]["name"] as! String)
                
                print(searchArray.count)

                // predicates        
                
                
                let searchString = searchTxt.text!.trimmingCharacters(in: .whitespaces)
                if searchString.characters.count == 0 {
                    let predicate = NSPredicate(format: "country contains[cd] %@", countryTxt.text!)
                    searchArray = finalArray.filter { predicate.evaluate(with: $0) }
                } else {
                
                    let predicate1 = NSPredicate(format: "resturant_name contains[cd] %@", searchString)
                    let predicate2 = NSPredicate(format: "country contains[cd] %@", countryTxt.text!)
                    let predicate = NSCompoundPredicate.init(andPredicateWithSubpredicates: [predicate1, predicate2])
                    searchArray = finalArray.filter { predicate.evaluate(with: $0) }
                }
                restaurantTbl.reloadData()

                
            } else {
                countryTxt.text = ""
                
                // predicates

                let searchString = searchTxt.text!.trimmingCharacters(in: .whitespaces)
                let predicate = NSPredicate(format: "resturant_name contains[cd] %@", searchString)
                searchArray = finalArray.filter { predicate.evaluate(with: $0) }
                restaurantTbl.reloadData()

            }
            
            
            let predicate = NSPredicate(format: "country_id == %@", (countryArray[row]["id"] as! String))
            let array = NSMutableArray(array: finalStateArray.filter { predicate.evaluate(with: $0) } )
            stateArray = array as! [[String : Any]];
            
            let pickerView1 = UIPickerView()
            stateTxt.inputView = pickerView1
            pickerView1.tag = 1
            pickerView1.delegate = self
            
            let pickerView2 = UIPickerView()
            citiesTxt.inputView = pickerView2
            pickerView2.tag = 2
            pickerView2.delegate = self
            
            
            stateTxt.text = ""
            citiesTxt.text = ""
            
            citiesArray = [[
                "id": "-8",
                "name": "Cities",
                "state_id": "-8"
                ]]
            
            stateArray.insert([
                "id": "-8",
                "name": "State",
                "country_id": "-8"
                ], at: 0)

        } else if (pickerView.tag == 1) {
            if (stateArray[row]["name"] as! String) != "State" {
                stateTxt.text = (stateArray[row]["name"] as! String)
                
                // predicates

                let searchString = searchTxt.text!.trimmingCharacters(in: .whitespaces)
                var predicate = NSCompoundPredicate()
                if searchString.characters.count == 0 {
                    
                    let predicate2 = NSPredicate(format: "country contains[cd] %@", countryTxt.text!)
                    let predicate3 = NSPredicate(format: "state contains[cd] %@", stateTxt.text!)
                    predicate = NSCompoundPredicate.init(andPredicateWithSubpredicates: [predicate2, predicate3])

                } else {
                    
                    let predicate1 = NSPredicate(format: "resturant_name contains[cd] %@", searchString)
                    let predicate2 = NSPredicate(format: "country contains[cd] %@", countryTxt.text!)
                    let predicate3 = NSPredicate(format: "state contains[cd] %@", stateTxt.text!)
                    predicate = NSCompoundPredicate.init(andPredicateWithSubpredicates: [predicate1, predicate2, predicate3])
                }
                print(predicate)
                searchArray = finalArray.filter { predicate.evaluate(with: $0) }
                restaurantTbl.reloadData()
                
            } else {
                stateTxt.text = ""
                
                // predicates

                let searchString = searchTxt.text!.trimmingCharacters(in: .whitespaces)
                if searchString.characters.count == 0 {
                    let predicate = NSPredicate(format: "country contains[cd] %@", countryTxt.text!)
                    searchArray = finalArray.filter { predicate.evaluate(with: $0) }
                } else {
                    
                    let predicate1 = NSPredicate(format: "resturant_name contains[cd] %@", searchString)
                    let predicate2 = NSPredicate(format: "country contains[cd] %@", countryTxt.text!)
                    let predicate = NSCompoundPredicate.init(andPredicateWithSubpredicates: [predicate1, predicate2])
                    searchArray = finalArray.filter { predicate.evaluate(with: $0) }
                }
                restaurantTbl.reloadData()

                
                
            }
            
            let predicate = NSPredicate(format: "state_id == %@", (stateArray[row]["id"] as! String))
            let array = NSMutableArray(array: finalCitiesArray.filter { predicate.evaluate(with: $0) } )
            citiesArray = array as! [[String : Any]];
            
            let pickerView2 = UIPickerView()
            citiesTxt.inputView = pickerView2
            pickerView2.tag = 2
            pickerView2.delegate = self
            
            citiesTxt.text = ""

            citiesArray.insert([
                "id": "-8",
                "name": "Cities",
                "state_id": "-8"
                ], at: 0)
        } else {
            
            if (citiesArray[row]["name"] as! String) != "Cities" {
                citiesTxt.text = (citiesArray[row]["name"] as! String)
                // predicates
  
                let searchString = searchTxt.text!.trimmingCharacters(in: .whitespaces)
                var predicate = NSCompoundPredicate()
                if searchString.characters.count == 0 {
                    
                    let predicate2 = NSPredicate(format: "country contains[cd] %@", countryTxt.text!)
                    let predicate3 = NSPredicate(format: "state contains[cd] %@", stateTxt.text!)
                    let predicate4 = NSPredicate(format: "city contains[cd] %@", citiesTxt.text!)

                    predicate = NSCompoundPredicate.init(andPredicateWithSubpredicates: [predicate2, predicate3, predicate4])
                    
                } else {
                    
                    let predicate1 = NSPredicate(format: "resturant_name contains[cd] %@", searchString)
                    let predicate2 = NSPredicate(format: "country contains[cd] %@", countryTxt.text!)
                    let predicate3 = NSPredicate(format: "state contains[cd] %@", stateTxt.text!)
                    let predicate4 = NSPredicate(format: "city contains[cd] %@", citiesTxt.text!)

                    predicate = NSCompoundPredicate.init(andPredicateWithSubpredicates: [predicate1, predicate2, predicate3, predicate4])
                }
                print(predicate)
                searchArray = finalArray.filter { predicate.evaluate(with: $0) }
                restaurantTbl.reloadData()

                
                
            } else {
                citiesTxt.text = ""
                
                
                // predicates

                let searchString = searchTxt.text!.trimmingCharacters(in: .whitespaces)
                var predicate = NSCompoundPredicate()
                if searchString.characters.count == 0 {
                    
                    let predicate2 = NSPredicate(format: "country contains[cd] %@", countryTxt.text!)
                    let predicate3 = NSPredicate(format: "state contains[cd] %@", stateTxt.text!)
                    predicate = NSCompoundPredicate.init(andPredicateWithSubpredicates: [predicate2, predicate3])
                    
                } else {
                    
                    let predicate1 = NSPredicate(format: "resturant_name contains[cd] %@", searchString)
                    let predicate2 = NSPredicate(format: "country contains[cd] %@", countryTxt.text!)
                    let predicate3 = NSPredicate(format: "state contains[cd] %@", stateTxt.text!)
                    predicate = NSCompoundPredicate.init(andPredicateWithSubpredicates: [predicate1, predicate2, predicate3])
                }
                print(predicate)
                searchArray = finalArray.filter { predicate.evaluate(with: $0) }
                restaurantTbl.reloadData()

                
            }
            
            
        }

    }
    

    
    
    //MARK: Button Actions
    
    @IBAction func Country(_ sender: Any) {
        countryTxt.becomeFirstResponder()
    }
    
    @IBAction func State(_ sender: Any) {
        stateTxt.becomeFirstResponder()
    }
    
    @IBAction func City(_ sender: Any) {
        citiesTxt.becomeFirstResponder()
    }

    @IBAction func HidePickerView(_ sender: Any) {
        pickerView.isHidden = true
    }
    @IBAction func AddPerformance(_ sender: Any) {
        indicator.startAnimating()
        indicator.isHidden = false
        if performanceSchduleId != 0{
        
            editRestaurants()
            
        }
        
        else{
            
            addPerformanceMethod()

        }
    }
    
    @IBAction func Back(_ sender: Any) {
        
        var isMoved = false
        for viewcontroller in self.navigationController!.viewControllers as Array {
            if viewcontroller.isKind(of:Profile.self) { // change HomeVC to your viewcontroller in which you want to back.
                isMoved = true
                self.navigationController?.popToViewController(viewcontroller , animated: true)
                break
            }
        }
        if isMoved == false {
            _ = navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func Search(_ sender: Any) {

        if performanceSchduleId != 0{
            
        addBtn.setTitle("Edit", for:UIControlState.normal)
            
            
        }
        else{
            
            addBtn.setTitle("Add", for:UIControlState.normal)

            
        }
        
        let searchString = searchTxt.text!.trimmingCharacters(in: .whitespaces)

        if countryTxt.text == "" {
            
            print("only name")
            
            let predicate = NSPredicate(format: "resturant_name contains[cd] %@", searchString)
            searchArray = finalArray.filter { predicate.evaluate(with: $0) }

        } else if stateTxt.text == "" {
            
            print("only name, country")

            
            if searchString.characters.count == 0 {
                let predicate = NSPredicate(format: "country contains[cd] %@", countryTxt.text!)
                searchArray = finalArray.filter { predicate.evaluate(with: $0) }
            } else {
                var predicate = NSCompoundPredicate()
                let predicate1 = NSPredicate(format: "resturant_name contains[cd] %@", searchString)
                let predicate2 = NSPredicate(format: "country contains[cd] %@", countryTxt.text!)
                predicate = NSCompoundPredicate.init(andPredicateWithSubpredicates: [predicate1, predicate2])
                searchArray = finalArray.filter { predicate.evaluate(with: $0) }
            }
        } else if citiesTxt.text == "" {
            print("only name, country, state")

            var predicate = NSCompoundPredicate()
            if searchString.characters.count == 0 {
                
                let predicate2 = NSPredicate(format: "country contains[cd] %@", countryTxt.text!)
                let predicate3 = NSPredicate(format: "state contains[cd] %@", stateTxt.text!)
                
                predicate = NSCompoundPredicate.init(andPredicateWithSubpredicates: [predicate2, predicate3])
                
            } else {
                
                let predicate1 = NSPredicate(format: "resturant_name contains[cd] %@", searchString)
                let predicate2 = NSPredicate(format: "country contains[cd] %@", countryTxt.text!)
                let predicate3 = NSPredicate(format: "state contains[cd] %@", stateTxt.text!)
                
                predicate = NSCompoundPredicate.init(andPredicateWithSubpredicates: [predicate1, predicate2, predicate3])
            }
            print(predicate)
            searchArray = finalArray.filter { predicate.evaluate(with: $0) }

        
        } else {
            print("only name, country, state, city")

            var predicate = NSCompoundPredicate()
            if searchString.characters.count == 0 {
                
                let predicate2 = NSPredicate(format: "country contains[cd] %@", countryTxt.text!)
                let predicate3 = NSPredicate(format: "state contains[cd] %@", stateTxt.text!)
                let predicate4 = NSPredicate(format: "city contains[cd] %@", citiesTxt.text!)
                
                predicate = NSCompoundPredicate.init(andPredicateWithSubpredicates: [predicate2, predicate3, predicate4])
                
            } else {
                
                let predicate1 = NSPredicate(format: "resturant_name contains[cd] %@", searchString)
                let predicate2 = NSPredicate(format: "country contains[cd] %@", countryTxt.text!)
                let predicate3 = NSPredicate(format: "state contains[cd] %@", stateTxt.text!)
                let predicate4 = NSPredicate(format: "city contains[cd] %@", citiesTxt.text!)
                
                predicate = NSCompoundPredicate.init(andPredicateWithSubpredicates: [predicate1, predicate2, predicate3, predicate4])
            }
            print(predicate)
            searchArray = finalArray.filter { predicate.evaluate(with: $0) }

        }
        restaurantTbl.reloadData()
    }

    //MARK: UITableView Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchArray.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:SearchRestaurantCell = restaurantTbl.dequeueReusableCell(withIdentifier: "SearchRestaurantCell") as! SearchRestaurantCell

        
        cell.nameLbl.text = searchArray[indexPath.row]["resturant_name"] as! String?
//        cell.restaurantImg.sd_setImage(with: URL(string: searchArray[indexPath.row]["profile_pic_url"] as! String), placeholderImage: UIImage(named: "user"))
        
        // 25 May :-
        let urlStr : String = searchArray[indexPath.row]["profile_pic_url"] as! String
        let urlFormattedStr = urlStr.replacingOccurrences(of: " ", with: "%20")
        cell.restaurantImg.sd_setImage(with: URL(string:urlFormattedStr), placeholderImage: UIImage(named: "user"))
        
        
        cell.locationTxt.text = searchArray[indexPath.row]["address"] as! String!
        let city = searchArray[indexPath.row]["city"] as! String!
        let state = searchArray[indexPath.row]["state"] as! String!
        let zipCode = searchArray[indexPath.row]["zip_code"]
        cell.locationTxt.text = "\(cell.locationTxt.text!), \(city!), \(state!), \(zipCode!)"
         return cell
      }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        // 15 May :-
        let boolValue:Bool = UserDefaults.standard.bool(forKey: "search")
        print(boolValue)
        
       if isPerformance == true {
            searchTxt.resignFirstResponder()
            pickerView.isHidden = false
            restaurantId = String(describing: searchArray[indexPath.row]["user_id"]!)
            restaurantName = (searchArray[indexPath.row]["resturant_name"] as! String?)!

        }
        // 15 May :- boolValue == false is applied with condition.
        else if isPerformance == false && boolValue == false {
            
            print(searchArray[indexPath.row])
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "Menu") as! Menu
            vc.otherRestaurantId = String(describing: searchArray[indexPath.row]["user_id"]!)
            print(vc.otherRestaurantId)
        
        // 25 June :-
       // UserDefaults.standard.set(vc.otherRestaurantId, forKey: "restroid")
       // UserDefaults.standard.synchronize()
        
            vc.userId = recieverIdStr
            print(vc.userId)
            vc.isOther = true
            vc.isSendDrink = true
            vc.recieverIdStr = recieverIdStr
            print(vc.recieverIdStr)
        
        // 25 June :-
        let cart_idd = UserDefaults.standard.value(forKey: "cart_id")
        
        if cart_idd != nil  {
              removeCart()
        //  navigationController?.pushViewController(vc,animated: true)
        }
        
        else
          {
            navigationController?.pushViewController(vc,animated: true)
          }
        
        }
        
        // 15 May :-
       else if boolValue == true {
        
        // 17 May :-
        restaurantId = String(describing: searchArray[indexPath.row]["user_id"]!)
        print(restaurantId)
        restaurantName = (searchArray[indexPath.row]["resturant_name"] as! String?)!
 
        // 22 May :-
       //   UserDefaults.standard.set(false, forKey: "search") //Bool
       //   UserDefaults.standard.synchronize()
        
        searchTxt.resignFirstResponder()
        let alertController = UIAlertController(title: "Video Note", message: "Enter note below(optional)", preferredStyle: .alert)
        alertController.addTextField { (textField : UITextField!) -> Void in
            self.textFieldString = alertController.textFields![0] as UITextField
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: { alert -> Void in
            
        })
        let doneAction = UIAlertAction(title: "Done", style: .default, handler: { (action : UIAlertAction!) -> Void in
            
            // 22 May :-
            UserDefaults.standard.set(false, forKey: "search") //Bool
            UserDefaults.standard.synchronize()
            
             self.videoApi(textFieldString:self.textFieldString.text!,restaurantIdString: self.restaurantId)
            
            print(self.textFieldString.text!)
            
        })
        
        alertController.addAction(cancelAction)
        alertController.addAction(doneAction)
        self.present(alertController, animated: true, completion: nil)
        
       }
 
    }
    
    // 25 June :-
    func removeCart()  {
        
        let alert = UIAlertController(title: "You have already saved cart", message: "Click OK to view cart.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let CartDetailVC = storyBoard.instantiateViewController(withIdentifier: "CartDetailVC") as! CartDetailVC
            self.navigationController?.pushViewController(CartDetailVC,animated: true)
            
            
//            let cart_idd = UserDefaults.standard.value(forKey: "cart_id")
//          //  UserDefaults.standard.removeObject(forKey: "cart_id")
//         //   UserDefaults.standard.synchronize()
//
//
//            let Parameters =     ["user_id":Singleton.sharedInstance.userIdStr,
//                                  "auth_token":Singleton.sharedInstance.userTokenStr,
//                                  "cart_id": cart_idd!]
//                as [String:Any]
//
//            Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/users/removecart", method: .post, parameters: Parameters , encoding: JSONEncoding.default)
//                .responseJSON { response in
//                    print(response.result)   // result of response serialization
//                    print(response)
//                    if let jsonData = response.result.value {
//                        print("JSON: \(jsonData)")
//                        let json = JSON(data: response.data! )
//                        print(json)
//                        if let resDict = json["data"].dictionaryObject {
//                            print(resDict)
//                            if let message = resDict["message"] as? String {
//                                print(message)
//                                if message == "successfully remove cart" {
//
//                                    UserDefaults.standard.removeObject(forKey: "cart_id")
//                                    UserDefaults.standard.synchronize()
//
//                                    self.indicator.stopAnimating()
//                                    self.indicator.isHidden = true
//
//                                    let alert = UIAlertController(title: "", message: "Cart clear successfully", preferredStyle: UIAlertControllerStyle.alert)
//                                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
//                                    self.present(alert, animated: true, completion: nil)
//
//
//
//                                }
//                            }
//                        }
//
//
//                    } else if((response.error) != nil){
//                        self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
//                    }
//
//                    self.indicator.stopAnimating()
//                    self.indicator.isHidden = true
//
//            }
            
        } )); self.present(alert, animated: true)
        
}
    
    
   
    // MARK: Add Performance Method
    
    func addPerformanceMethod () {
        
        let  json = ["user_id":Singleton.sharedInstance.userIdStr,
                     "auth_token":Singleton.sharedInstance.userTokenStr,
                     "resturant_id":restaurantId,
                     "resturant_name":restaurantName,
                     "performance_time":datePickerView.date.timeIntervalSince1970

                     ] as [String : Any]
        
        
        print(datePickerView.date)
        
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM, hh:mm a" 
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
        let dateStr = dateFormatter.string(from: datePickerView.date as Date)
        print(dateStr)

        
        print(json)
        
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/members/schduleartist", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    let json = JSON(data: response.data! )
                    if let descriptionStr = json["description"].string {
                        print(descriptionStr)
                        if descriptionStr == "Performance Save  Successfully" {
                            //   self.alertViewMethod(titleStr: "", messageStr: "Product has been successfully added.")
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "ArtistProfile") as! ArtistProfile
                            self.navigationController?.pushViewController(vc,animated: true)


                            
                        } else if descriptionStr == "Wrong auth token"  || descriptionStr == "Something wrong"{
                            
                            UserDefaults.standard.setValue(nil, forKey: "auth_token")
                            UserDefaults.standard.setValue(nil, forKey: "user_id")
                            UserDefaults.standard.setValue(nil, forKey: "user_type")
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "Login") as! Login
                            vc.isExpired = true
                            self.navigationController?.pushViewController(vc,animated: true)
                            
                        }
                        //Now you got your value
                    }
                    else if let messageStr = json["message"].string {
                        
                        if messageStr == "Missing parameter" {
                            
                            UserDefaults.standard.setValue(nil, forKey: "auth_token")
                            UserDefaults.standard.setValue(nil, forKey: "user_id")
                            UserDefaults.standard.setValue(nil, forKey: "user_type")
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "Login") as! Login
                            vc.isExpired = true
                            self.navigationController?.pushViewController(vc,animated: true)
                            
                        }
                        
                    }
                } else if((response.error) != nil){
                    
                    self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                    
                }
                

                self.indicator.stopAnimating()
                self.indicator.isHidden = true
                
        }
        
    }
    // MARK: Text Field Delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool  {
        textField.resignFirstResponder()
        return true;
    }
    
  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
