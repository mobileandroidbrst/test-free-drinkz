//
//  SearchCell.swift
//  FreeDrinkz
//
//  Created by Brst-Pc109 on 29/03/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

import UIKit

class SearchCell: UICollectionViewCell {
    
    @IBOutlet weak var searchImg: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var searchTypeImg: UIImageView!
    @IBOutlet weak var sendMsgView: UIView!
    @IBOutlet weak var onlineImg: UIImageView!
    @IBOutlet weak var profileBtn: UIButton!
    @IBOutlet weak var profileBtn2: UIButton!
    @IBOutlet weak var sendMsgBtn: UIButton!
    @IBOutlet weak var sendMsgBtn2: UIButton!
    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var sendMsgView2: UIView!
    @IBOutlet weak var viewProfileView: UIView!
    @IBOutlet weak var sendDrink: UIButton!
    @IBOutlet weak var btnShowRestroStatus: UIButton!


}
