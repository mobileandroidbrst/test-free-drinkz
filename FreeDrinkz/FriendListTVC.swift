//
//  FriendListTVC.swift
//  FreeDrinkz
//
//  Created by tbi-pc-57 on 5/20/19.
//  Copyright © 2019 Brst-Pc109. All rights reserved.
//

import UIKit

class FriendListTVC: UITableViewCell {

 // Outlets :-
    @IBOutlet var userImage: UIImageView!
    @IBOutlet var userNameLbl: UILabel!
    @IBOutlet var locationLbl: UILabel!
    @IBOutlet var dateLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
  }

}
