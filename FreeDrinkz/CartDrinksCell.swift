//
//  CartDrinksCell.swift
//  FreeDrinkz
//
//  Created by tbi-pc-57 on 6/20/19.
//  Copyright © 2019 Brst-Pc109. All rights reserved.
//
// 20 June :-

import UIKit

class CartDrinksCell: UITableViewCell {

    //Outlets :-
     @IBOutlet var drinkImageView: UIImageView!
     @IBOutlet var drinkRestNameLbl: UILabel!
     @IBOutlet var restAddressLbl: UILabel!
     @IBOutlet var senderNameLbl: UILabel!
     @IBOutlet var dateTimeLbl: UILabel!
      @IBOutlet var deleteBtn: UIButton!
     @IBOutlet var recievrNameLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
   
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
   }

    override func layoutSubviews() {
        drinkImageView.layer.cornerRadius = 10
        drinkImageView.clipsToBounds = true
    }
    
    
    
    
}
