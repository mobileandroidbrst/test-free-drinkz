//
//  FeedCellVideo.swift
//  FreeDrinkz
//
//  Created by brst on 02/01/19.
//  Copyright © 2019 Brst-Pc109. All rights reserved.
//

import UIKit

class FeedCellVideo: UITableViewCell {
    
    
    
    @IBOutlet weak var imgVideo: UIImageView!
    
    @IBOutlet weak var lblAddress: UILabel!
    
    @IBOutlet weak var lblRestroName: UILabel!
    
    @IBOutlet weak var lblNote: UILabel!
 
    // 17 May :-
    @IBOutlet var LblDate: UILabel!
    
    // 12 June(For Artist) :-
    @IBOutlet var lblRestNameArt: UILabel!
    @IBOutlet var lblDateArt: UILabel!
    
    @IBOutlet weak var btnVideoClick: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
