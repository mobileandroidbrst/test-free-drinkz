//
//  ImBuying.swift
//  FreeDrinkz
//
//  Created by Brst-Pc109 on 04/04/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class ImBuying: UIViewController,UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate {

    @IBOutlet weak var imBuyimgTbl: UITableView!
    @IBOutlet weak var detailTbl: UITableView!
    @IBOutlet weak var detailView: UIView!
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    
    
    //Outlets
    var requestArray = [JSON]()
    var requestDetailsArray = [JSON]()
    var requestIdStr = String()
    var isChecked = Int()
    var restroId = String()
    var type = String()
    var typeString = String()
    
    // 21 June :-
    var recievr_id = String()
    
    @IBOutlet weak var imgView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        detailView.isHidden = true
        
        
        if typeString == "push"{
            
            let playButton  = UIButton(type: .custom)
            playButton.setImage(UIImage(named: "play.png"), for: .normal)
            
        }
        
        else{
            
            let playButton  = UIButton(type: .custom)
            playButton.setImage(UIImage(named: "play.png"), for: .normal)
            
        }
        
        if type == "all"{
            
            indicator.startAnimating()
            indicator.isHidden = false
            getRequestMethod()
            
        }
        
        
        
        if (Singleton.sharedInstance.userTypeStr == "member") {
            
            if Singleton.sharedInstance.checkInStatus == true{
                indicator.startAnimating()
                indicator.isHidden = false
                getRequestMethod()
            } else {
                //statusLbl.isHidden = false
            }
            
        } else if(Singleton.sharedInstance.userTypeStr == "place" && type == "all") {
            indicator.startAnimating()
            indicator.isHidden = false
            getRequestMethod()
        }
        
        
        isChecked = 2
        
        
        
        self.LongPressGesture()
        
    }
    
    func LongPressGesture() {
        //Long Press
        let longPressGesture:UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
        longPressGesture.minimumPressDuration = 0.5
        longPressGesture.delegate = self
        self.imBuyimgTbl.addGestureRecognizer(longPressGesture)
    }
    func handleLongPress(longPressGesture:UILongPressGestureRecognizer) {
   
        let p = longPressGesture.location(in: self.imBuyimgTbl)
        let indexPath = self.imBuyimgTbl.indexPathForRow(at: p)
        
        let alert = UIAlertController(title:"Are you sure you want to delete?", message:"", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
            
            print("Long press on row, at \(indexPath!.row)")

            if indexPath != nil {
                print("Long press on table view, not row.")
                let id =  self.requestArray[(indexPath?.row)!]["id"].int
                self.iamBuyingMethod(requestId:id!)

            }
            
            
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler:nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
// Mark: Pop Back
    
    @IBAction func popBack(_ sender: Any) {
   
    self.navigationController?.popViewController(animated: true)
    
    }
    // MARK: Get Requests
    
    func getRequestMethod () {
        
        
        var json = [String : Any]()
        //if(Singleton.sharedInstance.userTypeStr == "place") {
           // json = ["user_id":Singleton.sharedInstance.userIdStr,
                 //   "auth_token":Singleton.sharedInstance.userTokenStr,
                 //   "resturant_id":Singleton.sharedInstance.userIdStr
              //  ] as [String : Any]

       // }// else {
            json = ["user_id":Singleton.sharedInstance.userIdStr,
                    "auth_token":Singleton.sharedInstance.userTokenStr,
                    "resturant_id":self.restroId
                ] as [String : Any]

      //  }
        
        print(json)
        
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/users/getiambuying", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    let json = JSON(data: response.data! )
                    if let descriptionStr = json["description"].string {
                        print(descriptionStr)
                        if descriptionStr == "Wrong auth token" {
                            
                            
                            UserDefaults.standard.setValue(nil, forKey: "auth_token")
                            UserDefaults.standard.setValue(nil, forKey: "user_id")
                            UserDefaults.standard.setValue(nil, forKey: "user_type")
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "Login") as! Login
                            vc.isExpired = true
                            self.navigationController?.pushViewController(vc,animated: true)
                            
                        }  else if descriptionStr == "I am Buying listing" {
                            
                            self.requestArray = json["data"]["i_am_buying_listing"].array!
                            if self.requestArray.count == 0 {
                                self.statusLbl.isHidden = false;
                                self.statusLbl.text = "There is no request posted in I'm Buying Request section. Click on button + to post new request."
                            } else {
                                self.statusLbl.isHidden = true;
                            }
                            print(self.requestArray)
                        }
                        
                        self.imBuyimgTbl.reloadData()
                        //Now you got your value
                    }
                } else if((response.error) != nil){
                    
                    self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                    
                }
                
                self.indicator.stopAnimating()
                self.indicator.isHidden = true
                
        }
        
    }
    
    
    // MARK: Get Own Request Details
    
    func viewDetailsMethod () {
        
        
        let json  = ["user_id":Singleton.sharedInstance.userIdStr,
                    "auth_token":Singleton.sharedInstance.userTokenStr,
                    "request_id":requestIdStr
                ] as [String : Any]
            
        
        print(json)
        
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/members/buyinglist", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    let json = JSON(data: response.data! )
                    if let descriptionStr = json["description"].string {
                        print(descriptionStr)
                        if descriptionStr == "Wrong auth token" {
                            
                            
                            UserDefaults.standard.setValue(nil, forKey: "auth_token")
                            UserDefaults.standard.setValue(nil, forKey: "user_id")
                            UserDefaults.standard.setValue(nil, forKey: "user_type")
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "Login") as! Login
                            vc.isExpired = true
                            self.navigationController?.pushViewController(vc,animated: true)
                            
                        }  else if descriptionStr == "Buy for me details" {
                            
                            self.requestDetailsArray = json["data"]["detail_list"].array!
                            if self.requestDetailsArray.count == 0 {
                                self.statusLbl.isHidden = false;
                                self.statusLbl.text = "There is no respond corresponding to this request."
                            } else {
                                self.statusLbl.isHidden = true;
                            }
                            print(self.requestDetailsArray.count)
                        }
                        
                        self.detailTbl.reloadData()
                        //Now you got your value
                    }
                } else if((response.error) != nil){
                    
                    self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                    
                }
                
                self.indicator.stopAnimating()
                self.indicator.isHidden = true
                
        }
        
    }
    

    
    //MARK: UITableView Methods

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.tag == 1 {
            return requestDetailsArray.count

        }

        return self.requestArray.count
    }
  
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView.tag == 1 {

            let cell:BuyingDetailCell = detailTbl.dequeueReusableCell(withIdentifier: "BuyingDetailCell") as! BuyingDetailCell
       
            cell.memberNameLbl.text = "\(requestDetailsArray[indexPath.row]["firstname"].string!) \(requestDetailsArray[indexPath.row]["lastname"].string!)"
            
            // 10 June :-
           // cell.memberImg.sd_setImage(with: URL(string: requestDetailsArray[indexPath.row]["profile_pic_url"].string!), placeholderImage: UIImage(named: "user"))
            let urlStr : String = (requestDetailsArray[indexPath.row]["profile_pic_url"].string!)
            let urlFormattedStr = urlStr.replacingOccurrences(of: " ", with: "%20")
            cell.memberImg.sd_setImage(with: URL(string:urlFormattedStr), placeholderImage: UIImage(named: "user"))
            
            
            
             cell.locationTxt.text = requestDetailsArray[indexPath.row]["address"].string!
            let city = requestDetailsArray[indexPath.row]["city"].string!
            let state = requestDetailsArray[indexPath.row]["state"].string!
            let zipCode = requestDetailsArray[indexPath.row]["zip_code"]
            cell.locationTxt.text = "\(cell.locationTxt.text!), \(city), \(state), \(zipCode)"

            cell.acceptBtn.addTarget(self, action: #selector(SendDrinkz(_:)), for: .touchUpInside)
            cell.acceptBtn.tag = indexPath.row

            cell.rejectBtn.addTarget(self, action: #selector(RejectRequest(_:)), for: .touchUpInside)
            cell.rejectBtn.tag = indexPath.row

            
            cell.profileBtn.addTarget(self, action: #selector(viewProfileDetails(_:)), for: .touchUpInside)
            cell.profileBtn.tag = indexPath.row

            
            return cell

        }
        
        
        let cell:ImBuyingCell = imBuyimgTbl.dequeueReusableCell(withIdentifier: "ImBuyingCell") as! ImBuyingCell
        cell.requestBtn.setTitle("Buy for me", for: UIControlState.normal)
        cell.requestBtn.backgroundColor = UIColor(red: 36/255, green: 69/255, blue: 138/255, alpha: 1)
        if(Singleton.sharedInstance.userTypeStr == "place") {
            if requestArray[indexPath.row]["user_id"].string! != Singleton.sharedInstance.userIdStr {
                cell.requestBtn.isEnabled = false
                cell.requestBtn.alpha = 0.5;
            } else {
                cell.requestBtn.isEnabled = true
                cell.requestBtn.alpha = 1;
            }
        }
       
        if requestArray[indexPath.row]["user_type"].string! == "1" {
            
            cell.memberNameLbl.text = "\(requestArray[indexPath.row]["firstname"].string!) \(requestArray[indexPath.row]["lastname"].string!)"
            
            // 10 June :-
           // cell.memberImg.sd_setImage(with: URL(string: requestArray[indexPath.row]["profile_pic_url"].string!), placeholderImage: UIImage(named: "user"))
             let urlStr : String = (requestArray[indexPath.row]["profile_pic_url"].string!)
             let urlFormattedStr = urlStr.replacingOccurrences(of: " ", with: "%20")
             cell.memberImg.sd_setImage(with: URL(string:urlFormattedStr), placeholderImage: UIImage(named: "user"))
            
            

        } else if requestArray[indexPath.row]["user_type"].string! == "2" {
           
            cell.memberNameLbl.text = requestArray[indexPath.row]["resturant_name"].string!
            // 10 June :-
           // cell.memberImg.sd_setImage(with: URL(string: requestArray[indexPath.row]["profile_pic_url"].string!), placeholderImage: UIImage(named: "user"))
            let urlStr : String = (requestArray[indexPath.row]["profile_pic_url"].string!)
            let urlFormattedStr = urlStr.replacingOccurrences(of: " ", with: "%20")
            cell.memberImg.sd_setImage(with: URL(string:urlFormattedStr), placeholderImage: UIImage(named: "user"))
            
            
        }
        
        // 5 June :-
        else if requestArray[indexPath.row]["user_type"].string! == "3" {
            
              cell.memberNameLbl.text = "\(requestArray[indexPath.row]["firstname"].string!) \(requestArray[indexPath.row]["lastname"].string!)"
            // 10 June :-
           // cell.memberImg.sd_setImage(with: URL(string: requestArray[indexPath.row]["profile_pic_url"].string!), placeholderImage: UIImage(named: "user"))
            let urlStr : String = (requestArray[indexPath.row]["profile_pic_url"].string!)
            let urlFormattedStr = urlStr.replacingOccurrences(of: " ", with: "%20")
            cell.memberImg.sd_setImage(with: URL(string:urlFormattedStr), placeholderImage: UIImage(named: "user"))
            
            
        }
        
        
        cell.locationTxt.text = requestArray[indexPath.row]["address"].string!
        let city = requestArray[indexPath.row]["city"].string!
        let state = requestArray[indexPath.row]["state"].string!
        let zipCode = requestArray[indexPath.row]["zip_code"]
        cell.locationTxt.text = "\(cell.locationTxt.text!), \(city), \(state), \(zipCode)"
        
        
        
        if requestArray[indexPath.row]["user_status_buy"].int! == 0 {
            // 3 June :-
            cell.requestBtn.addTarget(self, action: #selector(BuyAction(_:)), for: .touchUpInside)
            cell.requestBtn.tag = indexPath.row
            cell.requestBtn.setTitle("Buy for me", for: UIControlState.normal)

        } else if requestArray[indexPath.row]["user_status_buy"].int! == 1 {
           
            cell.requestBtn.setTitle("Pending", for: UIControlState.normal)

        } else if requestArray[indexPath.row]["user_status_buy"].int! == 2 {
            cell.requestBtn.backgroundColor = UIColor(red: 0/255, green: 166/255, blue: 90/255, alpha: 1)
            cell.requestBtn.setTitle("Accepted", for: UIControlState.normal)
        } else if requestArray[indexPath.row]["user_status_buy"].int! == 3 {
            cell.requestBtn.backgroundColor = UIColor(red: 217/255, green: 41/255, blue: 39/255, alpha: 1)
            cell.requestBtn.setTitle("Rejected", for: UIControlState.normal)
        }

         if requestArray[indexPath.row]["user_id"].string! == Singleton.sharedInstance.userIdStr {
          
            cell.requestBtn.setTitle("View Details", for: UIControlState.normal)
            cell.requestBtn.addTarget(self, action: #selector(BuyAction(_:)), for: .touchUpInside)
            cell.requestBtn.tag = indexPath.row
          
        }
       
        
        cell.profileBtn.addTarget(self, action: #selector(viewProfile(_:)), for: .touchUpInside)
        cell.profileBtn.tag = indexPath.row

        return cell
    }

// func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool{
//        
//        
//        if requestArray[indexPath.row]["user_id"].string! == Singleton.sharedInstance.userIdStr {
//            
//             return true
//
//            
//        }
//        
//        return false
//    }
//    
//    
//    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
//        
//        
//        if requestArray[indexPath.row]["user_id"].string! == Singleton.sharedInstance.userIdStr{
//            
//            if (editingStyle == UITableViewCellEditingStyle.delete) {
//                let alert = UIAlertController(title:"", message:"Are you sure you want to delete?", preferredStyle: UIAlertControllerStyle.alert)
//                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: {(action:UIAlertAction!) in
//                    
//                    let id =  self.requestArray[indexPath.row]["id"].int
//                    self.iamBuyingMethod(requestId:id!)
//                   
//                }))
//                alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil))
//                self.present(alert, animated: true, completion: nil)
//            
//            }
//            
//        }
//        
//    }
    
    // Mark: Long Press
    func longPress(sender: UILongPressGestureRecognizer) {
        
        
        let alert = UIAlertController(title:"Are you sure you want to delete?", message:"", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
           
                let touchPoint = sender.location(in: self.imBuyimgTbl)
                print(touchPoint)
                if let indexPath = self.imBuyimgTbl.indexPathForRow(at: touchPoint) {

                    print("Long pressed row: \(indexPath.row)")
                }
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler:nil))
        self.present(alert, animated: true, completion: nil)
        
}
    
    
    
    
    // MARK: TableView Buttons Action
    
    func RejectRequest(_ button: UIButton) {
        indicator.startAnimating()
        indicator.isHidden = false
        rejectRequestMethod(requestId: String(describing: requestDetailsArray[button.tag]["id"].int!))

    }
  
    func SendDrinkz(_ button: UIButton) {
        
        // 21 June :-
        let recievr_idd = requestDetailsArray[button.tag]["id"]
        print(recievr_idd)
        self.recievr_id = String(describing:recievr_idd)
        print(self.recievr_id)
        
        
        detailView.isHidden = true

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "Menu") as! Menu
        
        // 21 June(Sending reciver-id to menu-screen for payment from I'm buying screen) :-
        UserDefaults.standard.set(true, forKey: "iambuying")
        UserDefaults.standard.synchronize()
        vc.recieverIdStrI = (self.recievr_id)
        
        if(Singleton.sharedInstance.userTypeStr == "place") {
            vc.otherRestaurantId = Singleton.sharedInstance.userIdStr

        } else {
            vc.otherRestaurantId = Singleton.sharedInstance.checkInRestId
        }

        vc.isOther = true
        vc.isSendDrink = true
        vc.isAcceptIm = true
        vc.requestIdStr = String(describing: requestDetailsArray[button.tag]["id"].int!)
        UserDefaults.standard.set(isChecked, forKey: "ThreeKey")
        UserDefaults.standard.synchronize()
        
        // 25 June :-
        let cart_idd = UserDefaults.standard.value(forKey: "cart_id")
        
        if cart_idd != nil  {
            
            let alert = UIAlertController(title: "You have already saved cart", message: "Click OK to view cart.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let CartDetailVC = storyBoard.instantiateViewController(withIdentifier: "CartDetailVC") as! CartDetailVC
                self.navigationController?.pushViewController(CartDetailVC,animated: true)
            } )); self.present(alert, animated: true)
        }
        
        
        else {
        navigationController?.pushViewController(vc,animated: true)
        }
        
        // 25 June :-
       // navigationController?.pushViewController(vc,animated: true)
        
    }
    
    func BuyAction(_ button: UIButton) {
 
        // 3 June :-
//        if requestArray[button.tag]["user_id"].string! == Singleton.sharedInstance.userIdStr {
//            indicator.isHidden = false
//            indicator.startAnimating()
//            requestIdStr = String(describing: requestArray[button.tag]["id"].int!)
//            viewDetailsMethod()
//            detailView.isHidden = false
//
//        }
//
//       else if requestArray[button.tag]["user_status_buy"].int! == 0 {
//            indicator.startAnimating()
//            indicator.isHidden = false
//            buyRespondMethod(requestId: String(describing: requestArray[button.tag]["id"].int!))
//        }
        
        // 3 June :-
                if requestArray[button.tag]["user_status_buy"].int! == 0 {
                     indicator.startAnimating()
                    indicator.isHidden = false
                    buyRespondMethod(requestId: String(describing: requestArray[button.tag]["id"].int!))
                  }
        
               else if requestArray[button.tag]["user_id"].string! == Singleton.sharedInstance.userIdStr  {
                    indicator.isHidden = false
                    indicator.startAnimating()
                    requestIdStr = String(describing: requestArray[button.tag]["id"].int!)
                    viewDetailsMethod()
                    detailView.isHidden = false
                }
      
    }
    
    func viewProfileDetails(_ button: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "Profile") as! Profile
        if String(describing: requestDetailsArray[button.tag]["user_id"].int!) != Singleton.sharedInstance.userIdStr {
            vc.fromNavStr = "other"
            vc.fromViewControllerStr = "ImBuying"
        }
        vc.memberProfileId = String(describing: requestDetailsArray[button.tag]["user_id"].int!)
        navigationController?.pushViewController(vc,animated: true)
    }

    func viewProfile(_ button: UIButton) {
        if requestArray[button.tag]["user_type"].string! == "1" {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "Profile") as! Profile
            if requestArray[button.tag]["user_id"].string! != Singleton.sharedInstance.userIdStr {
                vc.fromNavStr = "other"
                vc.fromViewControllerStr = "ImBuying"
            }
            vc.memberProfileId = requestArray[button.tag]["user_id"].string!
            navigationController?.pushViewController(vc,animated: true)
            
            
        } else if requestArray[button.tag]["user_type"].string! == "2" {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "PlaceProfile") as! PlaceProfile
            if requestArray[button.tag]["user_id"].string! != Singleton.sharedInstance.userIdStr {
                vc.fromNavStr = "other"
            }
            vc.restaurantId = requestArray[button.tag]["user_id"].string!
            navigationController?.pushViewController(vc,animated: true)
            
        } else if requestArray[button.tag]["user_type"].string! == "3" {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ArtistProfile") as! ArtistProfile
            if requestArray[button.tag]["user_id"].string! != Singleton.sharedInstance.userIdStr {
                vc.fromNavStr = "other"
            }
            vc.artistId = requestArray[button.tag]["user_id"].string!
            navigationController?.pushViewController(vc,animated: true)
            
        }
        
    }
    
    //MARK: Button Actions
   
    @IBAction func Cancel(_ sender: Any) {
        detailView.isHidden = true
        statusLbl.isHidden=true
    }
    @IBAction func Add(_ sender: Any) {
        
       
        if (Singleton.sharedInstance.checkInRestId) == (restroId){
         
             postRequestMethod()
            indicator.startAnimating()
            indicator.isHidden = false
            
        }
        else{
            
         self.alertViewMethod(titleStr: "", messageStr: "You have not checked in a restaurant")
            
        }
     
    }
 
    // MARK: Reject Request Method
    
    func rejectRequestMethod (requestId: String) {
        
        self.view.isUserInteractionEnabled = false
        
        let json = ["user_id":Singleton.sharedInstance.userIdStr,
                    "auth_token":Singleton.sharedInstance.userTokenStr,
                    "request_id":requestId,
                    "status":"3"
            ] as [String : Any]
        
        print(json)
        
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/members/updatebuystatus", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
                self.view.isUserInteractionEnabled = true
                print(response.result)   // result of response serialization
                print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    let json = JSON(data: response.data! )
                    if let descriptionStr = json["description"].string {
                        print(descriptionStr)
                        if descriptionStr == "Wrong auth token" {
                            UserDefaults.standard.setValue(nil, forKey: "auth_token")
                            UserDefaults.standard.setValue(nil, forKey: "user_id")
                            UserDefaults.standard.setValue(nil, forKey: "user_type")
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "Login") as! Login
                            vc.isExpired = true
                            self.navigationController?.pushViewController(vc,animated: true)
                            
                        }  else if descriptionStr == "Request Already Sent." {
                            self.alertViewMethod(titleStr: "", messageStr: "You have already posted the request!")
                        }   else if descriptionStr == "Drink Reject successfully" {
                            self.alertViewMethod(titleStr: "", messageStr: "Request Reject successfully")
                            self.indicator.startAnimating()
                            self.indicator.isHidden = false
                            self.viewDetailsMethod()
                            
                        }
                        
                        self.imBuyimgTbl.reloadData()
                        //Now you got your value
                    }
                } else if((response.error) != nil){
                    self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                }
                self.indicator.stopAnimating()
                self.indicator.isHidden = true
        }
        
    }
    
    
    //MARK: deleting i am buying method
    func iamBuyingMethod (requestId: Int) {
        
        self.view.isUserInteractionEnabled = false
        
        let json = ["user_id":Singleton.sharedInstance.userIdStr,
                    "auth_token":Singleton.sharedInstance.userTokenStr,
                    "delete_id":requestId
            ] as [String : Any]
        
        print(json)
        
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/users/deleteiambuying", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
                self.view.isUserInteractionEnabled = true
                print(response.result)   // result of response serialization
                print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")

                    
                    
                    self.getRequestMethod ()
                    
                   // self.imBuyimgTbl.reloadData()
                    if((response.error) != nil){
                    self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                }
                self.indicator.stopAnimating()
                self.indicator.isHidden = true
        }
        
    }
    
    }
    
    // MARK: Buy Respond Method
    func buyRespondMethod (requestId: String) {
        
        self.view.isUserInteractionEnabled = false
        
        let json = ["user_id":Singleton.sharedInstance.userIdStr,
                    "auth_token":Singleton.sharedInstance.userTokenStr,
                    "resturant_id":Singleton.sharedInstance.checkInRestId,
                    "buy_status":"1",
                    "request_id":requestId
            ] as [String : Any]

        print(json)
        
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/members/buyforme", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
                self.view.isUserInteractionEnabled = true
                print(response.result)   // result of response serialization
                print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    let json = JSON(data: response.data! )
                    if let descriptionStr = json["description"].string {
                        print(descriptionStr)
                        if descriptionStr == "Wrong auth token" {
                            UserDefaults.standard.setValue(nil, forKey: "auth_token")
                            UserDefaults.standard.setValue(nil, forKey: "user_id")
                            UserDefaults.standard.setValue(nil, forKey: "user_type")
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "Login") as! Login
                            vc.isExpired = true
                            self.navigationController?.pushViewController(vc,animated: true)
                            
                        }  else if descriptionStr == "Request Already Sent." {
                            self.alertViewMethod(titleStr: "", messageStr: "You have already posted the request!")
                        }   else if descriptionStr == "Buy for me Save  Successfully" {
                          //  self.alertViewMethod(titleStr: "", messageStr: "Your request has been successfully sent!")
                            self.indicator.startAnimating()
                            self.indicator.isHidden = false
                            self.getRequestMethod()

                        }
                        
                        self.imBuyimgTbl.reloadData()
                        //Now you got your value
                    }
                } else if((response.error) != nil){
                    self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                }
                self.indicator.stopAnimating()
                self.indicator.isHidden = true
        }
        
    }
    
    // MARK: Post Request Method

    func postRequestMethod() {
        
        self.view.isUserInteractionEnabled = false

        var json = [String : Any]()
        
        if(Singleton.sharedInstance.userTypeStr == "place") {
            json = ["user_id":Singleton.sharedInstance.userIdStr,
                    "auth_token":Singleton.sharedInstance.userTokenStr,
                    "resturant_id":Singleton.sharedInstance.userIdStr
                ] as [String : Any]
        } else {
            json = ["user_id":Singleton.sharedInstance.userIdStr,
                    "auth_token":Singleton.sharedInstance.userTokenStr,
                    "resturant_id":Singleton.sharedInstance.checkInRestId
                ] as [String : Any]
        }

        
        print(json)
        
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/users/iambuying", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
                self.view.isUserInteractionEnabled = true
                print(response.result)   // result of response serialization
                print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    let json = JSON(data: response.data! )
                    if let descriptionStr = json["description"].string {
                        print(descriptionStr)
                        if descriptionStr == "Wrong auth token" {
                            
                            
                            UserDefaults.standard.setValue(nil, forKey: "auth_token")
                            UserDefaults.standard.setValue(nil, forKey: "user_id")
                            UserDefaults.standard.setValue(nil, forKey: "user_type")
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "Login") as! Login
                            vc.isExpired = true
                            self.navigationController?.pushViewController(vc,animated: true)
                            
                        }  else if descriptionStr == "You have already posted the request." {
                            self.alertViewMethod(titleStr: "", messageStr: "You have already posted the request!")
                        }   else if descriptionStr == "Request Sent Successfully!" {
                         //   self.alertViewMethod(titleStr: "", messageStr: "You have been successfully posted!")
                            self.indicator.startAnimating()
                            self.indicator.isHidden = false
                            self.getRequestMethod()
                            
                        }
                        
                        self.imBuyimgTbl.reloadData()
                        //Now you got your value
                    }
                } else if((response.error) != nil){
                    
                    self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                    
                }
                
                self.indicator.stopAnimating()
                self.indicator.isHidden = true
                
        }
        
    }

    // MARK: AlertView
    
    func alertViewMethod(titleStr:String, messageStr:String) {
        let alert = UIAlertController(title: titleStr, message: messageStr, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

   
}
