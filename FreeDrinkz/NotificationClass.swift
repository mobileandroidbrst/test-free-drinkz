//
//  NotificationClass.swift
//  FreeDrinkz
//
//  Created by brst on 03/01/19.
//  Copyright © 2019 Brst-Pc109. All rights reserved.
//

import UIKit

class NotificationClass: NSObject {
    
    
    public static let sharedInstance = NotificationClass()
    
    func handelNotification(videoPath:[String:NSURL]){
    
        NotificationCenter.default.post(name:NSNotification.Name(rawValue:"notificationName"), object:nil, userInfo:videoPath)
        
        
    }
    

}
