  //
//  Search.swift
//  FreeDrinkz
//
//  Created by Brst-Pc109 on 29/03/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SideMenu
import Firebase
import CoreLocation
import UserNotifications


class Search: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, UIPickerViewDelegate,CLLocationManagerDelegate,UIScrollViewDelegate {
 
    @IBOutlet weak var nameTxt: UITextField!
    @IBOutlet weak var locationTxt: UITextField!
    @IBOutlet weak var genderTxt: IQDropDownTextField!
    @IBOutlet weak var memberTxt: IQDropDownTextField!
    @IBOutlet weak var searchCollec: UICollectionView!
    @IBOutlet weak var searchBtn: UIButton!
    @IBOutlet weak var memberCollec: UICollectionView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var roundLbl: UILabel!
    @IBOutlet weak var ageSlider: UISlider!
    @IBOutlet weak var pointView1: UIView!
    @IBOutlet weak var pointView2: UIView!
    @IBOutlet weak var pointView3: UIView!
    @IBOutlet weak var pointView4: UIView!
    @IBOutlet weak var pointView5: UIView!

    @IBOutlet weak var ageGenderView: UIView!

    @IBOutlet weak var countryTxt: UITextField!
    @IBOutlet weak var stateTxt: UITextField!
    @IBOutlet weak var citiesTxt: UITextField!

    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var lblShowRestroName: UILabel!
    @IBOutlet weak var indicator: UIActivityIndicatorView!

    //variables
    var hideStr = String()
    var membersArray = [JSON]()
    var allUsersArray = [JSON]()
    var allUsersArrayNew = [JSON]()
    var newSearchArray = [JSON]()
    var isSearch = Bool()
    var countryArray = [[String:Any]]()
    var stateArray = [[String:Any]]()
    var citiesArray = [[String:Any]]()
    var finalStateArray = [[String:Any]]()
    var finalCitiesArray = [[String:Any]]()
    var locationKey = NSDictionary()
    var locationManager = CLLocationManager()
   // var restroMutableList = [NSMutableDictionary]()
    let kUserDefault = UserDefaults.standard
    var error = NSError()
    var isChecked = Int()
    var userType =  String()
    var pushType =  String()
    var userTypeValue = String()
    var locationValue = String()
    var notificationValue = String()
    var addressString = String()
    var paginationDict = [String:Any]()
    
    private func readJson(resources: String) -> [[String:Any]] {
        var resultArray = [[String:Any]]()

        do {
            if let file = Bundle.main.url(forResource: resources, withExtension: "json") {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                if let object = json as? [String: Any] {
                    // json is a dictionary
                    resultArray = object[resources] as! [[String : Any]]

                } else if json is [Any] {
                    // json is an array

                } else {
                    print("JSON is invalid")
                }
                
                return resultArray
                
            } else {
                print("no file")
            }
        } catch {
            print(error.localizedDescription)
        }
        return resultArray
        
        }
    
    func addKeyboardToolBar(pickerView: UIPickerView,textField: UITextField) {
        
        var nextButton: UIBarButtonItem?
        let keyboardToolBar = UIToolbar(frame: CGRect(x: CGFloat(0), y:
            CGFloat(0), width: CGFloat(pickerView.frame.size.width), height: CGFloat(25)))
        keyboardToolBar.sizeToFit()
        keyboardToolBar.barStyle = .default
        textField.inputAccessoryView = keyboardToolBar
        nextButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.hidePicker))
        keyboardToolBar.items = [UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil), nextButton] as? [UIBarButtonItem]
        
    }
    func hidePicker() {
        self.view.endEditing(true)
    }
     override func viewDidLoad() {
        super.viewDidLoad()
        
        searchCollec.register(UICollectionReusableView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader,withReuseIdentifier: "HeaderView");
        memberCollec.register(UICollectionReusableView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader,withReuseIdentifier: "HeaderView1");

        print(Singleton.sharedInstance.userTypeStr)

        self.navigationController?.isNavigationBarHidden = true
       DispatchQueue.global(qos: .default).async(execute: {() -> Void in
            self.countryArray = self.readJson(resources: "countries")
            self.finalStateArray = self.readJson(resources: "states")
            self.finalCitiesArray = self.readJson(resources: "cities")
            DispatchQueue.main.sync(execute: {() -> Void in

            })
        })
        citiesArray = [[
            "id": "-8",
            "name": "Cities",
            "state_id": "-8"
            ]]
        
        
        stateArray = [[
            "id": "-8",
            "name": "State",
            "country_id": "-8"
            ]]

        
        let pickerView = UIPickerView()
        countryTxt.inputView = pickerView
        pickerView.tag = 0
        pickerView.delegate = self


        let pickerView1 = UIPickerView()
        stateTxt.inputView = pickerView1
        pickerView1.tag = 1
        pickerView1.delegate = self

        let pickerView2 = UIPickerView()
        citiesTxt.inputView = pickerView2
        pickerView2.tag = 2
        pickerView2.delegate = self

        self.addKeyboardToolBar(pickerView: pickerView,textField: countryTxt)
        self.addKeyboardToolBar(pickerView: pickerView1,textField: stateTxt)

        self.addKeyboardToolBar(pickerView: pickerView2,textField: citiesTxt)


        
        roundLbl.layer.cornerRadius = 12
        roundLbl.layer.borderWidth = 1
        roundLbl.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.2).cgColor
        roundLbl.clipsToBounds = true

        ageSlider.isContinuous = false
        
        ageSlider.setThumbImage(UIImage(named: "Untitled"), for: UIControlState.normal)
        
        
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border.frame = CGRect(x: 0, y: locationTxt.frame.size.height - width, width:  locationTxt.frame.size.width, height: locationTxt.frame.size.height)
        border.borderWidth = width
        locationTxt.layer.addSublayer(border)
        locationTxt.layer.masksToBounds = true
        
        
        let border1 = CALayer()
        border1.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border1.frame = CGRect(x: 0, y: memberTxt.frame.size.height - width, width:  memberTxt.frame.size.width, height: memberTxt.frame.size.height)
        border1.borderWidth = width
        memberTxt.layer.addSublayer(border1)
        memberTxt.layer.masksToBounds = true
        
        
        
        let border2 = CALayer()
        border2.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border2.frame = CGRect(x: 0, y: genderTxt.frame.size.height - width, width:  genderTxt.frame.size.width, height: genderTxt.frame.size.height)
        border2.borderWidth = width
        genderTxt.layer.addSublayer(border2)
        genderTxt.layer.masksToBounds = true
        nameTxt.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)

        memberTxt.keyboardDistanceFromTextField = 20;
        var itemLists = [NSString]()
        itemLists.append("Members")
        itemLists.append("Bars, Restaurants, Clubs, Hotels")
        itemLists.append("DJ's, Musical Artist, Bands")
        memberTxt.itemList = itemLists
        
        
        genderTxt.keyboardDistanceFromTextField = 20;
        var itemLists1 = [NSString]()
        itemLists1.append("Male")
        itemLists1.append("Female")
        genderTxt.itemList = itemLists1
        
        
//        let headerlayout = UICollectionViewFlowLayout()
//        headerlayout.headerReferenceSize = CGSize(width:self.view.frame.size.width, height:238)
//        searchCollec.collectionViewLayout = headerlayout
//
        
        indicator.startAnimating()
        indicator.isHidden = false
        
        getUsersMethod(page:"1")
        setupSideMenu()
        
        
        if FIRInstanceID.instanceID().token() != nil {
            let values = ["fcm_token": FIRInstanceID.instanceID().token()!] as [String : Any]
            FIRDatabase.database().reference().child("users").child(Singleton.sharedInstance.userIdStr).child("credentials").updateChildValues(values, withCompletionBlock: { (errr, _) in
                if errr == nil {
                    
                }
            })
        }
        
        isChecked = 3
        
        // Mark: Fetching the user type from login screen
        let userType = UserDefaults.standard.value(forKey: "user_type") as? String
        if userType == "" || userType == nil{
        
        }
            
        else{
            
            self.userTypeValue = (UserDefaults.standard.value(forKey: "user_type") as? String)!
            
        }
    
        // Mark: Checking whether location is on/off
        let locationStatus = UserDefaults.standard.value(forKey: "locationKey") as? String
        if locationStatus != nil{
            
            self.locationValue = (UserDefaults.standard.value(forKey: "locationKey") as? String)!
            
        }
       
        //Mark: Checking whether the notification is on/off
        let notification = UserDefaults.standard.value(forKey: "push_type") as? String
        if notification != nil{
            
            self.notificationValue = notification!
            
        }
        
        self.lblShowRestroName.isHidden = true
        
        GetprofileMethod()
    }
    override func viewWillAppear(_ animated: Bool) {
     
        
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = 0.5
        locationManager.delegate = self
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        
   }
    
    func buildGeofenceData() {
       
        
        // Mark: Checking artist and member case
       if self.userTypeValue == "1" || self.userTypeValue == "3"{
          
            let json = ["user_id":Singleton.sharedInstance.userIdStr,
                        "auth_token":Singleton.sharedInstance.userTokenStr,
                        "latitude":DataManager.currentLat ?? "",
                        "longitude":DataManager.currentLong ?? "",
                        "user_type":self.userTypeValue
                ] as [String : Any]
            print(json)
            Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/users/sendpushtonearbyrestaurant", method: .post, parameters: json, encoding: JSONEncoding.default)
                .responseJSON {  response in
                    DispatchQueue.main.async {
                        
                        print(response)
                        if let jsonData = response.result.value {
                            print("JSON: \(jsonData)")
                            let json = JSON(data: response.data! )
                            //  let json = jsonData as! NSDictionary
                            if let descriptionStr = json["description"].string {
                                print(descriptionStr)
                                print(json)
                                if descriptionStr == "Wrong auth token"{
                                    
                                    
                                    UserDefaults.standard.setValue(nil, forKey: "auth_token")
                                    UserDefaults.standard.setValue(nil, forKey: "user_id")
                                    UserDefaults.standard.setValue(nil, forKey: "user_type")
                                    
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let vc = storyboard.instantiateViewController(withIdentifier: "Login") as! Login
                                    vc.isExpired = true
                                    self.navigationController?.pushViewController(vc,animated: true)
                                    self.locationManager.startUpdatingLocation()
                                    
                                }
                            }
                        }else if((response.error) != nil){
                            
                            self.buildGeofenceData()
                            
                            
                        }
                        
                    }
            }
            
            
            
        }
      // checking  restro login case
        else{
            
        print("Nothing to perform")
            
            
        }
        
        
   
    }
    
    
    // Hitting the moving address api
    func movingAddressData(){
        
        
      // Mark: checking member and artist case
        if self.userTypeValue == "1" || self.userTypeValue == "3" {
            
            let json = ["user_id":Singleton.sharedInstance.userIdStr,
                        "auth_token":Singleton.sharedInstance.userTokenStr,
                        "moving_lat":DataManager.currentLat ?? "",
                        "moving_long":DataManager.currentLong ?? "",
                        "moving_address":self.addressString
                ] as [String : Any]
            print(json)
            Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/users/movingaddress", method: .post, parameters: json, encoding: JSONEncoding.default)
                .responseJSON {  response in
                    DispatchQueue.main.async {
                        
                        print(response)
                        if let jsonData = response.result.value {
                            print("JSON: \(jsonData)")
                            let json = JSON(data: response.data! )
                            //  let json = jsonData as! NSDictionary
                            if let descriptionStr = json["description"].string {
                                print(descriptionStr)
                                print(json)
                                if descriptionStr == "Wrong auth token" {
                                    
                                    
                                    UserDefaults.standard.setValue(nil, forKey: "auth_token")
                                    UserDefaults.standard.setValue(nil, forKey: "user_id")
                                    UserDefaults.standard.setValue(nil, forKey: "user_type")
                                    
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let vc = storyboard.instantiateViewController(withIdentifier: "Login") as! Login
                                    vc.isExpired = true
                                    self.navigationController?.pushViewController(vc,animated: true)
                                    self.locationManager.startUpdatingLocation()
                                    
                                }
                            }
                        }else if((response.error) != nil){
                            
                            self.movingAddressData()
                            
                            
                        }
                        
                    }
            }
            
            
            
        }
            // Mark: checking restro login case
        else{
            
            print("We have entered into the restro login case")
            
            
        }
    
    }
    //MARK: Location manager delegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){

      //  let valueToCompare: Double = 50

        DataManager.currentLat = locations.last?.coordinate.latitude
        DataManager.currentLong = locations.last?.coordinate.longitude
        self.locationManager.stopUpdatingLocation()
        if self.notificationValue == "1"{
            
            self.buildGeofenceData()

        }
        if self.locationValue == "1"{
            
            self.getAddressFromLatLon(pdblLatitude: DataManager.currentLat! , withLongitude:  DataManager.currentLong! )
            
        }
        else{
            
          print("Our location is off")
            
            
        }
        
        return
        
        
    }
    
   // Getting current address from current latitude and longitude
    func getAddressFromLatLon(pdblLatitude: Double, withLongitude pdblLongitude: Double) {
        
        
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
      
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = pdblLatitude
        center.longitude = pdblLongitude
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
         ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                
                let pm = placemarks! as [CLPlacemark]
                  if pm.count > 0 {
                    let pm = placemarks![0]
                    print(pm.country ?? String())
                    print(pm.locality  ?? String())
                    print(pm.subLocality  ?? String())
                    print(pm.postalCode  ?? String())
                 
                    self.addressString = ""
                    if pm.subLocality != nil {
                        self.addressString =  self.addressString + pm.subLocality! + ", "
                    }
                    
                    if pm.locality != nil {
                        self.addressString = self.addressString + pm.locality! + ", "
                    }
                    if pm.country != nil {
                       self.addressString = self.addressString + pm.country! + ", "
                    }
                    if pm.postalCode != nil {
                        self.addressString = self.addressString + pm.postalCode! + " "
                    }
                    print(self.addressString)
                    UserDefaults.standard.set(self.addressString, forKey: "savedAddress")
                    UserDefaults.standard.synchronize()
                }
                
                self.movingAddressData()
        })
        
    }
    //MARK: SetUpSlideMenu
    
    fileprivate func setupSideMenu() {
        // Define the menus
        SideMenuManager.menuLeftNavigationController = storyboard!.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as? UISideMenuNavigationController
        
        // Enable gestures. The left and/or right menus must be set up above for these to work.
        // Note that these continue to work on the Navigation Controller independent of the View Controller it displays!
        SideMenuManager.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
        SideMenuManager.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
        
        
        // Set up a cool background image for demo purposes
        //     SideMenuManager.menuAnimationBackgroundColor = UIColor(patternImage: UIImage(named: "background")!)
        SideMenuManager.menuAnimationBackgroundColor = UIColor(white: 1, alpha: 0.0)
    }

    
    // MARK: Get Users Method
    
    func getUsersMethod(page:String) {

        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 60
        let  json = ["user_id":Singleton.sharedInstance.userIdStr,
                     "auth_token":Singleton.sharedInstance.userTokenStr,
                     "latitude":Singleton.sharedInstance.latitudeStr,
                     "longtitude":Singleton.sharedInstance.longitudeStr,
                     "user_type":Singleton.sharedInstance.userTypeIdStr,
                     "page":page
                     ] as [String : Any]
        
        print(json)
        
        manager.request("http://beta.brstdev.com/freedrinkz/api/web/v1/members/searchmember", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
             //   print(response.result)   // result of response serialization
               // print(response)
                if let jsonData = response.result.value {
                print("JSON: \(jsonData)")
                    let json = JSON(data: response.data! )
                   
                    if let descriptionStr = json["description"].string {
                    //    print(descriptionStr)
                        if descriptionStr == "Wrong auth token" {
                            
                            
                            UserDefaults.standard.setValue(nil, forKey: "auth_token")
                            UserDefaults.standard.setValue(nil, forKey: "user_id")
                            UserDefaults.standard.setValue(nil, forKey: "user_type")
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "Login") as! Login
                            vc.isExpired = true
                            self.navigationController?.pushViewController(vc,animated: true)
                            
                        } else if descriptionStr == "Member list" {

                            self.membersArray = json["data"]["Userlist"].array!
                         //   self.allUsersArray = json["data"]["Allusers"].array!
                            self.allUsersArrayNew = json["data"]["Allusers"].array!
                            self.newSearchArray = json["data"]["Allusers"].array!
                            for var i  in 0 ..< self.newSearchArray.count{
                            self.allUsersArray.append(self.newSearchArray[i])

                                //print(self.newSearchArray)

                            }
//                            var items = self.allUsersArray
//                            items.append(json["data"]["Allusers"])
//                            self.allUsersArray = items
                         
                          //  print(self.allUsersArray)
                            self.paginationDict = json["data"]["pegination"].dictionaryObject!
                         //   print(self.paginationDict)
                         
                           // print(self.allUsersArray)
                            
                            
                        }
                        
                        
                        //Now you got your value
                       if self.allUsersArray.count == 0 {
                            self.statusLbl.isHidden = false
                        } else {
                            self.statusLbl.isHidden = true
                        }

                    }
                } else if((response.error) != nil){
                    self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                    
                }
                else if(self.error._code == NSURLErrorTimedOut){
                    self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                    
                }
                DispatchQueue.main.async {
                    
                    self.indicator.stopAnimating()
                    self.indicator.isHidden = true
                    self.memberCollec.reloadData()
                    self.searchCollec.reloadData()
                    
                }
            
        }
        
    }
    // MARK: Pagination Method
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView){
    
        
        guard paginationDict.count > 0, let currentpage = (paginationDict["currentpage"] as? NSString)?.integerValue, let lastpage = (paginationDict["lastpage"] as? NSInteger), currentpage < lastpage , let nextpage = (paginationDict["nextpage"] as? NSInteger) , let bottomEdge: Float = Float(self.searchCollec.contentOffset.y + self.searchCollec.frame.size.height)
           else {
                    print("We are on last page")
                    return
                        }
        if  bottomEdge >= Float(self.searchCollec.contentSize.height) && allUsersArray.count > 0{
            print(allUsersArray.count)
           getUsersMethod(page:String(describing: nextpage))
        }
      
}
    
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath){

        
    }
    
    
    // MARK: Get Filter Users Method
    
    func getFilterUsersMethod () {
        
        
        var ageStr = String()
        var genderStr = String()
        var countryStr = String()
        var cityStr = String()
        var stateStr = String()


        genderStr = genderTxt.text!
        countryStr = countryTxt.text!
        cityStr = citiesTxt.text!
        stateStr = stateTxt.text!


        if ageSlider.value == 16 {
            ageStr = "16-30"
        } else if ageSlider.value == 40 {
            ageStr = "31-40"
        } else if ageSlider.value == 65 {
            ageStr = "41-65"
        } else if ageSlider.value == 85 {
            ageStr = "66-85"
        } else if ageSlider.value == 100 {
            ageStr = "86-100"
        } else {
            ageStr = ""
        }

        
        var userTypeStr = String()
        if memberTxt.text == "Members" {
            userTypeStr = "1"
        }
        else if memberTxt.text == "Hotels" {
            userTypeStr = "2"
            ageStr = ""
        }
        else if memberTxt.text == "Restaurants" {
            userTypeStr = "4"
            ageStr = ""
        }
        else if memberTxt.text == "Bar" {
            userTypeStr = "5"
            ageStr = ""
        }
        else if memberTxt.text == "Club" {
            userTypeStr = "6"
            ageStr = ""
        }
        else if memberTxt.text == " Resort" {
            userTypeStr = "7"
            ageStr = ""
        }
        else if memberTxt.text == "DJ's, Musical Artist, Bands" {
            userTypeStr = "3"
        }
        
        // 6 June :-
        else if memberTxt.text == "Bars, Restaurants, Clubs, Hotels" {
            userTypeStr = "2"
            ageStr = ""
        }
        
        

        if isSearch == true  {
            ageStr = ""
            genderStr = ""
            userTypeStr = ""
            countryStr = ""
            stateStr = ""
            cityStr = ""

        }
        

        let  json = ["user_id":Singleton.sharedInstance.userIdStr,
                     "auth_token":Singleton.sharedInstance.userTokenStr,
                     "gender":genderStr,
                     "user_type":userTypeStr,
                     "search_name":nameTxt.text!,
                     "limit":"",
                     "offset":"",
                     "age":ageStr,
                     "country":countryStr,
                     "city":cityStr,
                     "state":stateStr
            ] as [String : Any]

        print(json)
        
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/members/searchmemberfilter", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    let json = JSON(data: response.data! )
                    if let descriptionStr = json["description"].string {
                        print(descriptionStr)
                        if descriptionStr == "Member list" {
                            
                            
                            self.allUsersArray = json["data"]["Userlist"].array!
                            print(self.allUsersArray.count)
                            print(self.allUsersArray)
                            print(self.locationKey)
                            self.hideStr = "yes"
                            self.headerView.resignFirstResponder()
                            self.searchCollec.reloadData()

                        } else  if descriptionStr == "No User list" {
                            self.allUsersArray = json["data"]["Userlist"].array!
                            self.hideStr = "yes"
                            self.headerView.isHidden = true
                            self.nameTxt.resignFirstResponder()
                            self.searchCollec.reloadData()
                        
                        }
                        
                        if self.allUsersArray.count == 0 {
                            self.statusLbl.isHidden = false
                        } else {
                            self.statusLbl.isHidden = true
                        }
                        
                        //Now you got your value
                        
                    }
                } else if((response.error) != nil){
                    
                    self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                    
                }
                self.indicator.stopAnimating()
                self.indicator.isHidden = true

        }
        
    }
    


    // MARK: AlertView
    
    func alertViewMethod(titleStr:String, messageStr:String) {
        let alert = UIAlertController(title: titleStr, message: messageStr, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
      

    }

    // MARK: UICollectionView Methods
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 1 {
            return membersArray.count
        }
        return allUsersArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView.tag == 1 {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MembersNearByCell", for: indexPath) as! MembersNearByCell
            
            cell.memberImg.layer.cornerRadius=cell.memberImg.frame.size.height/2
            cell.memberImg.clipsToBounds=true


            cell.memberNameLbl.text = "\(membersArray[indexPath.row]["firstname"].string!) \(membersArray[indexPath.row]["lastname"].string!)"
            
            // 10 June :-
            // cell.memberImg.sd_setImage(with: URL(string: membersArray[indexPath.row]["profile_pic_url"].string!), placeholderImage: UIImage(named: "user"))
            let urlStr : String = (membersArray[indexPath.row]["profile_pic_url"].string!)
            let urlFormattedStr = urlStr.replacingOccurrences(of: " ", with: "%20")
            cell.memberImg.sd_setImage(with: URL(string:urlFormattedStr), placeholderImage: UIImage(named: "user"))
            
           return cell

            
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SearchCell", for: indexPath) as! SearchCell
        cell.searchImg.layer.cornerRadius=cell.searchImg.frame.size.height/2
        cell.searchImg.clipsToBounds=true
 
         if (allUsersArray[indexPath.row]["user_type"].string! == "1"){
            
            if allUsersArray[indexPath.row]["online_status"].string! == "1" {
                cell.onlineImg.isHidden = false
            } else {
                cell.onlineImg.isHidden = true
            }
            cell.nameLbl.text = "\(allUsersArray[indexPath.row]["firstname"].string!) \(allUsersArray[indexPath.row]["lastname"].string!)"
         
            // 25 May :-
//            cell.searchImg.sd_setImage(with: URL(string: allUsersArray[indexPath.row]["profile_pic_url"].string!), placeholderImage: UIImage(named: "user"))
            
            let urlStr : String = allUsersArray[indexPath.row]["profile_pic_url"].string ?? ""
            let urlFormattedStr = urlStr.replacingOccurrences(of: " ", with: "%20")
            
            cell.searchImg.sd_setImage(with: URL(string:urlFormattedStr), placeholderImage: UIImage(named: "user"))
            
            
            
            cell.searchTypeImg.image = UIImage(named: "user_logo")
            cell.sendMsgView.isHidden = true
            cell.sendMsgView2.isHidden = false

            // Mark: all restro login
     } else if allUsersArray[indexPath.row]["user_type"].string! == "2" {
            cell.nameLbl.text = allUsersArray[indexPath.row]["resturant_name"].string!
           
            
            // 25 May :-
//            cell.searchImg.sd_setImage(with: URL(string: allUsersArray[indexPath.row]["profile_pic_url"].string!), placeholderImage: UIImage(named: "gallery"))
            
            let urlStr : String = allUsersArray[indexPath.row]["profile_pic_url"].string ?? ""
            let urlFormattedStr = urlStr.replacingOccurrences(of: " ", with: "%20")
            
            cell.searchImg.sd_setImage(with: URL(string:urlFormattedStr), placeholderImage: UIImage(named: "gallery"))
            
            cell.searchTypeImg.image = UIImage(named: "hotel-building")//{
                
            cell.onlineImg.isHidden = true
            cell.sendMsgView.isHidden = false
            cell.sendMsgView2.isHidden = true
            
            if Singleton.sharedInstance.userTypeStr == "place" {
                // 4 July :-
                 // cell.sendMsgView.isHidden = true
                    cell.sendMsgView.isHidden = false
                
                    cell.sendMsgView2.isHidden = true
                
             }
       
        }
        else if allUsersArray[indexPath.row]["user_type"].string! == "4" {
            cell.nameLbl.text = allUsersArray[indexPath.row]["resturant_name"].string!
            // 10 June :-
           // cell.searchImg.sd_setImage(with: URL(string: allUsersArray[indexPath.row]["profile_pic_url"].string!), placeholderImage: UIImage(named: "gallery"))
            let urlStr : String = (allUsersArray[indexPath.row]["profile_pic_url"].string!)
            let urlFormattedStr = urlStr.replacingOccurrences(of: " ", with: "%20")
            cell.searchImg.sd_setImage(with: URL(string:urlFormattedStr), placeholderImage: UIImage(named: "gallery"))
            
           cell.searchTypeImg.image = UIImage(named: "bar")
            cell.onlineImg.isHidden = true
            cell.sendMsgView.isHidden = false
            cell.sendMsgView2.isHidden = true
            if Singleton.sharedInstance.userTypeStr == "place" {
                cell.sendMsgView.isHidden = true
                cell.sendMsgView2.isHidden = true
            }
            
        }
        else if allUsersArray[indexPath.row]["user_type"].string! == "5" {
            cell.nameLbl.text = allUsersArray[indexPath.row]["resturant_name"].string!
            // 10 June :-
            // cell.searchImg.sd_setImage(with: URL(string: allUsersArray[indexPath.row]["profile_pic_url"].string!), placeholderImage: UIImage(named: "gallery"))
            let urlStr : String = (allUsersArray[indexPath.row]["profile_pic_url"].string!)
            let urlFormattedStr = urlStr.replacingOccurrences(of: " ", with: "%20")
            cell.searchImg.sd_setImage(with: URL(string:urlFormattedStr), placeholderImage: UIImage(named: "gallery"))
            
            
            cell.searchTypeImg.image = UIImage(named: "restaurant")
            cell.onlineImg.isHidden = true
            cell.sendMsgView.isHidden = false
            cell.sendMsgView2.isHidden = true
            if Singleton.sharedInstance.userTypeStr == "place" {
                cell.sendMsgView.isHidden = true
                cell.sendMsgView2.isHidden = true
            }
            
        }
        else if allUsersArray[indexPath.row]["user_type"].string! == "6" {
            cell.nameLbl.text = allUsersArray[indexPath.row]["resturant_name"].string!
            // 10 June :-
           // cell.searchImg.sd_setImage(with: URL(string: allUsersArray[indexPath.row]["profile_pic_url"].string!), placeholderImage: UIImage(named: "gallery"))
            let urlStr : String = (allUsersArray[indexPath.row]["profile_pic_url"].string!)
            let urlFormattedStr = urlStr.replacingOccurrences(of: " ", with: "%20")
            cell.searchImg.sd_setImage(with: URL(string:urlFormattedStr), placeholderImage: UIImage(named: "gallery"))
            
            cell.searchTypeImg.image = UIImage(named: "club")
            cell.onlineImg.isHidden = true
            cell.sendMsgView.isHidden = false
            cell.sendMsgView2.isHidden = true
            if Singleton.sharedInstance.userTypeStr == "place" {
                cell.sendMsgView.isHidden = true
                cell.sendMsgView2.isHidden = true
            }
            
        }
        else if allUsersArray[indexPath.row]["user_type"].string! == "7" {
            cell.nameLbl.text = allUsersArray[indexPath.row]["resturant_name"].string!
            // 10 June :-
           // cell.searchImg.sd_setImage(with: URL(string: allUsersArray[indexPath.row]["profile_pic_url"].string!), placeholderImage: UIImage(named: "gallery"))
            let urlStr : String = (allUsersArray[indexPath.row]["profile_pic_url"].string!)
            let urlFormattedStr = urlStr.replacingOccurrences(of: " ", with: "%20")
            cell.searchImg.sd_setImage(with: URL(string:urlFormattedStr), placeholderImage: UIImage(named: "gallery"))
            
            
            cell.searchTypeImg.image = UIImage(named: "resort")
            cell.onlineImg.isHidden = true
            cell.sendMsgView.isHidden = false
            cell.sendMsgView2.isHidden = true
            if Singleton.sharedInstance.userTypeStr == "place" {
                cell.sendMsgView.isHidden = true
                cell.sendMsgView2.isHidden = true
            }
            
        }
        
            // Mark: For artist
        else if allUsersArray[indexPath.row]["user_type"].string! == "3" {
            cell.nameLbl.text = "\(allUsersArray[indexPath.row]["firstname"].string!) \(allUsersArray[indexPath.row]["lastname"].string!)"
            // 10 June :-
           // cell.searchImg.sd_setImage(with: URL(string: allUsersArray[indexPath.row]["profile_pic_url"].string!), placeholderImage: UIImage(named: "user"))
            let urlStr : String = (allUsersArray[indexPath.row]["profile_pic_url"].string!)
            let urlFormattedStr = urlStr.replacingOccurrences(of: " ", with: "%20")
            cell.searchImg.sd_setImage(with: URL(string:urlFormattedStr), placeholderImage: UIImage(named: "user"))
            
            cell.searchTypeImg.image = UIImage(named: "guitar")
            cell.onlineImg.isHidden = true
            cell.sendMsgView.isHidden = true
            cell.sendMsgView2.isHidden = false

            
        }
        

        

              cell.locationLbl.text = "\(allUsersArray[indexPath.row]["city"].string!),\(allUsersArray[indexPath.row]["state"].string!)"
    
   

        cell.layer.cornerRadius = 4
        cell.layer.masksToBounds = false;
        cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.shadowOpacity = 0.3
        cell.layer.shadowRadius = 2
        cell.layer.shadowOffset = CGSize(width: 1, height: 1)
        
        
        cell.profileBtn.addTarget(self, action: #selector(viewProfile(_:)), for: .touchUpInside)
     //   cell.profileBtn2.addTarget(self, action: #selector(viewProfile(_:)), for: .touchUpInside)
        cell.sendMsgBtn.addTarget(self, action: #selector(Message(_:)), for: .touchUpInside)
        cell.sendMsgBtn2.addTarget(self, action: #selector(Message(_:)), for: .touchUpInside)
        cell.sendDrink.addTarget(self, action: #selector(SendDrinkz(_:)), for: .touchUpInside)

        cell.sendMsgBtn.tag = indexPath.row
        cell.sendMsgBtn2.tag = indexPath.row
        cell.sendDrink.tag = indexPath.row

        cell.profileBtn.tag = indexPath.row
     //   cell.profileBtn2.tag = indexPath.row
        cell.btnShowRestroStatus.tag = indexPath.row
        cell.btnShowRestroStatus.addTarget(self, action: #selector(showRestroImages), for: UIControlEvents.touchUpInside)
        return cell
    }
    
    func showRestroImages( _ sender: UIButton){

        
        var index = IndexPath()
        index = IndexPath.init(item: Int(sender.tag), section: 0)
        let cell = self.searchCollec.cellForItem(at: index) as! SearchCell
        
        if cell.searchTypeImg.image == UIImage(named: "user_logo"){
          
            self.lblShowRestroName.text = "This is a member"
            self.lblShowRestroName.isHidden = false;
            Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(self.dismissAlert), userInfo: nil, repeats: false)
            
        }
        
        if cell.searchTypeImg.image == UIImage(named: "guitar"){
          
            self.lblShowRestroName.text = "This is an artist"
            self.lblShowRestroName.isHidden = false;
            Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(self.dismissAlert), userInfo: nil, repeats: false)
            
        }
        
        if cell.searchTypeImg.image == UIImage(named: "hotel-building"){
            
        self.lblShowRestroName.text = "This is a hotel"
            self.lblShowRestroName.isHidden = false;
             Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(self.dismissAlert), userInfo: nil, repeats: false)
            
        }
        
        else if cell.searchTypeImg.image == UIImage(named: "bar"){
            
            self.lblShowRestroName.text = "This is a bar"
            self.lblShowRestroName.isHidden = false;
             Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(self.dismissAlert), userInfo: nil, repeats: false)
            
        }
        
        else if cell.searchTypeImg.image == UIImage(named: "restaurant"){
            
            
            self.lblShowRestroName.text = "This is a restaurant"
            self.lblShowRestroName.isHidden = false;
            Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(self.dismissAlert), userInfo: nil, repeats: false)
            
        }
        
        else if cell.searchTypeImg.image == UIImage(named: "club"){
            
            
            self.lblShowRestroName.text = "This is a club"
            self.lblShowRestroName.isHidden = false;
             Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(self.dismissAlert), userInfo: nil, repeats: false)
            
        }
        
        else if cell.searchTypeImg.image == UIImage(named: "resort"){
         
            
            self.lblShowRestroName.text = "This is a resort"
            self.lblShowRestroName.isHidden = false;
             Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(self.dismissAlert), userInfo: nil, repeats: false)
            
        }
        
       
        
        
    }
    
    func dismissAlert(){
        
     self.lblShowRestroName.isHidden = true
        
    }
    
    
    // MARK: TableView Buttons Action
    func SendDrinkz(_ button: UIButton) {
       
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SearchRestaurant") as! SearchRestaurant
        vc.recieverIdStr = allUsersArray[button.tag]["user_id"].string!
        vc.hideSkipTf = true
        UserDefaults.standard.set(isChecked, forKey: "ThreeKey")
        UserDefaults.standard.synchronize()
       navigationController?.pushViewController(vc,animated: true)
    }

    
    func viewProfile(_ button: UIButton) {

        if allUsersArray[button.tag]["user_type"].string! == "1" {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "Profile") as! Profile
            vc.fromNavStr = "other"
            vc.showPostVideoBtn = true
            vc.fromViewControllerStr = "Search"
            vc.memberProfileId = allUsersArray[button.tag]["user_id"].string!
            vc.isLocation = allUsersArray[button.tag]["notification_setting"].string!
            print(vc.isLocation)

            navigationController?.pushViewController(vc,animated: true)

        } else if allUsersArray[button.tag]["user_type"].string! == "2" {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "PlaceProfile") as! PlaceProfile
            vc.fromNavStr = "other"
            vc.restaurantId = allUsersArray[button.tag]["user_id"].string!
            navigationController?.pushViewController(vc,animated: true)

        } else if allUsersArray[button.tag]["user_type"].string! == "3" {

            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ArtistProfile") as! ArtistProfile
            vc.fromNavStr = "other"
            vc.artistId = allUsersArray[button.tag]["user_id"].string!
           // vc.isLocation = allUsersArray[button.tag]["notification_setting"].string!
            //print(vc.isLocation)
            navigationController?.pushViewController(vc,animated: true)

        }
        else if allUsersArray[button.tag]["user_type"].string! == "4" {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "PlaceProfile") as! PlaceProfile
            vc.fromNavStr = "other"
            vc.restaurantId = allUsersArray[button.tag]["user_id"].string!
            navigationController?.pushViewController(vc,animated: true)
            
        }
        
        else if allUsersArray[button.tag]["user_type"].string! == "5" {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "PlaceProfile") as! PlaceProfile
            vc.fromNavStr = "other"
            vc.restaurantId = allUsersArray[button.tag]["user_id"].string!
            navigationController?.pushViewController(vc,animated: true)
            
        }
        
        else if allUsersArray[button.tag]["user_type"].string! == "6" {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "PlaceProfile") as! PlaceProfile
            vc.fromNavStr = "other"
            vc.restaurantId = allUsersArray[button.tag]["user_id"].string!
            navigationController?.pushViewController(vc,animated: true)
            
        }
        
        else if allUsersArray[button.tag]["user_type"].string! == "7" {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "PlaceProfile") as! PlaceProfile
            vc.fromNavStr = "other"
            vc.restaurantId = allUsersArray[button.tag]["user_id"].string!
            navigationController?.pushViewController(vc,animated: true)
            
        }

    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    
        if (collectionView.tag==1) {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "Profile") as! Profile
            vc.fromNavStr = "other"
            vc.fromViewControllerStr = "Search"
            vc.showPostVideoBtn = true
            vc.memberProfileId = membersArray[indexPath.row]["user_id"].string!
         navigationController?.pushViewController(vc,animated: true)
        
        
        
        }
   
}
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        
        
        if collectionView.tag == 1 {
            return CGSize(width: 62,height: 74)
        }
        
        if (UIScreen.main.bounds.size.height==568 || UIScreen.main.bounds.size.height==480) {
            return CGSize(width: 137,height: 176)
        }
        if (UIScreen.main.bounds.size.height==736 ) {
            return CGSize(width: 125,height: 176)
        }
        return CGSize(width: 112,height: 176)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,referenceSizeForHeaderInSection section: Int) -> CGSize {
        if collectionView.tag == 1 {
            return CGSize(width: 0,height: 0)
        }
        if hideStr == "no" && collectionView.tag == 0 {
            return headerView.frame.size

        }
        return CGSize(width: 0,height: 0)
    }
    
   
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {

        
        
        let headerView2 = UICollectionReusableView()
        
            switch kind {
                
                
            case UICollectionElementKindSectionHeader:
                
                if collectionView.tag == 1 {
                    let headerView1 = collectionView.dequeueReusableSupplementaryView(ofKind: kind,withReuseIdentifier: "HeaderView1", for: indexPath)

                    return headerView1
                }
                let headerView1 = collectionView.dequeueReusableSupplementaryView(ofKind: kind,withReuseIdentifier: "HeaderView", for: indexPath)

                headerView1.frame=headerView.frame;
                headerView1.addSubview(headerView)
                
                searchCollec.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
                return headerView1
            default:
                
                assert(false, "Unexpected element kind")
            }
    

        return headerView2
     
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if collectionView.tag == 0 {
            if (UIScreen.main.bounds.size.height==568 || UIScreen.main.bounds.size.height==480) {
                return UIEdgeInsets(top: 10, left: 15, bottom: 10, right: 15)
            }
            return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        }
        return UIEdgeInsets(top: 0, left: 6, bottom: 0, right: 6)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView.tag == 1 {
            return 0
        }
        if (UIScreen.main.bounds.size.height==568 || UIScreen.main.bounds.size.height==480) {
            return 15;
        }
        return 8;
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView.tag == 1 {
            return 0
        }
        if (UIScreen.main.bounds.size.height==568 || UIScreen.main.bounds.size.height==480) {
            return 15;
        }
        return 10;
    }
    
    
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
//    {
//        var headerView1 = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 30))
//        return headerView1
//    }
    
    
    
    // MARK: Button Actions
    
    @IBAction func Country(_ sender: Any) {
        countryTxt.becomeFirstResponder()
    }
    
    @IBAction func State(_ sender: Any) {
        stateTxt.becomeFirstResponder()
    }
  
    @IBAction func City(_ sender: Any) {
        citiesTxt.becomeFirstResponder()
    }

    @IBAction func Message(_ sender: Any) {
        let buttonPosition = (sender as AnyObject).convert(CGPoint(), to:searchCollec)
        let indexPath = searchCollec.indexPathForItem(at:buttonPosition)
        let cell = searchCollec.cellForItem(at: indexPath!)  as! SearchCell
        let chatView = ChatViewController()
        chatView.navigationItem.title=cell.nameLbl.text
        chatView.recieverIdStr = allUsersArray[(sender as AnyObject).tag]["user_id"].string!
        chatView.senderIdStr = Singleton.sharedInstance.userIdStr
        chatView.recieverImgStr = allUsersArray[(sender as AnyObject).tag]["profile_pic_url"].string!
        let chatNavigationController = UINavigationController(rootViewController: chatView)
        present(chatNavigationController, animated: true, completion: nil)
        
    }

    
    @IBAction func SliderAction(_ sender: Any) {
        
        pointView1.isHidden = false
        pointView2.isHidden = false
        pointView3.isHidden = false
        pointView4.isHidden = false
        pointView5.isHidden = false

        if ageSlider.value>=16 && ageSlider.value<28 {
        
            ageSlider.value = 16
            pointView1.isHidden = true
        } else if ageSlider.value>=28 && ageSlider.value<50 {
            
            ageSlider.value = 40
            pointView2.isHidden = true

        } else if ageSlider.value>=50 && ageSlider.value<75 {
            
            ageSlider.value = 65
            pointView3.isHidden = true

        } else if ageSlider.value>=75 && ageSlider.value<90 {
            
            ageSlider.value = 85
            pointView4.isHidden = true

        } else if ageSlider.value>=90 && ageSlider.value<101 {
            
            ageSlider.value = 100
            pointView5.isHidden = true
        }
        
        
        
        print(ageSlider.value)

    }
    @IBAction func SearchHide(_ sender: Any) {
        if nameTxt.text != "" {
            
            hideStr = "yes"
            headerView.isHidden = true
            nameTxt.resignFirstResponder()
            isSearch = true
            indicator.startAnimating()
            indicator.isHidden = false
            getFilterUsersMethod()
        } else {
            self.allUsersArray = self.allUsersArrayNew
            statusLbl.isHidden = true
        }
        
        searchCollec.reloadData()
    }
    
    @IBAction func Filter(_ sender: Any) {
        if hideStr == "no" {
            hideStr = "yes"
            headerView.isHidden = true
            if self.allUsersArray.count == 0 {
                self.statusLbl.isHidden = false
            } else {
                self.statusLbl.isHidden = true
            }

          //  locationTxt.resignFirstResponder()
        } else {
            hideStr = "no"
            headerView.isHidden = false
            searchCollec.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
           // locationTxt.becomeFirstResponder()
            self.statusLbl.isHidden = true

        }
        
         searchCollec.reloadData()
    }

    @IBAction func Search(_ sender: Any) {

        indicator.startAnimating()
        indicator.isHidden = false
        
        isSearch = false

        getFilterUsersMethod()

    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Text Field Delegate

    func textFieldShouldReturn(_ textField: UITextField) -> Bool  {
        textField.resignFirstResponder()
        return true;
    }

    
    func textFieldDidChange(_ textField: UITextField) {
        if textField == nameTxt {
            if nameTxt.text == "" {
                self.allUsersArray = self.allUsersArrayNew
                searchCollec.reloadData()
            }
            if hideStr != "no" {
            
                if self.allUsersArray.count == 0 {
                    self.statusLbl.isHidden = false
                } else {
                    self.statusLbl.isHidden = true
                }
 
            }

        }
    }

   
    func textField(_ textField: IQDropDownTextField, didSelectItem item: String) {
        print(item)

        
        if item == "Bars, Restaurants, Clubs, Hotels" {
            genderTxt.isEnabled = false
            ageSlider.isEnabled = false
            genderTxt.text = ""
            genderTxt.setSelectedRow(0, animated: false)
            self.ageGenderView.isHidden = true
            self.headerView.frame.size.height = self.headerView.frame.size.height-140
            searchBtn.frame.origin.y = 110
            
        } else {
            genderTxt.isEnabled = true
            ageSlider.isEnabled = true
            self.ageGenderView.isHidden = false
            self.headerView.frame.size.height = 307
            searchBtn.frame.origin.y = 252

        }
        searchCollec.reloadData()
        textField.resignFirstResponder()
    }
     func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == countryTxt {

        }
    }
    
    // MARK: UIPickerView Methods
    
    // returns the number of 'columns' to display.
    func numberOfComponentsInPickerView(pickerView: UIPickerView!) -> Int{
        return 1
    }
    
    // returns the # of rows in each component..
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        if pickerView.tag == 0 {
            return countryArray.count
        } else if (pickerView.tag == 1) {
            return stateArray.count
        }
        return citiesArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 0 {
            return (countryArray[row]["name"] as! String)
        } else if (pickerView.tag == 1) {
            return (stateArray[row]["name"] as! String)
        }
        return (citiesArray[row]["name"] as! String)
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 0 {
            
            if (countryArray[row]["name"] as! String) != "Country" {
                countryTxt.text = (countryArray[row]["name"] as! String)
            } else {
                countryTxt.text = ""
            }
            
           
            let predicate = NSPredicate(format: "country_id == %@", (countryArray[row]["id"] as! String))
            let array = NSMutableArray(array: finalStateArray.filter { predicate.evaluate(with: $0) } )
            print(array)
            stateArray = array as! [[String : Any]];
          
            let pickerView11 = UIPickerView()
            stateTxt.inputView = pickerView11
            pickerView11.tag = 1
            pickerView11.delegate = self
            
            let pickerView12 = UIPickerView()
            citiesTxt.inputView = pickerView12
            pickerView12.tag = 2
            pickerView12.delegate = self

            
            stateTxt.text = ""
            citiesTxt.text = ""

            stateArray.insert([
                "id": "-8",
                "name": "State",
                "country_id": "-8"
                ], at: 0)
            citiesArray = [[
                "id": "-8",
                "name": "Cities",
                "state_id": "-8"
                ]]

        } else if (pickerView.tag == 1) {
            if (stateArray[row]["name"] as! String) != "State" {
                stateTxt.text = (stateArray[row]["name"] as! String)
            } else {
                stateTxt.text = ""
            }

            let predicate = NSPredicate(format: "state_id == %@", (stateArray[row]["id"] as! String))
            let array = NSMutableArray(array: finalCitiesArray.filter { predicate.evaluate(with: $0) } )
            print(array)
            citiesArray = array as! [[String : Any]];
            
            let pickerView2 = UIPickerView()
            citiesTxt.inputView = pickerView2
            pickerView2.tag = 2
            pickerView2.delegate = self
            
            citiesTxt.text = ""
            
            citiesArray.insert([
                "id": "-8",
                "name": "Cities",
                "state_id": "-8"
                ], at: 0)
        } else {
            
            if (citiesArray[row]["name"] as! String) != "Cities" {
                citiesTxt.text = (citiesArray[row]["name"] as! String)
            } else {
                citiesTxt.text = ""
            }


        }
        
    }
    

    
    // MARK: Get Profile Method
    
    func GetprofileMethod () {
        
        let json = ["user_id":Singleton.sharedInstance.userIdStr,
                    "auth_token":Singleton.sharedInstance.userTokenStr
            ] as [String : Any]
        
        
        
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/users/getprofile", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    let json = JSON(data: response.data! )
                    if let descriptionStr = json["description"].string {
                        print(descriptionStr)
                        if descriptionStr == "Wrong auth_token" {
                            
                            
                            //                            UserDefaults.standard.setValue(nil, forKey: "auth_token")
                            //                            UserDefaults.standard.setValue(nil, forKey: "user_id")
                            //                            UserDefaults.standard.setValue(nil, forKey: "user_type")
                            //
                            //                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            //                            let vc = storyboard.instantiateViewController(withIdentifier: "Login") as! Login
                            //                            vc.isExpired = true
                            //                            self.navigationController?.pushViewController(vc,animated: true)
                            
                        } else if descriptionStr == "User Profile" {
                            
                            
                            if (Singleton.sharedInstance.userTypeStr == "member") {
                                Singleton.sharedInstance.blockArray = json["data"]["block_user"].arrayObject! as! [[String : Any]]
                                print(Singleton.sharedInstance.blockArray.count)
                                
                            }
                            
                            
                            if json["data"]["profile_pic_url"].string != nil {
                                Singleton.sharedInstance.userImgStr =  String(describing: json["data"]["profile_pic_url"].string!)
                                Singleton.sharedInstance.userImgStr =  Singleton.sharedInstance.userImgStr.replacingOccurrences(of: "_normal", with: "")
                                
                            } else {
                                Singleton.sharedInstance.userImgStr =  ""
                                
                            }
                            if json["data"]["email"].string != nil {
                                Singleton.sharedInstance.emailStr =  String(describing: json["data"]["email"].string!)
                            } else {
                                Singleton.sharedInstance.emailStr =  ""
                            }
                            
                            if json["data"]["latitude"].string != nil {
                                Singleton.sharedInstance.latitudeStr =  String(describing: json["data"]["latitude"].string!)
                                UserDefaults.standard.setValue(Singleton.sharedInstance.latitudeStr, forKey: "latitude")
                            } else {
                                Singleton.sharedInstance.latitudeStr =  "0.0"
                            }
                            
                            if json["data"]["longitude"].string != nil {
                                Singleton.sharedInstance.longitudeStr =  String(describing: json["data"]["longitude"].string!)
                                UserDefaults.standard.setValue(Singleton.sharedInstance.longitudeStr, forKey: "longitude")
                                
                            } else {
                                Singleton.sharedInstance.longitudeStr =  "0.0"
                            }
                            
                            
                            if json["data"]["address"].string != nil {
                                Singleton.sharedInstance.addressStr =  String(describing: json["data"]["address"].string!)
                            } else {
                                Singleton.sharedInstance.addressStr =  ""
                            }
                            
                            if json["data"]["country"].string != nil {
                                Singleton.sharedInstance.cityStr =  String(describing: json["data"]["country"].string!)
                            } else {
                                Singleton.sharedInstance.countryStr =  ""
                            }
                            
                            if json["data"]["city"].string != nil {
                                Singleton.sharedInstance.cityStr =  String(describing: json["data"]["city"].string!)
                            } else {
                                Singleton.sharedInstance.cityStr =  ""
                            }
                            
                            if json["data"]["state"].string != nil {
                                Singleton.sharedInstance.stateStr =  String(describing: json["data"]["state"].string!)
                            } else {
                                Singleton.sharedInstance.stateStr =  ""
                            }
                            
                            if json["data"]["zip_code"].string != nil {
                                Singleton.sharedInstance.zipcodeStr =  String(describing: json["data"]["zip_code"].string!)
                            } else {
                                Singleton.sharedInstance.zipcodeStr =  ""
                            }
                            
                            
                            if json["data"]["check_in_status"].int != nil {
                                let status =  json["data"]["check_in_status"].int!
                                if status == 0 {
                                    Singleton.sharedInstance.checkInStatus =  false
                                } else {
                                    Singleton.sharedInstance.checkInStatus =  true
                                }
                            } else {
                                
                                Singleton.sharedInstance.checkInStatus =  false
                            }
                            
                            if json["data"]["resturant_id"].int != nil {
                                Singleton.sharedInstance.checkInRestId =  String(describing: json["data"]["resturant_id"].int!)
                            } else {
                                Singleton.sharedInstance.checkInRestId =  ""
                                
                            }
                            
                            var nameStr = String()
                            
                            // 11 June(Applied condition to check which type of place id is login and its name is saved accordingly.) :-
                            if Singleton.sharedInstance.userTypeIdStr == "2"
                            {
                                if json["data"]["resturant_name"].string != nil {
                                    Singleton.sharedInstance.restaurantStr =  String(describing: json["data"]["resturant_name"].string!)
                                } else {
                                    Singleton.sharedInstance.restaurantStr =  ""
                                }
                                nameStr = Singleton.sharedInstance.restaurantStr
                                
                            }
                                
                            else if Singleton.sharedInstance.userTypeIdStr == "4"
                            {
                                if json["data"]["resturant_name"].string != nil {
                                    Singleton.sharedInstance.restaurantStr =  String(describing: json["data"]["resturant_name"].string!)
                                } else {
                                    Singleton.sharedInstance.restaurantStr =  ""
                                }
                                nameStr = Singleton.sharedInstance.restaurantStr
                                
                            }
                                
                            else if Singleton.sharedInstance.userTypeIdStr == "5"
                            {
                                if json["data"]["resturant_name"].string != nil {
                                    Singleton.sharedInstance.restaurantStr =  String(describing: json["data"]["resturant_name"].string!)
                                } else {
                                    Singleton.sharedInstance.restaurantStr =  ""
                                }
                                nameStr = Singleton.sharedInstance.restaurantStr
                            }
                                
                            else if Singleton.sharedInstance.userTypeIdStr == "6"
                            {
                                if json["data"]["resturant_name"].string != nil {
                                    Singleton.sharedInstance.restaurantStr =  String(describing: json["data"]["resturant_name"].string!)
                                } else {
                                    Singleton.sharedInstance.restaurantStr =  ""
                                }
                                nameStr = Singleton.sharedInstance.restaurantStr
                            }
                                
                            else if Singleton.sharedInstance.userTypeIdStr == "7"
                            {
                                if json["data"]["resturant_name"].string != nil {
                                    Singleton.sharedInstance.restaurantStr =  String(describing: json["data"]["resturant_name"].string!)
                                } else {
                                    Singleton.sharedInstance.restaurantStr =  ""
                                }
                                nameStr = Singleton.sharedInstance.restaurantStr
                            }
                                
                                
                                
                            else {
                                if json["data"]["firstname"].string != nil {
                                    Singleton.sharedInstance.firstNameStr =  String(describing: json["data"]["firstname"].string!)
                                } else {
                                    Singleton.sharedInstance.firstNameStr =  ""
                                }
                                
                                
                                if json["data"]["lastname"].string != nil {
                                    Singleton.sharedInstance.lastNameStr =  String(describing: json["data"]["lastname"].string!)
                                } else {
                                    Singleton.sharedInstance.lastNameStr =  ""
                                }
                                nameStr = "\(Singleton.sharedInstance.firstNameStr) \(Singleton.sharedInstance.lastNameStr)"
                                
                            }
                            
                            if Singleton.sharedInstance.firstNameStr == " " {
                                Singleton.sharedInstance.firstNameStr = String(describing: json["data"]["resturant_name"].string!)
                            }
                            
                            var token = String()
                            if FIRInstanceID.instanceID().token() != nil {
                                token = FIRInstanceID.instanceID().token()!
                            } else {
                                token = ""
                            }
                            let values = ["name": nameStr, "userid": Singleton.sharedInstance.userIdStr as String, "fcm_token": token, "profile_pic_url": Singleton.sharedInstance.userImgStr] as [String : Any]
                            FIRDatabase.database().reference().child("users").child(Singleton.sharedInstance.userIdStr).child("credentials").updateChildValues(values, withCompletionBlock: { (errr, _) in
                                if errr == nil {
                                    
                                }
                            })
                            
                            
                        }
                        //Now you got your value
                    }
                    
                } else if((response.error) != nil){
                    
                    
                }
        }
        
    }
}
