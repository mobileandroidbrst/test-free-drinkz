//
//  Login.swift
//  FreeDrinkz
//
//  Created by Brst-Pc109 on 24/03/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FBSDKCoreKit.FBSDKGraphRequest
import TwitterKit
import Alamofire
import SwiftyJSON
import Firebase
import IQKeyboardManagerSwift

class Login: UIViewController,UITextFieldDelegate, GIDSignInUIDelegate, GIDSignInDelegate, URLSessionDelegate{
   
    
    
    @IBOutlet weak var emailtxt: UITextField!
    @IBOutlet weak var passwordTxt: UITextField!
    @IBOutlet weak var signInBtn: UIButton!
    @IBOutlet weak var signUpBtn: UIButton!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var fbButton: UIButton!
    @IBOutlet weak var twitterButton: UIButton!
    @IBOutlet weak var fbImage: UIImageView!
    @IBOutlet weak var twitterImage: UIImageView!
    
    
    var allDictionaries = [[String : AnyObject]]()
    var firstNameSocialStr = String()
    var lastNameSocialStr = String()
    var emailSocialStr = String()
    var socialIdStr = String()
    var genderSocialStr = String()
    var profilePicStr = String()
    var isExpired = Bool()
    var facebookFeedsArray = NSMutableArray()
    var twitterFeedsArray = [JSON]()
    
  

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 11.0, *) {
            emailtxt.textContentType = .username
        } else {
            // Fallback on earlier versions
        }
       // passwordTextField.textContentType = .password
        
        self.navigationController?.isNavigationBarHidden = true
        
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border.frame = CGRect(x: 0, y: emailtxt.frame.size.height - width, width:  emailtxt.frame.size.width, height: emailtxt.frame.size.height)
        border.borderWidth = width
        emailtxt.layer.addSublayer(border)
        emailtxt.layer.masksToBounds = true
        
        let border1 = CALayer()
        border1.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border1.frame = CGRect(x: 0, y: passwordTxt.frame.size.height - width, width:  passwordTxt.frame.size.width, height: passwordTxt.frame.size.height)
        border1.borderWidth = width
        passwordTxt.layer.addSublayer(border1)
        passwordTxt.layer.masksToBounds = true

        
        
        let fontSize: CGFloat = 16.0
        let fontFamily: String = "FuturaBT-LightItalic"
        let attrString = NSMutableAttributedString(attributedString: signUpBtn.currentAttributedTitle!)
        attrString.addAttribute(NSFontAttributeName, value: UIFont(name: fontFamily, size: fontSize)!, range: NSMakeRange(0, attrString.length))
        signUpBtn.setAttributedTitle(attrString, for: .normal)
        
        let panGesture = UIPanGestureRecognizer(target: self, action:(#selector(handlePanGesture(panGesture:))))
        self.view.addGestureRecognizer(panGesture)
        
        if(isExpired) {
            let alert = UIAlertController(title: "", message: "Your session has been expired. Please Login again!", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler:nil))
            self.present(alert, animated: true, completion: nil)
            
        }
        
        self.fbButton.isEnabled = false
        self.twitterButton.isEnabled = false
        self.fbImage.isHidden = true
        self.twitterImage.isHidden = true
    
    }
    
    // MARK: Google Activities
    /*
     func fetchData(){
     
     let defaultSession = URLSession(configuration: URLSessionConfiguration.default)
     
     var dataTask: URLSessionDataTask?
     
     
     
     let url = NSURL(string: "https://www.googleapis.com/plus/v1/people/113669386337872650911/activities/public?key=AIzaSyBxU-DIMTrbHjggG3uL1Fz0oJ8hs9RTtn8")
     
     dataTask = defaultSession.dataTask(with: url! as URL) {
     data, response, error in
     
     if let error = error {
     print(error.localizedDescription)
     } else if let httpResponse = response as? HTTPURLResponse {
     if httpResponse.statusCode == 200 {
     
     do {
     
     let decoded = try JSONSerialization.jsonObject(with: data!, options: [])
     // here "decoded" is of type `Any`, decoded from JSON data
     print(decoded)
     
     
     // first
     let dictFromJSON = decoded as! NSDictionary
     print("result: \(dictFromJSON)")
     let itemArray = (dictFromJSON["items"]! as! NSArray).mutableCopy() as! NSMutableArray
     print("result: \(itemArray)")
     //                        let valueId = itemArray[0] as! NSDictionary
     //                        print("id: \(valueId["id"])")
     
     
     
     
     } catch {
     print(error.localizedDescription)
     }
     
     
     
     
     
     
     
     }
     }
     }
     
     dataTask?.resume()
     
     
     }
     */
    
    
    
    func handlePanGesture(panGesture: UIPanGestureRecognizer) {
        // get translation
        if panGesture.state == .began || panGesture.state == .changed {
        }
    }
    
    
    // MARK: Social Login Method
    
    
    func socialLoginMethod () {
        
        
        let json = ["social_id":socialIdStr,
                    "profile_pic":profilePicStr,
                    "age":"16",
                    "city":"",
                    "email":emailSocialStr,
                    "firstname":firstNameSocialStr,
                    "gender":genderSocialStr,
                    "lastname":lastNameSocialStr,
                    "login_type":"1",
                    "password":"",
                    "state":"",
                    "usertype":"",
                    "zip_code":"",
                    "band_name":"",
                    "resturant_name":"",
                    "device_token": Singleton.sharedInstance.deviceTokenStr,
                    "device_type":"iphone"
            ] as [String : Any]
        
        
        print(json)
        
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/users/userlogin", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    let json = JSON(data: response.data! )
                    if let descriptionStr = json["description"].string {
                        print(descriptionStr)
                        if descriptionStr == "Missing parameter" {
                            self.alertViewMethod(titleStr: "", messageStr: "The email address is already in use by another account.")
                        } else if descriptionStr == "User Register Successfully!" {
                            
                            let userId = String(describing: json["data"]["user_id"].int!)
                            Singleton.sharedInstance.userIdStr = userId
                            
                            
                             let auth_token = json["data"]["auth_token"].string
                            Singleton.sharedInstance.userTokenStr = auth_token!
                            
                            Singleton.sharedInstance.userImgStr = self.profilePicStr
                            Singleton.sharedInstance.firstNameStr = self.firstNameSocialStr
                            Singleton.sharedInstance.lastNameStr = self.lastNameSocialStr
                            Singleton.sharedInstance.emailStr = self.emailSocialStr
                            Singleton.sharedInstance.stateStr = ""
                            Singleton.sharedInstance.cityStr = ""
                            Singleton.sharedInstance.zipcodeStr = ""
                           // Singleton.sharedInstance.genderStr = self.genderSocialStr
                            Singleton.sharedInstance.ageStr = ""
                            Singleton.sharedInstance.addressStr = ""
                            
                            
                            let alert = UIAlertController(title: nil, message: "Select your choice", preferredStyle: UIAlertControllerStyle.actionSheet)
                            
                            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
                            let memberBtn = UIAlertAction(title: "Member", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                                
                                self.getOnlineMethod()
                                
                                Singleton.sharedInstance.userTypeStr = "member"
                                Singleton.sharedInstance.userTypeIdStr = "1"
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let vc = storyboard.instantiateViewController(withIdentifier: "EditProfile") as! EditProfile
                                vc.isSocial = true
                                self.navigationController?.pushViewController(vc,animated: true)
                                })
                            let placeBtn = UIAlertAction(title: "Places(Bar, club or restaurant)", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                                
                                Singleton.sharedInstance.userImgStr = ""
                                
                                Singleton.sharedInstance.userTypeStr = "place"
                                Singleton.sharedInstance.userTypeIdStr = "2"
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let vc = storyboard.instantiateViewController(withIdentifier: "EditProfile") as! EditProfile
                                vc.isSocial = true
                                self.navigationController?.pushViewController(vc,animated: true)
                                
                            })
                            let artistBtn = UIAlertAction(title: "Musical Artist", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                                
                                Singleton.sharedInstance.userTypeStr = "artist"
                                Singleton.sharedInstance.userTypeIdStr = "3"
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let vc = storyboard.instantiateViewController(withIdentifier: "EditProfile") as! EditProfile
                                vc.isSocial = true
                                self.navigationController?.pushViewController(vc,animated: true)
                                
                            })
                            
                            
                            
                            alert.addAction(memberBtn)
                            alert.addAction(placeBtn)
                            alert.addAction(artistBtn)
                            
                            alert.addAction(cancelAction)
                            self.present(alert, animated: true)
                            
                            
                            
                        } else if descriptionStr == "Login Successfully" {
                            
                            let userId = String(describing: json["data"]["user_id"].int!)
                            Singleton.sharedInstance.userIdStr = userId
                            
                            
                            let auth_token = json["data"]["auth_token"].string
                            Singleton.sharedInstance.userTokenStr = auth_token!
                            
                            
                            
                            let userType = json["data"]["user_type"].string
                            
                            if userType == nil {
                                Singleton.sharedInstance.userImgStr = self.profilePicStr
                                Singleton.sharedInstance.firstNameStr = self.firstNameSocialStr
                                Singleton.sharedInstance.lastNameStr = self.lastNameSocialStr
                                Singleton.sharedInstance.emailStr = self.emailSocialStr
                                Singleton.sharedInstance.stateStr = ""
                                Singleton.sharedInstance.cityStr = ""
                                Singleton.sharedInstance.zipcodeStr = ""
                              //  Singleton.sharedInstance.genderStr = self.genderSocialStr
                                Singleton.sharedInstance.ageStr = ""
                                Singleton.sharedInstance.addressStr = ""
                                
                                
                                
                                let alert = UIAlertController(title: nil, message: "Select your choice", preferredStyle: UIAlertControllerStyle.actionSheet)
                                
                                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
                                let memberBtn = UIAlertAction(title: "Member", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                                    Singleton.sharedInstance.userTypeStr = "member"
                                    Singleton.sharedInstance.userTypeIdStr = "1"
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let vc = storyboard.instantiateViewController(withIdentifier: "EditProfile") as! EditProfile
                                    vc.isSocial = true
                                    self.navigationController?.pushViewController(vc,animated: true)
                                    
                                    
                                    
                                    
                                })
                                let placeBtn = UIAlertAction(title: "Places(Bar, club or restaurant)", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                                    
                                    Singleton.sharedInstance.userImgStr = ""
                                    
                                    Singleton.sharedInstance.userTypeStr = "place"
                                    Singleton.sharedInstance.userTypeIdStr = "2"
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let vc = storyboard.instantiateViewController(withIdentifier: "EditProfile") as! EditProfile
                                    vc.isSocial = true
                                    self.navigationController?.pushViewController(vc,animated: true)
                                    
                                })
                                let artistBtn = UIAlertAction(title: "Musical Artist", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                                    
                                    Singleton.sharedInstance.userTypeStr = "artist"
                                    Singleton.sharedInstance.userTypeIdStr = "3"
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let vc = storyboard.instantiateViewController(withIdentifier: "EditProfile") as! EditProfile
                                    vc.isSocial = true
                                    self.navigationController?.pushViewController(vc,animated: true)
                                    
                                })
                                
                                
                                
                                alert.addAction(memberBtn)
                                alert.addAction(placeBtn)
                                alert.addAction(artistBtn)
                                alert.addAction(cancelAction)
                                self.present(alert, animated: true)
                                
                            } else {
                                print(userType!)
                                Singleton.sharedInstance.userTypeIdStr = userType!
                                
                                UserDefaults.standard.setValue(auth_token!, forKey: "auth_token")
                                UserDefaults.standard.setValue(userId, forKey: "user_id")
                                UserDefaults.standard.setValue(userType!, forKey: "user_type")
                                
                                
                                if json["data"]["latitude"].string != nil {
                                    Singleton.sharedInstance.latitudeStr =  String(describing: json["data"]["latitude"].string!)
                                } else {
                                    Singleton.sharedInstance.latitudeStr =  "0.0"
                                }
                                
                                if json["data"]["longitude"].string != nil {
                                    Singleton.sharedInstance.longitudeStr =  String(describing: json["data"]["longitude"].string!)
                                } else {
                                    Singleton.sharedInstance.longitudeStr =  "0.0"
                                }
                                UserDefaults.standard.setValue(Singleton.sharedInstance.latitudeStr, forKey: "latitude")
                                UserDefaults.standard.setValue(Singleton.sharedInstance.longitudeStr, forKey: "longitude")

                                
                                
                                if userType == "1" {
                                    print("member")
                                    self.getOnlineMethod()
                                    Singleton.sharedInstance.userTypeStr = "member"
//                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                                    let vc = storyboard.instantiateViewController(withIdentifier: "Profile") as! Profile
//                                    self.navigationController?.pushViewController(vc,animated: true)
                                    
                                    DataManager.isCallRestroWebservice = true //prabhjot
                                    
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let vc = storyboard.instantiateViewController(withIdentifier: "Search") as! Search
                                    self.navigationController?.pushViewController(vc,animated: true)

                                    
                                } else if userType == "2" {
                                    print("place")
                                    Singleton.sharedInstance.userImgStr = ""
                                    
                                    Singleton.sharedInstance.userTypeStr = "place"
//                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                                    let vc = storyboard.instantiateViewController(withIdentifier: "PlaceProfile") as! PlaceProfile
//                                    self.navigationController?.pushViewController(vc,animated: true)
                                    DataManager.isCallRestroWebservice = true
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let vc = storyboard.instantiateViewController(withIdentifier: "Search") as! Search
                                    self.navigationController?.pushViewController(vc,animated: true)

                                    
                                }else if userType == "3" {
                                    print("artist")
                                    Singleton.sharedInstance.userTypeStr = "artist"
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let vc = storyboard.instantiateViewController(withIdentifier: "ArtistProfile") as! ArtistProfile
                                    self.navigationController?.pushViewController(vc,animated: true)
                                } else {
                                    
                                    
                                    Singleton.sharedInstance.userImgStr = self.profilePicStr
                                    Singleton.sharedInstance.firstNameStr = self.firstNameSocialStr
                                    Singleton.sharedInstance.lastNameStr = self.lastNameSocialStr
                                    Singleton.sharedInstance.emailStr = self.emailSocialStr
                                    Singleton.sharedInstance.stateStr = ""
                                    Singleton.sharedInstance.cityStr = ""
                                    Singleton.sharedInstance.zipcodeStr = ""
                                  //  Singleton.sharedInstance.genderStr = self.genderSocialStr
                                    Singleton.sharedInstance.ageStr = ""
                                    Singleton.sharedInstance.addressStr = ""
                                    
                                    
                                    
                                    let alert = UIAlertController(title: nil, message: "Select your choice", preferredStyle: UIAlertControllerStyle.actionSheet)
                                    
                                    let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
                                    let memberBtn = UIAlertAction(title: "Member", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                                        Singleton.sharedInstance.userTypeStr = "member"
                                        Singleton.sharedInstance.userTypeIdStr = "1"
                                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                        let vc = storyboard.instantiateViewController(withIdentifier: "EditProfile") as! EditProfile
                                        vc.isSocial = true
                                        self.navigationController?.pushViewController(vc,animated: true)
                                        })
                                    let placeBtn = UIAlertAction(title: "Places(Bar, club or restaurant)", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                                        
                                        Singleton.sharedInstance.userImgStr = ""
                                        
                                        Singleton.sharedInstance.userTypeStr = "place"
                                        Singleton.sharedInstance.userTypeIdStr = "2"
                                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                        let vc = storyboard.instantiateViewController(withIdentifier: "EditProfile") as! EditProfile
                                        vc.isSocial = true
                                        self.navigationController?.pushViewController(vc,animated: true)
                                        
                                    })
                                    let artistBtn = UIAlertAction(title: "Musical Artist", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                                        
                                        Singleton.sharedInstance.userTypeStr = "artist"
                                        Singleton.sharedInstance.userTypeIdStr = "3"
                                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                        let vc = storyboard.instantiateViewController(withIdentifier: "EditProfile") as! EditProfile
                                        vc.isSocial = true
                                        self.navigationController?.pushViewController(vc,animated: true)
                                        
                                    })
                                    
                                    
                                    
                                    alert.addAction(memberBtn)
                                    alert.addAction(placeBtn)
                                    alert.addAction(artistBtn)
                                    
                                    alert.addAction(cancelAction)
                                    self.present(alert, animated: true)
                                    
                                    
                                }
                                
                                
                            }
                            
                            
                            
                        }
                        //Now you got your value
                    }
                } else if((response.error) != nil){
                    
                    self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                    
                }
                
                
                self.indicator.stopAnimating()
                self.indicator.isHidden = true
                
        }
        
    }
    
    // MARK: Login Method
    
    func loginMethod() {

        UserDefaults.standard.removeObject(forKey: "user_id")
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 120 * 1000
        let json = ["email":emailtxt.text!,
                    "password":passwordTxt.text!,
                    "device_token":Singleton.sharedInstance.deviceTokenStr,
                    "device_type":"iphone"
            ] as [String : Any]
        
        print(json)

     manager.request("http://beta.brstdev.com/freedrinkz/api/web/v1/users/userlogin", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    let json = JSON(data: response.data! )
                    if let descriptionStr = json["description"].string {
                        print(descriptionStr)
                        if descriptionStr == "Wrong Email and Password" {
                            self.alertViewMethod(titleStr: "", messageStr: "The email or password you entered is invalid.")
                        } else if descriptionStr == "User Login Successfully" {


                            let userId = String(describing: json["data"]["user_id"].int!)
                            print(userId)
                            
                        // 26 April :-
                        // Storing Login-Userid person's user_id in User-Defaults
                            UserDefaults.standard.set(userId, forKey: "userId")
                        
                            
                            Singleton.sharedInstance.userIdStr = userId

                            let auth_token = json["data"]["auth_token"].string
                            print(auth_token!)
                            
                            // 26 April :-
                            // Storing Login-AuthToken person's auth_token in User-Defaults
                            UserDefaults.standard.set(auth_token, forKey: "auth_token")
                            
                            Singleton.sharedInstance.userTokenStr = auth_token!
                            
                            
                            let receivePush = String(describing: json["data"]["receive_push"].int!)
                            print(receivePush)


                            let userType = json["data"]["usertype"].string
                            print(userType!)
                            Singleton.sharedInstance.userTypeIdStr = userType!
                            UserDefaults.standard.set(userType, forKey: "userTypeKey")
                            UserDefaults.standard.synchronize()


                            if json["data"]["latitude"].string != nil {
                                Singleton.sharedInstance.latitudeStr =  String(describing: json["data"]["latitude"].string!)
                            } else {
                                Singleton.sharedInstance.latitudeStr =  "0.0"
                            }

                            if json["data"]["longitude"].string != nil {
                                Singleton.sharedInstance.longitudeStr =  String(describing: json["data"]["longitude"].string!)
                            } else {
                                Singleton.sharedInstance.longitudeStr =  "0.0"
                            }
                            UserDefaults.standard.setValue(Singleton.sharedInstance.latitudeStr, forKey: "latitude")
                            UserDefaults.standard.setValue(Singleton.sharedInstance.longitudeStr, forKey: "longitude")


                            UserDefaults.standard.setValue(auth_token!, forKey: "auth_token")
                            UserDefaults.standard.setValue(userId, forKey: "user_id")
                            print(userId)
                            
                            
                            UserDefaults.standard.setValue(userType!, forKey: "user_type")
                           UserDefaults.standard.setValue(receivePush, forKey: "push_type")


                            if userType == "1" {
                                
                                
                                print("member")
                                self.getOnlineMethod()
                                Singleton.sharedInstance.userTypeStr = "member"
                                 DataManager.isCallRestroWebservice = true
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let vc = storyboard.instantiateViewController(withIdentifier: "Search") as! Search
                                vc.userType = userType!
                                //vc.pushType = receivePush
                                print(vc.userType)
                                self.navigationController?.pushViewController(vc,animated: true)

                            } else if userType == "2" {
                               
                                
                                print("place")
                                Singleton.sharedInstance.userTypeStr = "place"
                                 DataManager.isCallRestroWebservice = true  //prabhjot
                                 let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let vc = storyboard.instantiateViewController(withIdentifier: "Search") as! Search
                            self.navigationController?.pushViewController(vc,animated: true)

                            }
                            else if userType == "4" {
                                
                                
                                print("place")
                                Singleton.sharedInstance.userTypeStr = "place"
                                DataManager.isCallRestroWebservice = true  //prabhjot
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let vc = storyboard.instantiateViewController(withIdentifier: "Search") as! Search
                                self.navigationController?.pushViewController(vc,animated: true)
                                
                            }
                            else if userType == "5" {
                                
                                
                                print("place")
                                Singleton.sharedInstance.userTypeStr = "place"
                                DataManager.isCallRestroWebservice = true  //prabhjot
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let vc = storyboard.instantiateViewController(withIdentifier: "Search") as! Search
                                self.navigationController?.pushViewController(vc,animated: true)
                             }
                            else if userType == "6" {
                                
                                
                                print("place")
                                Singleton.sharedInstance.userTypeStr = "place"
                                DataManager.isCallRestroWebservice = true  //prabhjot
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let vc = storyboard.instantiateViewController(withIdentifier: "Search") as! Search
                                self.navigationController?.pushViewController(vc,animated: true)
                                
                            }
                            else if userType == "7" {
                                
                                
                                print("place")
                                Singleton.sharedInstance.userTypeStr = "place"
                                DataManager.isCallRestroWebservice = true  //prabhjot
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let vc = storyboard.instantiateViewController(withIdentifier: "Search") as! Search
                                self.navigationController?.pushViewController(vc,animated: true)
                                
                            }
                            else if userType == "3" {
                                
                                
                                print("artist")
                                Singleton.sharedInstance.userTypeStr = "artist"
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let vc = storyboard.instantiateViewController(withIdentifier: "ArtistProfile") as! ArtistProfile
                                 vc.userType = userType!
                                self.navigationController?.pushViewController(vc,animated: true)
                                 

                            }



                        }
                        //Now you got your value
                    }
                } else if((response.error) != nil){

                     self.loginMethod()
                    
                }

                self.indicator.stopAnimating()
                self.indicator.isHidden = true

        }

    }
    
    // MARK: Get Online Method
    
    func getOnlineMethod () {
     
        let json = ["user_id":Singleton.sharedInstance.userIdStr,
                    "auth_token":Singleton.sharedInstance.userTokenStr,
                    "online_status":"1"
            ] as [String : Any]
        
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/users/getonline", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
        }
        
    }
    
    
    // MARK: Email Validation
    
    func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    // MARK: AlertView
    
    func alertViewMethod(titleStr:String, messageStr:String) {
        let alert = UIAlertController(title: titleStr, message: messageStr, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    // MARK: Buttons Action
    
    @IBAction func SignIn(_ sender: Any) {
        
        Singleton.sharedInstance.socialLoginType = "1"
        UserDefaults.standard.setValue(Singleton.sharedInstance.socialLoginType, forKey: "socialLoginType")
        
        if (emailtxt.text?.isEmpty)! {
            print("enter email address") //prompt ALert or toast
            alertViewMethod(titleStr: "", messageStr: "Please enter the email.")
        } else if !isValidEmail(testStr: emailtxt.text!) {
            alertViewMethod(titleStr: "", messageStr: "Please enter the valid email.")
        } else if (passwordTxt.text?.isEmpty)! {
            alertViewMethod(titleStr: "", messageStr: "Please enter the password.")
        } else {
            indicator.startAnimating()
            indicator.isHidden = false
            loginMethod()
        }
    }
    @IBAction func ForgorPassword(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ForgotPassword") as! ForgotPassword
        navigationController?.pushViewController(vc,animated: true)
        
        
    }
    @IBAction func SignUp(_ sender: Any) {
    }
    
    @IBAction func TwitterAction(_ sender: Any) {
        UserDefaults.standard.removeObject(forKey: "user_id")
        Singleton.sharedInstance.socialLoginType = "2"
        UserDefaults.standard.setValue(Singleton.sharedInstance.socialLoginType, forKey: "socialLoginType")
        
        self.indicator.startAnimating()
        self.indicator.isHidden = false
        
        Twitter.sharedInstance().logIn { session, error in
            if (session != nil) {
                print("signed in as \(session!.userName)");
                print("signed in as \(session!.userID)");
                
                let twitterClient = TWTRAPIClient(userID: session!.userID)
                twitterClient.loadUser(withID: session!.userID) { (user, error) in
                    
                    if ((user?.profileImageURL) != nil) {
                        self.profilePicStr  = user!.profileImageURL
                    }
                    
                    let name = session!.userName
                    
                    
                    var fullNameArr = name.components(separatedBy: " ")
                    let firstName: String = fullNameArr[0]
                    let lastName: String? = fullNameArr.count > 1 ? fullNameArr[1] : nil
                    
                    if ((lastName) != nil) {
                        self.lastNameSocialStr  = lastName!
                    } else {
                        
                        self.lastNameSocialStr  = ""
                    }
                    
                    self.firstNameSocialStr  = firstName
                   // self.genderSocialStr  = ""
                    self.socialIdStr  = session!.userID
                    self.emailSocialStr  = ""
                    
                    
                    print(self.firstNameSocialStr)
                    print(self.lastNameSocialStr)
                    print(self.profilePicStr)
                    print(self.emailSocialStr)
                    
                    
                    self.indicator.startAnimating()
                    self.indicator.isHidden = false
                    
                    self.socialLoginMethod()
                    
                    
                    
                    
                }
                
                
            } else {
                print("error: \(String(describing: error?.localizedDescription))");
                self.indicator.stopAnimating()
                self.indicator.isHidden = true
                
            }
        }
        
    }
    @IBAction func Facebook(_ sender: Any) {
        UserDefaults.standard.removeObject(forKey: "user_id")
        Singleton.sharedInstance.socialLoginType = "1"
        UserDefaults.standard.setValue(Singleton.sharedInstance.socialLoginType, forKey: "socialLoginType")
        
        
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["public_profile", "email", "user_friends", "user_posts"], from: self) { (result, error) in
            if (error == nil){
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                if fbloginresult.grantedPermissions != nil {
                    if(fbloginresult.grantedPermissions.contains("email"))
                    {
                        
                        // fbLoginManager.logOut()
                    }
                    
                    
                    print("token is \(FBSDKAccessToken.current().tokenString)")
                    print("token is \(FBSDKAccessToken.current().userID)")

                    let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "email, id, name, first_name, last_name, gender, relationship_status, location, feed"])
                    graphRequest.start(completionHandler: { (connection, result, error) -> Void in
                        
                        if ((error) != nil)
                        {
                            // Process error
                        }
                        else
                        {
                            print("the access token is \(FBSDKAccessToken.current().tokenString!)")
                            print("the userid is \(FBSDKAccessToken.current().userID!)")
                            print("http://graph.facebook.com/\(FBSDKAccessToken.current().userID!)/picture?type=large")
                            let fbDetails = result as! NSDictionary
                            
                            self.firstNameSocialStr  = fbDetails.value(forKey: "first_name") as! String
                            self.lastNameSocialStr  = fbDetails.value(forKey: "last_name") as! String
                           // self.genderSocialStr  = fbDetails.value(forKey: "gender") as! String
                            self.profilePicStr  = "http://graph.facebook.com/\(FBSDKAccessToken.current().userID!)/picture?type=large"
                            self.socialIdStr  = FBSDKAccessToken.current().userID!
                            
                            if fbDetails.value(forKey: "email") != nil {
                                self.emailSocialStr  = fbDetails.value(forKey: "email") as! String
                            } else {
                                self.emailSocialStr = ""
                            }
                            
                        //    self.genderSocialStr = self.genderSocialStr.capitalized
                            
                            
                            self.indicator.startAnimating()
                            self.indicator.isHidden = false
                            
                            self.socialLoginMethod()
                            
                            
                        }
                    })
                }
            } else {
            print(error!)
            }
        }
        
    }
    
    
    @IBAction func GooglePlus(_ sender: Any) {
        UserDefaults.standard.removeObject(forKey: "user_id")
        Singleton.sharedInstance.socialLoginType = "1"
        UserDefaults.standard.setValue(Singleton.sharedInstance.socialLoginType, forKey: "socialLoginType")
        
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().signIn()
    }
    
    
    
    //MARK:Google SignIn Delegate
    
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
        //  myActivityIndicator.stopAnimating()
    }
    // Present a view that prompts the user to sign in with Google
    
    func sign(_ signIn: GIDSignIn!,  present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    // Dismiss the "Sign in with Google" view
    
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
    //completed sign In
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if (error == nil) {
            // Perform any operations on signed in user here.
            
            self.firstNameSocialStr  = user.profile.givenName!
            self.lastNameSocialStr  = user.profile.familyName!
            self.profilePicStr  = ""
            self.socialIdStr  = user.userID!
            self.emailSocialStr  = user.profile.email!
            self.genderSocialStr = ""
            
            if user.profile.hasImage{
                self.profilePicStr = user.profile.imageURL(withDimension: 200).absoluteString
                print(self.profilePicStr)
                
            }
            
            self.indicator.startAnimating()
            self.indicator.isHidden = false
            
            self.socialLoginMethod()
            
            
            
        } else {
            print("\(error.localizedDescription)")
        }
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        emailtxt.resignFirstResponder()
        passwordTxt.resignFirstResponder()
    }
    
    // MARK: Text Field Delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool  {
        textField.resignFirstResponder()
        return true;
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
}

