//
//  MyOrders.swift
//  FreeDrinkz
//
//  Created by Brst-Pc109 on 27/03/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

import UIKit
import FBSDKShareKit
import FBSDKCoreKit.FBSDKGraphRequest
import FBSDKLoginKit
import Alamofire
import SwiftyJSON
import IQKeyboardManagerSwift
import AVFoundation
import Photos

class MyOrders: UIViewController,UITableViewDataSource,UITableViewDelegate, FBSDKSharingDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate{

    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var receivedLbl: UILabel!
    @IBOutlet weak var sentLbl: UILabel!
    @IBOutlet weak var receivedBtn: UIButton!
    @IBOutlet weak var sentBtn: UIButton!
    @IBOutlet weak var ordersTbl: UITableView!
    @IBOutlet weak var coupanView: UIView!
    @IBOutlet weak var coupanView1: UIView!
    @IBOutlet weak var coupanLbl: UILabel!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var searchTxt: UITextField!

    var receiveOrderArray = NSMutableArray()
    var sentOrderArray = NSMutableArray()
    var searchArray = NSMutableArray()
    var isSearch = Bool()
    var typeStr = String()
    var receiverDrinkImage = String()
    var receiverSenderId = Int()
    var receiverImage = String()
    var receiverOrderId = Int()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        IQKeyboardManager.sharedManager().enable = true

        backView.layer.cornerRadius = 16
        backView.layer.borderColor = UIColor.white.cgColor
        backView.layer.borderWidth = 1
        backView.clipsToBounds = true
        typeStr = "received"
        coupanView.isHidden = true
      
        
        indicator.startAnimating()
        indicator.isHidden = false

        getOrderMethod()
    }
    
    
      override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.navigationBar.isHidden = true
    }

    
    // MARK: Get Orders
    
    func getOrderMethod() {
        
        
        let json = ["user_id":Singleton.sharedInstance.userIdStr,
                    "auth_token":Singleton.sharedInstance.userTokenStr
                    
                ] as [String : Any]
        print(json)
        
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/members/orderlist", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    let json = JSON(data: response.data! )
                    if let descriptionStr = json["description"].string {
                        print(descriptionStr)
                        if descriptionStr == "Wrong auth token" {
                            
                            
                            UserDefaults.standard.setValue(nil, forKey: "auth_token")
                            UserDefaults.standard.setValue(nil, forKey: "user_id")
                            UserDefaults.standard.setValue(nil, forKey: "user_type")
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "Login") as! Login
                            vc.isExpired = true
                            self.navigationController?.pushViewController(vc,animated: true)
                            
                        }  else if descriptionStr == "Order list" {
                           
                            self.sentOrderArray = NSMutableArray(array: json["data"]["sender_list"].arrayObject!)
                            self.receiveOrderArray = NSMutableArray(array: json["data"]["reciver_list"].arrayObject!)

                            print(self.sentOrderArray.count)
                            print(self.receiveOrderArray.count)

                            if (self.typeStr == "received") {
                                
                                
                                if self.receiveOrderArray.count == 0 {
                                    self.statusLbl.isHidden = false;
                                    self.statusLbl.text = "You have not received any new order."
                                } else {
                                    self.statusLbl.isHidden = true;
                                }
                                
                            } else if (self.typeStr == "sent") {
                                
                                if self.sentOrderArray.count == 0 {
                                    self.statusLbl.isHidden = false;
                                    self.statusLbl.text = "You have not sent any order to other member."
                                } else {
                                    self.statusLbl.isHidden = true;
                                }
                                }
                        
                        self.ordersTbl.reloadData()
                        //Now you got your value
                    }
                } else if((response.error) != nil){
                    
                    self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                    
                }
                
                self.indicator.stopAnimating()
                self.indicator.isHidden = true
                
        }
        
      }

    }
    // MARK: AlertView
    
    func alertViewMethod(titleStr:String, messageStr:String) {
        let alert = UIAlertController(title: titleStr, message: messageStr, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
 
    // MARK: UITableView Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearch {
            return searchArray.count
        }
        
        if typeStr == "received" {
            return receiveOrderArray.count
        } else if typeStr == "sent" {
            return sentOrderArray.count
        }
        return 0
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:OrderCell = ordersTbl.dequeueReusableCell(withIdentifier: "OrderCell") as! OrderCell
        cell.completedView.isHidden = true
        cell.backView1.isHidden = true
        cell.coupanView.isHidden = false
        var dict = NSDictionary()
        
        if (typeStr == "received") {
            cell.fromToLbl.text = "From:"
            
            // 6 June :-
            cell.shareBtn.isHidden = false
            cell.shareBtnImg.isHidden = false
            cell.shareBtn.addTarget(self, action: #selector(postDrink(_:)), for: .touchUpInside)
            cell.shareBtn.tag = indexPath.row
            
            
            if isSearch {
                dict = searchArray[indexPath.row] as! NSDictionary
            } else {
                
                // 9 May(Nothing-done) :-
                dict = receiveOrderArray[indexPath.row] as! NSDictionary
                let utcTimeStamp = dict.value(forKey:"created") as? String
                if utcTimeStamp != "" && utcTimeStamp != nil {
                    
                    let doubleKey = Double(utcTimeStamp!)
                    let date = Date(timeIntervalSince1970:doubleKey!)
                    let dateFormatter = DateFormatter()
                    // 25 June :-
                  //  dateFormatter.timeZone = TimeZone(abbreviation: "UTC") //Set timezone that you want
                    dateFormatter.locale = NSLocale.current
                    dateFormatter.dateFormat = "yyyy-MM-dd hh:mm a" //Specify your format that you want
                    dateFormatter.timeZone = NSTimeZone.local
                    let strDate = dateFormatter.string(from: date)
                    cell.dateTimeLbl.text = "Date And Time: \(strDate)"
                    cell.dateTimeLbl.isHidden = false

                }
                else{
                    
                 cell.dateTimeLbl.isHidden = true
                    
                }
            }
            
            cell.usernameLbl.text = dict["sender_name"] as? String
            
            // 5 June :-
           //  cell.userImg.sd_setImage(with: URL(string: (dict["sender_image"] as? String)!), placeholderImage: UIImage(named: "user"))
            
            let urlStr : String = (dict["sender_image"] as? String)!
            let urlFormattedStr = urlStr.replacingOccurrences(of: " ", with: "%20")
             cell.userImg.sd_setImage(with: URL(string: urlFormattedStr), placeholderImage: UIImage(named: "user"))
           
            
           }
        
        else {
            // 6 June :-
            cell.shareBtnImg.isHidden = true
            cell.shareBtn.isHidden = true
            
            cell.fromToLbl.text = "To:"
            
            if isSearch {
                dict = searchArray[indexPath.row] as! NSDictionary
            } else {
                dict = sentOrderArray[indexPath.row] as! NSDictionary
                let utcTimeStamp = dict.value(forKey:"created") as? String
                
                // 23 May :-
                if utcTimeStamp != "" && utcTimeStamp != nil {
                    
                    let doubleKey = Double(utcTimeStamp!)
                    let date = Date(timeIntervalSince1970:doubleKey!)
                    let dateFormatter = DateFormatter()
                    dateFormatter.timeZone = TimeZone(abbreviation: "UTC") //Set timezone that you want
                    dateFormatter.locale = NSLocale.current
                    dateFormatter.dateFormat = "yyyy-MM-dd hh:mm a" //Specify your format that you want
                    dateFormatter.timeZone = NSTimeZone.local
                    let strDate = dateFormatter.string(from: date)
                    cell.dateTimeLbl.text = "Date And Time: \(strDate)"
                     cell.dateTimeLbl.isHidden = false
                }
                
                else{
                    
                    cell.dateTimeLbl.isHidden = true

                }
               
            
            }

            cell.usernameLbl.text = dict["receiver_name"] as? String
            
            // 5 June :-
           // cell.userImg.sd_setImage(with: URL(string: (dict["receiver_image"] as? String)!), placeholderImage: UIImage(named: "user"))
            
            let urlStr : String = (dict["receiver_image"] as? String)!
            let urlFormattedStr = urlStr.replacingOccurrences(of: " ", with: "%20")
            cell.userImg.sd_setImage(with: URL(string: urlFormattedStr), placeholderImage: UIImage(named: "user"))
            
         }
        
        cell.addressLbl.text = "\(dict["address"] as! String), \(dict["city"] as! String), \(dict["state"] as! String), \(dict["zip_code"]!)"

        cell.drinkLbl.text = "\(dict["product_name"] as! String) @ \(dict["resturant_name"] as! String)"
//        cell.drinkImg.sd_setImage(with: URL(string: (dict["drink_image"] as? String)!), completed: {(image,  error,  cacheType, imageURL) -> Void in
//        })

        
        // 5 June :-
        //  cell.drinkImg.sd_setImage(with: URL(string: (dict["drink_image"] as? String)!), placeholderImage: UIImage(named: "gallery"))
        
         let urlStr : String = (dict["drink_image"] as? String)!
        let urlFormattedStr = urlStr.replacingOccurrences(of: " ", with: "%20")
       cell.drinkImg.sd_setImage(with: URL(string: urlFormattedStr), placeholderImage: UIImage(named: "user"))
       
        
        if dict["status"] as? Int == 1 {
            cell.completedView.isHidden = false
            cell.backView1.isHidden = false
            cell.coupanView.isHidden = true
        }

        cell.userImg.layer.cornerRadius = cell.userImg.frame.size.height/2
        cell.userImg.clipsToBounds = true

        cell.backView.layer.cornerRadius = 4
        cell.backView.layer.masksToBounds = false;
        cell.backView.layer.shadowColor = UIColor.black.cgColor
        cell.backView.layer.shadowOpacity = 0.3
        cell.backView.layer.shadowRadius = 2
        cell.backView.layer.shadowOffset = CGSize(width: 1, height: 1)
       
       
        
        cell.coupanViewBtn.addTarget(self, action: #selector(viewCoupan(_:)), for: .touchUpInside)
        cell.coupanViewBtn.tag = indexPath.row
        
        // 6 June :-
        //cell.shareBtn.addTarget(self, action: #selector(postDrink(_:)), for: .touchUpInside)
        //cell.shareBtn.tag = indexPath.row

        
        cell.profileBtn.addTarget(self, action: #selector(ViewProfile(_:)), for: .touchUpInside)
        cell.profileBtn.tag = indexPath.row
        
        
        return cell
    }
    
    
    func ViewProfile(_ button: UIButton) {
        
        var dict = NSDictionary()
        
        if (typeStr == "received") {
            
            
            if isSearch {
                dict = searchArray[button.tag] as! NSDictionary
            } else {
                dict = receiveOrderArray[button.tag] as! NSDictionary
            }
            
            if dict["sender_user_type"] as! String == "1" {
                
                let predicate = NSPredicate(format: "user_id == %d", dict["sender_id"]! as! Int)
                let array = Singleton.sharedInstance.blockArray.filter { predicate.evaluate(with: $0) }
                if array.count == 0 {
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "Profile") as! Profile
                    vc.fromNavStr = "other"
                    vc.fromViewControllerStr = "MyOrders"
                    vc.memberProfileId = String(describing: dict["sender_id"]!)
                    navigationController?.pushViewController(vc,animated: true)
                } else {
                    alertViewMethod(titleStr: "", messageStr: "User is not available for your profile.")
                }

                
            } else if dict["sender_user_type"] as! String == "2" {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "PlaceProfile") as! PlaceProfile
                vc.fromNavStr = "other"
                vc.restaurantId = String(describing: dict["sender_id"]!)
                navigationController?.pushViewController(vc,animated: true)
            }

            
            
        } else {
            
            if isSearch {
                dict = searchArray[button.tag] as! NSDictionary
            } else {
                dict = sentOrderArray[button.tag] as! NSDictionary
            }

            
            let predicate = NSPredicate(format: "user_id == %d",  dict["receiver_id"]! as! Int)
            let array = Singleton.sharedInstance.blockArray.filter { predicate.evaluate(with: $0) }
            if array.count == 0 {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "Profile") as! Profile
                vc.fromNavStr = "other"
                vc.fromViewControllerStr = "MyOrders"
                vc.memberProfileId = String(describing: dict["receiver_id"]!)
                navigationController?.pushViewController(vc,animated: true)
            } else {
                alertViewMethod(titleStr: "", messageStr: "User is not available for your profile.")
            }
        }
    }
    

    func postDrink(_ sender: UIButton) {
        
        
        if (typeStr == "received"){
            
            
            let dict = receiveOrderArray[sender.tag] as! NSDictionary
            receiverDrinkImage = (dict.value(forKey:"drink_image") as? String)!
            receiverImage = (dict.value(forKey:"receiver_image") as? String)!
            receiverOrderId = (dict.value(forKey:"order_id") as? Int)!
            receiverSenderId = ((dict.value(forKey:"sender_id") as? Int)!)
         hitApi(receiverDrinkImg:receiverDrinkImage,receiverImage:receiverImage,receiverOrderId:receiverOrderId,receiverSenderId:receiverSenderId)
            
            
        }
    
    }
    
    //MARK: Order Share Api Implementation
    func hitApi(receiverDrinkImg:String,receiverImage:String,receiverOrderId:Int,receiverSenderId:Int){
    
        let userId = Singleton.sharedInstance.userIdStr
        let authToken = Singleton.sharedInstance.userTokenStr
        var parameterDict = JSONDictionary()
       parameterDict["user_id"] = userId
       parameterDict["auth_token"] = authToken
       parameterDict["other_userid"] = receiverSenderId
       parameterDict["type"] = "image"
       parameterDict["image"] = receiverDrinkImage
       parameterDict["offer_drink"] = receiverOrderId
       print(parameterDict)
        ApiExtension.sharedInstance.shareOrderScreen(parameter:parameterDict, completionHandler:{(result) in
           print(result)
            
            let statusCode = result.value(forKey:"status_code") as? NSString
            if statusCode == "200"{
                
                let data = result.value(forKey:"data") as? NSDictionary
                let description = data?.value(forKey:"description") as? NSString
                self.alertViewMethod(titleStr:"", messageStr:description! as String)
            }
          
            else{
                
                let description = result.value(forKey:"description") as? NSString
                self.alertViewMethod(titleStr:"", messageStr:description! as String)
                
            }
            
            
        })
    
    }

    
    func viewCoupan(_ button: UIButton) {
        
        var dict = NSDictionary()
        
        
        if (typeStr == "received") {
            if isSearch {
                dict = searchArray[button.tag] as! NSDictionary
            } else {
                dict = receiveOrderArray[button.tag] as! NSDictionary
            }
            

        } else {
            if isSearch {
                dict = searchArray[button.tag] as! NSDictionary
            } else {
                dict = sentOrderArray[button.tag] as! NSDictionary
            }

        }
        coupanLbl.text = dict["coupon_code"] as? String
        coupanView.isHidden = false
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
    }
    //MARK: CheckCameraAuthorization
    
    func checkCameraAuthorization() {
        
        let status: AVAuthorizationStatus = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo)
        if status == .authorized {
            // authorized
            let picker = UIImagePickerController()
            picker.delegate = self
        //    picker.allowsEditing = true
            picker.sourceType = .camera
            present(picker, animated: true, completion: { _ in })
        } else if status == .denied {
            // denied
            if AVCaptureDevice.responds(to: #selector(AVCaptureDevice.requestAccess(forMediaType:completionHandler:))) {
                AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: {(_ granted: Bool) -> Void in
                    // Will get here on both iOS 7 & 8 even though camera permissions weren't required
                    // until iOS 8. So for iOS 7 permission will always be granted.
                    print("DENIED")
                    if granted {
                        // Permission has been granted. Use dispatch_async for any UI updating
                        // code because this block may be executed in a thread.
                        
                        DispatchQueue.main.async(execute: {() -> Void in
                            let picker = UIImagePickerController()
                            picker.delegate = self
                        //    picker.allowsEditing = true
                            picker.sourceType = .camera
                            self.present(picker, animated: true, completion: { _ in })
                        })
                        
                    }
                    else {
                        // Permission has been denied.
                        
                        
                        DispatchQueue.main.async(execute: {() -> Void in
                            self.accessMethod("Please go to Settings and enable the camera for this app to use this feature.")
                        })
                        
                        
                    }
                })
            }
        } else if status == .restricted {
            // restricted
            DispatchQueue.main.async(execute: {() -> Void in
                self.accessMethod("Please go to Settings and enable the camera for this app to use this feature.")
            })
        }
        else if status == .notDetermined {
            // not determined
            AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: {(_ granted: Bool) -> Void in
                if granted {
                    // Access has been granted ..do something
                    DispatchQueue.main.async(execute: {() -> Void in
                        let picker = UIImagePickerController()
                        picker.delegate = self
                     //   picker.allowsEditing = true
                        picker.sourceType = .camera
                        self.present(picker, animated: true, completion: { _ in })
                        
                    })
                    
                    
                }
                else {
                    // Access denied ..do something
                    DispatchQueue.main.async(execute: {() -> Void in
                        self.accessMethod("Please go to Settings and enable the camera for this app to use this feature.")
                    })
                    
                }
            })
        }
    }
    
    //MARK: CheckPhotoAuthorization
    
    func checkPhotoAuthorization()  {
        let status: PHAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
        if status == .authorized {
            let picker = UIImagePickerController()
            picker.delegate = self
        //    picker.allowsEditing = true
            picker.sourceType = .photoLibrary
            present(picker, animated: true, completion: { _ in })
        }
        else if status == .denied {
            DispatchQueue.main.async(execute: {() -> Void in
                self.accessMethod("Please go to Settings and enable the photos for this app to use this feature.")
            })
        } else if status == .notDetermined {
            // Access has not been determined.
            PHPhotoLibrary.requestAuthorization({(_ status: PHAuthorizationStatus) -> Void in
                if status == .authorized {
                    
                    DispatchQueue.main.async(execute: {() -> Void in
                        let picker = UIImagePickerController()
                        picker.delegate = self
                     //   picker.allowsEditing = true
                        picker.sourceType = .photoLibrary
                        self.present(picker, animated: true, completion: { _ in })
                    })
                    
                }
                else {
                    DispatchQueue.main.async(execute: {() -> Void in
                        self.accessMethod("Please go to Settings and enable the photos for this app to use this feature.")
                    })
                }
            })
        }
        else if status == .restricted {
            // Restricted access - normally won't happen.
        }
        
        
    }
    
    func accessMethod(_ message: String) {
        let alertController = UIAlertController(title: "Not Authorized", message: message, preferredStyle: .alert)
        let cancel = UIAlertAction(title: "OK", style: .default, handler: nil)
        let Settings = UIAlertAction(title: "Settings", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
        })
        alertController.addAction(Settings)
        alertController.addAction(cancel)
        present(alertController, animated: true, completion: { _ in })
    }
    
    //MARK: UIImagePickerControllerDelegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let chosenImage: UIImage? = info[UIImagePickerControllerOriginalImage] as! UIImage?
        picker.dismiss(animated: true, completion: { _ in })
        
        let photo = FBSDKSharePhoto()
        photo.image = chosenImage
        photo.imageURL = NSURL(string: "http://beta.brstdev.com/freedrinkz/uploads/149450498133D419DC-819B-45B5-BEA5-37F9B8F23DE8.jpeg") as URL!
        photo.isUserGenerated = true
        let photoContent = FBSDKSharePhotoContent()
        photoContent.photos = [photo]
        
        let shareDialog = FBSDKShareDialog()
        shareDialog.fromViewController = self
        shareDialog.shareContent = photoContent
        shareDialog.delegate = self
        if !shareDialog.canShow() {
            print("cannot show native share dialog")
        }
        shareDialog.show()

    }

    
    

    //MARK: Button Actions
    @IBAction func Cancel(_ sender: Any) {
        coupanView.isHidden = true
    }
    @IBAction func Search(_ sender: Any) {
        if isSearch {
            isSearch = false
            searchTxt.resignFirstResponder()
            searchTxt.isHidden = true
        } else {
            isSearch = true
            searchTxt.becomeFirstResponder()
            searchTxt.isHidden = false
        }
        
        
//        let alert = UIAlertController(title: "", message: "Work in Progress", preferredStyle: UIAlertControllerStyle.alert)
//        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
//        self.present(alert, animated: true, completion: nil)
        

    }
    
    @IBAction func SearchOrder(_ sender: Any) {
        
        if (typeStr == "received") {
            let predicate1 = NSPredicate(format: "product_name contains[cd] %@", searchTxt.text!)
            let predicate2 = NSPredicate(format: "sender_name contains[cd] %@", searchTxt.text!)
            let predicate3 = NSPredicate(format: "resturant_name contains[cd] %@", searchTxt.text!)

            
            let predicate = NSCompoundPredicate.init(orPredicateWithSubpredicates: [predicate1, predicate2, predicate3])
            searchArray = NSMutableArray(array: receiveOrderArray.filter { predicate.evaluate(with: $0) })
            if searchTxt.text == "" {
                searchArray = receiveOrderArray
            }
        } else if (typeStr == "sent") {
            let predicate1 = NSPredicate(format: "product_name contains[cd] %@", searchTxt.text!)
            let predicate2 = NSPredicate(format: "receiver_name contains[cd] %@", searchTxt.text!)
            let predicate3 = NSPredicate(format: "resturant_name contains[cd] %@", searchTxt.text!)

            let predicate = NSCompoundPredicate.init(orPredicateWithSubpredicates: [predicate1, predicate2, predicate3])
            searchArray = NSMutableArray(array: sentOrderArray.filter { predicate.evaluate(with: $0) })
            if searchTxt.text == "" {
                searchArray = sentOrderArray
            }
        }
        print(searchArray.count)
        print(searchArray)
        ordersTbl.reloadData()
    }

    
    @IBAction func ReceivedSent(_ sender: Any) {
        
        if ((sender as AnyObject).tag == 0) {
            receivedBtn.backgroundColor = UIColor.white
            receivedLbl.textColor = UIColor(red: 217/255, green: 41/255, blue: 39/255, alpha: 1)
            sentBtn.backgroundColor = UIColor(red: 217/255, green: 41/255, blue: 39/255, alpha: 1)
            sentLbl.textColor = UIColor.white
            typeStr = "received"
        } else if ((sender as AnyObject).tag == 1) {
            sentBtn.backgroundColor = UIColor.white
            sentLbl.textColor = UIColor(red: 217/255, green: 41/255, blue: 39/255, alpha: 1)
            receivedBtn.backgroundColor = UIColor(red: 217/255, green: 41/255, blue: 39/255, alpha: 1)
            receivedLbl.textColor = UIColor.white
            typeStr = "sent"
        }
        ordersTbl.reloadData()
        
        
        if (self.typeStr == "received") {
            
            
            if self.receiveOrderArray.count == 0 {
                self.statusLbl.isHidden = false;
                self.statusLbl.text = "You have not received any new order."
            } else {
                self.statusLbl.isHidden = true;
            }
            
        } else if (self.typeStr == "sent") {
            
            if self.sentOrderArray.count == 0 {
                self.statusLbl.isHidden = false;
                self.statusLbl.text = "You have not sent any order to other member."
            } else {
                self.statusLbl.isHidden = true;
            }
            
        }
        

        if isSearch {
            if (typeStr == "received") {
                let predicate1 = NSPredicate(format: "product_name contains[cd] %@", searchTxt.text!)
                let predicate2 = NSPredicate(format: "sender_name contains[cd] %@", searchTxt.text!)
                let predicate3 = NSPredicate(format: "resturant_name contains[cd] %@", searchTxt.text!)

                let predicate = NSCompoundPredicate.init(orPredicateWithSubpredicates: [predicate1, predicate2, predicate3])
                searchArray = NSMutableArray(array: receiveOrderArray.filter { predicate.evaluate(with: $0) })
                if searchTxt.text == "" {
                    searchArray = receiveOrderArray
                }
            } else if (typeStr == "sent") {
                let predicate1 = NSPredicate(format: "product_name contains[cd] %@", searchTxt.text!)
                let predicate2 = NSPredicate(format: "receiver_name contains[cd] %@", searchTxt.text!)
                let predicate3 = NSPredicate(format: "resturant_name contains[cd] %@", searchTxt.text!)

                let predicate = NSCompoundPredicate.init(orPredicateWithSubpredicates: [predicate1, predicate2, predicate3])
                searchArray = NSMutableArray(array: sentOrderArray.filter { predicate.evaluate(with: $0) })
                if searchTxt.text == "" {
                    searchArray = sentOrderArray
                }
            }
            ordersTbl.reloadData()
        }
        

        
        
    }
    
    
    //MARK: Facebook Share Method Delegates
    
    /**
     Sent to the delegate when the share completes without error or cancellation.
     - Parameter sharer: The FBSDKSharing that completed.
     - Parameter results: The results from the sharer.  This may be nil or empty.
     */
    public func sharer(_ sharer: FBSDKSharing!, didCompleteWithResults results: [AnyHashable : Any]!) {
        print(results)
        
    }
    
    func sharer(_ sharer: FBSDKSharing!, didFailWithError error: Error!)
    {
        print("sharer NSError")
        print(error.localizedDescription)
    }
    
    func sharerDidCancel(_ sharer: FBSDKSharing!)
    {
        print("sharerDidCancel")
    }

    // MARK: Text Field Delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool  {
        textField.resignFirstResponder()
        return true;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
}
