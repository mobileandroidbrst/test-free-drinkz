//
//  ForgotPassword.swift
//  FreeDrinkz
//
//  Created by Brst-Pc109 on 30/03/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ForgotPassword: UIViewController {

    @IBOutlet weak var sendBtn: UIButton!
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var indicator: UIActivityIndicatorView!

    override func viewDidLoad() {
        super.viewDidLoad()

       
        

    }

    // MARK: Buttons Action
    
    @IBAction func Back(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
        
    }

    
    @IBAction func ForgotPassword(_ sender: Any) {
        
        if (emailTxt.text?.isEmpty)! {
            print("enter email address") //prompt ALert or toast
            alertViewMethod(titleStr: "", messageStr: "Please enter the email.")
        } else if !isValidEmail(testStr: emailTxt.text!) {
            alertViewMethod(titleStr: "", messageStr: "Please enter the valid email.")
        } else {
            indicator.startAnimating()
            indicator.isHidden = false
            forgotPasswordMethod()
        }
    }

    
    // MARK: AlertView
    
    func alertViewMethod(titleStr:String, messageStr:String) {
        let alert = UIAlertController(title: titleStr, message: messageStr, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: Email Validation
    
    func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    

    
    // MARK: Forgot Password Method
    
    func forgotPasswordMethod () {
        
        let json = ["email":emailTxt.text!
                    ] as [String : Any]
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/members/forgotpassword", method: .post, parameters: json, encoding: URLEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    let json = JSON(data: response.data!)
                    if let statusCode = json["status_code"].string {
                        print(statusCode)
                        if statusCode == "200" {
                            
                        //    self.alertViewMethod(titleStr: "", messageStr: "Please check your email to get new password.")
                            let data = json["data"].dictionaryObject as NSDictionary?
                            let id = data?.value(forKey:"id") as? Int
                            let otpVC = self.storyboard?.instantiateViewController(withIdentifier:"OtpVC") as? OtpVC
                            otpVC?.id  = id!
                           self.navigationController?.pushViewController(otpVC!, animated:true)
                            
                        } else if statusCode != "200" {
                            self.alertViewMethod(titleStr: "", messageStr: "There is no record corresponding to this email.")
                        }
                        //Now you got your value
                    }
                } else if((response.error) != nil){
                    
                    self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                    
                }
                self.indicator.stopAnimating()
                self.indicator.isHidden = true
                
        }
        
    }
    

    
    // MARK: Text Field Delegate
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        emailTxt.resignFirstResponder()
        
    }
    
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool  {
        textField.resignFirstResponder()
        return true;
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
