//
//  BlockCell.swift
//  FreeDrinkz
//
//  Created by Brst-Pc109 on 13/07/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

import UIKit

class BlockCell: UITableViewCell {

    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var blockBtn: UIButton!
    @IBOutlet weak var locationTxt: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
