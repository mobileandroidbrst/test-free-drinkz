//
//  CartDetailVC.swift
//  FreeDrinkz
//
//  Created by tbi-pc-57 on 6/19/19.
//  Copyright © 2019 Brst-Pc109. All rights reserved.
//
// 20 June :-

import UIKit
import Alamofire
import SwiftyJSON

class CartDetailVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    // Outlets :-
   @IBOutlet var cartTableView: UITableView!
   @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    var dataArray = [Any]()
    var cartId = String()
    var reciever_Name = String()
    
    // 21 June :-
    var cart_Id = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
     //   getCartData()
        self.cartTableView.rowHeight = 75
        
 }
    
    override func viewWillAppear(_ animated: Bool) {
        getCartData()
        
}
    
    
    // Mark :- getting Cart-Data
    
    func getCartData() {
       
        let json = ["user_id":Singleton.sharedInstance.userIdStr,
                    "auth_token":Singleton.sharedInstance.userTokenStr
            ] as [String : Any]
        
        print(json)
        
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/users/getcart", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
                
                print(response.result)   // result of response serialization
                print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    
                    let json = JSON(data: response.data! )
                    print(json)
                    if let dataDict = json["data"].dictionaryObject {
                        print(dataDict)
                        self.dataArray = dataDict["cart"] as! [Any]
                        print(self.dataArray)
                        print((self.dataArray as AnyObject).count!)
                        
                       // 27 June :-
                        if self.dataArray.count == 0 {
                            UserDefaults.standard.removeObject(forKey: "cart_id")
                            UserDefaults.standard.synchronize()
                        }
                        
                         self.cartTableView.reloadData()
                        
                    }
                }
                    
                    // self.imBuyimgTbl.reloadData()
                    if((response.error) != nil){
                        self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                    }
                    self.indicator.stopAnimating()
                    self.indicator.isHidden = true
                }
          
    }
   
    // MARK: AlertView
    func alertViewMethod(titleStr:String, messageStr:String) {
        let alert = UIAlertController(title: titleStr, message: messageStr, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
   
// Mark :- Tableview - Delegates and DataSources.

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return((self.dataArray as AnyObject).count!)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:CartCell = cartTableView.dequeueReusableCell(withIdentifier: "CartCell") as! CartCell
        cell.selectionStyle = .none
        
       let dict = dataArray[indexPath.row] as! NSDictionary
        
         if let id = dict["cart_id"] as? Int {
         cell.cartLbl.text = String(describing: id)
         self.cartId = String(describing: id)
        }
        
        if let name = dict["reciever_name"]{
            cell.receiverNameLbl.text = String(describing: name)
        }
        
        cell.deleteBtn.tag = indexPath.row
        cell.deleteBtn.addTarget(self, action: #selector(deleteAction(_:)), for: .touchUpInside)
        
        return cell
}
   
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let dict = dataArray[indexPath.row] as! NSDictionary
        print(dict)
        self.reciever_Name = dict["reciever_name"] as! String
        
      // 21 June - (Cart-id is sending to new Cart-Drinks screen so that it will help in further payment from that screen)
        // self.cart_Id = dict["cart_id"] as! String
        
        if let id = dict["cart_id"] {
            self.cart_Id = String(describing:id)
        }
        
        if let drinkArray = dict["offerdrinks"] as? NSArray {
            print(drinkArray)
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "CartDrinksVC") as! CartDrinksVC
            vc.drinksArray = NSMutableArray(array: drinkArray)
            vc.recievr_Name = self.reciever_Name
            // 21 June :-
            vc.cart_Id = self.cart_Id
            self.navigationController?.pushViewController(vc,animated: true)
            
         }
        
    }
    

    // Mark :- Delete-Button Action in Table-View
    @IBAction func deleteAction(_ sender: UIButton) {
          dataArray.remove(at:sender.tag)
       // self.cartTableView.reloadData()
        deleteCart()
    }
    
    
    func deleteCart() {
        let json = ["user_id":Singleton.sharedInstance.userIdStr,
                    "auth_token":Singleton.sharedInstance.userTokenStr,
                    "cart_id": cartId
            ] as [String : Any]
        
        print(json)
        
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/users/removecart", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
                
                print(response.result)   // result of response serialization
                print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    
                    let json = JSON(data: response.data! )
                    print(json)
                    if let dataDict = json["data"].dictionaryObject {
                        print(dataDict)
                        let message = dataDict["message"] as? String
                        if message == "successfully remove cart" {
                          
                            // 25 June :-
                            UserDefaults.standard.removeObject(forKey: "cart_id")
                            UserDefaults.standard.synchronize()
                            
                            let alert = UIAlertController(title: "", message: "Cart is removed", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            
                            self.cartTableView.reloadData()
                            
                        }
                     }
                }
                
                // self.imBuyimgTbl.reloadData()
                if((response.error) != nil){
                    self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                }
                self.indicator.stopAnimating()
                self.indicator.isHidden = true
        }
        
   
    }

    @IBAction func backAction(_ sender: UIButton) {
     _ = navigationController?.popViewController(animated: true)
     }
    
}
    
    
    
    

