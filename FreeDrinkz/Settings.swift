//
//  Settings.swift
//  FreeDrinkz
//
//  Created by Brst-Pc109 on 13/07/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

import UIKit
import Firebase
import Alamofire
import SwiftyJSON

class Settings: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var settingsTbl: UITableView!
    var  isOn = Bool()
    var isLocationOn = Bool()
    var isPushOn = Bool()
    var settingsArray = NSMutableArray()
    let locationSavedKey = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        FIRDatabase.database().reference().child("users").child(Singleton.sharedInstance.userIdStr).child("credentials").child("message_push").observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.exists() {
                
                let message_push = snapshot.value! as! String
                print(message_push)
                if message_push == "1" {
                    self.isOn = true
                } else {
                    self.isOn = false
                }
            }
                else {
                self.isOn = true
            }
            self.settingsArray = ["Message Notifications on/off","Blocked Users","Card details","Location on/off","Turn on/off for near by hotels,bars,restaurants etc"]
            self.settingsTbl.reloadData()
            
        })
        settingsTbl.tableFooterView  = UIView(frame: CGRect.zero)
        
        // toggling the location switch on the basis of userdefaults value
        let locationSavedKey =  UserDefaults.standard.value(forKey: "locationKey") as? String
        if locationSavedKey == "1" {
            self.isLocationOn = true
        } else {
            self.isLocationOn = false
        }
      
        
        
      // toggling the push switch on the basis of userdefaults value
        let pushSavedKey =  UserDefaults.standard.value(forKey: "push_type") as? String
        if pushSavedKey == "1" {
            self.isPushOn = true
        } else {
            self.isPushOn = false
        }
    
    }
    // MARK: UITableView Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return settingsArray.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:SettingsCell = settingsTbl.dequeueReusableCell(withIdentifier: "SettingsCell") as! SettingsCell
        
        cell.titleLbl.text = (settingsArray[indexPath.row] as! String)
        if cell.titleLbl.text ==  "Blocked Users"{
            cell.onSwitch.isHidden = true
            cell.arrowImg.isHidden = false
        } else if cell.titleLbl.text ==  "Card details"{
            cell.onSwitch.isHidden = true
            cell.arrowImg.isHidden = false
        }
        else if cell.titleLbl.text ==  "Location on/off"{
          
            cell.onSwitch.addTarget(self, action: #selector(locationOnOff(sender:)), for: .valueChanged)
            cell.onSwitch.tag = indexPath.row
            cell.onSwitch.isHidden = false
            cell.arrowImg.isHidden = true
       
            
        }
        
        else if cell.titleLbl.text ==  "Turn on/off for near by hotels,bars,restaurants etc"{
            
            cell.onSwitch.addTarget(self, action: #selector(pushOnOff(sender:)), for: .valueChanged)
            cell.onSwitch.tag = indexPath.row
            cell.onSwitch.isHidden = false
            cell.arrowImg.isHidden = true
        }
            
        else {
            cell.onSwitch.isHidden = false
            cell.arrowImg.isHidden = true
        }
        
        if indexPath.row == 0{
          
            if isOn {
                cell.onSwitch.setOn(true, animated: false)
                // 30 May :-
                cell.onSwitch.addTarget(self, action: #selector(pushOnOff(sender:)), for: .valueChanged)
                
            } else {
                cell.onSwitch.setOn(false, animated: false)
                // 30 May :-
                 cell.onSwitch.addTarget(self, action: #selector(pushOnOff(sender:)), for: .valueChanged)
                
            }
        
        }
        
        if indexPath.row == 3{
          
            if isLocationOn {
                
                cell.onSwitch.setOn(true, animated: false)
            } else {
                cell.onSwitch.setOn(false, animated: false)
            }
            
        }
        
        if indexPath.row == 4{
          
            if isPushOn {
                cell.onSwitch.setOn(true, animated: false)
            } else {
                cell.onSwitch.setOn(false, animated: false)
            }

            
        }
        cell.selectionStyle = .none
        return cell
    
    }
    
   
    
    // Mark: For switch on/off
    func pushOnOff(sender: UISwitch){
        
        if sender.tag == 0{
            var values = [String: String]()
            if sender.isOn {
                values = ["message_push": "1"]
            } else {
                values = ["message_push": "0"]
            }
            
            FIRDatabase.database().reference().child("users").child(Singleton.sharedInstance.userIdStr).child("credentials").updateChildValues(values, withCompletionBlock: { (errr, _) in
                if errr == nil {
                    
                }
            })
            
        } else {
          
            if sender.isOn{
                
                pushOnOff(pushMark:"1")
                
            }
            else{
                
                pushOnOff(pushMark:"0")
                
            }
            
        }
        
        print(sender.isOn)

    }
    
    
    // Mark: Further push on/off
    func pushOnOff(pushMark:String){
        
        let json = ["user_id":Singleton.sharedInstance.userIdStr,
                    "auth_token":Singleton.sharedInstance.userTokenStr,
                    "receive_push" :pushMark
            ] as [String : Any]
        
        print(json)
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/users/updatereceivepushsettings", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON {  response  in
                DispatchQueue.main.async {
                    
                    print(response)
                     let jsonData = response.result.value
                     print("JSON: \(String(describing: jsonData))")
                        let json = jsonData as! NSDictionary
                           print(json)
                        let description = json["description"] as! NSString
                        print(description)
                        let statusCode = json["status_code"] as! NSString
                        print(statusCode)
                       UserDefaults.standard.setValue(pushMark, forKey: "push_type")
                       UserDefaults.standard.synchronize()
                    
                    }
                
          }
        
  }
    
    
    
    // Mark: Table view
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        if indexPath.row == 1{
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "BlockedUsers") as!
            BlockedUsers
            navigationController?.pushViewController(vc, animated: true)
            
        }

        
        if indexPath.row == 2{
       
      // 24 April :-
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CardDetailsNew") as!
            CardDetailsNew
        navigationController?.pushViewController(vc, animated: true)
    }
       
  
}

    // Mark: Location
    func locationOnOff(sender: UISwitch) {
       
        
        if sender.tag == 0{
            var values = [String: String]()
            if sender.isOn {
                values = ["message_push": "1"]
                UserDefaults.standard.set("1", forKey: "locationKey")
            } else {
                values = ["message_push": "0"]
                UserDefaults.standard.set("0", forKey: "locationKey")
            }
            
            FIRDatabase.database().reference().child("users").child(Singleton.sharedInstance.userIdStr).child("credentials").updateChildValues(values, withCompletionBlock: { (errr, _) in
                if errr == nil {
                    
                }
            })

        } else {
        // location on off
            
            if sender.isOn {
                
                locationOnOff(location: "1")
                UserDefaults.standard.set("1", forKey: "locationKey")
            } else {
           
             locationOnOff(location: "0")
                UserDefaults.standard.set("0", forKey: "locationKey")
          }
   
        }
        
        print(sender.isOn)
        
    }
    
    // MARK: Location
    func locationOnOff(location : String){
        
        let json = ["user_id":Singleton.sharedInstance.userIdStr,
                    "auth_token":Singleton.sharedInstance.userTokenStr,
                    "status" : location
            ] as [String : Any]
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/members/notificationSetting", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON {  response  in
                DispatchQueue.main.async {
                    
                    print(response)
                    if let jsonData = response.result.value {
                        print("JSON: \(jsonData)")
                        let json = jsonData as! NSDictionary
                        print(json)
                        let description = json["description"] as! NSString
                        print(description)
                        let statusCode = json["status_code"] as! NSString
                        print(statusCode)
                        UserDefaults.standard.set(location, forKey: "locationKey")
                        UserDefaults.standard.synchronize()
 
        
                    }
                
                
                    
                }
                
        }
    
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
