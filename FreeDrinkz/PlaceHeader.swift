//
//  PlaceHeader.swift
//  FreeDrinkz
//
//  Created by Brst-Pc109 on 28/03/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

import UIKit
import MapKit
class PlaceHeader: UITableViewCell {

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var restaurantBackView: UIView!
    @IBOutlet weak var restaurantImg: UIImageView!

    
    @IBOutlet weak var postLbl: UILabel!
    @IBOutlet weak var detailsLbl: UILabel!
    @IBOutlet weak var membersLbl: UILabel!

    @IBOutlet weak var postBtn: UIButton!
    @IBOutlet weak var detailsBtn: UIButton!
    @IBOutlet weak var membersBtn: UIButton!
    @IBOutlet weak var sendMsgBtn: UIButton!
    @IBOutlet weak var checkInBtn: UIButton!
    
    // 23 May :-
     @IBOutlet var addFrndBtn: UIButton!
    
    @IBOutlet weak var changeTxt: UILabel!
  
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var locationLbl: UITextView!
    @IBOutlet weak var fbBtn: UIButton!
    @IBOutlet weak var twitterBtn: UIButton!

    @IBOutlet weak var routeView: UIView!
    @IBOutlet weak var routeBtn: UIButton!
    @IBOutlet weak var sendView: UIView!


    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    
    
}
