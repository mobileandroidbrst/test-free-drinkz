//
//  Register.swift
//  FreeDrinkz
//
//  Created by Brst-Pc109 on 24/03/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AVFoundation
import Photos
import Firebase
import GooglePlaces

class Register: UIViewController,UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, GMSAutocompleteViewControllerDelegate,UIPickerViewDelegate {
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    @IBOutlet weak var scrView: UIScrollView!
    @IBOutlet weak var firstNameTxt: UITextField!
    @IBOutlet weak var lastNameTxt: UITextField!
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var passwordTxt: UITextField!
    @IBOutlet weak var confirmPasswordTxt: UITextField!
    @IBOutlet weak var stateTxt: UITextField!
    @IBOutlet weak var cityTxt: UITextField!
    @IBOutlet weak var zipCodeTxt: UITextField!
    @IBOutlet weak var ageTxt: UITextField!
    @IBOutlet weak var maleTxt: IQDropDownTextField!
    @IBOutlet weak var signInBtn: UIButton!
    @IBOutlet weak var registerBtn: UIButton!
    @IBOutlet weak var choiceView: UIView!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var signInBtn2: UIButton!
    @IBOutlet weak var memberImg: UIImageView!
    @IBOutlet weak var placeImg: UIImageView!
    @IBOutlet weak var artistImg: UIImageView!
    @IBOutlet weak var memberBtn: UIButton!
    @IBOutlet weak var placeBtn: UIButton!
    @IBOutlet weak var artistBtn: UIButton!
    @IBOutlet weak var memberLbl: UILabel!
    @IBOutlet weak var placeLbl: UILabel!
    @IBOutlet weak var artistLbl: UILabel!
    @IBOutlet weak var userImg: UIImageView!
    
    @IBOutlet weak var userArtistImg: UIImageView!
    @IBOutlet weak var userPlaceImg: UIImageView!
    
    
    @IBOutlet weak var firstNameArtistTxt: UITextField!
    @IBOutlet weak var lastNameArtistTxt: UITextField!
    @IBOutlet weak var emailArtistTxt: UITextField!
    @IBOutlet weak var passwordArtistTxt: UITextField!
    @IBOutlet weak var confirmPasswordArtistTxt: UITextField!
    @IBOutlet weak var countryArtistTxt: UITextField!
    @IBOutlet weak var stateArtistTxt: UITextField!
    @IBOutlet weak var cityArtistTxt: UITextField!
    @IBOutlet weak var zipCodeArtistTxt: UITextField!
    @IBOutlet weak var ageArtistTxt: UITextField!
    @IBOutlet weak var bandNameArtistTxt: UITextField!
    @IBOutlet weak var maleArtistTxt: IQDropDownTextField!
    @IBOutlet weak var signInArtistBtn: UIButton!
    @IBOutlet weak var registerArtistBtn: UIButton!
    
    
    
    @IBOutlet weak var placeNameTxt: UITextField!
    @IBOutlet weak var emailPlaceTxt: UITextField!
    @IBOutlet weak var passwordPlaceTxt: UITextField!
    @IBOutlet weak var confirmPasswordPlaceTxt: UITextField!
    @IBOutlet weak var statePlaceTxt: UITextField!
    @IBOutlet weak var cityPlaceTxt: UITextField!
    @IBOutlet weak var zipCodePlaceTxt: UITextField!
    @IBOutlet weak var signInPlaceBtn: UIButton!
    @IBOutlet weak var registerPlaceBtn: UIButton!
    
    
    
    @IBOutlet weak var AddressTxt: UITextField!
    @IBOutlet weak var CountryTxt: UITextField!
    @IBOutlet weak var AddressArtistTxt: UITextField!
    @IBOutlet weak var AddressPlaceTxt: UITextField!
    @IBOutlet weak var CountryPlaceTxt: UITextField!

    
    @IBOutlet weak var memberView: UIView!
    @IBOutlet weak var artistView: UIView!
    @IBOutlet weak var placeView: UIView!
    @IBOutlet weak var restroPlacesView: UIView!

    
    @IBOutlet weak var hotelImg: UIImageView!
    @IBOutlet weak var barImg: UIImageView!
    @IBOutlet weak var restroImg: UIImageView!
    @IBOutlet weak var clubImg: UIImageView!
    @IBOutlet weak var resortImg: UIImageView!


    
    var uniquePhotoNameStr = String()
    var latitude = String()
    var longitude = String()
    var countryStr = String()
    var showRestroScreen = String()
    var placeType = String()
    var citiesArray = [[String:Any]]()
    var finalStateArray = [[String:Any]]()
    var finalCitiesArray = [[String:Any]]()
    var countryArray = [[String:Any]]()
    var stateArray = [[String:Any]]()
    
    private func readJson(resources: String) -> [[String:Any]] {
        var resultArray = [[String:Any]]()
        
        do {
            if let file = Bundle.main.url(forResource: resources, withExtension: "json") {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                if let object = json as? [String: Any] {
                    // json is a dictionary
                    resultArray = object[resources] as! [[String : Any]]
                    
                } else if json is [Any] {
                    // json is an array
                    
                } else {
                    print("JSON is invalid")
                }
                
                return resultArray
                
            } else {
                print("no file")
            }
        } catch {
            print(error.localizedDescription)
        }
        return resultArray
        
    }

    
    //MARK: viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.global( qos: .default).async(execute: {() -> Void in
            self.countryArray = self.readJson(resources: "countries")
            self.finalStateArray = self.readJson(resources: "states")
            self.finalCitiesArray = self.readJson(resources: "cities")
            DispatchQueue.main.sync(execute: {() -> Void in
                
            })
        })
        UserDefaults.standard.removeObject(forKey: "user_id")
        
        citiesArray = [[
            "id": "-8",
            "name": "Cities",
            "state_id": "-8"
            ]]
        
        
        stateArray = [[
            "id": "-8",
            "name": "State",
            "country_id": "-8"
            ]]
        
        
        let pickerView = UIPickerView()
        CountryTxt.inputView = pickerView
        countryArtistTxt.inputView = pickerView
        CountryPlaceTxt.inputView = pickerView
        pickerView.tag = 0
        pickerView.delegate = self
        
        
        let pickerView1 = UIPickerView()
        stateTxt.inputView = pickerView1
        statePlaceTxt.inputView = pickerView1
        stateArtistTxt.inputView = pickerView1
        pickerView1.tag = 1
        pickerView1.delegate = self
        
        let pickerView2 = UIPickerView()
        cityTxt.inputView = pickerView2
        cityPlaceTxt.inputView = pickerView2
        cityArtistTxt.inputView = pickerView2
        pickerView2.tag = 2
        pickerView2.delegate = self
        
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border.frame = CGRect(x: 0, y: firstNameTxt.frame.size.height - width, width:  firstNameTxt.frame.size.width, height: firstNameTxt.frame.size.height)
        border.borderWidth = width
        firstNameTxt.layer.addSublayer(border)
        firstNameTxt.layer.masksToBounds = true
        
        
        let border1 = CALayer()
        border1.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border1.frame = CGRect(x: 0, y: lastNameTxt.frame.size.height - width, width:  lastNameTxt.frame.size.width, height: lastNameTxt.frame.size.height)
        border1.borderWidth = width
        lastNameTxt.layer.addSublayer(border1)
        lastNameTxt.layer.masksToBounds = true
        
        
        
        let border2 = CALayer()
        border2.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border2.frame = CGRect(x: 0, y: emailTxt.frame.size.height - width, width:  emailTxt.frame.size.width, height: emailTxt.frame.size.height)
        border2.borderWidth = width
        emailTxt.layer.addSublayer(border2)
        emailTxt.layer.masksToBounds = true
        
        
        let border3 = CALayer()
        border3.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border3.frame = CGRect(x: 0, y: passwordTxt.frame.size.height - width, width:  passwordTxt.frame.size.width, height: passwordTxt.frame.size.height)
        border3.borderWidth = width
        passwordTxt.layer.addSublayer(border3)
        passwordTxt.layer.masksToBounds = true
        
        
        let border4 = CALayer()
        border4.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border4.frame = CGRect(x: 0, y: confirmPasswordTxt.frame.size.height - width, width:  confirmPasswordTxt.frame.size.width, height: confirmPasswordTxt.frame.size.height)
        border4.borderWidth = width
        confirmPasswordTxt.layer.addSublayer(border4)
        confirmPasswordTxt.layer.masksToBounds = true
        
        
        let border5 = CALayer()
        border5.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border5.frame = CGRect(x: 0, y: stateTxt.frame.size.height - width, width:  stateTxt.frame.size.width, height: stateTxt.frame.size.height)
        border5.borderWidth = width
        stateTxt.layer.addSublayer(border5)
        stateTxt.layer.masksToBounds = true
        
        
        let border6 = CALayer()
        border6.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border6.frame = CGRect(x: 0, y: cityTxt.frame.size.height - width, width:  cityTxt.frame.size.width, height: cityTxt.frame.size.height)
        border6.borderWidth = width
        cityTxt.layer.addSublayer(border6)
        cityTxt.layer.masksToBounds = true
        
        
        let border7 = CALayer()
        border7.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border7.frame = CGRect(x: 0, y: zipCodeTxt.frame.size.height - width, width:  zipCodeTxt.frame.size.width, height: zipCodeTxt.frame.size.height)
        border7.borderWidth = width
        zipCodeTxt.layer.addSublayer(border7)
        zipCodeTxt.layer.masksToBounds = true
        
        let border8 = CALayer()
        border8.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border8.frame = CGRect(x: 0, y: ageTxt.frame.size.height - width, width:  ageTxt.frame.size.width, height: ageTxt.frame.size.height)
        border8.borderWidth = width
        ageTxt.layer.addSublayer(border8)
        ageTxt.layer.masksToBounds = true
        
        let border9 = CALayer()
        border9.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border9.frame = CGRect(x: 0, y: maleTxt.frame.size.height - width, width:  maleTxt.frame.size.width, height: maleTxt.frame.size.height)
        border9.borderWidth = width
        maleTxt.layer.addSublayer(border9)
        maleTxt.layer.masksToBounds = true
        
        
        let border10 = CALayer()
        border10.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border10.frame = CGRect(x: 0, y: memberBtn.frame.size.height - width, width:  memberBtn.frame.size.width, height: memberBtn.frame.size.height)
        border10.borderWidth = width
        memberBtn.layer.addSublayer(border10)
        memberBtn.layer.masksToBounds = true
        
        
        let border11 = CALayer()
        border11.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border11.frame = CGRect(x: 0, y: placeBtn.frame.size.height - width, width:  placeBtn.frame.size.width, height: placeBtn.frame.size.height)
        border11.borderWidth = width
        placeBtn.layer.addSublayer(border11)
        placeBtn.layer.masksToBounds = true
        
        let border12 = CALayer()
        border12.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border12.frame = CGRect(x: 0, y: artistBtn.frame.size.height - width, width:  artistBtn.frame.size.width, height: artistBtn.frame.size.height)
        border12.borderWidth = width
        artistBtn.layer.addSublayer(border12)
        artistBtn.layer.masksToBounds = true
        
        
        
        let border13 = CALayer()
        border13.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border13.frame = CGRect(x: 0, y: firstNameArtistTxt.frame.size.height - width, width:  firstNameArtistTxt.frame.size.width, height: firstNameArtistTxt.frame.size.height)
        border13.borderWidth = width
        firstNameArtistTxt.layer.addSublayer(border13)
        firstNameArtistTxt.layer.masksToBounds = true
        
        
        let border14 = CALayer()
        border14.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border14.frame = CGRect(x: 0, y: lastNameArtistTxt.frame.size.height - width, width:  lastNameArtistTxt.frame.size.width, height: lastNameArtistTxt.frame.size.height)
        border14.borderWidth = width
        lastNameArtistTxt.layer.addSublayer(border14)
        lastNameArtistTxt.layer.masksToBounds = true
        
        
        
        let border15 = CALayer()
        border15.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border15.frame = CGRect(x: 0, y: emailArtistTxt.frame.size.height - width, width:  emailArtistTxt.frame.size.width, height: emailArtistTxt.frame.size.height)
        border15.borderWidth = width
        emailArtistTxt.layer.addSublayer(border15)
        emailArtistTxt.layer.masksToBounds = true
        
        
        let border16 = CALayer()
        border16.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border16.frame = CGRect(x: 0, y: passwordArtistTxt.frame.size.height - width, width:  passwordArtistTxt.frame.size.width, height: passwordArtistTxt.frame.size.height)
        border16.borderWidth = width
        passwordArtistTxt.layer.addSublayer(border16)
        passwordArtistTxt.layer.masksToBounds = true
        
        
        let border17 = CALayer()
        border17.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border17.frame = CGRect(x: 0, y: confirmPasswordArtistTxt.frame.size.height - width, width:  confirmPasswordArtistTxt.frame.size.width, height: confirmPasswordTxt.frame.size.height)
        border17.borderWidth = width
        confirmPasswordArtistTxt.layer.addSublayer(border17)
        confirmPasswordArtistTxt.layer.masksToBounds = true
        
        
        let border18 = CALayer()
        border18.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border18.frame = CGRect(x: 0, y: stateArtistTxt.frame.size.height - width, width:  stateArtistTxt.frame.size.width, height: stateArtistTxt.frame.size.height)
        border18.borderWidth = width
        stateArtistTxt.layer.addSublayer(border18)
        stateArtistTxt.layer.masksToBounds = true
        
        
        let border19 = CALayer()
        border19.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border19.frame = CGRect(x: 0, y: cityArtistTxt.frame.size.height - width, width:  cityArtistTxt.frame.size.width, height: cityArtistTxt.frame.size.height)
        border19.borderWidth = width
        cityArtistTxt.layer.addSublayer(border19)
        cityArtistTxt.layer.masksToBounds = true
        
        
        let border20 = CALayer()
        border20.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border20.frame = CGRect(x: 0, y: zipCodeArtistTxt.frame.size.height - width, width:  zipCodeArtistTxt.frame.size.width, height: zipCodeArtistTxt.frame.size.height)
        border20.borderWidth = width
        zipCodeArtistTxt.layer.addSublayer(border20)
        zipCodeArtistTxt.layer.masksToBounds = true
        
        let border21 = CALayer()
        border21.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border21.frame = CGRect(x: 0, y: ageArtistTxt.frame.size.height - width, width:  ageArtistTxt.frame.size.width, height: ageArtistTxt.frame.size.height)
        border21.borderWidth = width
        ageArtistTxt.layer.addSublayer(border21)
        ageArtistTxt.layer.masksToBounds = true
        
        let border22 = CALayer()
        border22.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border22.frame = CGRect(x: 0, y: maleArtistTxt.frame.size.height - width, width:  maleArtistTxt.frame.size.width, height: maleArtistTxt.frame.size.height)
        border22.borderWidth = width
        maleArtistTxt.layer.addSublayer(border22)
        maleArtistTxt.layer.masksToBounds = true
        
        let border23 = CALayer()
        border23.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border23.frame = CGRect(x: 0, y: bandNameArtistTxt.frame.size.height - width, width:  bandNameArtistTxt.frame.size.width, height: bandNameArtistTxt.frame.size.height)
        border23.borderWidth = width
        bandNameArtistTxt.layer.addSublayer(border23)
        bandNameArtistTxt.layer.masksToBounds = true
        
        
        let border24 = CALayer()
        border24.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border24.frame = CGRect(x: 0, y: placeNameTxt.frame.size.height - width, width:  placeNameTxt.frame.size.width, height: placeNameTxt.frame.size.height)
        border24.borderWidth = width
        placeNameTxt.layer.addSublayer(border24)
        placeNameTxt.layer.masksToBounds = true
        
        
        let border25 = CALayer()
        border25.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border25.frame = CGRect(x: 0, y: emailPlaceTxt.frame.size.height - width, width:  emailPlaceTxt.frame.size.width, height: emailPlaceTxt.frame.size.height)
        border25.borderWidth = width
        emailPlaceTxt.layer.addSublayer(border25)
        emailPlaceTxt.layer.masksToBounds = true
        
        
        
        let border26 = CALayer()
        border26.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border26.frame = CGRect(x: 0, y: passwordPlaceTxt.frame.size.height - width, width:  passwordPlaceTxt.frame.size.width, height: passwordPlaceTxt.frame.size.height)
        border26.borderWidth = width
        passwordPlaceTxt.layer.addSublayer(border26)
        passwordPlaceTxt.layer.masksToBounds = true
        
        
        let border27 = CALayer()
        border27.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border27.frame = CGRect(x: 0, y: confirmPasswordPlaceTxt.frame.size.height - width, width:  confirmPasswordPlaceTxt.frame.size.width, height: confirmPasswordPlaceTxt.frame.size.height)
        border27.borderWidth = width
        confirmPasswordPlaceTxt.layer.addSublayer(border27)
        confirmPasswordPlaceTxt.layer.masksToBounds = true
        
        
        let border28 = CALayer()
        border28.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border28.frame = CGRect(x: 0, y: statePlaceTxt.frame.size.height - width, width:  statePlaceTxt.frame.size.width, height: statePlaceTxt.frame.size.height)
        border28.borderWidth = width
        statePlaceTxt.layer.addSublayer(border28)
        statePlaceTxt.layer.masksToBounds = true
        
        
        let border29 = CALayer()
        border29.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border29.frame = CGRect(x: 0, y: cityPlaceTxt.frame.size.height - width, width:  cityPlaceTxt.frame.size.width, height: cityPlaceTxt.frame.size.height)
        border29.borderWidth = width
        cityPlaceTxt.layer.addSublayer(border29)
        cityPlaceTxt.layer.masksToBounds = true
        
        
        let border30 = CALayer()
        border30.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border30.frame = CGRect(x: 0, y: zipCodePlaceTxt.frame.size.height - width, width:  zipCodePlaceTxt.frame.size.width, height: zipCodePlaceTxt.frame.size.height)
        border30.borderWidth = width
        zipCodePlaceTxt.layer.addSublayer(border30)
        zipCodePlaceTxt.layer.masksToBounds = true
        
        
        
        let border31 = CALayer()
        border31.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border31.frame = CGRect(x: 0, y: AddressTxt.frame.size.height - width, width:  AddressTxt.frame.size.width, height: AddressTxt.frame.size.height)
        border31.borderWidth = width
        AddressTxt.layer.addSublayer(border31)
        AddressTxt.layer.masksToBounds = true
        
        
        let border32 = CALayer()
        border32.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border32.frame = CGRect(x: 0, y: AddressPlaceTxt.frame.size.height - width, width:  AddressPlaceTxt.frame.size.width, height: AddressPlaceTxt.frame.size.height)
        border32.borderWidth = width
        AddressPlaceTxt.layer.addSublayer(border32)
        AddressPlaceTxt.layer.masksToBounds = true
        
        
        let border33 = CALayer()
        border33.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border33.frame = CGRect(x: 0, y: AddressArtistTxt.frame.size.height - width, width:  AddressArtistTxt.frame.size.width, height: AddressArtistTxt.frame.size.height)
        border33.borderWidth = width
        AddressArtistTxt.layer.addSublayer(border33)
        AddressArtistTxt.layer.masksToBounds = true
        
        let border34 = CALayer()
        border34.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border34.frame = CGRect(x: 0, y: CountryTxt.frame.size.height - width, width:  CountryTxt.frame.size.width, height: CountryTxt.frame.size.height)
        border34.borderWidth = width
        CountryTxt.layer.addSublayer(border34)
        CountryTxt.layer.masksToBounds = true
        
        
        let border35 = CALayer()
        border35.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border35.frame = CGRect(x: 0, y: CountryPlaceTxt.frame.size.height - width, width:  CountryPlaceTxt.frame.size.width, height: CountryPlaceTxt.frame.size.height)
        border35.borderWidth = width
        CountryPlaceTxt.layer.addSublayer(border35)
        CountryPlaceTxt.layer.masksToBounds = true
        

        let border36 = CALayer()
        border36.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border36.frame = CGRect(x: 0, y: countryArtistTxt.frame.size.height - width, width:  countryArtistTxt.frame.size.width, height: countryArtistTxt.frame.size.height)
        border36.borderWidth = width
        countryArtistTxt.layer.addSublayer(border36)
        countryArtistTxt.layer.masksToBounds = true
        
        let fontSize: CGFloat = 16.0
        let fontFamily: String = "FuturaBT-LightItalic"
        let attrString = NSMutableAttributedString(attributedString: signInBtn.currentAttributedTitle!)
        attrString.addAttribute(NSFontAttributeName, value: UIFont(name: fontFamily, size: fontSize)!, range: NSMakeRange(0, attrString.length))
        signInBtn.setAttributedTitle(attrString, for: .normal)
        signInBtn2.setAttributedTitle(attrString, for: .normal)
        signInArtistBtn.setAttributedTitle(attrString, for: .normal)
        signInPlaceBtn.setAttributedTitle(attrString, for: .normal)
        
        
        if (UIScreen.main.bounds.size.height==568 || UIScreen.main.bounds.size.height==480) {
            scrView.contentSize = CGSize(width: 320,height: 650)
        } else if (UIScreen.main.bounds.size.height==667) {
            scrView.contentSize = CGSize(width: 320,height: 650)
        }
        
        
        maleTxt.keyboardDistanceFromTextField = 150;
        var itemLists = [NSString]()
        itemLists.append("Male")
        itemLists.append("Female")
        maleTxt.itemList = itemLists
        maleArtistTxt.keyboardDistanceFromTextField = 150;
        maleArtistTxt.itemList = itemLists
        
        
        
        let panGesture = UIPanGestureRecognizer(target: self, action:(#selector(handlePanGesture(panGesture:))))
        self.view.addGestureRecognizer(panGesture)
        
        
        placeView.isHidden = true
        artistView.isHidden = true
        memberView.isHidden = false
        restroPlacesView.isHidden = true
        Singleton.sharedInstance.userTypeStr="member"
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.red
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: Selector(("donePicker")))
     
        
        toolBar.setItems([doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        CountryTxt.inputAccessoryView = toolBar
        countryArtistTxt.inputAccessoryView = toolBar
        CountryPlaceTxt.inputAccessoryView = toolBar
        stateTxt.inputAccessoryView = toolBar
        stateArtistTxt.inputAccessoryView = toolBar
        statePlaceTxt.inputAccessoryView = toolBar
        cityTxt.inputAccessoryView = toolBar
        cityArtistTxt.inputAccessoryView = toolBar
        cityPlaceTxt.inputAccessoryView = toolBar
        
    }
    
   func donePicker(){
        
     CountryTxt.resignFirstResponder()
     countryArtistTxt.resignFirstResponder()
     CountryPlaceTxt.resignFirstResponder()
     stateTxt.resignFirstResponder()
     stateArtistTxt.resignFirstResponder()
     statePlaceTxt.resignFirstResponder()
     cityTxt.resignFirstResponder()
     cityArtistTxt.resignFirstResponder()
     cityPlaceTxt.resignFirstResponder()
    
    
    }
    func handlePanGesture(panGesture: UIPanGestureRecognizer) {
        // get translation
        if panGesture.state == .began || panGesture.state == .changed {
        }
    }
    
    //MARK: CheckCameraAuthorization
    
    func checkCameraAuthorization() {
        
        let status: AVAuthorizationStatus = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo)
        if status == .authorized {
            // authorized
            let picker = UIImagePickerController()
            picker.delegate = self
            picker.allowsEditing = true
            picker.sourceType = .camera
            present(picker, animated: true, completion: { _ in })
        } else if status == .denied {
            // denied
            if AVCaptureDevice.responds(to: #selector(AVCaptureDevice.requestAccess(forMediaType:completionHandler:))) {
                AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: {(_ granted: Bool) -> Void in
                    // Will get here on both iOS 7 & 8 even though camera permissions weren't required
                    // until iOS 8. So for iOS 7 permission will always be granted.
                    print("DENIED")
                    if granted {
                        // Permission has been granted. Use dispatch_async for any UI updating
                        // code because this block may be executed in a thread.
                        
                        DispatchQueue.main.async(execute: {() -> Void in
                            let picker = UIImagePickerController()
                            picker.delegate = self
                            picker.allowsEditing = true
                            picker.sourceType = .camera
                            self.present(picker, animated: true, completion: { _ in })
                        })
                        
                    }
                    else {
                        // Permission has been denied.
                        
                        
                        DispatchQueue.main.async(execute: {() -> Void in
                            self.accessMethod("Please go to Settings and enable the camera for this app to use this feature.")
                        })
                        
                        
                    }
                })
            }
        } else if status == .restricted {
            // restricted
            DispatchQueue.main.async(execute: {() -> Void in
                self.accessMethod("Please go to Settings and enable the camera for this app to use this feature.")
            })
        }
        else if status == .notDetermined {
            // not determined
            AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: {(_ granted: Bool) -> Void in
                if granted {
                    // Access has been granted ..do something
                    DispatchQueue.main.async(execute: {() -> Void in
                        let picker = UIImagePickerController()
                        picker.delegate = self
                        picker.allowsEditing = true
                        picker.sourceType = .camera
                        self.present(picker, animated: true, completion: { _ in })
                        
                    })
                    
                    
                }
                else {
                    // Access denied ..do something
                    DispatchQueue.main.async(execute: {() -> Void in
                        self.accessMethod("Please go to Settings and enable the camera for this app to use this feature.")
                    })
                    
                }
            })
        }
    }
    
    //MARK: CheckPhotoAuthorization
    
    func checkPhotoAuthorization()  {
        let status: PHAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
        if status == .authorized {
            let picker = UIImagePickerController()
            picker.delegate = self
            picker.allowsEditing = true
            picker.sourceType = .photoLibrary
            present(picker, animated: true, completion: { _ in })
        }
        else if status == .denied {
            DispatchQueue.main.async(execute: {() -> Void in
                self.accessMethod("Please go to Settings and enable the photos for this app to use this feature.")
            })
        } else if status == .notDetermined {
            // Access has not been determined.
            PHPhotoLibrary.requestAuthorization({(_ status: PHAuthorizationStatus) -> Void in
                if status == .authorized {
                    
                    DispatchQueue.main.async(execute: {() -> Void in
                        let picker = UIImagePickerController()
                        picker.delegate = self
                        picker.allowsEditing = true
                        picker.sourceType = .photoLibrary
                        self.present(picker, animated: true, completion: { _ in })
                    })
                    
                }
                else {
                    DispatchQueue.main.async(execute: {() -> Void in
                        self.accessMethod("Please go to Settings and enable the photos for this app to use this feature.")
                    })
                }
            })
        }
        else if status == .restricted {
            // Restricted access - normally won't happen.
        }
        
        
    }
    
    func accessMethod(_ message: String) {
        let alertController = UIAlertController(title: "Not Authorized", message: message, preferredStyle: .alert)
        let cancel = UIAlertAction(title: "OK", style: .default, handler: nil)
        let Settings = UIAlertAction(title: "Settings", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
        })
        alertController.addAction(Settings)
        alertController.addAction(cancel)
        present(alertController, animated: true, completion: { _ in })
    }
    
    //MARK: UIImagePickerControllerDelegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        var chosenImage: UIImage? = info[UIImagePickerControllerEditedImage] as! UIImage?
        chosenImage = resizeImage(image: chosenImage!, newWidth: 400)
        
        
        picker.dismiss(animated: true, completion: { _ in })
        
        if (Singleton.sharedInstance.userTypeStr == "member") {
            userImg.image = chosenImage
            userImg.layer.cornerRadius = userImg.frame.size.width / 2
            userImg.clipsToBounds = true
        } else if (Singleton.sharedInstance.userTypeStr == "artist") {
            userArtistImg.image = chosenImage
            userArtistImg.layer.cornerRadius = userArtistImg.frame.size.width / 2
            userArtistImg.clipsToBounds = true
        } else if (Singleton.sharedInstance.userTypeStr == "place") {
            userPlaceImg.image = chosenImage
            userPlaceImg.layer.cornerRadius = userPlaceImg.frame.size.width / 2
            userPlaceImg.clipsToBounds = true
        }
    }
    
    //MARK: Resize Image
    
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0,y: 0,width: newWidth,height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    
    // MARK: Button Actions
    @IBAction func Photo(_ sender: Any) {
        
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let camera = UIAlertAction(title: "Camera", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            self.checkCameraAuthorization()
        })
        let gallery = UIAlertAction(title: "Gallery", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            self.checkPhotoAuthorization()
        })
        alertController.addAction(gallery)
        alertController.addAction(camera)
        alertController.addAction(cancel)
        present(alertController, animated: true, completion: { _ in })
        
        
    }
    @IBAction func Next(_ sender: Any) {
        
        
        if showRestroScreen == "ShowRestroScreen"{
            
            restroPlacesView.isHidden = false
        }
        
        else{
            
         choiceView.isHidden=true
            
        }
       
        
    }
    
    @IBAction func NextRestro(_ sender: Any){
        
         choiceView.isHidden=true
         restroPlacesView.isHidden = true
        
    }
    
    
    
    
    @IBAction func Choice(_ sender: Any) {
        placeView.isHidden = true
        artistView.isHidden = true
        memberView.isHidden = true
        
        if ((sender as AnyObject).tag == 0) {
            memberImg.image = UIImage(named: "red")
            placeImg.image = UIImage(named: "gray")
            artistImg.image = UIImage(named: "gray")
            Singleton.sharedInstance.userTypeStr="member"
            
            memberLbl.textColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1)
            placeLbl.textColor = UIColor(red: 128/255, green: 128/255, blue: 128/255, alpha: 1)
            artistLbl.textColor = UIColor(red: 128/255, green: 128/255, blue: 128/255, alpha: 1)
            
            
            memberView.isHidden = false
            
        } else if ((sender as AnyObject).tag == 1) {
            
            
            
            placeImg.image = UIImage(named: "red")
            memberImg.image = UIImage(named: "gray")
            artistImg.image = UIImage(named: "gray")
            Singleton.sharedInstance.userTypeStr="place"
            placeView.isHidden = false
            
            placeLbl.textColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1)
            memberLbl.textColor = UIColor(red: 128/255, green: 128/255, blue: 128/255, alpha: 1)
            artistLbl.textColor = UIColor(red: 128/255, green: 128/255, blue: 128/255, alpha: 1)
            showRestroScreen = "ShowRestroScreen"
            
            
            
        } else if ((sender as AnyObject).tag == 2) {
            artistImg.image = UIImage(named: "red")
            placeImg.image = UIImage(named: "gray")
            memberImg.image = UIImage(named: "gray")
            Singleton.sharedInstance.userTypeStr="artist"
            artistView.isHidden = false
            
            artistLbl.textColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1)
            memberLbl.textColor = UIColor(red: 128/255, green: 128/255, blue: 128/255, alpha: 1)
            placeLbl.textColor = UIColor(red: 128/255, green: 128/255, blue: 128/255, alpha: 1)
            
        }
        
    }
    
     @IBAction func chooseAnyRestro(_ sender: Any){
        
       
        if ((sender as AnyObject).tag == 0){
        
            hotelImg.image = UIImage(named: "red")
            barImg.image = UIImage(named: "gray")
            restroImg.image = UIImage(named: "gray")
            clubImg.image = UIImage(named: "gray")
            resortImg.image = UIImage(named: "gray")
             Singleton.sharedInstance.userTypeStr="place"
            self.placeType = "placeHotel"
            
              placeView.isHidden = false


            
            
        }
        else if ((sender as AnyObject).tag == 1){
            
          
            hotelImg.image = UIImage(named: "gray")
            barImg.image = UIImage(named: "red")
            restroImg.image = UIImage(named: "gray")
            clubImg.image = UIImage(named: "gray")
            resortImg.image = UIImage(named: "gray")
              Singleton.sharedInstance.userTypeStr="place"
            placeView.isHidden = false
             self.placeType = "placeBar"

            
            
            
        }
        else if ((sender as AnyObject).tag == 2){
            
            hotelImg.image = UIImage(named: "gray")
            barImg.image = UIImage(named: "gray")
            restroImg.image = UIImage(named: "red")
            clubImg.image = UIImage(named: "gray")
            resortImg.image = UIImage(named: "gray")
              Singleton.sharedInstance.userTypeStr="place"
            placeView.isHidden = false
            self.placeType = "placeRestro"

            
            
        }
        else if ((sender as AnyObject).tag == 3){
            
            hotelImg.image = UIImage(named: "gray")
            barImg.image = UIImage(named: "gray")
            restroImg.image = UIImage(named: "gray")
            clubImg.image = UIImage(named: "red")
            resortImg.image = UIImage(named: "gray")
              Singleton.sharedInstance.userTypeStr="place"
            placeView.isHidden = false
            self.placeType = "placeClub"

            
            
        }
        else if ((sender as AnyObject).tag == 4){
           
            hotelImg.image = UIImage(named: "gray")
            barImg.image = UIImage(named: "gray")
            restroImg.image = UIImage(named: "gray")
            clubImg.image = UIImage(named: "gray")
            resortImg.image = UIImage(named: "red")
              Singleton.sharedInstance.userTypeStr="place"
            placeView.isHidden = false
            self.placeType = "placeResort"

            
        }
        
    }
    
    
    
    
    @IBAction func Register(_ sender: Any) {
        
        Singleton.sharedInstance.socialLoginType = "1"
        UserDefaults.standard.setValue(Singleton.sharedInstance.socialLoginType, forKey: "socialLoginType")
        
        if (Singleton.sharedInstance.userTypeStr == "member") {
            if emailTxt.text == "" || firstNameTxt.text == "" || lastNameTxt.text == "" || passwordTxt.text == "" || confirmPasswordTxt.text == "" || maleTxt.text == "" || stateTxt.text == "" || cityTxt.text == "" || ageTxt.text == "" || zipCodeTxt.text == ""  || AddressTxt.text == ""{
                alertViewMethod(titleStr: "", messageStr: "Please enter all fields.")
            }else if !isValidEmail(testStr: emailTxt.text!) {
                alertViewMethod(titleStr: "", messageStr: "Please enter the valid email.")
            } else if (passwordTxt.text != confirmPasswordTxt.text) {
                alertViewMethod(titleStr: "", messageStr: "Password does not match with confirm password.")
            } else {
                if case 16 ... 100 = Int(ageTxt.text!)! {
                    indicator.startAnimating()
                    indicator.isHidden = false
                    uploadImageDataMethod()
                } else {
                    alertViewMethod(titleStr: "", messageStr: "User's age limit should be between 16 to 100 years.")
                }
            }
            
            
            
        } else if (Singleton.sharedInstance.userTypeStr == "artist") {
            
            if emailArtistTxt.text == "" || firstNameArtistTxt.text == "" || lastNameArtistTxt.text == "" || passwordArtistTxt.text == "" || confirmPasswordArtistTxt.text == "" || maleArtistTxt.text == "" || stateArtistTxt.text == "" || cityArtistTxt.text == "" || ageArtistTxt.text == "" || zipCodeArtistTxt.text == ""  || bandNameArtistTxt.text == ""  || AddressArtistTxt.text == ""{
                alertViewMethod(titleStr: "", messageStr: "Please enter all fields.")
            }else if !isValidEmail(testStr: emailArtistTxt.text!) {
                alertViewMethod(titleStr: "", messageStr: "Please enter the valid email.")
            } else if (passwordArtistTxt.text != confirmPasswordArtistTxt.text) {
                alertViewMethod(titleStr: "", messageStr: "Password does not match with confirm password.")
            } else {
                if case 16 ... 100 = Int(ageArtistTxt.text!)! {
                    indicator.startAnimating()
                    indicator.isHidden = false
                    uploadImageDataMethod()
                } else {
                    alertViewMethod(titleStr: "", messageStr: "User's age limit should be between 16 to 100 years.")
                }
            }
            
        } else if (Singleton.sharedInstance.userTypeStr == "place") {
            
            if emailPlaceTxt.text == "" || placeNameTxt.text == "" || passwordPlaceTxt.text == "" || confirmPasswordPlaceTxt.text == "" || statePlaceTxt.text == "" || cityPlaceTxt.text == "" || zipCodePlaceTxt.text == ""  || AddressPlaceTxt.text == ""{
                alertViewMethod(titleStr: "", messageStr: "Please enter all fields.")
            }else if !isValidEmail(testStr: emailPlaceTxt.text!) {
                alertViewMethod(titleStr: "", messageStr: "Please enter the valid email.")
            } else if (passwordPlaceTxt.text != confirmPasswordPlaceTxt.text) {
                alertViewMethod(titleStr: "", messageStr: "Password does not match with confirm password.")
            } else {
                indicator.startAnimating()
                indicator.isHidden = false
                uploadImageDataMethod()
            }
            
            
            
        }
        
        
    }
    @IBAction func Back(_ sender: Any) {
        if ((sender as AnyObject).tag == 1) {
            
             restroPlacesView.isHidden = false
              choiceView.isHidden = false
            
        }
      else {
            _ = navigationController?.popViewController(animated: true)
        }
    }
    
    
    
    
    // MARK: Registration Method
    
    // Upload Photo
    func uploadImageDataMethod()  {
        
        
        uniquePhotoNameStr = UUID().uuidString
        uniquePhotoNameStr = "http://beta.brstdev.com/freedrinkz/uploads/\(uniquePhotoNameStr).jpeg"
        print(uniquePhotoNameStr)
        
        
        let parameters = [
            "profile_pic": uniquePhotoNameStr,
            "image_type": "1",
            "is_register": "0"
        ]
        
        
        
        var userImg = UIImage()
        
        if (Singleton.sharedInstance.userTypeStr == "member") {
            userImg = self.userImg.image!
        } else if (Singleton.sharedInstance.userTypeStr == "artist") {
            userImg = self.userArtistImg.image!
        } else if (Singleton.sharedInstance.userTypeStr == "place") {
            userImg = self.userPlaceImg.image!
        }
        
        
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(UIImagePNGRepresentation(userImg)!, withName: "profile_pic", fileName: self.uniquePhotoNameStr, mimeType: "image/jpeg")
            for (key, value) in parameters {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
        }, to:"http://beta.brstdev.com/freedrinkz/api/web/v1/users/getprofilepic")
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    //Print progress
                    //  print(progress)
                    
                })
                
                upload.responseJSON { response in
                    //print response.result
                    print(response)
                    
                    if response.result.value != nil {
                        
                        let json = JSON(data: response.data! )
                        if let descriptionStr = json["description"].string {
                            print(descriptionStr)
                            if descriptionStr == "Image uploads Successfully" {
                                
                                let image = json["data"]["profile_pic_url"].string
                                self.uniquePhotoNameStr = image!
                                print(self.uniquePhotoNameStr)
                                
                            }
                        }
                        
                    }
                    
                    self.registrationMethod()
                    
                }
                
            case .failure(let encodingError):
                print(encodingError.localizedDescription)
                self.registrationMethod()
                break
                //print encodingError.description
                
            }
        }
    }
    
    // MARK: GeoCodeAddressString
    
    func getLatLongFromAddress()  {
        var address = ""
        
        if (Singleton.sharedInstance.userTypeStr == "member") {
            address = AddressTxt.text!
        } else if (Singleton.sharedInstance.userTypeStr == "artist") {
            address = AddressArtistTxt.text!
        } else if (Singleton.sharedInstance.userTypeStr == "place") {
            address = AddressPlaceTxt.text!
        }
        
        let geocoder = CLGeocoder()
        geocoder.geocodeAddressString(address) { (placemarks, error) in
            if let validPlacemark = placemarks?[0] {
                print(validPlacemark)
                print(validPlacemark.location!.coordinate.latitude)
                print(validPlacemark.location!.coordinate.longitude)
                self.latitude = String(describing:validPlacemark.location!.coordinate.latitude)
                self.longitude = String(describing:validPlacemark.location!.coordinate.longitude)
            } else {
                self.latitude = "0.0"
                self.longitude = "0.0"
            }
            self.registrationMethod()
        }
    }
    
    
    func registrationMethod() {
        
        var json = [String : Any]()
        
        if (Singleton.sharedInstance.userTypeStr == "member") {
            
            json = ["username":"\(firstNameTxt.text!) \(lastNameTxt.text!)",
                "email":emailTxt.text!,
                "firstname":firstNameTxt.text!,
                "lastname":lastNameTxt.text!,
                "user_type":"1",
                "gender":maleTxt.text!,
                "state":stateTxt.text!,
                "city":cityTxt.text!,
                "age":ageTxt.text!,
                "password":passwordTxt.text!,
                "zip_code":zipCodeTxt.text!,
                "login_type":"4",
                "profile_pic": uniquePhotoNameStr,
                "resturant_name": " ",
                "band_name": " ",
                "address": AddressTxt.text!,
                "latitude": latitude,
                "longitude": longitude,
                "device_token": Singleton.sharedInstance.deviceTokenStr,
                "device_type":"iphone",
                "country": CountryTxt.text!
                
                ] as [String : Any]
            
            
        } else if (Singleton.sharedInstance.userTypeStr == "artist") {
            
            json = ["username":"\(firstNameArtistTxt.text!) \(lastNameArtistTxt.text!)",
                "email":emailArtistTxt.text!,
                "firstname":firstNameArtistTxt.text!,
                "lastname":lastNameArtistTxt.text!,
                "user_type":"3",
                "gender":maleArtistTxt.text!,
                "state":stateArtistTxt.text!,
                "city":cityArtistTxt.text!,
                "age":ageArtistTxt.text!,
                "password":passwordArtistTxt.text!,
                "zip_code":zipCodeArtistTxt.text!,
                "login_type":"4",
                "profile_pic": uniquePhotoNameStr,
                "resturant_name": " ",
                "band_name": bandNameArtistTxt.text!,
                "address": AddressArtistTxt.text!,
                "latitude": latitude,
                "longitude": longitude,
                "device_token": Singleton.sharedInstance.deviceTokenStr,
                "device_type":"iphone",
                "country": countryArtistTxt.text!

                
                ] as [String : Any]
            
            
        } else if (Singleton.sharedInstance.userTypeStr == "place") {
            
            var userType = String()
            
            if self.placeType == "placeHotel"{
                
                userType = "2"
                
            }
            else if self.placeType == "placeBar"{
                
                userType = "5"
                
            }
            else if self.placeType == "placeRestro"{
             
              userType = "4"
                
            }
            
            else if self.placeType == "placeClub"{
                
              userType = "6"
                
            }
            else if self.placeType == "placeResort"{
                
              userType = "7"
            }
            json = [
                "email":emailPlaceTxt.text!,
                "firstname":" ",
                "lastname":" ",
                "user_type":userType,
                "gender":" ",
                "state":statePlaceTxt.text!,
                 "city":cityPlaceTxt.text!,
                "age":"16",
                "password":passwordPlaceTxt.text!,
                "zip_code":zipCodePlaceTxt.text!,
                "login_type":"4",
                "profile_pic": uniquePhotoNameStr,
                "resturant_name": placeNameTxt.text!,
                "band_name": " ",
                "address": AddressPlaceTxt.text!,
                "latitude": latitude,
                "longitude": longitude,
                "device_token": Singleton.sharedInstance.deviceTokenStr,
                "device_type":"iphone",
                "country": CountryPlaceTxt.text!

                
                ] as [String : Any]
        }
        
        
        print(json)
        
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/users/register", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    let json = JSON(data: response.data! )
                    if let descriptionStr = json["description"].string {
                        print(descriptionStr)
                        if descriptionStr == "User already exists!" {
                            self.alertViewMethod(titleStr: "", messageStr: "The email address is already in use by another account.")
                        } else if descriptionStr == "User registered successfully!" {
                            
                            let userId = String(describing: json["data"]["user_id"].int!)
                            Singleton.sharedInstance.userIdStr = userId
                            
                            
                            
                            let auth_token = json["data"]["auth_token"].string
                            Singleton.sharedInstance.userTokenStr = auth_token!
                            
                            let userType = json["data"]["usertype"].string
                            print(userType!)
                            Singleton.sharedInstance.userTypeIdStr = userType!
                            
                            
                            if json["data"]["latitude"].string != nil {
                                Singleton.sharedInstance.latitudeStr =  String(describing: json["data"]["latitude"].string!)
                            } else {
                                Singleton.sharedInstance.latitudeStr =  "0.0"
                            }
                            
                            if json["data"]["longitude"].string != nil {
                                Singleton.sharedInstance.longitudeStr =  String(describing: json["data"]["longitude"].string!)
                            } else {
                                Singleton.sharedInstance.longitudeStr =  "0.0"
                            }
                            UserDefaults.standard.setValue(Singleton.sharedInstance.latitudeStr, forKey: "latitude")
                            UserDefaults.standard.setValue(Singleton.sharedInstance.longitudeStr, forKey: "longitude")

                            
                            UserDefaults.standard.setValue(auth_token!, forKey: "auth_token")
                            UserDefaults.standard.setValue(userId, forKey: "user_id")
                            UserDefaults.standard.setValue(userType!, forKey: "user_type")
                            
                            var token = String()
                            if FIRInstanceID.instanceID().token() != nil {
                                token = FIRInstanceID.instanceID().token()!
                            } else {
                                token = ""
                            }

                            
                            if userType == "1" {
                                print("member")
                                self.getOnlineMethod()
                                
                                let values = ["name": "\(self.firstNameTxt.text!) \(self.lastNameTxt.text!)" as String, "userid": Singleton.sharedInstance.userIdStr as String, "fcm_token": token, "profile_pic_url": self.uniquePhotoNameStr] as [String : Any]
                                FIRDatabase.database().reference().child("users").child(Singleton.sharedInstance.userIdStr).child("credentials").updateChildValues(values, withCompletionBlock: { (errr, _) in
                                    if errr == nil {
                                        
                                    }
                                })

                                
                                DataManager.isCallRestroWebservice = true  //prabhjot

                                Singleton.sharedInstance.userTypeStr = "member"
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let vc = storyboard.instantiateViewController(withIdentifier: "Search") as! Search
                                self.navigationController?.pushViewController(vc,animated: true)
                                
                            } else if userType == "2" {
                                print("place")
                                
                                
                                let values = ["name": self.placeNameTxt.text! as String, "userid": Singleton.sharedInstance.userIdStr as String, "fcm_token": token, "profile_pic_url": self.uniquePhotoNameStr] as [String : Any]
                                FIRDatabase.database().reference().child("users").child(Singleton.sharedInstance.userIdStr).child("credentials").updateChildValues(values, withCompletionBlock: { (errr, _) in
                                    if errr == nil {
                                        
                                    }
                                })

                                DataManager.isCallRestroWebservice = true  //prabhjot

                                Singleton.sharedInstance.userTypeStr = "place"
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let vc = storyboard.instantiateViewController(withIdentifier: "Search") as! Search
                                self.navigationController?.pushViewController(vc,animated: true)
                                
                                
                            }else if userType == "3" {
                                print("artist")
                                Singleton.sharedInstance.userTypeStr = "artist"
                                
                                
                                let values = ["name": "\(self.firstNameArtistTxt.text!) \(self.lastNameArtistTxt.text!)" as String, "userid": Singleton.sharedInstance.userIdStr as String, "fcm_token": token, "profile_pic_url": self.uniquePhotoNameStr] as [String : Any]
                                FIRDatabase.database().reference().child("users").child(Singleton.sharedInstance.userIdStr).child("credentials").updateChildValues(values, withCompletionBlock: { (errr, _) in
                                    if errr == nil {
                                        
                                    }
                                })

                                
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let vc = storyboard.instantiateViewController(withIdentifier: "ArtistProfile") as! ArtistProfile
                                self.navigationController?.pushViewController(vc,animated: true)
                                
                            }
                            
                            
                            
                            
                        }
                        //Now you got your value
                    }
                } else if((response.error) != nil){
                    
                    self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                    
                }
                
                self.indicator.stopAnimating()
                self.indicator.isHidden = true
                
        }
        
    }
    // MARK: Get Online Method
    
    func getOnlineMethod () {
        
        let json = ["user_id":Singleton.sharedInstance.userIdStr,
                    "auth_token":Singleton.sharedInstance.userTokenStr,
                    "online_status":"1"
            ] as [String : Any]
        
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/users/getonline", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
        }
        
    }
    
    // MARK: Email Validation
    
    func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    // MARK: AlertView
    
    func alertViewMethod(titleStr:String, messageStr:String) {
        let alert = UIAlertController(title: titleStr, message: messageStr, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    // MARK: Text Field Delegate
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        firstNameTxt.resignFirstResponder()
        lastNameTxt.resignFirstResponder()
        emailTxt.resignFirstResponder()
        passwordTxt.resignFirstResponder()
        confirmPasswordTxt.resignFirstResponder()
        stateTxt.resignFirstResponder()
        cityTxt.resignFirstResponder()
        
        zipCodeTxt.resignFirstResponder()
        ageTxt.resignFirstResponder()
        firstNameArtistTxt.resignFirstResponder()
        lastNameArtistTxt.resignFirstResponder()
        emailArtistTxt.resignFirstResponder()
        passwordArtistTxt.resignFirstResponder()
        confirmPasswordArtistTxt.resignFirstResponder()
        
        stateArtistTxt.resignFirstResponder()
        cityArtistTxt.resignFirstResponder()
        ageArtistTxt.resignFirstResponder()
        bandNameArtistTxt.resignFirstResponder()
        
        emailArtistTxt.resignFirstResponder()
        passwordArtistTxt.resignFirstResponder()
        confirmPasswordArtistTxt.resignFirstResponder()
        
        
        placeNameTxt.resignFirstResponder()
        emailPlaceTxt.resignFirstResponder()
        passwordPlaceTxt.resignFirstResponder()
        confirmPasswordPlaceTxt.resignFirstResponder()
        
        statePlaceTxt.resignFirstResponder()
        cityPlaceTxt.resignFirstResponder()
        zipCodePlaceTxt.resignFirstResponder()
        
    }
    
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool  {
        textField.resignFirstResponder()
        return true;
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == AddressTxt || textField == AddressPlaceTxt || textField == AddressArtistTxt {
            //   Present the Autocomplete view controller when the button is pressed.
//            let autocompleteController = GMSAutocompleteViewController()
//            autocompleteController.delegate = self
//            present(autocompleteController, animated: true, completion: nil)
        }
        
        scrView.isScrollEnabled = true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        scrView.isScrollEnabled = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Goole Place Delegate Methods
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {

        
        // Get the address components.
        if let addressLines = place.addressComponents {
            // Populate all of the address fields we can find.
            for field in addressLines {
                switch field.type {
                case kGMSPlaceTypeCountry:
                    countryStr = field.name

                // Print the items we aren't using.
                default:
                    print("Type: \(field.type), Name: \(field.name)")
                }
            }
        }

        if (Singleton.sharedInstance.userTypeStr == "member") {
           // AddressTxt.text = place.name

        } else if (Singleton.sharedInstance.userTypeStr == "artist") {
           // AddressArtistTxt.text = place.name

        } else if (Singleton.sharedInstance.userTypeStr == "place") {
            //AddressPlaceTxt.text = place.name

        }
        //    }
        
        self.latitude = String(describing:place.coordinate.latitude)
        self.longitude = String(describing:place.coordinate.longitude)

        
        
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    
    // MARK: UIPickerView Methods
    
    // returns the number of 'columns' to display.
    func numberOfComponentsInPickerView(pickerView: UIPickerView!) -> Int{
        return 1
    }
    
    // returns the # of rows in each component..
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        if pickerView.tag == 0 {
            return countryArray.count
        } else if (pickerView.tag == 1) {
            return stateArray.count
        }
        return citiesArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 0 {
            return (countryArray[row]["name"] as! String)
        } else if (pickerView.tag == 1) {
            return (stateArray[row]["name"] as! String)
        }
        return (citiesArray[row]["name"] as! String)
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 0 {
            
            if (countryArray[row]["name"] as! String) != "Country" {
                CountryTxt.text = (countryArray[row]["name"] as! String)
                CountryPlaceTxt.text = (countryArray[row]["name"] as! String)
                countryArtistTxt.text = (countryArray[row]["name"] as! String)


            } else {
                CountryTxt.text = ""
                CountryPlaceTxt.text = ""
                countryArtistTxt.text = ""
}
            
            
            let predicate = NSPredicate(format: "country_id == %@", (countryArray[row]["id"] as! String))
            let array = NSMutableArray(array: finalStateArray.filter { predicate.evaluate(with: $0) } )
            print(array)
            stateArray = array as! [[String : Any]];
            
            let pickerView11 = UIPickerView()
            stateTxt.inputView = pickerView11
            stateArtistTxt.inputView = pickerView11
            statePlaceTxt.inputView = pickerView11
            pickerView11.tag = 1
            pickerView11.delegate = self
            
            let pickerView12 = UIPickerView()
            cityTxt.inputView = pickerView12
            cityArtistTxt.inputView = pickerView12
            cityPlaceTxt.inputView = pickerView12
            pickerView12.tag = 2
            pickerView12.delegate = self
            
            
            stateTxt.text = ""
            stateArtistTxt.text = ""
            statePlaceTxt.text = ""
            cityTxt.text = ""
            cityArtistTxt.text = ""
            cityPlaceTxt.text = ""
            
            stateArray.insert([
                "id": "-8",
                "name": "State",
                "country_id": "-8"
                ], at: 0)
            citiesArray = [[
                "id": "-8",
                "name": "Cities",
                "state_id": "-8"
                ]]
            
        } else if (pickerView.tag == 1) {
            if (stateArray[row]["name"] as! String) != "State" {
                stateTxt.text = (stateArray[row]["name"] as! String)
                stateArtistTxt.text = (stateArray[row]["name"] as! String)
                statePlaceTxt.text = (stateArray[row]["name"] as! String)
            } else {
                stateTxt.text = ""
                stateArtistTxt.text = ""
                statePlaceTxt.text = ""
            }
            
            let predicate = NSPredicate(format: "state_id == %@", (stateArray[row]["id"] as! String))
            let array = NSMutableArray(array: finalCitiesArray.filter { predicate.evaluate(with: $0) } )
            print(array)
            citiesArray = array as! [[String : Any]];
            
            let pickerView2 = UIPickerView()
            cityTxt.inputView = pickerView2
            pickerView2.tag = 2
            pickerView2.delegate = self
            
            cityTxt.text = ""
            cityArtistTxt.text = ""
            cityPlaceTxt.text = ""
            
            citiesArray.insert([
                "id": "-8",
                "name": "Cities",
                "state_id": "-8"
                ], at: 0)
        } else {
            
            if (citiesArray[row]["name"] as! String) != "Cities" {
                cityTxt.text = (citiesArray[row]["name"] as! String)
                cityArtistTxt.text = (citiesArray[row]["name"] as! String)
                cityPlaceTxt.text = (citiesArray[row]["name"] as! String)
            } else {
                cityTxt.text = ""
                cityArtistTxt.text = ""
                cityPlaceTxt.text = ""
            }
            
            
        }
    }

    
    
}
