//
//  LocationManager.swift
//  Wi Match
//
//  Created by Brst on 29/01/19.
//  Copyright © 2019 brst. All rights reserved.
//

import UIKit
import CoreLocation
class LocationManager: NSObject,CLLocationManagerDelegate {
    
    var locationManager: CLLocationManager!
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var locationValue = String()
    var addressString = String()
    
    func initLocation()
    {
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = 0.5
        locationManager.delegate = self
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        
        // Mark: Checking whether location is on/off
        let locationStatus = UserDefaults.standard.value(forKey: "locationKey") as? String
        if locationStatus != nil{
            
            self.locationValue = (UserDefaults.standard.value(forKey: "locationKey") as? String)!
            
        }
    }
    
    //MARK: Location manager delegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        
        //  let valueToCompare: Double = 50
        
        DataManager.currentLat = locations.last?.coordinate.latitude
        DataManager.currentLong = locations.last?.coordinate.longitude
        self.locationManager.stopUpdatingLocation()
        if self.locationValue == "1"{
            
            self.getAddressFromLatLon(pdblLatitude: DataManager.currentLat! , withLongitude:  DataManager.currentLong! )
        
       }
        else{
            
          print("Our location is off")
            
           
       }
     
        return
        
        
    }
    
    
    // Getting current address from current latitude and longitude
    func getAddressFromLatLon(pdblLatitude: Double, withLongitude pdblLongitude: Double) {
        
        
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = pdblLatitude
        center.longitude = pdblLongitude
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                
                let pm = placemarks! as [CLPlacemark]
                if pm.count > 0 {
                    let pm = placemarks![0]
                    print(pm.country ?? String())
                    print(pm.locality  ?? String())
                    print(pm.subLocality  ?? String())
                    print(pm.postalCode  ?? String())
                    
                 //   self.addressString = ""
                    if pm.subLocality != nil {
                       self.addressString =  self.addressString + pm.subLocality! + ", "
                    }
                    
                    if pm.locality != nil {
                       self.addressString = self.addressString + pm.locality! + ", "
                    }
                    if pm.country != nil {
                      self.addressString = self.addressString + pm.country! + ", "
                    }
                    if pm.postalCode != nil {
                       self.addressString = self.addressString + pm.postalCode! + " "
                    }
                 //   print(self.addressString)
                    
                }
                
        })
        
    }
   
}
