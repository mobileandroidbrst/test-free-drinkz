//
//  MyPlacesOrder.swift
//  FreeDrinkz
//
//  Created by Brst-Pc109 on 28/03/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class MyPlacesOrder: UIViewController,UITableViewDataSource,UITableViewDelegate {
   
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var newLbl: UILabel!
    @IBOutlet weak var completedLbl: UILabel!
    @IBOutlet weak var newBtn: UIButton!
    @IBOutlet weak var completedBtn: UIButton!
    @IBOutlet weak var ordersTbl: UITableView!
    @IBOutlet weak var coupanView: UIView!
    @IBOutlet weak var coupanView1: UIView!
    var typeStr = String()
    @IBOutlet weak var coupanLbl: UILabel!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var searchTxt: UITextField!

    var newOrderArray = NSMutableArray()
    var completedOrderArray = NSMutableArray()
    var searchArray = NSMutableArray()
    var isSearch = Bool()

    override func viewDidLoad() {
        super.viewDidLoad()

        backView.layer.cornerRadius = 16
        backView.layer.borderColor = UIColor.white.cgColor
        backView.layer.borderWidth = 1
        backView.clipsToBounds = true
        typeStr = "new"
        coupanView.isHidden = true

        
        indicator.startAnimating()
        indicator.isHidden = false
        getOrderMethod()
    }
    
    
    
    // MARK: Get Orders
    
    func getOrderMethod () {
        
        
        let json = ["user_id":Singleton.sharedInstance.userIdStr,
                    "auth_token":Singleton.sharedInstance.userTokenStr,
                    "resturant_id":Singleton.sharedInstance.userIdStr
            
            ] as [String : Any]
        print(json)
        
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/members/orderrestlist", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    let json = JSON(data: response.data! )
                    if let descriptionStr = json["description"].string {
                        print(descriptionStr)
                        if descriptionStr == "Wrong auth token" {
                            
                            
                            UserDefaults.standard.setValue(nil, forKey: "auth_token")
                            UserDefaults.standard.setValue(nil, forKey: "user_id")
                            UserDefaults.standard.setValue(nil, forKey: "user_type")
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "Login") as! Login
                            vc.isExpired = true
                            self.navigationController?.pushViewController(vc,animated: true)
                            
                        }  else if descriptionStr == "Order list" {
                            
                            self.newOrderArray = NSMutableArray(array: json["data"]["Order_incomplete"].arrayObject!)
                            self.completedOrderArray = NSMutableArray(array: json["data"]["order_complete"].arrayObject!)
                            
                            print(self.newOrderArray.count)
                            print(self.completedOrderArray.count)
                            
                            
                            if (self.typeStr == "completed") {

                                
                                if self.completedOrderArray.count == 0 {
                                    self.statusLbl.isHidden = false;
                                    self.statusLbl.text = "There is no order completed in complete section."
                                } else {
                                    self.statusLbl.isHidden = true;
                                }
                                
                            } else if (self.typeStr == "new") {
                              
                                if self.newOrderArray.count == 0 {
                                    self.statusLbl.isHidden = false;
                                    self.statusLbl.text = "You have not received any new order."
                                } else {
                                    self.statusLbl.isHidden = true;
                                }

                            }
                      }
                        
                        self.ordersTbl.reloadData()
                        //Now you got your value
                    }
                } else if((response.error) != nil){
                    
                    self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                    
                }
                
                                self.indicator.stopAnimating()
                                self.indicator.isHidden = true
                
        }
        
    }
    
    
    // MARK: Complete Order Method
    
    func completeOrderMethod (orderId: String) {
        
        
        let json = ["user_id":Singleton.sharedInstance.userIdStr,
                    "auth_token":Singleton.sharedInstance.userTokenStr,
                    "order_id":orderId,
                    "status":"1"
            
            ] as [String : Any]
        
        print(json)
        
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/members/updateorderstatus", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    let json = JSON(data: response.data! )
                    if let descriptionStr = json["description"].string {
                        print(descriptionStr)
                        if descriptionStr == "Wrong auth token" {
                            
                            
                            UserDefaults.standard.setValue(nil, forKey: "auth_token")
                            UserDefaults.standard.setValue(nil, forKey: "user_id")
                            UserDefaults.standard.setValue(nil, forKey: "user_type")
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "Login") as! Login
                            vc.isExpired = true
                            self.navigationController?.pushViewController(vc,animated: true)
                            
                        }  else if descriptionStr == "Order status Update Successfully" {
                            self.alertViewMethod(titleStr: "", messageStr: "The order has been successfully completed.")
                            
                            self.indicator.startAnimating()
                            self.indicator.isHidden = false
                            self.getOrderMethod()
                        }
                        
                        self.ordersTbl.reloadData()
                        //Now you got your value
                    }
                } else if((response.error) != nil){
                    
                    self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                    
                }
                
                                self.indicator.stopAnimating()
                                self.indicator.isHidden = true
                
        }
        
    }
    
    

    
    // MARK: AlertView
    
    func alertViewMethod(titleStr:String, messageStr:String) {
        let alert = UIAlertController(title: titleStr, message: messageStr, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

    
    
    
    //MARK: UITableView Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearch {
            return searchArray.count
        }

        if (typeStr == "completed") {
            return completedOrderArray.count
        } else if (typeStr == "new") {
            return newOrderArray.count
        }
        
        return 0
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:OrderCell = ordersTbl.dequeueReusableCell(withIdentifier: "OrderCell") as! OrderCell
        
        
        
        var dict = NSDictionary()
        
        if (typeStr == "completed") {

            cell.completedView.isHidden = false
            cell.backView1.isHidden = false
            cell.coupanView.isHidden = true
            cell.completeBtn.isHidden = true
            
            if isSearch {
                dict = searchArray[indexPath.row] as! NSDictionary
            } else {
                dict = completedOrderArray[indexPath.row] as! NSDictionary
            }
      
        } else {

            
            cell.completedView.isHidden = true
            cell.backView1.isHidden = true
            cell.coupanView.isHidden = false
            cell.completeBtn.isHidden = false
            
            if isSearch {
                dict = searchArray[indexPath.row] as! NSDictionary
            } else {
                dict = newOrderArray[indexPath.row] as! NSDictionary
            }

        }
        cell.drinkLbl.text = "\(dict["product_name"] as! String)"
        cell.usernameLbl.text = dict["receiver_name"] as? String
        cell.addressLbl.text = "\(Singleton.sharedInstance.addressStr), \(Singleton.sharedInstance.cityStr), \(Singleton.sharedInstance.stateStr), \(Singleton.sharedInstance.zipcodeStr)"
        
       
//        cell.drinkImg.sd_setImage(with: URL(string: (dict["drink_image"] as? String)!), completed: {(image,  error,  cacheType, imageURL) -> Void in
//        })
        

        // 5 June :-
        
//         cell.drinkImg.sd_setImage(with: URL(string: (dict["drink_image"] as? String)!), placeholderImage: UIImage(named: "gallery"))

        let urlStr : String = (dict["drink_image"] as? String)!
        let urlFormattedStr = urlStr.replacingOccurrences(of: " ", with: "%20")
         cell.drinkImg.sd_setImage(with: URL(string: urlFormattedStr), placeholderImage: UIImage(named: "gallery"))
 
        // 5 June :-
        
//         cell.userImg.sd_setImage(with: URL(string: (dict["receiver_image"] as? String)!), placeholderImage: UIImage(named: "user"))
        
        let urlStrr : String = (dict["receiver_image"] as? String)!
        let urlFormattedStrr = urlStrr.replacingOccurrences(of: " ", with: "%20")
         cell.userImg.sd_setImage(with: URL(string: urlFormattedStrr), placeholderImage: UIImage(named: "user"))
        
        
        cell.userImg.layer.cornerRadius = cell.userImg.frame.size.height/2
        cell.userImg.clipsToBounds = true
      
        // 15 May :- Placing Time stamp value in Table view's cell.
        
         print((dict.value(forKey:"timestamp") as! String))
         if let timestamp = dict["timestamp"] as? String {
          //  cell.dateTimeLbl.text = timestamp
            
            let timestampDouble = Double(timestamp)
            let date = Date(timeIntervalSince1970: timestampDouble!)
            let dateFormatter = DateFormatter()
            // dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone that you want
            dateFormatter.locale = NSLocale.current
           // dateFormatter.dateFormat = "yyyy-MM-dd HH:mm" //Specify your format that you want
             dateFormatter.dateFormat = "dd-MMMM-yyyy HH:mm a"
         
           let strDate = dateFormatter.string(from: date)
            
            cell.dateTimeLbl.text =  "Order date:" + strDate
            
            if (typeStr == "completed") {
              cell.dateTimeLbl.text =  "Completed date:" + strDate
                
            }
        }
        
   cell.backView.layer.cornerRadius = 4
        cell.backView.layer.masksToBounds = false;
        cell.backView.layer.shadowColor = UIColor.black.cgColor
        cell.backView.layer.shadowOpacity = 0.3
        cell.backView.layer.shadowRadius = 2
        cell.backView.layer.shadowOffset = CGSize(width: 1, height: 1)
 
        cell.completedView.layer.cornerRadius = 10
        cell.completedView.clipsToBounds = true
        
        
        cell.coupanViewBtn.addTarget(self, action: #selector(viewCoupan(_:)), for: .touchUpInside)
        cell.coupanViewBtn.tag = indexPath.row

        cell.completeBtn.addTarget(self, action: #selector(CompleteOrder(_:)), for: .touchUpInside)
        cell.completeBtn.tag = indexPath.row

        cell.profileBtn.addTarget(self, action: #selector(ViewProfile(_:)), for: .touchUpInside)
        cell.profileBtn.tag = indexPath.row

        
        return cell
    }
    
    
    func ViewProfile(_ button: UIButton) {
        
        var dict = NSDictionary()
        
        if (typeStr == "completed") {
            
            if isSearch {
                dict = searchArray[button.tag] as! NSDictionary
            } else {
                dict = completedOrderArray[button.tag] as! NSDictionary
            }

        } else {
            
            if isSearch {
                dict = searchArray[button.tag] as! NSDictionary
            } else {
                dict = newOrderArray[button.tag] as! NSDictionary
            }

        }
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "Profile") as! Profile
        vc.fromNavStr = "other"
        vc.memberProfileId = String(describing: dict["receiver_id"]!)
        navigationController?.pushViewController(vc,animated: true)
    }

    
    func CompleteOrder(_ button: UIButton) {
        indicator.startAnimating()
        indicator.isHidden = false

        var dict = NSDictionary()
        if (typeStr == "new") {
            dict = newOrderArray[button.tag] as! NSDictionary
            completeOrderMethod(orderId:String(describing: dict["order_id"]!))
        }
    }
    func viewCoupan(_ button: UIButton) {
        var dict = NSDictionary()
        
        if (typeStr == "completed") {
            if isSearch {
                dict = searchArray[button.tag] as! NSDictionary
            } else {
                dict = completedOrderArray[button.tag] as! NSDictionary
            }

        } else {
            if isSearch {
                dict = searchArray[button.tag] as! NSDictionary
            } else {
                dict = newOrderArray[button.tag] as! NSDictionary
            }
        }
        coupanLbl.text = dict["coupon_code"] as? String
        coupanView.isHidden = false
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
    }
    
    //MARK: Button Actions
    @IBAction func Cancel(_ sender: Any) {
        coupanView.isHidden = true
        
    }
    @IBAction func Search(_ sender: Any) {
        if isSearch {
            isSearch = false
            searchTxt.resignFirstResponder()
            searchTxt.isHidden = true
        } else {
            isSearch = true
            searchTxt.becomeFirstResponder()
            searchTxt.isHidden = false
        }
        

//        let alert = UIAlertController(title: "", message: "Work in Progress", preferredStyle: UIAlertControllerStyle.alert)
//        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
//        self.present(alert, animated: true, completion: nil)
        

        
    }
    
    @IBAction func SearchOrder(_ sender: Any) {
        
        if (typeStr == "completed") {
            let predicate1 = NSPredicate(format: "product_name contains[cd] %@", searchTxt.text!)
            let predicate2 = NSPredicate(format: "sender_name contains[cd] %@", searchTxt.text!)
            let predicate = NSCompoundPredicate.init(orPredicateWithSubpredicates: [predicate1, predicate2])
            searchArray = NSMutableArray(array: completedOrderArray.filter { predicate.evaluate(with: $0) })
            if searchTxt.text == "" {
                searchArray = completedOrderArray
            }
        } else if (typeStr == "new") {
            let predicate1 = NSPredicate(format: "product_name contains[cd] %@", searchTxt.text!)
            let predicate2 = NSPredicate(format: "receiver_name contains[cd] %@", searchTxt.text!)
            let predicate = NSCompoundPredicate.init(orPredicateWithSubpredicates: [predicate1, predicate2])
            searchArray = NSMutableArray(array: newOrderArray.filter { predicate.evaluate(with: $0) })
            if searchTxt.text == "" {
                searchArray = newOrderArray
            }
        }
        print(searchArray.count)
        print(searchArray)
        ordersTbl.reloadData()
    }

    
    @IBAction func ReceivedSent(_ sender: Any) {
        
        if ((sender as AnyObject).tag == 0) {
            newBtn.backgroundColor = UIColor.white
            newLbl.textColor = UIColor(red: 217/255, green: 41/255, blue: 39/255, alpha: 1)
            completedBtn.backgroundColor = UIColor(red: 217/255, green: 41/255, blue: 39/255, alpha: 1)
            completedLbl.textColor = UIColor.white
            typeStr = "new"
        } else if ((sender as AnyObject).tag == 1) {
            completedBtn.backgroundColor = UIColor.white
            completedLbl.textColor = UIColor(red: 217/255, green: 41/255, blue: 39/255, alpha: 1)
            newBtn.backgroundColor = UIColor(red: 217/255, green: 41/255, blue: 39/255, alpha: 1)
            newLbl.textColor = UIColor.white
            typeStr = "completed"
        }
        
        
        if (self.typeStr == "completed") {
            
            
            if self.completedOrderArray.count == 0 {
                self.statusLbl.isHidden = false;
                self.statusLbl.text = "There is no order completed in complete section."
            } else {
                self.statusLbl.isHidden = true;
            }
            
        } else if (self.typeStr == "new") {
            
            if self.newOrderArray.count == 0 {
                self.statusLbl.isHidden = false;
                self.statusLbl.text = "You have not received any new order."
            } else {
                self.statusLbl.isHidden = true;
            }
            
        }
        
        if (typeStr == "completed") {
            let predicate1 = NSPredicate(format: "product_name contains[cd] %@", searchTxt.text!)
            let predicate2 = NSPredicate(format: "sender_name contains[cd] %@", searchTxt.text!)
            let predicate = NSCompoundPredicate.init(orPredicateWithSubpredicates: [predicate1, predicate2])
            searchArray = NSMutableArray(array: completedOrderArray.filter { predicate.evaluate(with: $0) })
            if searchTxt.text == "" {
                searchArray = completedOrderArray
            }
        } else if (typeStr == "new") {
            let predicate1 = NSPredicate(format: "product_name contains[cd] %@", searchTxt.text!)
            let predicate2 = NSPredicate(format: "receiver_name contains[cd] %@", searchTxt.text!)
            let predicate = NSCompoundPredicate.init(orPredicateWithSubpredicates: [predicate1, predicate2])
            searchArray = NSMutableArray(array: newOrderArray.filter { predicate.evaluate(with: $0) })
            if searchTxt.text == "" {
                searchArray = newOrderArray
            }
        }

        
        ordersTbl.reloadData()
    }
    // MARK: Text Field Delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool  {
        textField.resignFirstResponder()
        return true;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

   
    
}
