//
//  StripeConnectVC.swift
//  FreeDrinkz
//
//  Created by tbi-pc-57 on 4/24/19.
//  Copyright © 2019 Brst-Pc109. All rights reserved.
//

import UIKit
import Stripe
import Alamofire

class StripeConnectVC: UIViewController,UIWebViewDelegate {

    // Mark :- Outlets
    @IBOutlet var webview: UIWebView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    // Mark :- Stripe-Connect dashboard values(Test-Keys)
   
     let CLIENT_ID = "ca_ErkgWvLYkzPcqv31KKDLQycoeUKTlzFK"
     let CLIENT_SECRET = "sk_test_0QJR5imHVoVKarbeyWJ0xM2U00sKsBOSsI"
    
    var isLoad = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
     // 26 April :-
    
      let urll = URL(string: "https://connect.stripe.com/oauth/authorize?response_type=code&client_id=" + CLIENT_ID + "&scope=" + "read_write" + "&response_type=code" + "&stripe_landing=login")
        
       print(urll!)
        
        webview.delegate = self
        activityIndicator.isHidden = true
        
        guard let url = urll else {
            self.alert(message: "The URL seems to be Invalid.")
            return
        }
        
         let path: String = "/authorize"
         let cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData
      
        // 26 April :-
       // let timeout: TimeInterval = 6.0
          let timeout: TimeInterval = 200.0
        
       // var request = URLRequest(url: urll!.appendingPathComponent(path), cachePolicy: cachePolicy, timeoutInterval: timeout)
        
         var request = URLRequest(url: urll!, cachePolicy: cachePolicy, timeoutInterval: timeout)
         request.httpMethod = "GET"
         activityIndicator.isHidden = false
         webview.loadRequest(request)
        
  }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        activityIndicator.startAnimating()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        activityIndicator.isHidden = true
        activityIndicator.stopAnimating()
    }

    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        print(error.localizedDescription)
        self.alert(message: error.localizedDescription)
    }
    
    // Main- logic :-
     func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        if let url = request.url {
            
            if let urlComponents = URLComponents(string: url.absoluteString) {
                if let queryString = urlComponents.queryItems {
                    print(queryString)
                    
                    
                    for query in queryString {
                        
                        print(query.name)
                        
                        if query.name == "code" && !isLoad {
                            if let value = query.value {
                                isLoad = true
                                
                          //      let urll = URL(string: "https://connect.stripe.com/oauth/token?code=\(value)&client_secret=sk_test_0QJR5imHVoVKarbeyWJ0xM2U00sKsBOSsI&grant_type=authorization_code")

                                // Calling function for hitting API :-
                                
                               movingAddressData(url: "https://connect.stripe.com/oauth/token?code=\(value)&client_secret=sk_test_0QJR5imHVoVKarbeyWJ0xM2U00sKsBOSsI&grant_type=authorization_code")
                                


                            }
                        }
                        
                    }
                }
            }
        }
        
        return true
}
    
    
    // Hitting the moving address api
    func movingAddressData(url: String){
        
         Alamofire.request(url, method: .post, parameters: nil, encoding: JSONEncoding.default)
                .responseJSON {  response in
                    DispatchQueue.main.async {
                        print(response)
                        
                    if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    let JSON = jsonData as! NSDictionary
                    print(JSON)
                    let stripeUserId:String = JSON.value(forKey: "stripe_user_id") as! String
                         print(stripeUserId)
                    let userId:String = UserDefaults.standard.value(forKey: "userId") as! String
                         print(userId)
                    let authToken:String = UserDefaults.standard.value(forKey: "auth_token") as! String
                         print(authToken)
              
                 // 2 May:-
                 //   self.savingUserStripeDetail(stripeUserId: stripeUserId,userId: userId,authToken: authToken)
                 
                 // 2 May :- If User Pressed Update Button From Payment Page.
                        let updatePaymentBool:Bool = UserDefaults.standard.bool(forKey: "update_payment")
                        print(updatePaymentBool)
                        if(updatePaymentBool == true)
                        {
//                            UserDefaults.standard.set(false, forKey: "update_payment")
//                            UserDefaults.standard.synchronize()
                            
           self.updatingUserStripeDetail(stripeUserId: stripeUserId,userId: userId,authToken: authToken)
                        }
                        else
                        {
           self.savingUserStripeDetail(stripeUserId: stripeUserId,userId: userId,authToken: authToken)
                        }
                    
    }
                 else if((response.error) != nil){
                    print(response.error!)
                 }
                        
            }
    }
            
       
}
    
    func savingUserStripeDetail(stripeUserId:String,userId:String,authToken:String) {
        
        let parameters: [String: String] = [
            "user_id" : userId,
            "auth_token" : authToken,
            "account_id" : stripeUserId,
        ]
        
      let url = "http://192.168.1.13/freedrinkz/api/web/v1/members/restaurantstripetoken"
        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON {  response in
                DispatchQueue.main.async {
                    print(response)
                        if let jsonData = response.result.value {
                        print("JSON: \(jsonData)")
                            
                    // Moving to Previous View-Controller
                        _ = self.navigationController?.popViewController(animated: true)
                            
             // Saving Bool value in User-Defaults so when it moves to previous VC. Previous VC don't show Stripe-Connect Button.It will show "Account is already added."
                            
                      UserDefaults.standard.set(true, forKey: "payment_done")
                      UserDefaults.standard.synchronize()
                            
             }
                            else if((response.error) != nil){
                        print(response.error!)
                    }
                    
    }
  }
        
}
   
// 2 May:- Updating Stripe-Account Id in database after pressing Update-button from previous page.
    
 func updatingUserStripeDetail(stripeUserId:String,userId:String,authToken:String){
        
        let parameters: [String: String] = [
            "user_id" : userId,
            "auth_token" : authToken,
            "account_id" : stripeUserId,
            ]
        
         let url = "http://192.168.1.13/freedrinkz/api/web/v1/members/updaterestaurantstripetoken"
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON {  response in
                DispatchQueue.main.async {
                    print(response)
                    if let jsonData = response.result.value {
                        print("JSON: \(jsonData)")
                        
                        // Moving to Previous View-Controller
                        _ = self.navigationController?.popViewController(animated: true)
                        
                        // Saving Bool value in User-Defaults so when it moves to previous VC. Previous VC don't show Stripe-Connect Button.It will show "Account is already added."
                        
                        UserDefaults.standard.set(true, forKey: "payment_done")
                        UserDefaults.standard.synchronize()
                        
                    }
                    else if((response.error) != nil){
                        print(response.error!)
                    }
               }
        }
       
}
    
  func alert(message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let action = UIAlertAction(title: "OK", style: .default, handler: {(alert: UIAlertAction!) in
            self.dismiss(animated: true, completion: nil)
        })
        
        alertController.addAction(action)
        
        DispatchQueue.main.async(execute: { () -> Void in
            self.present(alertController, animated: true, completion: nil)
        })
    }
    
    // 10 June :- Back Button
    
    @IBAction func backAction(_ sender: UIButton) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    
    
    
}
    
    
    
    


