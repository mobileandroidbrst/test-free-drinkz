//
//  BlockedUsers.swift
//  FreeDrinkz
//
//  Created by Brst-Pc109 on 13/07/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class BlockedUsers: UIViewController, UITableViewDelegate, UITableViewDataSource {
  
    @IBOutlet weak var blockTbl: UITableView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    var blockUserArray = [JSON]()
    @IBOutlet weak var statusLbl: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        getBlockUserMethod()
    }
    
    
    // MARK: Get Block Users
    
    func getBlockUserMethod () {
        
        self.indicator.startAnimating()
        self.indicator.isHidden = false

        
        let json = ["user_id":Singleton.sharedInstance.userIdStr,
                    "auth_token":Singleton.sharedInstance.userTokenStr,
                ] as [String : Any]
            
        
        print(json)
        
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/users/getblockuser", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    let json = JSON(data: response.data! )
                    if let descriptionStr = json["description"].string {
                        print(descriptionStr)
                        if descriptionStr == "Wrong auth token" {
                            
                            
                            UserDefaults.standard.setValue(nil, forKey: "auth_token")
                            UserDefaults.standard.setValue(nil, forKey: "user_id")
                            UserDefaults.standard.setValue(nil, forKey: "user_type")
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "Login") as! Login
                            vc.isExpired = true
                            self.navigationController?.pushViewController(vc,animated: true)
                            
                        }  else if descriptionStr == "Success" {
                            self.blockUserArray = json["data"]["block_user_list"].array!
                        }
                        
                        
                        
                        if self.blockUserArray.count == 0 {
                            self.statusLbl.isHidden = false;
                        } else {
                            self.statusLbl.isHidden = true;
                        }

                        self.blockTbl.reloadData()
                        //Now you got your value
                    }
                } else if((response.error) != nil){
                    
                    self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                    
                }
                
                self.indicator.stopAnimating()
                self.indicator.isHidden = true
                
        }
        
    }
    

    // MARK: AlertView
    
    func alertViewMethod(titleStr:String, messageStr:String) {
        let alert = UIAlertController(title: titleStr, message: messageStr, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

    
    // MARK: UITableView Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return blockUserArray.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:BlockCell = tableView.dequeueReusableCell(withIdentifier: "BlockCell") as! BlockCell
        
        cell.userNameLbl.text = "\(blockUserArray[indexPath.row]["firstname"].string!) \(blockUserArray[indexPath.row]["lastname"].string!)"
        
        // 10 June :-
       // cell.userImg.sd_setImage(with: URL(string: blockUserArray[indexPath.row]["profile_pic"].string!), placeholderImage: UIImage(named: "user"))
        let urlStr : String = (blockUserArray[indexPath.row]["profile_pic"].string!)
        let urlFormattedStr = urlStr.replacingOccurrences(of: " ", with: "%20")
        cell.userImg.sd_setImage(with: URL(string:urlFormattedStr), placeholderImage: UIImage(named: "user"))
        
        
        
        
        cell.blockBtn.addTarget(self, action: #selector(Unblock_(sender:)), for: .touchUpInside)
        cell.blockBtn.tag = indexPath.row
        return cell
    }
    

    //MARK: Button Actions
    
    func Unblock_ (sender: UIButton) {
        indicator.startAnimating()
        indicator.isHidden = false
        unblockUserMethod(blockId: String(describing: blockUserArray[sender.tag]["block_user_id"].int!))
    }

    
    
    // MARK: Get Block Users
    
    func unblockUserMethod (blockId: String) {
        
        
        let json = ["user_id":Singleton.sharedInstance.userIdStr,
                    "auth_token":Singleton.sharedInstance.userTokenStr,
                    "block_user_id":blockId,
                    "status":"0"
                    ] as [String : Any]
        
        
        print(json)
        
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/users/unblockuser", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    let json = JSON(data: response.data! )
                    if let descriptionStr = json["description"].string {
                        print(descriptionStr)
                        if descriptionStr == "Wrong auth token" {
                            
                            
                            UserDefaults.standard.setValue(nil, forKey: "auth_token")
                            UserDefaults.standard.setValue(nil, forKey: "user_id")
                            UserDefaults.standard.setValue(nil, forKey: "user_type")
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "Login") as! Login
                            vc.isExpired = true
                            self.navigationController?.pushViewController(vc,animated: true)
                            
                        }  else if descriptionStr == "User Unblocked Successfully" {
                          //  self.alertViewMethod(titleStr: "", messageStr: "User Unblocked Successfully")
                            self.getBlockUserMethod()
                        }
                        
                        self.blockTbl.reloadData()
                        //Now you got your value
                    }
                } else if((response.error) != nil){
                    
                    self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                    
                }
                
                self.indicator.stopAnimating()
                self.indicator.isHidden = true
                
        }
        
    }
    

    
    
    @IBAction func Back(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
