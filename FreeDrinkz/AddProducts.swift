//
//  AddProducts.swift
//  FreeDrinkz
//
//  Created by Brst-Pc109 on 28/03/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Alamofire
import SwiftyJSON
import AVFoundation
import Photos

class AddProducts: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate {

    @IBOutlet weak var titleTxt: UITextField!
    
    @IBOutlet weak var categoryTxt: IQDropDownTextField!
    
    @IBOutlet weak var priceTxt: UITextField!
    @IBOutlet weak var titleLbl: UILabel!

    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var productImg: UIImageView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var backView: UIView!
    var uniquePhotoNameStr = String()
    var isImage = Bool()
    var isEdit = Bool()

    var priceStr = String()
    var categoryStr = String()
    var titleStr = String()
    var productImgStr = String()
    var productIdStr = String()
    var catIdStr = String()

    var catLists = [NSString]()

    override func viewDidLoad() {
        super.viewDidLoad()

        print(isEdit)
        if (isEdit) {
          //  backView.isHidden = false
            productImg.sd_setImage(with: URL(string: productImgStr), placeholderImage: UIImage(named: "gallery"))
            titleTxt.text = titleStr
            categoryTxt.text = categoryStr
            priceTxt.text = priceStr
            isImage = true
            titleLbl.text = "Edit Products"
            
        } 
        productImg.layer.cornerRadius = productImg.frame.size.width / 2
        productImg.clipsToBounds = true

        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border.frame = CGRect(x: 0, y: titleTxt.frame.size.height - width, width:  titleTxt.frame.size.width, height: titleTxt.frame.size.height)
        border.borderWidth = width
        titleTxt.layer.addSublayer(border)
        titleTxt.layer.masksToBounds = true
        
        
        let border1 = CALayer()
        border1.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border1.frame = CGRect(x: 0, y: categoryTxt.frame.size.height - width, width:  categoryTxt.frame.size.width, height: categoryTxt.frame.size.height)
        border1.borderWidth = width
        categoryTxt.layer.addSublayer(border1)
        categoryTxt.layer.masksToBounds = true
        
        
        
        let border2 = CALayer()
        border2.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border2.frame = CGRect(x: 0, y: priceTxt.frame.size.height - width, width:  priceTxt.frame.size.width, height: priceTxt.frame.size.height)
        border2.borderWidth = width
        priceTxt.layer.addSublayer(border2)
        priceTxt.layer.masksToBounds = true
        categoryTxt.keyboardDistanceFromTextField = 150;

        if Singleton.sharedInstance.categoryArray.count>0 {
         
            for item in Singleton.sharedInstance.categoryArray {
                print(item)
                self.catLists.append(item["name"] as! NSString)
                print(self.catLists)
            }
            self.categoryTxt.itemList = self.catLists

            
        }
        
        getCategoryMethod()
    }
    
    
    // MARK: Get Category Method
    
    func getCategoryMethod () {
        
        let json = ["user_id":Singleton.sharedInstance.userIdStr,
                    "auth_token":Singleton.sharedInstance.userTokenStr
            ] as [String : Any]
        
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/users/getdrinkcategory", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    let json = JSON(data: response.data! )
                    if let descriptionStr = json["description"].string {
                        print(descriptionStr)
                        if descriptionStr == "Drink Category Listing" {
                            Singleton.sharedInstance.categoryArray = json["data"]["drinkcategory_listing"].arrayObject! as! [[String : Any]]
                            print(Singleton.sharedInstance.categoryArray.count)

                            var itemsLists = [NSString]()
                            
                            for item in Singleton.sharedInstance.categoryArray {
                                print(item)
                                itemsLists.append(item["name"] as! NSString)
                                print(itemsLists)
                            }
                            self.catLists = itemsLists
                            self.categoryTxt.itemList = self.catLists

                            
                        }
                        //Now you got your value
                    }
                }
        }
    }

    
    
    //MARK: CheckCameraAuthorization
    
    func checkCameraAuthorization() {
        
        let status: AVAuthorizationStatus = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo)
        if status == .authorized {
            // authorized
            let picker = UIImagePickerController()
            picker.delegate = self
            picker.allowsEditing = true
            picker.sourceType = .camera
            present(picker, animated: true, completion: { _ in })
        } else if status == .denied {
            // denied
            if AVCaptureDevice.responds(to: #selector(AVCaptureDevice.requestAccess(forMediaType:completionHandler:))) {
                AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: {(_ granted: Bool) -> Void in
                    // Will get here on both iOS 7 & 8 even though camera permissions weren't required
                    // until iOS 8. So for iOS 7 permission will always be granted.
                    print("DENIED")
                    if granted {
                        // Permission has been granted. Use dispatch_async for any UI updating
                        // code because this block may be executed in a thread.
                        
                        DispatchQueue.main.async(execute: {() -> Void in
                            let picker = UIImagePickerController()
                            picker.delegate = self
                            picker.allowsEditing = true
                            picker.sourceType = .camera
                            self.present(picker, animated: true, completion: { _ in })
                        })
                        
                    }
                    else {
                        // Permission has been denied.
                        
                        
                        DispatchQueue.main.async(execute: {() -> Void in
                            self.accessMethod("Please go to Settings and enable the camera for this app to use this feature.")
                        })
                        
                        
                    }
                })
            }
        } else if status == .restricted {
            // restricted
            DispatchQueue.main.async(execute: {() -> Void in
                self.accessMethod("Please go to Settings and enable the camera for this app to use this feature.")
            })
        }
        else if status == .notDetermined {
            // not determined
            AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: {(_ granted: Bool) -> Void in
                if granted {
                    // Access has been granted ..do something
                    DispatchQueue.main.async(execute: {() -> Void in
                        let picker = UIImagePickerController()
                        picker.delegate = self
                        picker.allowsEditing = true
                        picker.sourceType = .camera
                        self.present(picker, animated: true, completion: { _ in })
                        
                    })
                    
                    
                }
                else {
                    // Access denied ..do something
                    DispatchQueue.main.async(execute: {() -> Void in
                        self.accessMethod("Please go to Settings and enable the camera for this app to use this feature.")
                    })
                    
                }
            })
        }
    }
    
    //MARK: CheckPhotoAuthorization
    
    func checkPhotoAuthorization()  {
        let status: PHAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
        if status == .authorized {
            let picker = UIImagePickerController()
            picker.delegate = self
            picker.allowsEditing = true
            picker.sourceType = .photoLibrary
            present(picker, animated: true, completion: { _ in })
        }
        else if status == .denied {
            DispatchQueue.main.async(execute: {() -> Void in
                self.accessMethod("Please go to Settings and enable the photos for this app to use this feature.")
            })
        } else if status == .notDetermined {
            // Access has not been determined.
            PHPhotoLibrary.requestAuthorization({(_ status: PHAuthorizationStatus) -> Void in
                if status == .authorized {
                    
                    DispatchQueue.main.async(execute: {() -> Void in
                        let picker = UIImagePickerController()
                        picker.delegate = self
                        picker.allowsEditing = true
                        picker.sourceType = .photoLibrary
                        self.present(picker, animated: true, completion: { _ in })
                    })
                    
                }
                else {
                    DispatchQueue.main.async(execute: {() -> Void in
                        self.accessMethod("Please go to Settings and enable the photos for this app to use this feature.")
                    })
                }
            })
        }
        else if status == .restricted {
            // Restricted access - normally won't happen.
        }
        
        
    }

    func accessMethod(_ message: String) {
        let alertController = UIAlertController(title: "Not Authorized", message: message, preferredStyle: .alert)
        let cancel = UIAlertAction(title: "OK", style: .default, handler: nil)
        let Settings = UIAlertAction(title: "Settings", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
        })
        alertController.addAction(Settings)
        alertController.addAction(cancel)
        present(alertController, animated: true, completion: { _ in })
    }
    
    //MARK: UIImagePickerControllerDelegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        var chosenImage: UIImage? = info[UIImagePickerControllerEditedImage] as! UIImage?
        chosenImage = resizeImage(image: chosenImage!, newWidth: 400)

        picker.dismiss(animated: true, completion: { _ in })
        productImg.image = chosenImage
        isImage = true
    }
   
    //MARK: Resize Image
    
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0,y: 0,width: newWidth,height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    

    // MARK: Button Actions
    @IBAction func Photo(_ sender: Any) {
        
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let camera = UIAlertAction(title: "Camera", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            self.checkCameraAuthorization()
        })
        let gallery = UIAlertAction(title: "Gallery", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            self.checkPhotoAuthorization()
        })
        alertController.addAction(gallery)
        alertController.addAction(camera)
        alertController.addAction(cancel)
        present(alertController, animated: true, completion: { _ in })
        
        
    }

    
    // MARK: AlertView
    
    func alertViewMethod(titleStr:String, messageStr:String) {
        let alert = UIAlertController(title: titleStr, message: messageStr, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    

    // MARK: Buttons Action
    @IBAction func Back(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }

    @IBAction func AddProduct(_ sender: Any) {
        
        if titleTxt.text == "" || priceTxt.text == "" || categoryTxt.text == "" {
            alertViewMethod(titleStr: "", messageStr: "Please enter the all fields.")

        }
        
        else if(isImage == false) {
            
            alertViewMethod(titleStr: "", messageStr: "Please upload the image first.")

        }
        
        else if (NSString(string: priceTxt.text!).floatValue < 0.5){
         
            let objAlertController = UIAlertController(title: "" ,message: "You cannot enter value less than 0.50", preferredStyle: UIAlertControllerStyle.alert)
            let objAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler:nil)
            objAlertController.addAction(objAction)
           self.present(objAlertController, animated: true, completion: nil)
            
        }
            
        else {
            indicator.startAnimating()
            indicator.isHidden = false
            uploadImageDataMethod()
            
        }
    }

    
    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        
//        
//        if textField == priceTxt {
//            var newString: String = (priceTxt.text! as NSString).replacingCharacters(in: range, with: string)
//            let val = Double(newString)
//            
//            if val! < 0.50 {
//                let objAlertController = UIAlertController(title: "" ,message: "You cannot enter value less than 0.50", preferredStyle: UIAlertControllerStyle.alert)
//                let objAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler:nil)
//                objAlertController.addAction(objAction)
//                self.present(objAlertController, animated: true, completion: nil)
//                return false
//                
//            }
//            
//        }
//        return true
//        
//        
//    }
//    
    
    
    // MARK: Add Product Method
    
    // Upload Photo
    func uploadImageDataMethod()  {
        
        
        uniquePhotoNameStr = UUID().uuidString
        uniquePhotoNameStr = "http://beta.brstdev.com/freedrinkz/uploads/\(uniquePhotoNameStr).jpeg"
        print(uniquePhotoNameStr)
        
        
        let parameters = [
            "profile_pic": uniquePhotoNameStr,
            "image_type": "2",
            "is_register": "0"
        ]

        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(UIImagePNGRepresentation(self.productImg.image!)!, withName: "profile_pic", fileName: self.uniquePhotoNameStr, mimeType: "image/jpeg")
            for (key, value) in parameters {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
        }, to:"http://beta.brstdev.com/freedrinkz/api/web/v1/users/getprofilepic")
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    //Print progress
                    //  print(progress)
                    
                })
                
                upload.responseJSON { response in
                    //print response.result
                    print(response)
                    
                    if response.result.value != nil {
                        
                        let json = JSON(data: response.data! )
                        if let descriptionStr = json["description"].string {
                            print(descriptionStr)
                            if descriptionStr == "Image uploads Successfully" {
                                
                                let image = json["data"]["profile_pic_url"].string
                                self.uniquePhotoNameStr = image!
                                
                            }
                        }
                        
                    }
                    
                    if (self.isEdit) {
                        self.editProductMethod()
                    } else {
                        self.addProductMethod()
                    }
                    
                }
                
            case .failure(let encodingError):
                print(encodingError.localizedDescription)
                if (self.isEdit) {
                    self.editProductMethod()
                } else {
                    self.addProductMethod()
                }
                
                break
                //print encodingError.description
                
            }
        }
    }
    
    
    
    func addProductMethod () {
        
        let  json = ["user_id":Singleton.sharedInstance.userIdStr,
                     "auth_token":Singleton.sharedInstance.userTokenStr,
                     "title":titleTxt.text!,
                     "productimage":uniquePhotoNameStr,
                     "category": String(describing: Singleton.sharedInstance.categoryArray[categoryTxt.selectedRow]["id"]!),
                     "price":priceTxt.text!
            
            ] as [String : Any]
        
        print(json)
        
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/users/resturantmenu", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    let json = JSON(data: response.data! )
                    if let descriptionStr = json["description"].string {
                        print(descriptionStr)
                        if descriptionStr == "Product added Successfully" {
                            self.alertViewMethod(titleStr: "", messageStr: "Product has been successfully added.")

                            self.priceTxt.text = ""
                            self.categoryTxt.text = ""
                            self.titleTxt.text = ""
                            self.productImg.image = UIImage(named: "gallery")
                            self.categoryTxt.setSelectedRow(0, animated: false)

                        } else if descriptionStr == "Wrong auth token" {

                            UserDefaults.standard.setValue(nil, forKey: "auth_token")
                            UserDefaults.standard.setValue(nil, forKey: "user_id")
                            UserDefaults.standard.setValue(nil, forKey: "user_type")
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "Login") as! Login
                            vc.isExpired = true
                            self.navigationController?.pushViewController(vc,animated: true)
                            
                        }
                        
                        
                        //Now you got your value
                    }
                } else if((response.error) != nil){
                    
                    self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                    
                }

                
                self.indicator.stopAnimating()
                self.indicator.isHidden = true
                
        }
        
    }
    
    // MARK: Edit Product Method

    func editProductMethod () {
        var catId = String()
        if categoryTxt.selectedRow == -1 {
            catId = catIdStr
        } else {
            catId = String(describing: Singleton.sharedInstance.categoryArray[categoryTxt.selectedRow]["id"]!)
        }
        
        
        let  json = ["user_id":Singleton.sharedInstance.userIdStr,
                     "auth_token":Singleton.sharedInstance.userTokenStr,
                     "title":titleTxt.text!,
                     "productimage":uniquePhotoNameStr,
                     "category": catId,
                     "price":priceTxt.text!,
                     "product_id": productIdStr
            
            ] as [String : Any]
        

        print(json)
        
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/users/editproducts", method: .put, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    let json = JSON(data: response.data! )
                    if let descriptionStr = json["description"].string {
                        print(descriptionStr)
                        if descriptionStr == "Product updated Successfully" {
                         //   self.alertViewMethod(titleStr: "", messageStr: "Product has been successfully added.")
                            
                            self.priceTxt.text = ""
                            self.categoryTxt.text = ""
                            self.titleTxt.text = ""
                            self.productImg.image = UIImage(named: "gallery")
                            
                            _ = self.navigationController?.popViewController(animated: true)

                        } else if descriptionStr == "Wrong auth token" {
                         
                            UserDefaults.standard.setValue(nil, forKey: "auth_token")
                            UserDefaults.standard.setValue(nil, forKey: "user_id")
                            UserDefaults.standard.setValue(nil, forKey: "user_type")
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "Login") as! Login
                            vc.isExpired = true
                            self.navigationController?.pushViewController(vc,animated: true)
                            
                        }
                        //Now you got your value
                    }
                    else if let messageStr = json["message"].string {
                    
                        if messageStr == "Missing parameter" {
                            
                            UserDefaults.standard.setValue(nil, forKey: "auth_token")
                            UserDefaults.standard.setValue(nil, forKey: "user_id")
                            UserDefaults.standard.setValue(nil, forKey: "user_type")
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "Login") as! Login
                            vc.isExpired = true
                            self.navigationController?.pushViewController(vc,animated: true)
                            
                        }

                    }
                } else if((response.error) != nil){
                    
                    self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                    
                }

                
                self.indicator.stopAnimating()
                self.indicator.isHidden = true
                
        }
        
    }
    
    
    


    // MARK: Text Field Delegate
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool { // return NO to not change text
        

        if textField == priceTxt {
           
//            guard let text = textField.text else { return true }
//            let newLength = text.characters.count + string.characters.count - range.length
//            if (newLength >= 6) {
//                return newLength <= 6
//            }
            
            
            
            
            switch string {
            case "0","1","2","3","4","5","6","7","8","9":

                return true
            case ".":
                let array = Array(textField.text!.characters)
                var decimalCount = 0
                for character in array {
                    if character == "." {
                        decimalCount += 1
                    }
                }
                
                if decimalCount == 1 {
                    return false
                } else {
                    return true
                }
            default:
                let array = Array(string.characters)
                if array.count == 0 {
                    return true
                }
                return false
            }
        }

        
    return true

}
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        titleTxt.resignFirstResponder()
        categoryTxt.resignFirstResponder()
        priceTxt.resignFirstResponder()
        
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool  {
        textField.resignFirstResponder()
        return true;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
