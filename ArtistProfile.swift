//
//  ArtistProfile.swift
//  FreeDrinkz
//
//  Created by Brst-Pc109 on 28/03/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

import UIKit
import SideMenu
import Alamofire
import SwiftyJSON
import TwitterKit
import FBSDKCoreKit.FBSDKGraphRequest
import FBSDKLoginKit
import IQKeyboardManagerSwift
import Firebase
import MapKit
import CoreLocation
import AVKit


class ArtistProfile: UIViewController,UITableViewDelegate, UITableViewDataSource, UITextViewDelegate,MKMapViewDelegate,CLLocationManagerDelegate,UIGestureRecognizerDelegate {
  
    // 24 May :-
    var frndUnfrndStr = String()
    
    // 12 June :-
    var userId = String()
    var userTypee = String()
    
    
    @IBOutlet weak var profileTbl: UITableView!
    @IBOutlet weak var detailTbl: UITableView!
    
    @IBOutlet weak var postLbl: UILabel!
    @IBOutlet weak var detailsLbl: UILabel!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var detailView: UIView!
    @IBOutlet weak var changeLbl: UILabel!
    
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var searchView: UIView!
    
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    var fromNavStr = String()
    var nameStr = String()
    var locationStr = String()
    var userImgStr = String()
    var uploadAboutTxt = String()
    var locationManager = CLLocationManager()
    var facebookFeedsArray = NSMutableArray()
    var postArray = NSMutableArray()
    var twitterFeedsArray = [JSON]()
    var socialPostsArray = [JSON]()
    var performanceArray = [JSON]()
    var artistId = String()
    var aboutStr = String()
    var typeStr = String()
    var isConnected = Bool()
    var isEdit = Bool()
    var userType =  String()
    var headerCell = ArtistHeader()
    var ViewForDoneButtonOnKeyboard = UIToolbar()
    var socialFeedsArray = NSMutableArray()
    var notificationSetting = String()
    var  locationValue = String()
    var addressString = String()
    var currentlatitudeStr = Double()
    var currentlongitudeStr = Double()
    var lat = Double()
    var long = Double()
    var videoUrl = NSURL()
    var feedStr = String()
    var feedsArray = NSMutableArray()
    var paginationDict = NSDictionary()
    var type = String()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if Singleton.sharedInstance.socialLoginType == "" {
            Singleton.sharedInstance.socialLoginType = "1"
            UserDefaults.standard.setValue(Singleton.sharedInstance.socialLoginType, forKey: "socialLoginType")
        }
        
        
        detailTbl.sectionHeaderHeight = UITableViewAutomaticDimension;
        detailTbl.estimatedSectionHeaderHeight = 25;
        
        
        self.navigationController?.isNavigationBarHidden = true
        
        setupSideMenu()
        
        
        if fromNavStr == "other" {
            artistProfileMethod()
            
            // 24 May :-
           backView.isHidden = false
            
            //searchView.isHidden = true
        } else {
            
            artistId = Singleton.sharedInstance.userIdStr
        
            artistProfileMethod()
            twitterMethod()
            fetchFacebookFeeds()
            
        }
        ViewForDoneButtonOnKeyboard.sizeToFit()
        
        let btnDoneOnKeyboard3 = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneBtnFromKeyboardClicked))
        
        let btnDoneOnKeyboard1 = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelBtnFromKeyboardClicked))
        let btnDoneOnKeyboard2 = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        ViewForDoneButtonOnKeyboard.items = [btnDoneOnKeyboard1, btnDoneOnKeyboard2, btnDoneOnKeyboard3]
        
        
        
        profileTbl.register(UINib(nibName: "ArtistHeader", bundle: nil), forCellReuseIdentifier: "ArtistHeader")
        detailTbl.register(UINib(nibName: "ArtistHeader", bundle: nil), forCellReuseIdentifier: "ArtistHeader")
        headerCell = profileTbl.dequeueReusableCell(withIdentifier: "ArtistHeader") as! ArtistHeader
        
        
        // Mark: Checking whether location is on/off
        let locationStatus = UserDefaults.standard.value(forKey: "locationKey") as? String
        if locationStatus == nil{
            
        }
        else{
            
            self.locationValue = (UserDefaults.standard.value(forKey: "locationKey") as? String)!
            
        }
        
        // Mark: Fetching the current saved address
        let address = UserDefaults.standard.value(forKey: "savedAddress") as? String
        if address == nil{
            
        }
        else{
            
            
            self.addressString = (UserDefaults.standard.value(forKey: "savedAddress") as? String)!
            
        }
       //MARK: Notification Center Work
        NotificationCenter.default.addObserver(self, selector:#selector(self.videoPath(_:)), name:NSNotification.Name(rawValue: "notificationName"), object: nil)
        
        // 14 June :-
       //   newFeedsSection()
        
        // 14 June :-
        self.LongPressGesture()
        
    }
    
    // 14 June :-
    func LongPressGesture() {
        //Long Press
        let longPressGesture:UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
        longPressGesture.minimumPressDuration = 0.5
        longPressGesture.delegate = self
        self.profileTbl.addGestureRecognizer(longPressGesture)
    }
    
    func handleLongPress(longPressGesture:UILongPressGestureRecognizer) {
        
        let p = longPressGesture.location(in: self.profileTbl)
        let indexPath = self.profileTbl.indexPathForRow(at: p)
        
        let alert = UIAlertController(title:"", message:"Are you sure you want to delete?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: {(action:UIAlertAction!) in
            
            if indexPath != nil {
                let dict =  self.feedsArray[(indexPath?.row)!] as? NSDictionary
                let id = dict?.value(forKey:"id") as? String
                self.deleteFeed(requestId:id!)
            }
            
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    //MARK: deleting the feeds
    func deleteFeed(requestId: String) {
        
        self.view.isUserInteractionEnabled = false
        
        let json = ["user_id":Singleton.sharedInstance.userIdStr,
                    "auth_token":Singleton.sharedInstance.userTokenStr,
                    "feed_id":requestId
            ] as [String : Any]
        
        print(json)
        
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/users/deleteuserfeeds", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
                self.view.isUserInteractionEnabled = true
                print(response.result)   // result of response serialization
                print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    
                    self.newFeedsSection()
                    
                    // self.imBuyimgTbl.reloadData()
                    if((response.error) != nil){
                        self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                    }
                    self.indicator.stopAnimating()
                    self.indicator.isHidden = true
                }
                
        }
        
    }
    
    
    
    
    
    
    override func viewWillDisappear(_ animated: Bool) {
        
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
        
    }
    override func viewWillAppear(_ animated: Bool) {
        
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = 0.5
        locationManager.delegate = self as CLLocationManagerDelegate
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        
        // 14 June :-
        newFeedsSection()
        
        profileTbl.reloadData()
        detailTbl.reloadData()
    }
    
    
    //MARK: Video Path Method
    func videoPath(_ notification: NSNotification) {
        
        if let videoUrlTest = notification.userInfo?["video"]  {
            
            self.videoUrl = videoUrlTest as! NSURL
            // self.capture(outputFileURL:self.videoUrl)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "SearchRestaurant") as! SearchRestaurant
            vc.videoUrl =  self.videoUrl
            self.navigationController?.pushViewController(vc,animated: true)
            
        }
    }
    
    //MARK: Location manager delegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        
        
        DataManager.currentLat = locations.last?.coordinate.latitude
        DataManager.currentLong = locations.last?.coordinate.longitude
        self.currentlatitudeStr =  DataManager.currentLat!
        self.currentlongitudeStr = DataManager.currentLong!
        UserDefaults.standard.setValue( self.currentlatitudeStr, forKey: "currentlatitude")
        UserDefaults.standard.setValue( self.currentlongitudeStr, forKey: "currentlongitude")
     
        
        
        self.locationManager.stopUpdatingLocation()
        
        let dropPin = MKPointAnnotation()
        // For other user profile - for showing lat long
        if fromNavStr == "other" && self.notificationSetting == "1" {
            
            lat = currentlatitudeStr
            long = currentlongitudeStr
            let userLocation = CLLocationCoordinate2DMake(lat, long)
            dropPin.coordinate = userLocation
            dropPin.title = self.addressString
        }
            
        else if self.notificationSetting == "0" && fromNavStr == "other" {
            
            lat = (Singleton.sharedInstance.latitudeStr as NSString).doubleValue
            long = (Singleton.sharedInstance.longitudeStr as NSString).doubleValue
            let userLocation = CLLocationCoordinate2DMake(lat, long)
            dropPin.coordinate = userLocation
            dropPin.title = locationStr
        }
        return
        
    }
    
    // Getting current address from current latitude and longitude
    func getAddressFromLatLon(pdblLatitude: Double, withLongitude pdblLongitude: Double) {
        
        
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = pdblLatitude
        center.longitude = pdblLongitude
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                
                let pm = placemarks! as [CLPlacemark]
                if pm.count > 0 {
                    let pm = placemarks![0]
                    print(pm.country ?? String())
                    print(pm.locality  ?? String())
                    print(pm.subLocality  ?? String())
                    print(pm.postalCode  ?? String())
                    
                    //  self.addressString = ""
                    if pm.subLocality != nil {
                        self.addressString =  self.addressString + pm.subLocality! + ", "
                    }
                    
                    if pm.locality != nil {
                        self.addressString = self.addressString + pm.locality! + ", "
                    }
                    if pm.country != nil {
                        self.addressString = self.addressString + pm.country! + ", "
                    }
                    if pm.postalCode != nil {
                        self.addressString = self.addressString + pm.postalCode! + " "
                    }
                    print(self.addressString)
                    
                }
                
        })
        
    }
    
    
    //MARK: New Feeds Section Api Implementation
    func newFeedsSection(){
        
        feedStr = "Feeds"
        let userId = Singleton.sharedInstance.userIdStr
        let authToken = Singleton.sharedInstance.userTokenStr
       // DispatchQueue.main.async {
            
            //self.activityIndicator.isHidden = false
            // self.activityIndicator.startAnimating()
            
       // }
        var parameterDict = JSONDictionary()
        parameterDict["user_id"] = userId
        parameterDict["auth_token"] = authToken
        parameterDict["other_user_id"] = ""
        print(parameterDict)
        ApiExtension.sharedInstance.getFeeds(parameter:parameterDict, completionHandler:{(result)in
            
            let data = result.value(forKey:"data") as? NSDictionary
            let statusCode = data?.value(forKey:"Success_code") as? String
            if  statusCode == "201"{
                
                let array = data?.value(forKey:"feed_list_share") as? NSArray
                self.feedsArray = (array?.mutableCopy() as? NSMutableArray)!
                self.paginationDict = (data?.value(forKey:"meta") as? NSDictionary)!
                self.profileTbl.reloadData()
                
            }
            
        })
        
    }
    
    
    // MARK: Facebook Method
    
    func fetchFacebookFeeds() {
        let params: [AnyHashable: Any] = ["fields": "id, name, story, message, from, picture, object_id, created_time, description"]
        
        /* make the API call */
        let request : FBSDKGraphRequest  = FBSDKGraphRequest(graphPath: "/me/feed", parameters: params, httpMethod: "GET")
        
        
        request.start(completionHandler: { (connection, result,  error) in
            // Handle the result

            if (result != nil) {
                let dictFromJSON = result as! NSDictionary
                print("result: \(dictFromJSON)")
                self.facebookFeedsArray = (dictFromJSON["data"]! as! NSArray).mutableCopy() as! NSMutableArray
                print("result: \(self.facebookFeedsArray)")
                print("count: \(self.facebookFeedsArray.count)")
                if self.facebookFeedsArray.count>0 {
                    self.saveSocialMediaFeeds()
                } else {
                    if self.isConnected == true {
                        self.artistProfileMethod()
                    }
                }
                
                self.profileTbl.reloadData()
                
                
            }
            
            self.indicator.stopAnimating()
            self.indicator.isHidden = true
            
            
            
        })
        
    }
    
    // MARK: Twitter Method
    
    func twitterMethod() {
        
        let userID = Twitter.sharedInstance().sessionStore.session()?.userID
        let client = TWTRAPIClient(userID: userID)
        //        let statusesShowEndpoint = "https://api.twitter.com/1.1/statuses/home_timeline.json"
        //        let params = ["exclude_replies":"true","include_entities":"false","count": "15"]
        
        let statusesShowEndpoint = "https://api.twitter.com/1.1/statuses/user_timeline.json"
        let params = ["count": "5"]
        
        var clientError : NSError?
        
        let request = client.urlRequest(withMethod: "GET", url: statusesShowEndpoint, parameters: params, error: &clientError)
        
        client.sendTwitterRequest(request) { (response, data, connectionError) -> Void in
            if connectionError != nil {

            } else {
                let jsonVal = JSON(data: data! )
                self.twitterFeedsArray = jsonVal.array!
                print(self.twitterFeedsArray)
                if self.twitterFeedsArray.count>0 {
                    self.saveTwitterFeeds()
                }
                
                self.profileTbl.reloadData()
            }
            
            self.indicator.stopAnimating()
            self.indicator.isHidden = true
            
        }
        
    }
    
    // MARK: Social Media Feeds Method
    
    
    func saveTwitterFeeds() {
        
        
        let array = NSMutableArray()
        
        for item in twitterFeedsArray {
            let stringg = item["text"].string!
            let result = stringg.replacingOccurrences(of: "[^\\x00-\\x7F]", with: "", options: NSString.CompareOptions.regularExpression, range: nil)
            print(result)
            
            
            let nameStrr = item["user"]["name"].string!
            let nameResult = nameStrr.replacingOccurrences(of: "[^\\x00-\\x7F]", with: "", options: NSString.CompareOptions.regularExpression, range: nil)
            
            
            let image = item["user"]["profile_image_url_https"].string!
            let matchInfo: NSDictionary = ["id": String(describing: item["id"].int!), "created_time": item["created_at"].string!, "description": result, "from": ["name": nameResult, "id": image]]
            
            
            array.add(matchInfo)
            
        }
        
        
        //        let array1 = Array(array.prefix(1))
        //        print(array1.count)
        
        
        let json = ["user_id":Singleton.sharedInstance.userIdStr,
                    "auth_token":Singleton.sharedInstance.userTokenStr,
                    "feeds_type":"2",
                    "data": array
            ] as NSDictionary
        
        print(json)
        
        
        
        let postData: Data? = try? JSONSerialization.data(withJSONObject: json, options: [])
        
        let request = NSMutableURLRequest(url: NSURL(string: "http://beta.brstdev.com/freedrinkz/api/web/v1/users/savefeeds")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.httpBody = postData! as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {

            } else {
                let dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                
                if dataString != nil {
                    let json = JSON(data: data! )
                    if let descriptionStr = json["description"].string {
                        print(descriptionStr)
                        if descriptionStr == "Feed added Successfully" {
                            print("added")
                            if self.isConnected == true {
                                self.artistProfileMethod()
                            } else {
                                self.artistProfileMethod()
                            }
                        }
                        //Now you got your value
                    }
                }
                
                
                
            }
        })
        
        dataTask.resume()
        
    }
    
    
    
    
    
    func saveSocialMediaFeeds() {
        
        
        //        let array = Array(facebookFeedsArray.prefix(7))
        //        print(array.count)
        
        for (index, element) in facebookFeedsArray.enumerated() {
            
            var dict = element as!  [String: Any]
            
            if dict["description"] != nil {
                let stringg = dict["description"]! as! String
                let result = stringg.replacingOccurrences(of: "[^\\x00-\\x7F]", with: "", options: NSString.CompareOptions.regularExpression, range: nil)
                dict["description"] = result
            }
            if dict["story"] != nil {
                let stringg = dict["story"]! as! String
                let result = stringg.replacingOccurrences(of: "[^\\x00-\\x7F]", with: "", options: NSString.CompareOptions.regularExpression, range: nil)
                dict["story"] = result
            }
            
            if dict["message"] != nil {
                let stringg = dict["message"]! as! String
                let result = stringg.replacingOccurrences(of: "[^\\x00-\\x7F]", with: "", options: NSString.CompareOptions.regularExpression, range: nil)
                dict["message"] = result
            }
            facebookFeedsArray.replaceObject(at: index, with: dict)
            
        }
        
        let json = ["user_id":Singleton.sharedInstance.userIdStr,
                    "auth_token":Singleton.sharedInstance.userTokenStr,
                    "feeds_type":"1",
                    "data": facebookFeedsArray
            ] as NSDictionary
        
        print(json)
        
        let postData: Data? = try? JSONSerialization.data(withJSONObject: json, options: [])
        
        
        
        
        let request = NSMutableURLRequest(url: NSURL(string: "http://beta.brstdev.com/freedrinkz/api/web/v1/users/savefeeds")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.httpBody = postData! as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {

            } else {

                
                let dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)

                
                if dataString != nil {
                    let json = JSON(data: data! )
                    if let descriptionStr = json["description"].string {
                        print(descriptionStr)
                        if descriptionStr == "Feed added Successfully" {
                            print("added")
                            if self.isConnected == true {
                                self.artistProfileMethod()
                            } else {
                                self.artistProfileMethod()
                            }
                            
                        }
                        //Now you got your value
                    }
                }
                
                
                
            }
        })
        
        dataTask.resume()
        
    }
    
    
    // MARK: Get Artist Profile Method
    
    func artistProfileMethod () {
        
      let json = ["user_id":Singleton.sharedInstance.userIdStr,
                    "auth_token":Singleton.sharedInstance.userTokenStr,
                    "other_user_id":artistId
            ] as [String : Any]
        
           Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/members/artistprofile", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                 if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    let json = JSON(data: response.data! )
                    if let descriptionStr = json["description"].string {
                        print(descriptionStr)
                        if descriptionStr == "Wrong auth token" {
                            
                            
                            UserDefaults.standard.setValue(nil, forKey: "auth_token")
                            UserDefaults.standard.setValue(nil, forKey: "user_id")
                            UserDefaults.standard.setValue(nil, forKey: "user_type")
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "Login") as! Login
                            vc.isExpired = true
                            self.navigationController?.pushViewController(vc,animated: true)
                            
                        }  else if descriptionStr == "Normal profile with feeds list" {
                            
                            
                            
                            if self.fromNavStr == "other" {
                                
                                if json["data"]["User_data"][0]["profile_pic_url"].string != nil {
                                    self.userImgStr =  String(describing: json["data"]["User_data"][0]["profile_pic_url"].string!)
                                    
                                } else {
                                    self.userImgStr = ""
                                    
                                }
                                
                                if json["data"]["User_data"][0]["notification_setting"].string != nil {
                                    self.notificationSetting = String(describing: json["data"]["User_data"][0]["notification_setting"].string!)
                                }
                                
                                if json["data"]["User_data"][0]["firstname"].string != nil {
                                    self.nameStr =  String(describing: json["data"]["User_data"][0]["firstname"].string!)
                                } else {
                                    self.nameStr =  ""
                                }
                                
                                
                                if json["data"]["User_data"][0]["lastname"].string != nil {
                                    self.nameStr = "\(self.nameStr) \(String(describing: json["data"]["User_data"][0]["lastname"].string!))"
                                }
                                
//                                if json["data"]["User_data"][0]["address"].string != nil {
//                                    self.locationStr =  String(describing: json["data"]["User_data"][0]["address"].string!)
//                                } else {
//                                    self.locationStr =  ""
//                                }
                                
                                // 24 May :- (Checking is that particular Frnd is user's frnd or not)
                                if json["data"]["User_data"][0]["Friend_Unfriend"] == 1
                                {
                                    self.frndUnfrndStr = "1"
                                    let image = UIImage(named: "remove.png")
                                  //  self.addFrndBtn.setBackgroundImage(image, for: .normal)
                                }
                                else {
                                    self.frndUnfrndStr = "0"
                                    let image = UIImage(named: "add_friend.png")
                                 //   self.addFrndBtn.setBackgroundImage(image, for: .normal)
                                }
                                
                                // 6 June :-
                                if json["data"]["User_data"][0]["city"].string != nil {
                                    self.locationStr =  "\(String(describing: json["data"]["User_data"][0]["city"].string!))"//\(self.locationStr),
                                }
                                
                                if json["data"]["User_data"][0]["state"].string != nil {
                                    self.locationStr =  "\(self.locationStr), \(String(describing: json["data"]["User_data"][0]["state"].string!))"
                                }
                                
                                if json["data"]["User_data"][0]["zip_code"].string != nil {
                                    self.locationStr =  "\(self.locationStr), \(String(describing: json["data"]["User_data"][0]["zip_code"].string!))"
                                }
                                
                                
                            }  else {
                                
                             
                                if json["data"]["User_data"][0]["gender"].string != nil {
                                    Singleton.sharedInstance.genderStr =  String(describing: json["data"]["User_data"][0]["gender"].string!)
                                } else {
                                    Singleton.sharedInstance.genderStr =  ""
                                }
                                
                                if json["data"]["User_data"][0]["address"].string != nil {
                                    Singleton.sharedInstance.addressStr =  String(describing: json["data"]["User_data"][0]["address"].string!)
                                    //self.locationStr = Singleton.sharedInstance.addressStr

                                } else {
                                    Singleton.sharedInstance.addressStr =  ""
                                    self.locationStr = ""

                                }
                               // \(self.locationStr),
                                if json["data"]["User_data"][0]["city"].string != nil {
                                    Singleton.sharedInstance.cityStr =  String(describing: json["data"]["User_data"][0]["city"].string!)
                                    
                                    self.locationStr =  "\(String(describing: json["data"]["User_data"][0]["city"].string!))"
                                    
                                    
                                } else {
                                    Singleton.sharedInstance.cityStr =  ""
                                }
                                
                                if json["data"]["User_data"][0]["state"].string != nil {
                                    Singleton.sharedInstance.stateStr =  String(describing: json["data"]["User_data"][0]["state"].string!)
                                    
                                    self.locationStr =  "\(self.locationStr), \(String(describing: json["data"]["User_data"][0]["state"].string!))"
                                    
                                } else {
                                    Singleton.sharedInstance.stateStr =  ""
                                }
                                
                                if json["data"]["User_data"][0]["country"].string != nil {
                                    Singleton.sharedInstance.countryStr =  String(describing: json["data"]["User_data"][0]["country"].string!)
                                    
                                    self.locationStr =  "\(self.locationStr), \(String(describing: json["data"]["User_data"][0]["country"].string!))"
                                    
                                    
                                } else {
                                    Singleton.sharedInstance.countryStr =  ""
                                }
                                
                               if json["data"]["User_data"][0]["zip_code"].string != nil {
                                    Singleton.sharedInstance.zipcodeStr =  String(describing: json["data"]["User_data"][0]["zip_code"].string!)
                                    
                                    self.locationStr =  "\(self.locationStr), \(String(describing: json["data"]["User_data"][0]["zip_code"].string!))"
                                    
                                } else {
                                    Singleton.sharedInstance.zipcodeStr =  ""
                                }
                                
                                
                                
                                if json["data"]["User_data"][0]["profile_pic_url"].string != nil {
                                    Singleton.sharedInstance.userImgStr =  String(describing: json["data"]["User_data"][0]["profile_pic_url"].string!)
                                    Singleton.sharedInstance.userImgStr =  Singleton.sharedInstance.userImgStr.replacingOccurrences(of: "_normal", with: "")

                                    self.userImgStr = Singleton.sharedInstance.userImgStr
                                    
                                } else {
                                    Singleton.sharedInstance.userImgStr =  ""
                                    self.userImgStr = ""
                                    
                                }
                                if json["data"]["User_data"][0]["email"].string != nil {
                                    Singleton.sharedInstance.emailStr =  String(describing: json["data"]["User_data"][0]["email"].string!)
                                } else {
                                    Singleton.sharedInstance.emailStr =  ""
                                }
                                if json["data"]["User_data"][0]["age"].int != nil {
                                    Singleton.sharedInstance.ageStr =  String(describing: json["data"]["User_data"][0]["age"].int!)
                                } else {
                                    Singleton.sharedInstance.ageStr =  ""
                                }
                                
                                
                                if json["data"]["User_data"][0]["firstname"].string != nil {
                                    Singleton.sharedInstance.firstNameStr =  String(describing: json["data"]["User_data"][0]["firstname"].string!)
                                } else {
                                    Singleton.sharedInstance.firstNameStr =  ""
                                }
                                
                                
                                if json["data"]["User_data"][0]["lastname"].string != nil {
                                    Singleton.sharedInstance.lastNameStr =  String(describing: json["data"]["User_data"][0]["lastname"].string!)
                                } else {
                                    Singleton.sharedInstance.lastNameStr =  ""
                                }
                                
                                
                                
                                
                                
                                self.nameStr = "\(Singleton.sharedInstance.firstNameStr) \(Singleton.sharedInstance.lastNameStr)"
                                
                                
                                if json["data"]["User_data"][0]["band_name"].string != nil {
                                    Singleton.sharedInstance.bandNameStr =  String(describing: json["data"]["User_data"][0]["band_name"].string!)
                                } else {
                                    Singleton.sharedInstance.bandNameStr =  ""
                                }
                                
                                
                                if json["data"]["User_data"][0]["latitude"].string != nil {
                                    Singleton.sharedInstance.latitudeStr =  String(describing: json["data"]["User_data"][0]["latitude"].string!)
                                    UserDefaults.standard.setValue(Singleton.sharedInstance.latitudeStr, forKey: "latitude")

                                } else {
                                    Singleton.sharedInstance.latitudeStr =  "0.0"
                                }
                                
                                if json["data"]["User_data"][0]["longitude"].string != nil {
                                    Singleton.sharedInstance.longitudeStr =  String(describing: json["data"]["User_data"][0]["longitude"].string!)
                                    UserDefaults.standard.setValue(Singleton.sharedInstance.longitudeStr, forKey: "longitude")

                                } else {
                                    Singleton.sharedInstance.longitudeStr =  "0.0"
                                }
                                
                                
                                var token = String()
                                if FIRInstanceID.instanceID().token() != nil {
                                    token = FIRInstanceID.instanceID().token()!
                                } else {
                                    token = ""
                                }
                                let values = ["name": self.nameStr as String, "userid": Singleton.sharedInstance.userIdStr as String, "fcm_token": token, "profile_pic_url": Singleton.sharedInstance.userImgStr] as [String : Any]
                                FIRDatabase.database().reference().child("users").child(Singleton.sharedInstance.userIdStr).child("credentials").updateChildValues(values, withCompletionBlock: { (errr, _) in
                                    if errr == nil {
                                        
                                    }
                                })
                                
                            }
                            
                            
                            
                            if json["data"]["User_data"][0]["about_text"].string != nil {
                                self.aboutStr =  String(describing: json["data"]["User_data"][0]["about_text"].string!)
                            } else {
                                self.aboutStr =  "There is no description about artist."
                            }
                            
                            if self.aboutStr == "" {
                                self.aboutStr =  "There is no description about artist."
                            }
                            
                            self.uploadAboutTxt = self.aboutStr
                            
                            self.headerCell.aboutLbl.text = self.aboutStr
                            
                            self.performanceArray = json["data"]["Performance"].array!
                            
                            self.profileTbl.reloadData()
                            self.detailTbl.reloadData()
                        }
                        //Now you got your value
                    }
                } else if((response.error) != nil){
                    
                    self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                    
                }
                self.indicator.stopAnimating()
                self.indicator.isHidden = true
                
        }
        
    }
    
    
    
    // MARK: AlertView
    
    func alertViewMethod(titleStr:String, messageStr:String) {
        let alert = UIAlertController(title: titleStr, message: messageStr, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    fileprivate func setupSideMenu() {
        // Define the menus
        SideMenuManager.menuLeftNavigationController = storyboard!.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as? UISideMenuNavigationController
        
        // Enable gestures. The left and/or right menus must be set up above for these to work.
        // Note that these continue to work on the Navigation Controller independent of the View Controller it displays!
        SideMenuManager.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
        SideMenuManager.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
        
        
        // Set up a cool background image for demo purposes
        //     SideMenuManager.menuAnimationBackgroundColor = UIColor(patternImage: UIImage(named: "background")!)
        SideMenuManager.menuAnimationBackgroundColor = UIColor(white: 1, alpha: 0.0)
        
        typeStr = "post"
        //   detailView.isHidden = true
    }
    
    // MARK: Button Actions
    
    func AboutEdit(_ button: UIButton) {
        
        let indexPath = IndexPath(row: 0, section: 0)
        let cell = detailTbl.cellForRow(at: indexPath)  as? ArtistPerformanceCell
        
        
        if isEdit == false {
            isEdit = true
            cell?.aboutTxt.becomeFirstResponder()
            IQKeyboardManager.sharedManager().enableAutoToolbar = false
            
        } else {
            isEdit = false
            cell?.aboutTxt.resignFirstResponder()
            IQKeyboardManager.sharedManager().enableAutoToolbar = true
            
        }
        
    }
    
    
    func Performance(_ button: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SearchRestaurant") as! SearchRestaurant
        vc.isPerformance = true
        navigationController?.pushViewController(vc,animated: true)
    }
    
    func EditInformation(_ button: UIButton) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SearchRestaurant") as! SearchRestaurant
        vc.isPerformance = true
       let editRestroName = performanceArray[button.tag-1]["resturant_name"].string!
       let editSchduleId = performanceArray[button.tag-1]["schdule_id"].int!
       let editRestroId = performanceArray[button.tag-1]["resturant_id"].int!
        if editRestroId != 0 && editRestroName != "" && editSchduleId != 0{

            vc.performanceRestroName = editRestroName
            vc.performanceRestroId = editRestroId
            vc.performanceSchduleId = editSchduleId

        }
        navigationController?.pushViewController(vc,animated: true)
        
    }
    
    func DeleteInformation(_ button: UIButton) {
        
        let alert = UIAlertController(title:"", message:"Are you sure you want to delete?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: {(action:UIAlertAction!) in
            let editSchduleId = self.performanceArray[button.tag-1]["schdule_id"].int!
            self.deleteFunctionality(schduleId:editSchduleId)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func deleteFunctionality(schduleId:Int){
        
        let json = ["user_id":Singleton.sharedInstance.userIdStr,
                    "auth_token":Singleton.sharedInstance.userTokenStr,
                    "schdule_id":schduleId
            ] as [String : Any]
        
        Alamofire.request("http://192.168.1.13/freedrinkz/api/web/v1/members/deleteartist", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    let json = JSON(data: response.data! )
                    if let descriptionStr = json["description"].string {
                        print(descriptionStr)
                        if descriptionStr == "Wrong auth token" {
                            
                            UserDefaults.standard.setValue(nil, forKey: "auth_token")
                            UserDefaults.standard.setValue(nil, forKey: "user_id")
                            UserDefaults.standard.setValue(nil, forKey: "user_type")
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "Login") as! Login
                            vc.isExpired = true
                            self.navigationController?.pushViewController(vc,animated: true)
                            
                        }  else{
                            
                            //DispatchQueue.main.async {
                                
                                self.artistProfileMethod()
                          //  }
                         
                                
                            }
                    
                    }
                } else if((response.error) != nil){
                    
                    self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                    
                }
                self.indicator.stopAnimating()
                self.indicator.isHidden = true
                
        }
    
    
    }
    
    
    
    func ConnectedAction(_ button: UIButton) {
        
        if fromNavStr != "other" {
            isConnected = true
            
            if (button.tag == 0){
                
                
                Singleton.sharedInstance.socialLoginType = "1"
                UserDefaults.standard.setValue(Singleton.sharedInstance.socialLoginType, forKey: "socialLoginType")
                
                if FBSDKAccessToken.current()?.tokenString != nil {
                    self.indicator.startAnimating()
                    self.indicator.isHidden = false
                    self.fetchFacebookFeeds()
                } else {
                    let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
                    fbLoginManager.logIn(withReadPermissions: ["public_profile", "email", "user_friends", "user_posts"], from: self) { (result, error) in
                        if (error == nil){
                            let fbloginresult : FBSDKLoginManagerLoginResult = result!
                            if fbloginresult.grantedPermissions != nil {
                                if(fbloginresult.grantedPermissions.contains("email"))
                                {
                                    
                                    // fbLoginManager.logOut()
                                }
                                
                                self.indicator.startAnimating()
                                self.indicator.isHidden = false
                                self.fetchFacebookFeeds()
                                
                                print("token is \(FBSDKAccessToken.current().tokenString)")
                                print("token is \(FBSDKAccessToken.current().userID)")
                                
                                
                            }
                        }
                    }
                    
                }
                
            } else {
                
                Singleton.sharedInstance.socialLoginType = "2"
                UserDefaults.standard.setValue(Singleton.sharedInstance.socialLoginType, forKey: "socialLoginType")
                
                if Twitter.sharedInstance().sessionStore.session()?.userID != nil {
                    self.indicator.startAnimating()
                    self.indicator.isHidden = false
                    self.twitterMethod()
                } else {
                    Twitter.sharedInstance().logIn { session, error in
                        if (session != nil) {
                            print("signed in as \(session!.userName)");
                            print("signed in as \(session!.userID)");
                            
                            self.indicator.startAnimating()
                            self.indicator.isHidden = false
                            self.twitterMethod()
                        } else {
                            print("error: \(String(describing: error?.localizedDescription))");
                        }
                    }
                }
                
                
            }
        } else {
            if (button.tag == 0){
                Singleton.sharedInstance.socialLoginType = "1"
                UserDefaults.standard.setValue(Singleton.sharedInstance.socialLoginType, forKey: "socialLoginType")
                
                self.indicator.startAnimating()
                self.indicator.isHidden = false
                artistProfileMethod()
                
                
            } else {
                Singleton.sharedInstance.socialLoginType = "2"
                UserDefaults.standard.setValue(Singleton.sharedInstance.socialLoginType, forKey: "socialLoginType")
                
                self.indicator.startAnimating()
                self.indicator.isHidden = false
                artistProfileMethod()
                
                
            }
            
        }
    }
    
    @IBAction func Back(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
  
    // 24 May :-
    @IBAction func addFrndAction(_ sender: Any){
     
            let timestamp = NSDate().timeIntervalSince1970
            print(timestamp)
            print(String(format: "%.0f", (timestamp)))
            let timestampp = (String(format: "%.0f", (timestamp)))
            print(timestampp)
            
            let json = ["user_id":Singleton.sharedInstance.userIdStr,
                        "auth_token":Singleton.sharedInstance.userTokenStr,
                        "otheruser_id":self.artistId,
                        "timestamp" : timestampp
                ] as [String : String]
            print(json)
            
            if(frndUnfrndStr == "1") {
                
                self.indicator.isHidden = false
                self.indicator.startAnimating()
                
                Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/users/unfriend", method: .post, parameters: json, encoding: JSONEncoding.default)
                    .responseJSON { response in
                        
                     print(response.result)   // result of response serialization
                        print(response)
                        if let jsonData = response.result.value {
                            print("JSON: \(jsonData)")
                            //                        let json = JSON(data: response.data! )
                            //                         print(json)
                            
                            let alert = UIAlertController(title: "", message: "Removed as Friend", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            self.frndUnfrndStr = "0"
                            
                            // 22 May :-
                            UserDefaults.standard.set(true, forKey: "ProfileViewFrnd")
                            self.profileTbl.reloadData()
                            
                         
                            
//                            // 23 May :-
//                            let image = UIImage(named: "add_friend.png")
//                            self.addFrndBtn.setBackgroundImage(image, for: .normal)
                            
                            
                        }  else if((response.error) != nil){
                            
                            self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                            self.indicator.stopAnimating()
                            self.indicator.isHidden = true
                            
                        }
                }
            } // ending If-Statement
                
            else {
                
             
                self.indicator.isHidden = false
                self.indicator.startAnimating()
                
                Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/users/friend", method: .post, parameters: json, encoding: JSONEncoding.default)
                    .responseJSON { response in
                        
                        print(response.result)   // result of response serialization
                        print(response)
                        if let jsonData = response.result.value {
                            print("JSON: \(jsonData)")
                            //                    let json = JSON(data: response.data! )
                            //                    print(json)
                            let alert = UIAlertController(title: "", message: "Added as Friend", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            self.frndUnfrndStr = "1"
                            
                            self.profileTbl.reloadData()
                           
                            
//                            let image = UIImage(named: "remove.png")
//                            self.addFrndBtn.setBackgroundImage(image, for: .normal)
                            
                            
                        }
                        else if((response.error) != nil){
                            
                            self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                            self.indicator.stopAnimating()
                            self.indicator.isHidden = true
                            
                        }
                }
            } // ending else
            
     }
    
  @IBAction func PostDetailAction(_ sender: Any) {
        
        if ((sender as AnyObject).tag == 0) {
            //   postLbl.textColor = UIColor.black
            //   detailsLbl.textColor = UIColor.darkGray
            typeStr = "post"
            //   detailView.isHidden = true
            //    changeLbl.text = "Social Media Posts"
            
            profileTbl.isHidden = false
            detailTbl.isHidden = true
            profileTbl.reloadData()
            
        } else if ((sender as AnyObject).tag == 1) {
            //   detailsLbl.textColor = UIColor.black
            //    postLbl.textColor = UIColor.darkGray
            typeStr = "details"
            //    detailView.isHidden = false
            //    changeLbl.text = "Details"
            
            profileTbl.isHidden = true
            detailTbl.isHidden = false
            detailTbl.reloadData()
            
            
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if typeStr == "post" {
            
            
            if feedStr == "Feeds"{
                
                return feedsArray.count
                
            }
            
            if fromNavStr == "other" {
                
                return socialFeedsArray.count;
            }
            return socialFeedsArray.count;
            
        }
        if performanceArray.count == 0 {
            return 2
        }
         return performanceArray.count+1
        
        
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if typeStr != "post" {
            
            
            let cell:ArtistPerformanceCell = detailTbl.dequeueReusableCell(withIdentifier: "ArtistPerformanceCell") as! ArtistPerformanceCell
            if indexPath.row == 0 {
                cell.aboutView.isHidden = false
                cell.aboutTxt.text = aboutStr
                
                if fromNavStr == "other" {
                    cell.editAboutBtn.isHidden = true
                    //cell.editAboutImg.isHidden = true
                    cell.editPerformanceBtn.isHidden = true
                  //  cell.editPerformanceImg.isHidden = true
                    cell.aboutTxt.isEditable = false
                    cell.aboutTxt.isSelectable = false
                }
                cell.editAboutBtn.addTarget(self, action: #selector(AboutEdit(_:)), for: .touchUpInside)
                cell.editPerformanceBtn.addTarget(self, action: #selector(Performance(_:)), for: .touchUpInside)
             //   cell.deleteBtn.tag = indexPath.row
                cell.aboutTxt.delegate = self
                cell.aboutTxt.inputAccessoryView = ViewForDoneButtonOnKeyboard
                cell.editAboutBtn.tag = indexPath.row;
                
               
            } else {
               
                cell.aboutView.isHidden = true
                cell.editBtn.tag = indexPath.row
                cell.editBtn.addTarget(self, action: #selector(EditInformation(_:)), for: .touchUpInside)
                cell.deleteBtn.tag = indexPath.row
                cell.deleteBtn.addTarget(self, action: #selector(DeleteInformation(_:)), for: .touchUpInside)
                
                if performanceArray.count == 0 {
                    cell.performanceLbl.text = "There is no performances added."
                    cell.deleteBtn.setImage(UIImage(named:""), for:UIControlState.normal)
                    cell.editBtn.setImage(UIImage(named:""), for:UIControlState.normal)
                    
                } else {
                  
                    
                    print(performanceArray)
                    let restName = performanceArray[indexPath.row-1]["resturant_name"].string!
                    let timeStamp = String(describing: performanceArray[indexPath.row-1]["performance_time"].int!)
                    let datee = NSDate(timeIntervalSince1970: TimeInterval(timeStamp)!)
                    
                    
                    let timeZone = NSTimeZone.default
                    let dateFormatter = DateFormatter()
                    dateFormatter.timeZone = timeZone
                    dateFormatter.dateFormat = "dd MMM, hh:mm a"
                    let localDateString: String = dateFormatter.string(from: datee as Date)
//                    print(restName)
//                    print(timeStamp)
                    
                    cell.performanceLbl.text = "Performance @ \(restName) - \(localDateString)"
                    
                }
                
                
                print(indexPath.row)
                
            }
            
            
            
            return cell
            
        }
        
        // 12 June :- For Displaying Images in Table-View Cell.
        if feedStr == "Feeds"{
            
            
            let dict = feedsArray[indexPath.row] as? NSDictionary
            print(dict!)
            self.type = (dict?.value(forKey:"type") as? String)!
            if self.type == "image"{
                
                let cellOne:FeedCellImage = profileTbl.dequeueReusableCell(withIdentifier: "FeedCellImage") as! FeedCellImage
                
                // 12 June(For Sending user-id to next screen on feedOtherImgBtn) :-
                cellOne.selectionStyle = .none;
                
                if(dict?.value(forKey:"other_user") as! String != "") && (dict?.value(forKey:"other_user_type") as! String != "")
                {
                    self.userId = dict?.value(forKey:"other_user") as! String
                    self.userTypee = dict?.value(forKey:"other_user_type") as! String
                    
                }
                print(self.userId)
                print(self.userTypee)
                
                
                let otherUserProfilePic = dict?.value(forKey:"img_url") as? String
                let otherUserName = dict?.value(forKey:"other_user_name") as? String
                let userPic = dict?.value(forKey:"other_user_profile_pic") as? String
                cellOne.feedImage.sd_setImage(with:URL(string:otherUserProfilePic!), placeholderImage:UIImage(named:""))
                cellOne.lblFeed.text = "Received free drink from \(String(describing: otherUserName!)) using Free Drinkz App"
                cellOne.feedOtherImg.sd_setImage(with:URL(string:userPic!), placeholderImage:UIImage(named:""))
                cellOne.lblInfo.text = otherUserName
                cellOne.feedImage.layer.cornerRadius = 2.0
                cellOne.feedImage.clipsToBounds = true
                cellOne.feedOtherImg.layer.cornerRadius = cellOne.feedOtherImg.frame.height/2
                cellOne.feedOtherImg.clipsToBounds = true
                cellOne.clipsToBounds = true
                cellOne.lblInfo.text =  "From: \(String(describing: otherUserName!))"
                
                // 12 June :-
                cellOne.feedOtherImgBtn1.addTarget(self, action: #selector(MoveToSender1(_:)), for: .touchUpInside)
        
                // 12 June :- Placing Time stamp value in Table view's cell.
                print((dict!.value(forKey:"timestamp") as! String))
                if let timestamp = dict!["timestamp"] as? String {
                    let timestampDouble = Double(timestamp)
                    let date = Date(timeIntervalSince1970: timestampDouble!)
                    let dateFormatter = DateFormatter()
                    dateFormatter.locale = NSLocale.current
                    dateFormatter.dateFormat = "dd-MMMM-yyyy hh:mm a"
                    let strDate = dateFormatter.string(from: date)
                    cellOne.artistLblDate.text =  "Date:" + strDate
                    
                }
                
                return cellOne
            }
                // For Posting Videos :-
            else {
                
                let cellTwo:FeedCellVideo = profileTbl.dequeueReusableCell(withIdentifier: "FeedCellVideo") as! FeedCellVideo
                let otherUserProfilePic = dict?.value(forKey:"thumb_url") as? String
                cellTwo.imgVideo.sd_setImage(with:URL(string:otherUserProfilePic!), placeholderImage:UIImage(named:""))
                cellTwo.imgVideo.layer.cornerRadius = 2.0
                cellTwo.imgVideo.clipsToBounds = true
                cellTwo.lblAddress.text = "Address:\(String(describing: dict?.value(forKey:"location") as! String))"
                cellTwo.lblNote.text = "Notes:\(String(describing: dict?.value(forKey:"notes") as! String))"
                cellTwo.btnVideoClick.isEnabled = true
                cellTwo.btnVideoClick.tag = indexPath.row
                cellTwo.btnVideoClick.addTarget(self, action: #selector(showVideo), for: UIControlEvents.touchUpInside)
                
                // 12 June :- Placing Time stamp value in Table view's cell.
                print((dict!.value(forKey:"timestamp") as! String))
                if let timestamp = dict!["timestamp"] as? String {
                    let timestampDouble = Double(timestamp)
                    let date = Date(timeIntervalSince1970: timestampDouble!)
                    let dateFormatter = DateFormatter()
                    dateFormatter.locale = NSLocale.current
                    dateFormatter.dateFormat = "dd-MMMM-yyyy hh:mm a"
                    let strDate = dateFormatter.string(from: date)
                    cellTwo.lblDateArt.text =  "Date:" + strDate
                }
                
                // 12 June :-
                 cellTwo.lblRestNameArt.text = "Restuarant Name:\(String(describing: dict?.value(forKey:"other_user_name") as! String))"
                
                
                
                
                return cellTwo
                
            }
            
            
        }
        
        else{
            
            let cell:PostCell = profileTbl.dequeueReusableCell(withIdentifier: "PostCell") as! PostCell
            let dict = socialFeedsArray[indexPath.row] as! NSDictionary
            if dict["feeds_type"]  as! Int == 1 {
                cell.postTypeImg.image = UIImage(named: "fbgray")
                cell.postImg.sd_setImage(with: URL(string: "http://graph.facebook.com/\(dict["from_id"]!)/picture?type=large"), completed: {(image,  error,  cacheType, imageURL) -> Void in
                    
                })
            } else {
                
                cell.postTypeImg.image = UIImage(named: "twittergray")
                
                
                var image = dict["from_id"]  as! String
                image = image.replacingOccurrences(of: "_normal", with: "")
                
                cell.postImg.sd_setImage(with: URL(string: image), completed: {(image,  error,  cacheType, imageURL) -> Void in
                })
                
            }
            
            let date = dict["created_time"]! as! Date
            let dateString = timeAgoSinceDate(Date(), currentDate: date, numericDates: true)
            print(dateString)
            cell.timeLbl.text = dateString
            
            
            cell.nameLbl.text = dict["name"] as? String
            
            
            if dict["description"] as! String != "" {
                cell.descriptionLbl.text = dict["description"]! as? String
            } else if dict["message"] as! String != "" {
                cell.descriptionLbl.text = dict["message"] as? String
            } else if dict["name"]  as! String != "" {
                cell.descriptionLbl.text = dict["name"] as? String
                if dict["story"]  as! String != "" {
                    cell.descriptionLbl.text = "\(cell.descriptionLbl.text!)- \(dict["story"]!)"
                }
            } else if dict["story"]  as! String != "" {
                cell.descriptionLbl.text = dict["story"] as? String
            } else {
                cell.descriptionLbl.text = ""
            }
            
            return cell
            
        }
        
    }
    
    //MARK: To show image or video in detail
    @objc func showVideo(_ sender: Any) {
        
        let dict = feedsArray[(sender as AnyObject).tag] as? NSDictionary
        let videoUrl = dict?.value(forKey:"video_url") as? String
        
        
        let valueVideo = NSURL(string : videoUrl!)?.pathExtension
        if valueVideo == "mp4"{
            
            let videoURL = URL(string:videoUrl!)
            let player = AVPlayer(url: videoURL!)
            let playerViewController = AVPlayerViewController()
            playerViewController.player = player
            self.view.addSubview(playerViewController.view)
            self.present(playerViewController, animated: true) {
                playerViewController.player!.play()
            }
        }
    }
    
    // Mark: Convert Date
    
    func timeAgoSinceDate(_ date:Date,currentDate:Date, numericDates:Bool) -> String {
        let calendar = Calendar.current
        let now = currentDate
        let earliest = (now as NSDate).earlierDate(date)
        let latest = (earliest == now) ? date : now
        let components:DateComponents = (calendar as NSCalendar).components([NSCalendar.Unit.minute , NSCalendar.Unit.hour , NSCalendar.Unit.day , NSCalendar.Unit.weekOfYear , NSCalendar.Unit.month , NSCalendar.Unit.year , NSCalendar.Unit.second], from: earliest, to: latest, options: NSCalendar.Options())
        
        if (components.year! >= 2) {
            return "\(components.year!) years ago"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 year ago"
            } else {
                return "Last year"
            }
        } else if (components.month! >= 2) {
            return "\(components.month!) mon ago"
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 mon ago"
            } else {
                return "Last mon"
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!) weeks ago"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1 week ago"
            } else {
                return "Last week"
            }
        } else if (components.day! >= 2) {
            return "\(components.day!) days ago"
        } else if (components.day! >= 1){
            if (numericDates){
                return "1 day ago"
            } else {
                return "Yesterday"
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!) hours ago"
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1 hour ago"
            } else {
                return "An hour ago"
            }
        } else if (components.minute! >= 2) {
            return "\(components.minute!) min ago"
        } else if (components.minute! >= 1){
            if (numericDates){
                return "1 min ago"
            } else {
                return "A min ago"
            }
        } else if (components.second! >= 3) {
            return "\(components.second!) sec ago"
        } else {
            return "Just now"
        }
        
    }
  
    // 12 June :- (After Clicking From Where we are receiving drinkz we can can click and check the Sender-Profile) :-
    
    func MoveToSender1(_ button: UIButton) {
        
        if self.userTypee == "1" {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "Profile") as! Profile
            vc.fromNavStr = "other"
            vc.showPostVideoBtn = true
            print(self.userId)
             vc.memberProfileId = self.userId
            navigationController?.pushViewController(vc,animated: true)
            
        }
            
        else if self.userTypee == "2" {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "PlaceProfile") as! PlaceProfile
            vc.fromNavStr = "other"
           // print(userId)
            vc.restaurantId = self.userId
            navigationController?.pushViewController(vc,animated: true)
        }
            
        else if self.userTypee == "3" {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ArtistProfile") as! ArtistProfile
            vc.fromNavStr = "other"
            vc.artistId = self.userId
            navigationController?.pushViewController(vc,animated: true)
        }
            
        else if self.userTypee == "4" {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "PlaceProfile") as! PlaceProfile
            vc.fromNavStr = "other"
            vc.restaurantId = self.userId
            navigationController?.pushViewController(vc,animated: true)
        }
            
        else if self.userTypee == "5" {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "PlaceProfile") as! PlaceProfile
            vc.fromNavStr = "other"
            vc.restaurantId = self.userId
            navigationController?.pushViewController(vc,animated: true)
        }
            
        else if self.userTypee == "6" {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "PlaceProfile") as! PlaceProfile
            vc.fromNavStr = "other"
            vc.restaurantId = self.userId
            navigationController?.pushViewController(vc,animated: true)
            
        }
            
        else if self.userTypee == "7" {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "PlaceProfile") as! PlaceProfile
            vc.fromNavStr = "other"
            vc.restaurantId = self.userId
            navigationController?.pushViewController(vc,animated: true)
        }
        
        
    }
    
    
    
    
    // method to run when table view cell is tapped
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if typeStr == "post" {
            return 95
        }
        
        if indexPath.row == 0 {
            let cell:ArtistPerformanceCell = detailTbl.dequeueReusableCell(withIdentifier: "ArtistPerformanceCell") as! ArtistPerformanceCell
            cell.aboutTxt.text = aboutStr
            let fixedWidth = cell.aboutTxt.frame.size.width
            cell.aboutTxt.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            let newSize = cell.aboutTxt.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            return newSize.height+80
            
        }
        
        return 36
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        if typeStr == "post" {
            return 350
        }
        
        return 350
        
        }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if typeStr == "post" {
            return 600
        }
        
        return 600
        }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        var headerView1 = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 30))
        
        headerCell = profileTbl.dequeueReusableCell(withIdentifier: "ArtistHeader") as! ArtistHeader
        headerCell.mapView.delegate = self
        headerCell.aboutLbl.text = aboutStr
        
        // 3 July :- (To check if there is option of frndrequest to its own or not.)
        if (Singleton.sharedInstance.userIdStr == self.artistId) {
            headerCell.addFrndBtn.isHidden = true
         }
        
        
       if typeStr == "post" {
            headerView1 = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 600))
            headerCell.frame = CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 600)
            headerCell.contentView.frame = CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 600)
            
            headerCell.detailsLbl.textColor = UIColor.darkGray
            headerCell.postVideoBtn.addTarget(self, action: #selector(postVideo(_:)), for: .touchUpInside)
            
             headerCell.postVideoBtn.layer.cornerRadius = 2.0
             headerCell.clipsToBounds = true
            
        } else if typeStr == "details" {
        
            
            headerView1 = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 600))
            headerCell.frame = CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 600)
            headerCell.contentView.frame = CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 600)
            
            headerCell.detailsLbl.textColor = UIColor.black
            
            
        }
        headerCell.postBtn.addTarget(self, action: #selector(PostDetailAction(_:)), for: .touchUpInside)
        headerCell.detailsBtn.addTarget(self, action: #selector(PostDetailAction(_:)), for: .touchUpInside)
        
        // 24 May :-
         headerCell.addFrndBtn.addTarget(self, action: #selector(addFrndAction(_:)), for: .touchUpInside)
        
        // 24 May :-
        if(self.frndUnfrndStr == "1") {
            // cell.addFrndBtn.image = UIImage(named: "remove.png")
            let image = UIImage(named: "remove.png")
            headerCell.addFrndBtn.setBackgroundImage(image, for: .normal)
            self.indicator.stopAnimating()
            self.indicator.isHidden = true
            
        }
        else {
            // cell.addFrndBtn.image = UIImage(named: "add_friend.png")
            let image = UIImage(named: "add_friend.png")
            headerCell.addFrndBtn.setBackgroundImage(image, for: .normal)
            self.indicator.stopAnimating()
            self.indicator.isHidden = true
            
        }
        
       
        if fromNavStr == "other" {
            headerCell.editAboutBtn.isHidden = true
           // headerCell.editAboutImg.isHidden = true
            headerCell.editPerformanceBtn.isHidden = true
          //  headerCell.editPerformanceImg.isHidden = true
            headerCell.aboutLbl.isEditable = false
            headerCell.aboutLbl.isSelectable = false
        }
        
        
        if fromNavStr != "other" {
            userImgStr = Singleton.sharedInstance.userImgStr
        }
        
        headerCell.nameLbl.text = nameStr
        headerCell.locationLbl.text = locationStr
        headerCell.userImg.layer.cornerRadius = headerCell.userImg.frame.size.height/2
        headerCell.userImg.layer.borderColor = UIColor(red: 176/255, green: 176/255, blue: 176/255, alpha: 1).cgColor
        headerCell.userImg.layer.borderWidth = 1.5
        headerCell.userImg.clipsToBounds = true
        
        // 10 June :-
        // headerCell.userImg.sd_setImage(with: URL(string: userImgStr), placeholderImage: UIImage(named: "user"), options: .refreshCached)
        let urlStr : String = (userImgStr)
        let urlFormattedStr = urlStr.replacingOccurrences(of: " ", with: "%20")
        headerCell.userImg.sd_setImage(with: URL(string:urlFormattedStr), placeholderImage: UIImage(named: "user"), options: .refreshCached)
      
        
     //   headerCell.fbBtn.addTarget(self, action: #selector(ConnectedAction(_:)), for: .touchUpInside)
      //  headerCell.twitterBtn.addTarget(self, action: #selector(ConnectedAction(_:)), for: .touchUpInside)
        
        headerCell.editAboutBtn.addTarget(self, action: #selector(AboutEdit(_:)), for: .touchUpInside)
        headerCell.editPerformanceBtn.addTarget(self, action: #selector(Performance(_:)), for: .touchUpInside)
        headerCell.aboutLbl.delegate = self
        headerCell.aboutLbl.inputAccessoryView = ViewForDoneButtonOnKeyboard
         print(headerCell.frame.size.width)
        print(headerCell.contentView.frame.size.width)

        //Mark: for visting the other user profile and showing the current address

        if self.notificationSetting == "1" && fromNavStr == "other"{

            headerCell.locationLbl.text = self.addressString

        }
        else if self.notificationSetting == "0" && fromNavStr == "other"{

            headerCell.locationLbl.text = locationStr

        }

            //Mark: for showing address on the basis of location
        else if self.locationValue == "1"{

            headerCell.locationLbl.text = self.addressString
        }
        else{

            headerCell.locationLbl.text = locationStr
        }


        // Mark: Adding annotation on mao
        let dropPin = MKPointAnnotation()
        // For other user profile - for showing lat long
        if fromNavStr == "other" && self.notificationSetting == "1" {

            lat = currentlatitudeStr
            long = currentlongitudeStr
            let userLocation = CLLocationCoordinate2DMake(lat, long)
            dropPin.coordinate = userLocation
            dropPin.title = self.addressString
        }

        else if self.notificationSetting == "0" && fromNavStr == "other" {

            lat = (Singleton.sharedInstance.latitudeStr as NSString).doubleValue
            long = (Singleton.sharedInstance.longitudeStr as NSString).doubleValue
            let userLocation = CLLocationCoordinate2DMake(lat, long)
            dropPin.coordinate = userLocation
            dropPin.title = locationStr
        }
        

            // For my profile when location is on
        else if (self.locationValue == "1"){

            lat = currentlatitudeStr
            long = currentlongitudeStr
            let userLocation = CLLocationCoordinate2DMake(lat, long)
            dropPin.coordinate = userLocation
            dropPin.title = self.addressString


        }
           //  for my profile when location is off
        else{

            lat = (Singleton.sharedInstance.latitudeStr as NSString).doubleValue
            long = (Singleton.sharedInstance.longitudeStr as NSString).doubleValue
            let userLocation = CLLocationCoordinate2DMake(lat, long)
            dropPin.coordinate = userLocation
            dropPin.title = locationStr

        }

        if  (fromNavStr == "other")  {

            headerCell.mapView.addAnnotation(dropPin)
            let span = MKCoordinateSpanMake(0.010, 0.010)
            let region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: lat, longitude: long), span: span)
            headerCell.mapView.setRegion(region, animated: true)

        }
        else   {

            headerCell.mapView.addAnnotation(dropPin)
            let span = MKCoordinateSpanMake(0.010, 0.010)
            let region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: lat, longitude: long), span: span)
            headerCell.mapView.setRegion(region, animated: true)

        }
        headerView1.addSubview(headerCell.contentView)
        return headerView1
    }
    
    
    
    //MARK: Post Video
    func postVideo(_ sender: Any) {
        
      let cameraVC = storyboard!.instantiateViewController(withIdentifier: "CameraOverlay") as! CameraOverlay
        self.navigationController?.pushViewController(cameraVC, animated:true)
      
       
        
    }
    
    
    //MARK: UIMapViewDelegates
//
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        // Don't want to show a custom image if the annotation is the user's location.
        guard !(annotation is MKUserLocation) else {
            return nil
        }

        // Better to make this class property
        let annotationIdentifier = "AnnotationIdentifier"

        var annotationView: MKAnnotationView?
        if let dequeuedAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier) {
            annotationView = dequeuedAnnotationView
            annotationView?.annotation = annotation
        }
        else {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
            annotationView?.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        }

        if let annotationView = annotationView {
            // Configure your annotation view here
            annotationView.canShowCallout = true


            let annView:UIView = UIView(frame: CGRect(x:0, y: 27, width: 185, height: 40))
            if (fromNavStr == "other") {

                let annImg:UIImageView = UIImageView(frame: CGRect(x:5, y: 5, width: 30, height: 30))
                let annLbl:UILabel = UILabel(frame: CGRect(x:42, y: 0, width: 120, height: 40))
                //  annLbl.text="I am in \(self.checkInRest) club!"
                if self.notificationSetting == "1"{

                    annLbl.text = self.addressString

                }

                else{

                    annLbl.text = self.locationStr

                }
                annLbl.font=UIFont(name: "FuturaBT-Book", size: 10)!
                annLbl.textColor=UIColor.black
                annLbl.numberOfLines = 0


                annView.layer.cornerRadius = 20
                annView.layer.masksToBounds = false;
                annView.layer.shadowColor = UIColor.black.cgColor
                annView.layer.shadowOpacity = 0.3
                annView.layer.shadowRadius = 2
                annView.layer.shadowOffset = CGSize(width: 1, height: 1)

                annView.backgroundColor = UIColor.white
                // annImg.image =  UIImage(named: "me")
                
                // 10 June :-
               // annImg.sd_setImage(with: URL(string: self.userImgStr), placeholderImage: UIImage(named: "user"))
                
                let urlStr : String = (userImgStr)
                print(urlStr)
                let urlFormattedStr = urlStr.replacingOccurrences(of: " ", with: "%20")
                annImg.sd_setImage(with: URL(string:urlFormattedStr), placeholderImage: UIImage(named: "user"))
               
                annImg.layer.cornerRadius = annImg.frame.height/2
                annImg.clipsToBounds = true

                //  annImg.contentMode = .scaleAspectFit
                annView .addSubview(annImg)
                annView .addSubview(annLbl)

                annotationView.addSubview(annView)
            }

            else if (self.locationValue == "1") || (self.locationValue == "0"){

                let annImg:UIImageView = UIImageView(frame: CGRect(x:5, y: 5, width: 30, height: 30))
                let annLbl:UILabel = UILabel(frame: CGRect(x:42, y: 0, width: 120, height: 40))
                // annLbl.text="I am in \(self.checkInRest) club!"
                annLbl.font=UIFont(name: "FuturaBT-Book", size: 10)!
                annLbl.textColor=UIColor.black
                annLbl.numberOfLines = 0
                if self.locationValue == "1"{

                    annLbl.text = self.addressString

                }

                else{

                    annLbl.text = self.locationStr

                }

                annView.layer.cornerRadius = 20
                annView.layer.masksToBounds = false;
                annView.layer.shadowColor = UIColor.black.cgColor
                annView.layer.shadowOpacity = 0.3
                annView.layer.shadowRadius = 2
                annView.layer.shadowOffset = CGSize(width: 1, height: 1)

                annView.backgroundColor = UIColor.white
                // annImg.image =  UIImage(named: "me")
                
                // 10 June :-
               // annImg.sd_setImage(with: URL(string: self.userImgStr), placeholderImage: UIImage(named: "user"))
                let urlStr : String = (self.userImgStr)
                let urlFormattedStr = urlStr.replacingOccurrences(of: " ", with: "%20")
                annImg.sd_setImage(with: URL(string:urlFormattedStr), placeholderImage: UIImage(named: "user"))
                
                annImg.layer.cornerRadius = annImg.frame.height/2
                annImg.clipsToBounds = true

                //  annImg.contentMode = .scaleAspectFit
                annView .addSubview(annImg)
                annView .addSubview(annLbl)

                annotationView.addSubview(annView)


            }

            else {


                annView.removeFromSuperview()
                annView.isHidden = true
            }


            annotationView.image = UIImage(named: "marker1")
        }

        return annotationView
    }

    
    @IBAction func cancelBtnFromKeyboardClicked(sender: Any) {
        view.endEditing(true)
        detailTbl.reloadData()
        
    }
    
    @IBAction func doneBtnFromKeyboardClicked(sender: Any) {
        detailTbl.reloadData()
        view.endEditing(true)
        indicator.startAnimating()
        indicator.isHidden = false
        aboutApiMethod()
    }
    
    
    // MARK: About API Method
    
    func aboutApiMethod () {
        
        let  json = ["user_id":Singleton.sharedInstance.userIdStr,
                     "auth_token":Singleton.sharedInstance.userTokenStr,
                     "about_text":uploadAboutTxt,
                     ] as [String : Any]
        
        
        print(json)
        
        Alamofire.request("http://beta.brstdev.com/freedrinkz/api/web/v1/members/aboutartist", method: .post, parameters: json, encoding: JSONEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                if let jsonData = response.result.value {
                    print("JSON: \(jsonData)")
                    let json = JSON(data: response.data! )
                    if let descriptionStr = json["description"].string {
                        print(descriptionStr)
                        if descriptionStr == "About Text Save  Successfully" {
                            //   self.alertViewMethod(titleStr: "", messageStr: "Product has been successfully added.")
                            
                            
                            self.aboutStr = self.uploadAboutTxt
                            
                        } else if descriptionStr == "Wrong auth token"  || descriptionStr == "Something wrong"{
                            
                            UserDefaults.standard.setValue(nil, forKey: "auth_token")
                            UserDefaults.standard.setValue(nil, forKey: "user_id")
                            UserDefaults.standard.setValue(nil, forKey: "user_type")
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "Login") as! Login
                            vc.isExpired = true
                            self.navigationController?.pushViewController(vc,animated: true)
                            
                        }
                        //Now you got your value
                    }
                    else if let messageStr = json["message"].string {
                        
                        if messageStr == "Missing parameter" {
                            
                            UserDefaults.standard.setValue(nil, forKey: "auth_token")
                            UserDefaults.standard.setValue(nil, forKey: "user_id")
                            UserDefaults.standard.setValue(nil, forKey: "user_type")
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "Login") as! Login
                            vc.isExpired = true
                            self.navigationController?.pushViewController(vc,animated: true)
                            
                        }
                        
                    }
                } else if((response.error) != nil){
                    
                    self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                    
                }
                
                self.detailTbl.reloadData()
                self.indicator.stopAnimating()
                self.indicator.isHidden = true
                
        }
        
    }
    
    
    //MARK: UITextField Delegates
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        
    }
    func textViewDidChange(_ textView: UITextView) {
        print(textView.text)
        uploadAboutTxt = textView.text
        
        //    headerCell.aboutLbl.text = textView.text
        //        let fixedWidth = headerCell.aboutLbl.frame.size.width
        //        headerCell.aboutLbl.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        //        let newSize = headerCell.aboutLbl.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        //        var newFrame = headerCell.aboutLbl.frame
        //        newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
        //        headerCell.aboutLbl.frame = newFrame;
        //        
        //        print(headerCell.aboutArtistView.frame)
        //        
        //        var newFrame1 = headerCell.aboutArtistView.frame
        //        newFrame1.size = CGSize(width: newFrame1.width, height: 80+newFrame1.height)
        //        headerCell.aboutArtistView.frame = newFrame1
        //        
        //        print(headerCell.aboutArtistView.frame)
        
        
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
}
